<?php
namespace Finnegan\Routing;


use Finnegan\Contracts\Auth\AuthorizationException;
use Finnegan\Contracts\Modules\Module as ModuleContract;
use Finnegan\Modules\ModulesManager;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider;
use Illuminate\Routing\Route;
use Illuminate\Routing\Router;


class RoutingServiceProvider extends RouteServiceProvider
{
	
	
	protected $defer = true;
	
	
	public function register ()
	{
		$this->app->bind ( 'Illuminate\Routing\ResourceRegistrar', ResourceRegistar::class );
		
		$this->bindModelResolver ( $this->app[ 'router' ] );
	}
	
	
	protected function bindModelResolver ( Router $router )
	{
		$router->bind ( 'model', function ( $name, Route $route )
		{
			$prefix = $route->getPrefix ();
			$module = $this->app->make ( ModuleContract::class, [
				'name' => $this->app[ 'modules.manager' ]->resolvePrefix ( $prefix )
			] );
			
			$model = $this->app->make ( 'models.factory', [ $module ] )->load ( $name );
			if ( ! $model->definition ()->isUnguarded () and ! $model->definition ()->authorize ( $this->app['auth.driver'] ) )
			{
				throw new AuthorizationException ( 'Access forbidden to this model for the current user' );
			}
			return $model;
		} );
	}
	
	
	public function map ( ModulesManager $manager )
	{
		if ( count ( $modules = $manager->modules () ) )
		{
			$registar = $this->app->make ( ModuleRegistar::class );
			foreach ( $modules as $module )
			{
				$registar->registerRoutes ( $module );
			}
		}
	}
	
	
	public function provides ()
	{
		return [ 'Illuminate\Routing\ResourceRegistrar' ];
	}
	
	
}
