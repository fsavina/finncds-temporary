<?php

namespace Finnegan\Routing;


use Finnegan\Contracts\Modules\Module as ModuleContract;
use Illuminate\Contracts\Routing\Registrar;
use Illuminate\Routing\Router;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;


class ModuleRegistar
{
	
	/**
	 * @var Router
	 */
	protected $router;
	
	/**
	 * @var ModuleContract
	 */
	protected $module;
	
	
	/**
	 * RoutesRegistar constructor.
	 * @param Router $router
	 */
	public function __construct ( Router $router )
	{
		$this->router = $router;
	}
	
	
	/**
	 * @param ModuleContract $module
	 */
	public function registerRoutes ( ModuleContract $module )
	{
		$this->module = $module;
		
		if ( $this->module->isSecure () )
		{
			$this->registerAuthRoutes ();
		}
		
		if ( $routes = $this->module->config ( 'routing.routes' ) )
		{
			$this->registerExtraRoutes ( $routes );
		}
		
		$options = $this->secureGroupOptions ();
		
		$this->router->group ( $options, function ( Registrar $router ) {
			if ( ! $router->has ( $this->module->wrapRoute ( 'home' ) ) )
			{
				$router->get ( '/', [
					'as'   => 'home',
					'uses' => "{$this->getController()}@home"
				] );
			}
			
			if ( $this->module->config ( 'routing.settings' ) )
			{
				$this->registerSettingsRoutes ();
			}
			
			if ( $this->module->config ( 'routing.crud' ) )
			{
				$this->registerCrudRoutes ();
			}
		} );
	}
	
	
	/**
	 * @param array $routes
	 */
	protected function registerExtraRoutes ( array $routes )
	{
		$options = $this->module->isSecure () ?
			$this->secureGroupOptions () :
			$this->basicGroupOptions ( [ 'middleware' ] );
		
		$this->router->group ( $options, function ( Registrar $router ) use ( $routes ) {
			foreach ( $routes as $url => $route )
			{
				$methods = (array) Arr::get ( $route, 'method', [ 'get' ] );
				
				$options = $this->extractRouteOptions ( $route );
				
				$router->match ( $methods, $url, $options );
			}
		} );
	}
	
	
	/**
	 * @param array|string $route
	 * @return array|\Closure
	 */
	protected function extractRouteOptions ( $route )
	{
		if ( is_string ( $route ) )
		{
			if ( Str::contains ( $route, '@' ) )
			{
				$route = [ 'uses' => $route ];
			} else
			{
				$route = [ 'action' => $route ];
			}
		}
		
		if ( Arr::has ( $route, 'view' ) )
		{
			return function () use ( $route ) {
				$layout = $this->module->make ( 'Finnegan\Contracts\Layout\Manager' );
				return $layout->view ( $route[ 'view' ] );
			};
		}
		
		$options = Arr::only ( $route, [ 'middleware', 'as', 'uses' ] );
		
		if ( ! Arr::has ( $options, 'uses' ) )
		{
			$controller = $this->getController ();
			$action = Arr::get ( $route, 'action' );
		} else
		{
			list( $controller, $action ) = explode ( '@', $options[ 'uses' ] );
			$controller = $this->resolveControllerName ( $controller );
		}
		
		$options[ 'uses' ] = "{$controller}@{$action}";
		
		return $options;
	}
	
	
	protected function registerCrudRoutes ()
	{
		$this->router->resource ( '{model}', $this->getController () );
	}
	
	
	protected function registerSettingsRoutes ()
	{
		$controller = $this->resolveControllerName ( 'SettingsController' );
		
		$this->router->get ( 'settings', "$controller@editSettings" )->name ( 'settings' );
		$this->router->match ( [ 'PUT', 'PATCH' ], 'settings', "$controller@storeSettings" );
	}
	
	
	protected function registerAuthRoutes ()
	{
		$options = $this->basicGroupOptions ();
		
		$this->router->group ( $options, function ( Registrar $router ) {
			$controller = $this->getLoginControllerName ();
			
			$router->post ( 'auth/logout', "{$controller}@logout" )->name ( 'logout' );
			
			$router->group ( [ 'middleware' => 'finn.guest' ], function ( Registrar $router ) use ( $controller ) {
				$router->get ( 'auth/login', "{$controller}@showLoginForm" )->name ( 'login' );
				$router->post ( 'auth/login', "{$controller}@login" );
			} );
			
			$this->registerPasswordResetRoutes ( $router );
		} );
	}
	
	
	protected function registerPasswordResetRoutes ( Registrar $router )
	{
		if ( $this->module->config ( 'auth.password.reset' ) )
		{
			$router->group ( [ 'middleware' => 'finn.guest' ], function ( Registrar $router ) {
				$forgotController = $this->getForgotPasswordControllerName ();
				$resetController = $this->getResetPasswordControllerName ();
				
				$router->get ( 'auth/password/email', [
					'uses' => "{$forgotController}@showLinkRequestForm",
					'as'   => 'password_email'
				] );
				
				$router->post ( 'auth/password/email', [
					'uses' => "{$forgotController}@sendResetLinkEmail"
				] );
				
				$router->get ( 'auth/password/reset/{token}', [
					'uses' => "{$resetController}@showResetForm",
					'as'   => 'password_reset'
				] );
				
				$router->post ( 'auth/password/reset', [
					'uses' => "{$resetController}@reset",
					'as'   => 'password_reset_post'
				] );
			} );
		}
	}
	
	
	/**
	 * @param array $except
	 * @return array
	 */
	protected function basicGroupOptions ( array $except = null )
	{
		$options = [
			'as'         => "{$this->module->name()}.",
			'middleware' => [ 'finn.web' ]
		];
		
		if ( $prefix = $this->prefix () )
		{
			$options [ 'prefix' ] = $prefix;
		}
		
		return is_array ( $except ) ? Arr::except ( $options, $except ) : $options;
	}
	
	
	/**
	 * @return array
	 */
	protected function secureGroupOptions ()
	{
		$options = $this->basicGroupOptions ();
		
		if ( $this->module->isSecure () )
		{
			$options [ 'middleware' ][] = 'finn.auth';
			$options [ 'middleware' ][] = 'finn.active';
			if ( $role = $this->module->config ( 'auth.role' ) )
			{
				$options [ 'middleware' ] [] = 'finn.role:' . $role;
			}
		}
		
		return $options;
	}
	
	
	/**
	 * @param string $config
	 * @param string $default
	 * @return string
	 */
	protected function getController ( $config = 'routing.controller', $default = ModuleContract :: DEFAULT_CONTROLLER )
	{
		$controller = $this->module->config ( $config, $default );
		return $this->resolveControllerName ( $controller );
	}
	
	
	/**
	 * @return string
	 */
	protected function getLoginControllerName ()
	{
		return $this->getController ( 'auth.controller', ModuleContract :: DEFAULT_LOGIN_CONTROLLER );
	}
	
	
	/**
	 * @return string
	 */
	protected function getForgotPasswordControllerName ()
	{
		return $this->getController ( 'auth.passwords.controllers.forgot', ModuleContract :: DEFAULT_PASSWORD_FORGOT_CONTROLLER );
	}
	
	
	/**
	 * @return string
	 */
	protected function getResetPasswordControllerName ()
	{
		return $this->getController ( 'auth.passwords.controllers.reset', ModuleContract :: DEFAULT_PASSWORD_RESET_CONTROLLER );
	}
	
	
	/**
	 * @param string $controller
	 * @return string
	 */
	protected function resolveControllerName ( $controller )
	{
		return class_exists ( $controller ) ? $controller : "{$this->getNamespace()}\\{$controller}";
	}
	
	
	/**
	 * @return string
	 */
	protected function getNamespace ()
	{
		return $this->module->config ( 'routing.namespace', ModuleContract::DEFAULT_NAMESPACE );
	}
	
	
	/**
	 * @return string
	 */
	protected function prefix ()
	{
		return $this->module->config ( 'routing.prefix', $this->module->name () );
	}
	
}