<?php
namespace Finnegan\Routing;


use Illuminate\Routing\ResourceRegistrar as IlluminateResourceRegistrar;


class ResourceRegistar extends IlluminateResourceRegistrar
{


	protected function getExtraResourceMethods ()
	{
		return [
			'truncate',
			'copy',
			'sort',
			'field',
			'translate',
			'bulk',
			'relationCreate',
			'relationAttach',
			'relationEdit',
			'relationUpdate',
			'relationDetach',
		];
	}


	protected function getResourceMethods($defaults, $options)
	{
		$defaults = array_merge ( $this->getExtraResourceMethods (), $defaults );
		return parent :: getResourceMethods ( $defaults, $options );
	}


	public function getResourceWildcard($value)
	{
		return 'id';
	}


	protected function getResourceName($resource, $method, $options)
	{
		if (isset($options['names'][$method])) {
			return $options['names'][$method];
		}

		$prefix = isset($options['as']) ? $options['as'].'.' : '';

		if (! $this->router->hasGroupStack()) {
			return $prefix.'.'.$method;
		}

		return $this->getGroupResourceName($prefix, $resource, $method);
	}


	protected function getGroupResourceName ( $prefix, $resource, $method )
	{
		$group = trim ( str_replace ( '/', '.', $this->router->getLastGroupPrefix () ), '.' );

		if ( empty( $group ) )
		{
			return trim ( "{$prefix}.{$method}", '.' );
		}

		return trim ( "{$prefix}.{$method}", '.' );
	}


	protected function addResourceTruncate ( $name, $base, $controller, $options )
	{
		$uri = $this -> getResourceUri ( $name ) . '/truncate';

		$action = $this -> getResourceAction ( $name, $controller, 'truncate', $options );

		return $this -> router -> delete ( $uri, $action );
	}


	protected function addResourceCopy ( $name, $base, $controller, $options )
	{
		$uri = $this -> getResourceUri ( $name ) . '/{' . $base . '}/copy';

		$action = $this -> getResourceAction ( $name, $controller, 'copy', $options );

		return $this -> router -> get ( $uri, $action );
	}


	protected function addResourceSort ( $name, $base, $controller, $options )
	{
		$uri = $this -> getResourceUri ( $name ) . '/sort';

		$action = $this -> getResourceAction ( $name, $controller, 'sort', $options );

		return $this -> router -> post ( $uri, $action );
	}


	protected function addResourceField ( $name, $base, $controller, $options )
	{
		$uri = $this -> getResourceUri ( $name ) . '/field/{field}/{action}';

		$action = $this -> getResourceAction ( $name, $controller, 'field', $options );

		return $this -> router -> any ( $uri, $action );
	}


	protected function addResourceTranslate ( $name, $base, $controller, $options )
	{
		$uri = $this -> getResourceUri ( $name ) . '/{' . $base . '}/translate';

		$action = $this -> getResourceAction ( $name, $controller, 'translate', $options );

		return $this -> router -> get ( $uri, $action );
	}


	protected function addResourceBulk ( $name, $base, $controller, $options )
	{
		$uri = $this -> getResourceUri ( $name ) . '/bulk';

		$action = $this -> getResourceAction ( $name, $controller, 'bulk', $options );

		return $this -> router -> post ( $uri, $action );
	}


	protected function addResourceRelationCreate ( $name, $base, $controller, $options )
	{
		$uri = $this -> getResourceUri ( $name ) . '/{' . $base . '}/create-relation/{relation}';

		$action = $this -> getResourceAction ( $name, $controller, 'relationCreate', $options );

		return $this -> router -> get ( $uri, $action );
	}


	protected function addResourceRelationAttach ( $name, $base, $controller, $options )
	{
		$uri = $this -> getResourceUri ( $name ) . '/{' . $base . '}/attach/{relation}';

		$action = $this -> getResourceAction ( $name, $controller, 'relationAttach', $options );

		return $this -> router -> post ( $uri, $action );
	}


	protected function addResourceRelationEdit ( $name, $base, $controller, $options )
	{
		$uri = $this -> getResourceUri ( $name ) . '/{' . $base . '}/edit-relation/{relation}/{relatedId}';

		$action = $this -> getResourceAction ( $name, $controller, 'relationEdit', $options );

		return $this -> router -> get ( $uri, $action );
	}


	protected function addResourceRelationUpdate ( $name, $base, $controller, $options )
	{
		$uri = $this -> getResourceUri ( $name ) . '/{' . $base . '}/edit-relation/{relation}/{relatedId}';
		$action = $this -> getResourceAction ( $name, $controller, 'relationUpdate', $options );

		$this -> router -> put ( $uri, $action );
		return $this -> router -> patch ( $uri, $controller . '@relationUpdate' );
	}


	protected function addResourceRelationDetach ( $name, $base, $controller, $options )
	{
		$uri = $this -> getResourceUri ( $name ) . '/{' . $base . '}/detach/{relation}/{relatedId}';

		$action = $this -> getResourceAction ( $name, $controller, 'relationDetach', $options );

		return $this -> router -> delete ( $uri, $action );
	}

}