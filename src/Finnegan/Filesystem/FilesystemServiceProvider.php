<?php
namespace Finnegan\Filesystem;


use Finnegan\Contracts\Modules\Module;
use Finnegan\Support\BaseServiceProvider;
use Illuminate\Config\Repository as Config;


class FilesystemServiceProvider extends BaseServiceProvider
{
	
	const DEFAULT_DISK_NAME = 'finnegan-uploads';
	
	protected $defer = true;
	
	
	public function register ()
	{
		$this->setDefaultDiskConfig ( $this->app[ 'config' ] );
		$this->registerCroppaBinding ();
	}
	
	
	protected function setDefaultDiskConfig ( Config $config )
	{
		$key = 'filesystems.disks.' . static::DEFAULT_DISK_NAME;
		
		if ( ! $config->has ( $key ) )
		{
			$config->set ( $key, [
				'driver'     => 'local',
				'root'       => $this->app->make ( 'path.public' ) . DIRECTORY_SEPARATOR . 'uploads',
				'visibility' => 'public',
			] );
		}
	}
	
	
	protected function registerCroppaBinding ()
	{
		$this->app->singleton ( 'finnegan-croppa', function ( $app )
		{
			$module = $app->make ( Module::class );
			$disk = $module->config ( 'module.disks.uploads', static::DEFAULT_DISK_NAME );
			return $app[ 'filesystem' ]->disk ( $disk )->getDriver ();
		} );
	}
	
	
	public function provides ()
	{
		return [ 'finnegan-croppa' ];
	}
}