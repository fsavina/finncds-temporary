<?php
namespace Finnegan\Auth\Roles;


use Illuminate\Support\Str;


abstract class Role
{


	/**
	 * @param string $role
	 * @return \Finnegan\Auth\Roles\Role
	 */
	public static function make ( $role )
	{
		$role = self :: resolveClassName ( $role );
		return new $role ();
	}

	
	/**
	 * @param string $role
	 * @return string
	 */
	protected static function resolveClassName ( $role )
	{
		$class = config ( "finnegan.auth.roles.{$role}.class", Str:: studly ( $role ) );
		if ( class_exists ( $class ) )
		{
			return $class;
		}
		return __NAMESPACE__ . '\\' . $class;
	}
	

	/**
	 * @param string $role
	 * @return boolean
	 */
	public function achieve ( $role )
	{
		$role = self :: resolveClassName ( $role );
		return ( class_exists ( $role ) and is_a ( $this, $role ) );
	}
	
	
	public function is ( $role )
	{
		return ( get_class ( $this ) == static :: resolveClassName ( $role ) );
	}


	protected function config ( $key, $default = false )
	{
		return $this->roleConfig ( $this->name (), $key, $default );
	}


	protected function roleConfig ( $role, $key, $default = false )
	{
		return config ( "finnegan.auth.roles.{$role}.{$key}", $default );
	}


	protected function name ( $class = null )
	{
		return Str :: snake ( ( new \ReflectionClass( $class ?: $this ) )->getShortName () );
	}


	public function abilities ()
	{
		$abilities = (array) $this->config ( 'abilities', [ ] );

		$class = new \ReflectionClass( $this );
		while ( $parent = $class->getParentClass () )
		{
			if ( $parent->isAbstract () )
			{
				break;
			}

			$parentName = $this->name ( $parent->getName () );
			$abilities = array_merge ( $abilities, (array)$this->roleConfig ( $parentName, 'abilities', [ ] ) );
			$class = $parent;
		}
		return array_unique ( $abilities );
	}

}


