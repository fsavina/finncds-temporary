<?php
namespace Finnegan\Auth\Policies;


use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Arr;


class ModelPolicy
{

	use HandlesAuthorization;

	protected $defaultActions = ['index', 'show', 'create', 'edit', 'destroy', 'field'];


	public function __call ( $name, $arguments )
	{
		array_push ( $arguments, $name, in_array ( $name, $this->defaultActions ) );
		return call_user_func_array ( [ $this, 'authorizeAction' ], $arguments );
	}


	public function view ( $user, $model )
	{
		return $this->show ( $user, $model );
	}


	public function store ( $user, $model )
	{
		return $this->create ( $user, $model );
	}


	public function save_as_draft ( $user, $model )
	{
		return $this->create ( $user, $model );
	}


	public function send_to_review ( $user, $model )
	{
		return $this->create ( $user, $model );
	}


	public function publish ( $user, $model )
	{
		if ( $user->hasAbility ( 'publish' ) )
		{
			return $this->create ( $user, $model );
		}
		return false;
	}


	public function update ( $user, $model )
	{
		return $this->edit ( $user, $model );
	}
	
	
	public function edit_profile ( $user, $model )
	{
		if ( $user->name() == $model -> name() )
		{
			if ( $user->hasAbility ( 'edit_other_profiles' ) )
			{
				return $user->achieve( $model->role );
			}
			return ( $user->getKey () == $model->getKey () );
		}
		return false;
	}


	public function edit_if_author ( $user, $model )
	{
		return (
			$user->hasAbility ( 'edit_others_objects' )
			or
			( $model->hasAttribute ( 'users_id' ) and ( $user->getKey () == $model->users_id ) )
		);
	}


	public function copy ( $user, $model )
	{
		if ( $this->authorizeAction ( $user, $model, 'copy', true ) )
		{
			return $this->create ( $user, $model );
		}
		return false;
	}


	public function delete ( $user, $model )
	{
		return $this->destroy ( $user, $model );
	}


	public function truncate ( $user, $model )
	{
		if ( $this->authorizeAction ( $user, $model, 'truncate', true ) )
		{
			return $this->destroy ( $user, $model );
		}
		return false;
	}


	public function relations ( $user, $model )
	{
		return $this->edit ( $user, $model );
	}


	public function relationCreate ( $user, $model )
	{
		return $this -> relations( $user, $model );
	}


	public function relationAttach ( $user, $model )
	{
		return $this -> relations( $user, $model );
	}


	public function relationEdit ( $user, $model )
	{
		return $this -> relations( $user, $model );
	}


	public function relationUpdate ( $user, $model )
	{
		return $this -> relations( $user, $model );
	}


	public function relationDetach ( $user, $model )
	{
		return $this -> relations( $user, $model );
	}


	public function sort ( $user, $model )
	{
		return ( $model->definition ()->sortable () and $this->edit ( $user, $model) );
	}


	public function bulk ( $user, $model, $action )
	{
		$actions = $model->bulkActions ();
		return ( count ( $actions ) and isset( $actions[ $action ] ) and $this->__call ( $action, [ $user, $model ] ) );
	}


	protected function authorizeAction ( $user, $model, $action, $default = false )
	{
		$auth = $model->config ( "auth.actions.$action", null );
		if ( ! is_null ( $auth ) )
		{
			if ( is_bool ( $auth ) )
			{
				return $auth;
			} elseif ( is_string ( $auth ) )
			{
				switch ( $auth )
				{
					case 'allow':
						return true;
					case 'deny':
						return false;
					default:
						return $this->$auth( $user, $model );
				}
			} elseif ( is_array ( $auth ) )
			{
				if ( $role = Arr::get ( $auth, 'role' ) )
				{
					return $user->achieve ( $role );
				}
				if ( $ability = Arr::get ( $auth, 'ability' ) )
				{
					if ( method_exists ( $this, $ability ) )
					{
						return $this->$ability( $user, $model );
					}
					return $user->hasAbility ( $ability );
				}
			}
		}
		return $default;
	}
}