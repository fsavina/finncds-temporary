<?php
namespace Finnegan\Auth\Policies;


use Finnegan\Models\Translatable;


class TranslatableModelPolicy extends ModelPolicy
{


	public function translate ( $user, Translatable $model )
	{
		if ( $this->authorizeAction ( $user, $model, 'translate' ) and $model->availableLocales ( null, true ) )
		{
			return $this->create ( $user, $model );
		}
		return false;
	}
}