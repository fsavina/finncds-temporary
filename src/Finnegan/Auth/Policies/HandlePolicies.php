<?php

namespace Finnegan\Auth\Policies;


use Illuminate\Contracts\Auth\Access\Gate;


trait HandlePolicies
{
	
	protected $policies = [
		\Finnegan\Models\Model::class              => ModelPolicy::class,
		\Finnegan\Models\TranslatableFields::class => ModelPolicy::class,
		\Finnegan\Models\Translatable::class       => TranslatableModelPolicy::class,
	];
	
	
	public function boot ( Gate $gate )
	{
		foreach ( $this->policies as $key => $value )
		{
			$gate->policy ( $key, $value );
		}
	}
	
}