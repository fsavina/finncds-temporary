<?php
namespace Finnegan\Auth;


use Finnegan\Contracts\Modules\Module;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;


class AuthServiceProvider extends ServiceProvider
{


	public function register ()
	{
		$this->app[ 'config' ]->set ( 'auth.passwords.users.email', 'finnegan::emails.password' );
	}


	public function boot ()
	{
		$this->app[ 'auth' ]->extend ( 'session', function ( Application $app, $name, array $config )
		{
			// Keep module session separated by module name
			return $app[ 'auth' ]->createSessionDriver ( $app->make ( Module::class )->name (), $config );
		} );
	}

}


