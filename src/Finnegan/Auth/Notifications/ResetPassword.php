<?php
namespace Finnegan\Auth\Notifications;


use Finnegan\Contracts\Modules\Module as ModuleContract;
use Illuminate\Auth\Notifications\ResetPassword as IlluminateResetPassword;
use Illuminate\Notifications\Messages\MailMessage;


class ResetPassword extends IlluminateResetPassword
{

	/**
	 * @var ModuleContract
	 */
	protected $module;


	public function __construct ( $token )
	{
		parent::__construct ( $token );
		$this->module = app ( ModuleContract::class );
	}


	/**
	 * Build the mail representation of the notification.
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail ()
	{
		return ( new MailMessage )
			->line ( 'You are receiving this email because we received a password reset request for your account.' )
			->action ( 'Reset Password', $this->module->actionUrl ( 'password_reset', [ $this->token ] ) )
			->line ( 'If you did not request a password reset, no further action is required.' );
	}
}
