<?php

namespace Finnegan\Foundation\Providers;


use Finnegan\Support\BaseServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\AggregateServiceProvider;


class FoundationServiceProvider extends AggregateServiceProvider
{
	
	
	protected $providers = [
		\Finnegan\Auth\AuthServiceProvider::class,
		\Finnegan\Database\DatabaseServiceProvider::class,
		\Finnegan\Definitions\DefinitionsServiceProvider::class,
		\Finnegan\Filesystem\FilesystemServiceProvider::class,
		\Finnegan\Layout\LayoutServiceProvider::class,
		\Finnegan\Models\ModelsServiceProvider::class,
		\Finnegan\Modules\ModulesServiceProvider::class,
		\Finnegan\Routing\RoutingServiceProvider::class,
		\Finnegan\Translation\TranslationServiceProvider::class,
		\anlutro\LaravelSettings\ServiceProvider::class,
	];
	
	protected $commands  = [
		'Finnegan\Foundation\Console\Commands\DatabaseConfig',
		'Finnegan\Foundation\Console\Commands\Install',
		'Finnegan\Foundation\Console\Commands\UserMake'
	];
	
	protected $aliases   = [
		'Module'      => 'Finnegan\Support\Facades\Module',
		'Models'      => 'Finnegan\Support\Facades\Models',
		'Definitions' => 'Finnegan\Support\Facades\Definitions',
		'Layout'      => 'Finnegan\Support\Facades\Layout',
		'Assets'      => 'Finnegan\Support\Facades\Assets',
		
		'Html'     => 'Collective\Html\HtmlFacade',
		'Form'     => 'Collective\Html\FormFacade',
		'Settings' => 'anlutro\LaravelSettings\Facade',
	];
	
	
	public function register ()
	{
		$this->registerBindings ();
		
		parent::register ();
		
		$this->registerCommands ();
		
		$this->registerAliases ();
	}
	
	
	protected function registerBindings ()
	{
		$this->app->singleton (
			\Illuminate\Contracts\Http\Kernel::class,
			\Finnegan\Foundation\Http\Kernel::class
		);
	}
	
	
	protected function registerCommands ()
	{
		$this->commands ( $this->commands );
	}
	
	
	protected function registerAliases ()
	{
		$aliasLoader = AliasLoader::getInstance ();
		foreach ( $this->aliases as $alias => $class )
		{
			$aliasLoader->alias ( $alias, $class );
		}
	}
	
	
	public function boot ()
	{
		$this->publishes ( [
							   BaseServiceProvider::sourcePath () . 'config' => config_path ( 'finnegan' )
						   ], 'config' );
		
		$this->publishes ( [
							   BaseServiceProvider::resourcesPath () . 'definitions' => resource_path ( 'definitions' )
						   ], 'definitions' );
	}
	
	
	public function provides ()
	{
		return [ 'settings' ];
	}
	
}




