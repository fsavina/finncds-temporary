<?php
namespace Finnegan\Foundation\Actions;


use Finnegan\Models\Model;


class Truncate extends Destroy
{

	protected $context = 'general';

	protected $confirm = 'finnegan::messages.truncate_confirm';


	protected function formUrl ( Model $model )
	{
		return $model -> actionUrl ( $this -> name () );
	}

}