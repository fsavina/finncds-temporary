<?php
namespace Finnegan\Foundation\Actions;


use Finnegan\Models\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;


abstract class Action
{

	protected $context;

	protected $button;

	protected $method;

	protected $confirm;

	protected $availableActions = [];


	/**
	 * @param string $action
	 * @return Action
	 */
	public static function load ( $action )
	{
		$action = __NAMESPACE__ . '\\' . Str:: studly ( $action );
		return new $action ();
	}


	protected function name ()
	{
		return strtolower ( ( new \ReflectionClass( $this ) )->getShortName () );
	}


	public function renderButton ( Model $model, $config = [] )
	{
		$button = $this -> presenter () -> button ();

		if ( !$this -> method )
		{
			if ( $this -> context == 'record' )
			{
				$button -> modelAction ( $model, $this -> name () );
			} else
			{
				$button -> url ( $model -> actionUrl ( $this -> name () ) );
				$button -> action ( $this -> name () );
			}
		} else
		{
			$button -> action ( $this -> name () );
		}

		if ( $this -> confirm )
		{
			$button -> confirm ( $this -> confirm );
		}

		if ( $this -> button )
		{
			$button -> addClass ( $this -> button );
		}

		if ( Arr::get ( $config, 'icon-only', false ) )
		{
			$button->title ( trans ( $button->getCaption () ) )->caption ( '' );
		}
		if ( Arr::get ( $config, 'basic', false ) )
		{
			$button -> removeClass ( 'button' );
		}

		return $button -> renderIf ( Gate::allows ( $this -> name (), $model ) );
	}


	/**
	 * @return \Finnegan\Contracts\Layout\Presenter
	 */
	protected function presenter ()
	{
		return app ( 'Finnegan\Contracts\Layout\Manager' )->presenter ();
	}


	public function getAvailableActions ()
	{
		return $this -> availableActions;
	}


}