<?php
namespace Finnegan\Foundation\Actions;


use Finnegan\Models\Model;


class Destroy extends Action
{

	protected $context = 'record';

	protected $button = 'alert';

	protected $method = 'delete';

	protected $confirm = 'finnegan::messages.delete_confirm';


	public function renderButton ( Model $model, $config = [] )
	{
		return $this -> formWrap ( parent :: renderButton ( $model, $config ), $model );
	}


	protected function formWrap ( $button, Model $model )
	{
		$attributes = [
			'url' => $this -> formUrl ( $model ),
			'method' => $this -> method,
			'style' => 'display:inline'
		];

		$form = app('Finnegan\Contracts\Layout\Manager')->form();
		return $render = $form->open ( $attributes ) . $button . $form->close ();
	}


	protected function formUrl ( Model $model )
	{
		return $model -> recordActionUrl ( $this -> name () );
	}


}