<?php
namespace Finnegan\Foundation\Actions;


class Index extends Action
{
	
	protected $context = 'general';
	
	
	protected $availableActions = [
		'general' => [ 'create', 'truncate' ],
		'record'  => [ 'show', 'edit', 'copy', 'translate', 'destroy' ]
	];
	
	
}