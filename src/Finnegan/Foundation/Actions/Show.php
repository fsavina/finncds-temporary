<?php
namespace Finnegan\Foundation\Actions;


class Show extends Action
{

	protected $context = 'record';

	protected $availableActions = [
		'record' => [ 'edit', 'copy', 'translate', 'destroy' ]
	];

}