<?php
namespace Finnegan\Foundation\Http;


class Kernel extends \Illuminate\Foundation\Http\Kernel
{
	
	const VERSION = '0.6.12';
	
	protected $middleware = [
		\Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
	];
	
	
	protected $middlewareGroups = [
		'finn.web' => [
			\Illuminate\Cookie\Middleware\EncryptCookies::class,
			\Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
			\Illuminate\Session\Middleware\StartSession::class,
			\Illuminate\View\Middleware\ShareErrorsFromSession::class,
			\Illuminate\Foundation\Http\Middleware\VerifyCsrfToken::class,
			\Illuminate\Routing\Middleware\SubstituteBindings::class,
		],
		
		'finn.api' => [
			'finn.throttle:60,1',
			'finn.bindings',
		],
	];
	
	
	protected $routeMiddleware = [
		'finn.auth'     => Middleware\Authenticate::class,
		'finn.role'     => Middleware\RoleCheck::class,
		'finn.active'   => Middleware\UserIsActive::class,
		'finn.guest'    => Middleware\RedirectIfAuthenticated::class,
		'finn.bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
		'finn.can'      => \Illuminate\Auth\Middleware\Authorize::class,
		'finn.throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
	];
	
}


