<?php

namespace Finnegan\Foundation\Http\Controllers;


use anlutro\LaravelSettings\SettingStore;
use App\Setting;
use Finnegan\Auth\Policies\HandlePolicies;
use Finnegan\Contracts\Modules\Module;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Container\Container;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;


class SettingsController extends BaseController
{
	
	use AuthorizesRequests, HandlePolicies;
	
	
	/**
	 * @var SettingStore
	 */
	protected $settings;
	
	
	public function __construct ( Container $app, Module $module )
	{
		$this->settings = $app->make ( SettingStore::class );
		
		parent::__construct ( $app, $module );
	}
	
	
	public function editSettings ( Setting $model )
	{
		$model->forceFill ( $this->settings->all () );
		$model->exists = true;
		
		return $this->renderView ( 'form', [
			'model'       => $model->withSessionErrors (),
			'breadcrumbs' => $this->makeBreadcrumbs ( $model )
		] );
	}
	
	
	public function storeSettings ( Setting $model, Request $request )
	{
		$input = $request->input ( $model->name (), [] );
		$files = $request->file ( $model->name (), [] );
		
		$model->forceFill ( $this->settings->all () );
		$model->exists = true;
		
		if ( ! $model->fromForm ( $input, $files ) )
		{
			return back ()->withInput ()->withErrors (
				$model->validator ()->errors (),
				$model->name ()
			);
		}
		
		$this->settings->forgetAll ();
		
		$this->settings->set ( $model->getAttributes () );
		
		$this->settings->save ();
		
		$url = $this->module->actionUrl ( 'settings' );
		
		return $this->redirectWithMessage ( $url, trans ( "finnegan::messages.update_completed" ) );
	}
	
	
	public function makeBreadcrumbs ( Setting $model )
	{
		$presenter = $this->module->make ( \Finnegan\Contracts\Layout\Manager::class )->presenter ();
		
		$breadcrumbs = $presenter->breadcrumbs (
			[ $this->module->actionUrl ( 'home' ) => $presenter->icon ( 'home' )->render () ]
		);
		
		$breadcrumbs->add ( $model->definition ()->plural () );
		
		return $breadcrumbs;
	}
	
}
