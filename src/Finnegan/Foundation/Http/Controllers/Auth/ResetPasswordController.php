<?php
namespace Finnegan\Foundation\Http\Controllers\Auth;


use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;


class ResetPasswordController extends AuthController
{
	
	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/
	
	use ResetsPasswords;

	
	public function showResetForm ( Request $request, $token = null )
	{
		return $this->renderView ( 'auth.reset', [ ], 'finnegan' )->with (
			[ 'token' => $token, 'email' => $request->email ]
		);
	}
}
