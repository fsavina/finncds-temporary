<?php
namespace Finnegan\Foundation\Http\Controllers\Auth;


use Finnegan\Contracts\Modules\Module;
use Finnegan\Foundation\Http\Controllers\BaseController;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Container\Container;


abstract class AuthController extends BaseController
{
	
	
	/**
	 * Create a new authentication controller instance.
	 * @param Container $app
	 * @param Module    $module
	 * @param Guard     $auth
	 */
	public function __construct ( Container $app, Module $module, Guard $auth )
	{
		$this->auth = $auth;
		parent:: __construct ( $app, $module );
	}
	
	
	public function boot ()
	{
		if ( $this->module->isSecure () )
		{
			$this->redirectTo = $this->module->actionUrl ( 'home' );
		}
	}
	
}
