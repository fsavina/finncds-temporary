<?php
namespace Finnegan\Foundation\Http\Controllers\Auth;


use Illuminate\Foundation\Auth\SendsPasswordResetEmails;


class ForgotPasswordController extends AuthController
{
	
	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset emails and
	| includes a trait which assists in sending these notifications from
	| your application to your users. Feel free to explore this trait.
	|
	*/
	
	use SendsPasswordResetEmails;
	
	
	public function showLinkRequestForm ()
	{
		return $this->renderView ( 'auth.password', [ ], 'finnegan' );
	}
	
	
}
