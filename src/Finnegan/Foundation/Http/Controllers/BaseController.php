<?php
namespace Finnegan\Foundation\Http\Controllers;


use Finnegan\Contracts\Layout\Manager;
use Finnegan\Contracts\Modules\Module;
use Illuminate\Contracts\Container\Container;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;


class BaseController extends Controller
{

	use ValidatesRequests;

	protected $app;

	protected $module;

	protected $theme;


	/**
	 * @param Container $app
	 * @param Module      $module
	 */
	public function __construct ( Container $app, Module $module )
	{
		$this -> app = $app;
		$this -> module = $module;

		if ( method_exists ( $this, 'boot' ) )
		{
			$this->app->call ( [ $this, 'boot' ] );
		}
	}


	public function __call($method, $parameters)
	{
		if ($method == 'boot') {
			return;
		}

		throw new \BadMethodCallException("Call to undefined method [{$method}]");
	}


	public function home ()
	{
		return $this -> renderView ( 'home' );
	}


	/**
	 * @param string $view
	 * @param array  $params
	 * @return \Illuminate\View\View
	 */
	protected function renderView ( $view, $params = [] )
	{
		return $this->layoutManager ()->theme ()->view ( $view, $params );
	}


	protected function layoutManager ()
	{
		return $this->app->make ( Manager::class );
	}


	/**
	 * @param string $url
	 * @param string $message
	 * @return mixed
	 */
	protected function redirectWithMessage ( $url, $message )
	{
		return $this->app[ 'redirect' ]->to ( $url )->with ( 'message', $message );
	}

}


