<?php
namespace Finnegan\Foundation\Http\Controllers;


use Finnegan\Auth\Policies\HandlePolicies;
use Finnegan\Foundation\Actions\Action;
use Finnegan\Models\Model;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class CrudController extends BaseController
{
	
	use AuthorizesRequests, HandlePolicies;
	
	
	const ACTIONS_LIST = 'index';
	
	const ACTIONS_VIEW = 'show';
	
	const ACTIONS_FORM = 'form';
	
	
	/**
	 * @param Request $request
	 * @param Model   $model
	 * @return mixed
	 */
	public function index ( Request $request, Model $model )
	{
		$this->authorize ( $model );
		
		$builder = $model->newQueryWithoutScopes ()
						 ->search ( $this->module, $request )
						 ->sort ( $this->module, $request );
		
		$this->loadAssets ( $model, 'list' );
		
		return $this->renderView ( 'list', [
			'items'       => $builder->paginate (),
			'model'       => $model,
			'breadcrumbs' => $this->makeBreadcrumbs ( 'index', $model ),
			'actions'     => $this->availableActions ( 'index' ),
			'filters'     => [
				'*'    => $model->otherFilters (),
				'text' => $model->textFilters ()
			]
		] );
	}
	
	
	public function create ( Model $model )
	{
		$this->authorize ( $model );
		
		$this->loadAssets ( $model, 'form' );
		
		return $this->renderView ( 'form', [
			'model'       => $model->withSessionErrors (),
			'breadcrumbs' => $this->makeBreadcrumbs ( 'form', $model )
		] );
	}
	
	
	public function store ( Request $request, Model $model )
	{
		$action = $request->input ( 'save-action', 'store' );
		
		$this->authorize ( $action, $model );
		
		if ( $model->fromRequest ( $request, $action ) )
		{
			return $this->afterSave ( $model, "{$action}_completed" );
		}
		return $this->failedSave ( $model );
	}
	
	
	public function show ( Model $model, $id )
	{
		$model = $model->newQueryWithoutScopes ()->findOrFail ( $id );
		
		$this->authorize ( $model );
		
		$this->loadAssets ( $model, 'show' );
		
		return $this->renderView ( 'show', [
			'model'       => $model,
			'breadcrumbs' => $this->makeBreadcrumbs ( 'show', $model ),
			'actions'     => $this->availableActions ( 'show' )
		] );
	}
	
	
	public function edit ( Model $model, $id )
	{
		$model = $model->newQueryWithoutScopes ()->findOrFail ( $id );
		
		$this->authorize ( $model );
		
		$this->loadAssets ( $model, 'form' );
		
		return $this->renderView ( 'form', [
			'model'       => $model->withSessionErrors (),
			'breadcrumbs' => $this->makeBreadcrumbs ( 'form', $model )
		] );
	}
	
	
	public function update ( Request $request, Model $model, $id )
	{
		$model = $model->newQueryWithoutScopes ()->findOrFail ( $id );
		
		$action = $request->input ( 'save-action', 'update' );
		
		$this->authorize ( $action, $model );
		
		if ( $model->fromRequest ( $request, $action ) )
		{
			return $this->afterSave ( $model, "{$action}_completed" );
		}
		return $this->failedSave ( $model );
	}
	
	
	public function destroy ( Model $model, $id )
	{
		$model = $model->newQueryWithoutScopes ()->findOrFail ( $id );
		
		$this->authorize ( $model );
		
		$model->delete ();
		return $this->redirectWithMessage (
			$model->actionUrl ( static :: ACTIONS_LIST ),
			trans_choice ( 'finnegan::messages.delete_completed', 1 )
		);
	}
	
	
	public function truncate ( Model $model )
	{
		$this->authorize ( $model );
		
		$model->truncate ();
		
		return $this->redirectWithMessage (
			$model->actionUrl ( static :: ACTIONS_LIST ),
			trans ( 'finnegan::messages.truncate_completed' )
		);
	}
	
	
	public function copy ( Model $model, $id )
	{
		$this->authorize ( $model );
		
		$model = $model->copyPreset ( $id );
		
		$this->loadAssets ( $model, 'form' );
		
		return $this->renderView ( 'form', [ 'model' => $model->withSessionErrors () ] );
	}
	
	
	/**
	 * @todo migliorare authorize
	 */
	public function field ( Request $request, Model $model, $field, $action )
	{
		$this->authorize ( $model );
		
		$response = [ 'success' => false ];
		if ( $model->hasField ( $field ) )
		{
			$field = $model->fields ( $field );
			$method = 'run' . Str:: studly ( $action );
			if ( method_exists ( $field, $method ) )
			{
				parse_str ( $request->getQueryString (), $query );
				$response = [
					'success' => true,
					'data'    => call_user_func_array ( [ $field, $method ], [ $query, $request ] )
				];
			}
		}
		return response ()->json ( $response );
	}
	
	
	public function translate ( Model $model, $id )
	{
		$this->authorize ( $model );
		
		$translateFrom = $model->newQueryWithoutScopes ()->findOrFail ( $id );
		$model->translates = $translateFrom->translates;
		
		$this->loadAssets ( $model, 'form' );
		
		return $this->renderView ( 'form', [ 'model' => $model->withSessionErrors () ] );
	}
	
	
	public function sort ( Request $request, Model $model )
	{
		$this->authorize ( $model );
		
		$items = ( array ) $request->get ( 'items', [ ] );
		$min = ( int ) $request->get ( 'min', 1 );
		$max = ( int ) $request->get ( 'max' );
		
		$response = [ 'success' => false ];
		if ( ( $max - $min + 1 ) >= count ( $items ) )
		{
			$field = $model->definition ()->sortablePriorityField ();
			foreach ( $items as $id )
			{
				$model->newQueryWithoutScopes ()
					  ->where ( $model->getKeyName (), $id )
					  ->update ( [ $field => $min++ ] );
			}
			$response [ 'success' ] = true;
		}
		
		return response ()->json ( $response );
	}
	
	
	/**
	 * Apply the required bulk actions to the given record list
	 * @param Request $request
	 * @param Model   $model
	 * @return mixed
	 * @throws \Exception
	 */
	public function bulk ( Request $request, Model $model )
	{
		$input = $request->input ( $model->name () . '_bulk' );
		
		$action = data_get ( $input, 'action' );
		$this->authorize ( [ $model, $action ] );
		
		$method = 'bulk' . Str::studly ( $action );
		if ( ! method_exists ( $this, $method ) )
		{
			throw new \Exception ( "The '$action' bulk action is not supported'" );
		}
		
		$redirectUrl = $model->actionUrl ( static :: ACTIONS_LIST );
		return call_user_func_array ( [ $this, $method ], [ $model, $input, $redirectUrl ] );
	}
	
	
	/**
	 * @param Model  $model
	 * @param array  $input
	 * @param string $redirectUrl
	 * @return mixed
	 */
	protected function bulkDestroy ( Model $model, $input, $redirectUrl )
	{
		$records = (array) data_get ( $input, 'records', [ ] );
		if ( count ( $records ) )
		{
			foreach ( $model->newQueryWithoutScopes ()->findMany ( $records ) as $item )
			{
				$item->delete ();
			}
			$message = trans_choice ( 'finnegan::messages.delete_completed', count ( $input [ 'records' ] ) );
			return $this->redirectWithMessage ( $redirectUrl, $message );
		}
		return redirect ( $redirectUrl );
	}
	
	
	public function relationCreate ( Request $request, Model $model, $id, $relationName )
	{
		$model = $model->newQueryWithoutScopes ()->findOrFail ( $id );
		$this->authorize ( $model );
		
		$relation = $model->relation ( $relationName );
		
		return $this->renderView ( 'form', [
			'model'   => $relation->newPivot ()->withSessionErrors (),
			'backUrl' => $model->recordActionUrl ( static :: ACTIONS_VIEW ),
			'hidden'  => [ 'related_id' => $request->input ( 'related_id' ) ]
		] );
	}
	
	
	/**
	 * Attach a new object to the main model
	 * @param Request $request
	 * @param Model   $model
	 * @param int     $id
	 * @param string  $relationName
	 * @return mixed
	 */
	public function relationAttach ( Request $request, Model $model, $id, $relationName )
	{
		$model = $model->newQueryWithoutScopes ()->findOrFail ( $id );
		$this->authorize ( $model );
		
		$relation = $model->relation ( $relationName );
		if ( is_object ( $relation ) and ( $relatedId = $request->input ( 'related_id' ) ) )
		{
			$attributes = [ ];
			if ( $relation->hasExtraFields () )
			{
				$pivot = $relation->newPivot ();
				$input = $request->input ( $relationName, [ ] );
				$files = $request->file ( $relationName, [ ] );
				
				if ( ! $pivot->fromForm ( $input, $files ) )
				{
					return $this->failedSave ( $model );
				}
				
				$attributes = $pivot->fields ()->values ()->all ();
			}
			$relation->attach ( $relatedId, $attributes );
			
			if ( $relation->config ( 'syncReverse' ) )
			{
				$relation->attachReverse ( $relatedId, $attributes );
			}
		}
		return $this->relationAfterSave ( $model, trans ( 'finnegan::messages.relation_attach_completed' ), $relationName );
	}
	
	
	public function relationEdit ( Model $model, $id, $relationName, $relatedId )
	{
		$model = $model->newQueryWithoutScopes ()->findOrFail ( $id );
		$this->authorize ( $model );
		
		$relation = $model->relation ( $relationName );
		if ( is_object ( $relation ) )
		{
			$item = $relation->findOrFail ( $relatedId );
			return $this->renderView ( 'form', [
				'model'   => $item->pivot->withSessionErrors (),
				'backUrl' => $model->recordActionUrl ( static :: ACTIONS_VIEW )
			] );
		}
		
		return back ()->withErrors ( [ trans ( 'finnegan::messages.relation_missing' ) ], $model->name () );
	}
	
	
	public function relationUpdate ( Request $request, Model $model, $id, $relationName, $relatedId )
	{
		$model = $model->newQueryWithoutScopes ()->findOrFail ( $id );
		$this->authorize ( $model );
		
		$relation = $model->relation ( $relationName );
		if ( is_object ( $relation ) )
		{
			$item = $relation->findOrFail ( $relatedId );
			$input = $request->input ( $relationName, [ ] );
			$files = $request->file ( $relationName, [ ] );
			
			if ( $item->pivot->fromForm ( $input, $files ) )
			{
				$attributes = $item->pivot->fields ()->values ()->all ();
				$relation->updateExistingPivot ( $relatedId, $attributes );
				return $this->relationAfterSave ( $model, trans ( 'finnegan::messages.relation_edit_completed' ), $relationName );
			}
			return $this->failedSave ( $item->pivot );
		}
		return back ()->withErrors ( [ trans ( 'finnegan::messages.relation_missing' ) ], $model->name () );
	}
	
	
	/**
	 * Detach the given object from the main model
	 * @param Model  $model
	 * @param int    $id
	 * @param string $relationName
	 * @param int    $relationId
	 * @return mixed
	 */
	public function relationDetach ( Model $model, $id, $relationName, $relationId )
	{
		$model = $model->newQueryWithoutScopes ()->findOrFail ( $id );
		$this->authorize ( $model );
		
		$relation = $model->relation ( $relationName );
		if ( is_object ( $relation ) )
		{
			$relation->detach ( $relationId );
		}
		
		return $this->relationAfterSave ( $model, 'relation_detach_completed', $relationName );
	}
	
	
	protected function loadAssets ( Model $model, $action )
	{
		$assets = (array) $model->config ( "display.{$action}.assets", [ ] );
		if ( count ( $assets ) )
		{
			foreach ( $assets as $asset )
			{
				$this->layoutManager ()->assetManager ()->requireResource ( $asset );
			}
		}
		return $this;
	}
	
	
	protected function afterSave ( Model $model, $message, $action = self :: ACTIONS_VIEW )
	{
		$url = $model->recordActionUrl ( $action );
		return $this->redirectWithMessage ( $url, trans ( "finnegan::messages.{$message}" ) );
	}
	
	
	protected function relationAfterSave ( Model $model, $message, $relationName )
	{
		$url = $model->recordActionUrl ( static :: ACTIONS_VIEW ) . "#relation-{$relationName}";
		$message = trans ( "finnegan::messages.{$message}" );
		return $this->redirectWithMessage ( $url, $message );
	}
	
	
	protected function failedSave ( Model $model )
	{
		return back ()->withInput ()->withErrors (
			$model->validator ()->errors (),
			$model->name ()
		);
	}
	
	
	protected function availableActions ( $action )
	{
		$available = Action:: load ( $action )->getAvailableActions ();
		
		$actions = [ ];
		foreach ( $available as $context => $list )
		{
			foreach ( $list as $actionName )
			{
				$actions[ $context ][] = Action:: load ( $actionName );
			}
		}
		return $actions;
	}
	
	
	public function makeBreadcrumbs ( $context, $model = null, $current = null )
	{
		$presenter = $this->module->make ( 'Finnegan\Contracts\Layout\Manager' )->presenter ();
		
		$breadcrumbs = $presenter->breadcrumbs ( [
													 $this->module->actionUrl ( 'home' ) => $presenter->icon ( 'home' )
																									  ->render ()
												 ] );
		
		switch ( $context )
		{
			case 'show':
			case 'form':
				if ( app ( Gate::class )->allows ( 'index', $model ) )
				{
					$breadcrumbs->add ( $model->actionUrl ( 'index' ), $model->definition ()->plural () );
				}
				
				$breadcrumbs->add ( $presenter->title ( $model, $context, true ) );
				break;
			case 'index':
				$breadcrumbs->add ( $model->definition ()->plural () );
				break;
			default:
				if ( strlen ( $current ) )
				{
					$breadcrumbs->add ( $current );
				}
				break;
		}
		
		return $breadcrumbs;
	}
	
}
