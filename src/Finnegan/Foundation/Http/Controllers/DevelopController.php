<?php
namespace Finnegan\Foundation\Http\Controllers;


use Finnegan\Contracts\Auth\AuthorizationException;
use Finnegan\Contracts\Layout\AssetManager;
use Finnegan\Modules\ModulesManager;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Str;


class DevelopController extends CrudController
{


	public function authorize ( $ability, $arguments = [ ] )
	{
		$auth = $this->app->make ( 'Illuminate\Contracts\Auth\Guard' );
		if ( ! $auth->check () or ! $auth->user ()->achieve ( 'developer' ) )
		{
			throw new AuthorizationException( 'Access forbidden for the current user' );
		}
	}
	
	
	public function modulesList ( Request $request, ModulesManager $manager, Kernel $artisan )
	{
		$this->authorize ( 'modulesList' );

		if ( $request -> isMethod ( 'post' ) )
		{
			$message = '';
			if ( strlen ( $module = $request -> input ( 'module' ) ) )
			{
				$artisan->call ( 'finnegan:make-module', [
					'name'     => $module,
					'--crud'   => ( bool ) $request->input ( 'crud' ),
					'--secure' => ( bool ) $request->input ( 'secure' ),
					'--role'   => $request->input ( 'role', 'admin' )
				] );
				$message = 'Module created.';
				//Invalid module name: the module already exists
			}
			return $this->redirectWithMessage ( $request->url (), $message );
		}

		return $this->renderView ( 'tools.modules', [
			'modules'  => $manager->modules(),
			'title' => 'Modules',
			'icon'  => 'cubes'
		] );
	}


	/**
	 * @todo restituire errore se file esiste già, messaggio creazione avvenuta
	 */
	public function definitionWizard ( Request $request, AssetManager $assetManager )
	{
		$this->authorize ( 'definitionWizard' );

		if ( $request->isMethod ( 'post' ) )
		{
			$params = $request->only ( [ 'name', 'root' ] );

			$path = base_path ( 'config/finnegan/definitions' ) . '/';
			$name = Str::snake ( $params[ 'name' ] ) . '.json';
			if ( ! file_exists ( $path . $name ) )
			{
				$data = data_get ( $params, 'root', [ ] );
				file_put_contents ( $path . $name, json_encode ( $data, JSON_PRETTY_PRINT ) );
			}
		}

		$assetManager->requireResource ( 'assets/vendor/jsoneditor.min.js' );

		return $this->renderView ( 'tools.definition-wizard', [
			'breadcrumbs' => $this->makeBreadcrumbs ( '*', null, 'Definition wizard' )
		] );
	}


	public function configViewer ( Request $request, Repository $config, Kernel $artisan )
	{
		$this->authorize ( 'configViewer' );

		if ( $request->has ( 'cmd' ) )
		{
			$message = '';
			switch ( $request->get ( 'cmd' ) )
			{
				case 'refresh':
					$artisan->call ( 'config:cache' );
					$message = 'The cache has been refreshed.';
					break;
				case 'dump':
					$artisan->call ( 'config:clear' );
					$message = 'The cache has been dumped.';
					break;
			}
			return $this->redirectWithMessage ( $request->url (), $message );
		}

		return $this->renderView ( 'tools.config', [
			'dump'  => $config->all (),
			'cache' => $this->app->configurationIsCached (),
			'title' => 'View configuration',
			'icon'  => 'cog'
		] );
	}


	public function routesList ( Request $request, Router $router, Kernel $artisan )
	{
		$this->authorize ( 'routesList' );

		if ( $request->has ( 'cmd' ) )
		{
			$message = '';
			switch ( $request->get ( 'cmd' ) )
			{
				case 'refresh':
					try {
						$artisan->call ( 'route:cache' );
						$message = 'The cache has been refreshed.';
					} catch (\Exception $e)
					{
						$message = $e->getMessage ();
					}
					break;
				case 'dump':
					$artisan->call ( 'route:clear' );
					$message = 'The cache has been dumped.';
					break;
			}
			return $this->redirectWithMessage ( $request->url (), $message );
		}

		return $this->renderView ( 'tools.routes', [
			'routes' => $router->getRoutes (),
			'cache'  => $this->app->routesAreCached (),
			'title'  => 'Routes list',
			'icon'   => 'compass'
		] );
	}

}




