<?php
namespace Finnegan\Foundation\Http\Controllers;


class FrontendController extends BaseController
{

	public function page ( $slug )
	{
		$page = finn ( 'pages' )->where ( 'slug', $slug )->firstOrFail ();
		return $this->renderView ( 'page', [ 'page' => $page ] );
	}

}