<?php
namespace Finnegan\Foundation\Http\Middleware;


use Finnegan\Contracts\Modules\Module;
use Illuminate\Contracts\Auth\Guard;


class Middleware
{

	
	/**
	 * The Guard implementation.
	 * @var Guard
	 */
	protected $auth;

	/**
	 * The Finnegan app module
	 * @var Module
	 */
	protected $module;


	/**
	 * Create a new filter instance.
	 * @param Guard  $auth
	 * @param Module $module
	 */
	public function __construct ( Guard $auth, Module $module )
	{
		$this -> auth = $auth;
		$this -> module = $module;
	}

}
