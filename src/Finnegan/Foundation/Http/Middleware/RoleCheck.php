<?php
namespace Finnegan\Foundation\Http\Middleware;


class RoleCheck extends Middleware
{


	/**
	 * Handle an incoming request.
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure                 $next
	 * @param string                   $role
	 * @return mixed
	 */
	public function handle ( $request, \Closure $next, $role )
	{
		if ( $this -> auth -> guest () or ! $this -> auth -> user () -> achieve ( $role ) )
		{
			$this -> auth -> logout ();
			if ( $request -> ajax () )
			{
				return response ( 'Unauthorized.', 401 );
			} else
			{
				return redirect () -> guest ( $this -> module -> actionUrl ( 'login' ) );
			}
		}
		return $next ( $request );
	}

}
