<?php
namespace Finnegan\Foundation\Http\Middleware;


class UserIsActive extends Middleware
{


	/**
	 * Handle an incoming request.
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure                 $next
	 * @return mixed
	 */
	public function handle ( $request, \Closure $next )
	{
		if ( $this -> auth -> guest () or ! $this -> auth -> user () -> active )
		{
			$this -> auth -> logout ();
			if ( $request -> ajax () )
			{
				return response ( 'Unauthorized.', 401 );
			} else
			{
				return redirect () -> guest ( $this -> module -> actionUrl ( 'login' ) );
			}
		}
		return $next ( $request );
	}

}
