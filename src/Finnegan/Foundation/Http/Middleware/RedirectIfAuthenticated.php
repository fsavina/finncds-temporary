<?php
namespace Finnegan\Foundation\Http\Middleware;


use Illuminate\Http\RedirectResponse;


class RedirectIfAuthenticated extends Middleware
{


	/**
	 * Handle an incoming request.
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 * @return mixed
	 */
	public function handle ( $request, \Closure $next )
	{
		if ( ! $this -> module -> isSecure () or $this -> auth -> check () )
		{
			return new RedirectResponse ( $this -> module -> actionUrl ( 'home' ) );
		}
		return $next ( $request );
	}

}


