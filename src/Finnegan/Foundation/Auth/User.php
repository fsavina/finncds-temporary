<?php
namespace Finnegan\Foundation\Auth;


use Finnegan\Auth\Roles\Role;
use Finnegan\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Finnegan\Models\Model;
use Finnegan\Support\LoadingContext;
use Illuminate\Support\Facades\Auth;


class User
	extends
		Model
	implements
		\Illuminate\Contracts\Auth\Authenticatable,
		\Illuminate\Contracts\Auth\Access\Authorizable,
		\Illuminate\Contracts\Auth\CanResetPassword
{
	
	use \Illuminate\Auth\Authenticatable,
		\Illuminate\Auth\Passwords\CanResetPassword,
		\Illuminate\Foundation\Auth\Access\Authorizable,
		\Illuminate\Notifications\Notifiable;
	
	
	protected $roleInstance;

	protected $definition = 'users';


	/**
	 * @return \Finnegan\Definitions\ModelDefinition
	 */
	protected function loadDefinition ( LoadingContext $context = null )
	{
		return app ( 'definitions.factory' )->unguarded ( $this->definition );
	}
	
	
	/**
	 * Get the current user role
	 * @return \Finnegan\Auth\Roles\Role
	 */
	public function role ()
	{
		if ( is_null ( $this -> roleInstance ) )
		{
			$this -> roleInstance = Role :: make ( $this -> role );
		}
		return $this -> roleInstance;
	}
	
	
	/**
	 * Check if the current user achieve the given role
	 * @param string $role
	 * @return boolean
	 */
	public function achieve ( $role )
	{
		return $this -> role () -> achieve ( $role );
	}


	public function hasAbility ( $ability )
	{
		return in_array ( $ability, $this->role ()->abilities () );
	}
	
	
	/**
	 * Check if the current user is allowed to access to the given module
	 * @param string $module
	 * @return boolean
	 */
	public function canAccess ( $module )
	{
		$role = config ( "finnegan.modules.$module.auth.role", false );
		return ( ! $role or $this -> achieve ( $role ) );
	}
	
	
	/**
	 * Get the available roles
	 * @return array
	 */
	public static function roles ()
	{
		return config ( 'finnegan.auth.roles' );
	}


	/**
	 * @return array
	 */
	public static function roleOptions ()
	{
		$roles = [ ];
		foreach ( static :: roles () as $role => $roleConfig )
		{
			$label = trans ( "finnegan::auth.roles.{$role}" );
			$roles [ $role ] = trans ( data_get ( $roleConfig, 'label', $label ) );
		}
		return $roles;
	}


	public static function roleOptionsForm ()
	{
		$roles = [];
		foreach ( static :: roleOptions () as $role => $label )
		{
			if ( ! Auth :: check () or Auth :: user () -> achieve ( $role ) )
			{
				$roles [ $role ] = $label;
			}
		}
		return $roles;
	}


	public static function setAuthor ( Model $model )
	{
		if ( $model->hasAttribute ( 'users_id' ) )
		{
			if ( ! $model->exists or ! $model->users_id )
			{
				$model->users_id = Auth::user ()->getKey ();
			}
		}
	}
	
	/**
	 * Send the password reset notification.
	 * @param  string  $token
	 * @return void
	 */
	public function sendPasswordResetNotification ( $token )
	{
		$this->notify ( new ResetPasswordNotification( $token ) );
	}


}
