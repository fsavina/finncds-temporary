<?php
namespace Finnegan\Foundation\Console\Commands;


use Finnegan\Foundation\Auth\User;
use Finnegan\Models\Model;
use Finnegan\Models\ModelsFactory;
use Finnegan\Support\CommandsModuleTrait;
use Finnegan\Support\WizardCommand;


class UserMake extends WizardCommand
{

	use CommandsModuleTrait;
	
	protected $signature = 'finnegan:make-user {--role=developer : The user role}';


	protected $description = 'Create a new user';


	protected $questions =
	[
		'name'     => 'Insert the user name',
		'email'    =>
		[
			'text'    => 'Insert the user e-mail address',
			'confirm' => 'Confirm e-mail address'
		],
		'password' =>
		[
			'text'    => 'Choose a password',
			'secret'  => true,
			'confirm' => 'Confirm password'
		]
	];


	public function fire ()
	{
		$role = $this->getInputRole ();
		if ( ! $this->isRole ( $role ) )
		{
			$this->error ( 'Invalid role: ' . $role );
			return false;
		}

		$model = $this->getModelInstance ();
		$data = $this->getInputData ( $model->rules() );

		$this->createUser ( $model, $data, $role );

		$this->info ( 'User created successfully.' );
		return true;
	}


	protected function getInputRole ()
	{
		return $this->option ( 'role' );
	}


	protected function isRole ( $role )
	{
		return array_key_exists ( $role, User::roles () );
	}


	protected function getModelInstance ()
	{
		return ( new ModelsFactory ( $this->getModule () ) )->unguarded ( 'users' );
	}


	protected function createUser ( Model $model, $data, $role )
	{
		foreach ( $data as $field => $value )
		{
			if ( $model->hasField ( $field ) )
			{
				$field = $model->fields ( $field );
				$field->setValue ( $field->filter ( $value ) );
			}
		}

		$model->role = $role;
		$model->active = true;
		$model->save ();
	}

}



