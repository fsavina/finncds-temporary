<?php
namespace Finnegan\Foundation\Console\Commands;


use Finnegan\Foundation\Providers\FoundationServiceProvider;
use Finnegan\Layout\LayoutServiceProvider;
use Illuminate\Console\Command;
use Illuminate\Support\Str;


class Install extends Command
{
	
	protected $signature = 'finnegan:install';
	
	protected $description = 'Install FinneganCDS';
	
	/**
	 * List of the CMS default migrations
	 * @var array
	 */
	protected $migrations = [
		'users',
		'tags',
		'categories',
		'posts',
		'pages',
		'settings',
		'translations'
	];
	
	/**
	 * List of the CMS default models
	 * @var array
	 */
	protected $models = [
		'tags',
		'categories',
		'posts',
		'pages'
	];
	
	
	public function fire ()
	{
		$this->setApplicationName ();
		
		$this->generateApplicationKey ();
		
		$this->publishAssetsAndConfig ();
		
		$this->setDatabaseConfig ();
		
		while ( $error = $this->connectionFails() )
		{
			switch ( $error )
			{
				case 1049:
					$this->line ( 'The database is missing.', 'error' );
					if ( !$this->promptDatabaseCreation () )
					{
						$this->line ( "Create the database manually and launch again the '{$this->getName()}' command to complete the procedure.", 'error' );
						return false;
					}
					$this->createDatabase ();
					break;
				default:
					$this->line ( 'There is something wrong with the database configuration or the server died.', 'error' );
					if ( !$this->promptDatabaseConfigReset () )
					{
						$this->line ( 'Launch the command again once you\'ll be sure that the configuration is correct and the server is alive.', 'error' );
						return false;
					}
					$this->setDatabaseConfig ();
					break;
			}
		}
		
		
		if ( $this->promptModelsCreation () )
		{
			$this->createModels ();
		}
		
		if ( $this->promptMigrate () )
		{
			$this->runMigrate ();
		}
		
		if ( $this->promptUserCreation () )
		{
			$this->createAdminUser ();
		}
		
		$this->line ( 'Installation completed successfully.', 'info' );
		return true;
	}
	
	
	protected function setApplicationName ()
	{
		$name = $this -> ask ( 'Application namespace (leave empty to keep the current namespace)', false );
		if ( strlen ( $name ) )
		{
			$name = Str :: studly ( $name );
			$this -> call ( 'app:name', [ 'name' => $name ] );
		}
	}
	
	
	protected function generateApplicationKey ()
	{
		$this -> call ( 'key:generate' );
	}
	
	
	protected function publishAssetsAndConfig ()
	{
		$this -> call ( 'vendor:publish', [ '--provider' => FoundationServiceProvider::class ] );
		$this -> call ( 'vendor:publish', [ '--provider' => LayoutServiceProvider::class ] );
	}
	
	
	protected function setDatabaseConfig ()
	{
		$this -> call ( 'finnegan:database-config', [ '--update-config' => true ] );
	}
	
	
	protected function connectionFails ()
	{
		try {
			$this->laravel->make('db.connection')->reconnect();
			return false;
		} catch (\PDOException $e)
		{
			return $e->getCode();
		}
	}
	
	
	protected function promptDatabaseConfigReset ()
	{
		return ! $this->confirm ( 'The database configuration is correct?' );
	}
	
	
	protected function promptDatabaseCreation ()
	{
		return $this->confirm ( 'Do you want me to create the database?' );
	}
	
	
	protected function createDatabase ()
	{
		$driver = $this->laravel[ 'config' ]->get ( 'database.default' );
		
		/**
		 * @var string $host
		 * @var string $username
		 * @var string $password
		 * @var string $database
		 */
		extract ( $this->laravel[ 'config' ]->get ( "database.connections.$driver" ) );
		
		$connection = new \PDO ( "{$driver}:host={$host}", $username, $password );
		return $connection->exec (
			"CREATE DATABASE IF NOT EXISTS `$database` CHARACTER SET utf8 COLLATE utf8_general_ci;"
		);
	}
	
	
	protected function promptModelsCreation (  )
	{
		return $this->confirm ( 'Do you want to create the default models?' );
	}
	
	
	protected function createModels ()
	{
		$bar = $this->output->createProgressBar ( count ( $this->models ) );
		foreach ( $this->models as $model )
		{
			$this->line ( '' );
			
			$this->call ( 'finnegan:make-model', [ 'name' => $model ] );
			
			$bar->advance ();
			$this->line ( '' );
		}
		$this->line ( '' );
	}
	
	
	protected function promptMigrate ()
	{
		return $this->confirm ( 'Do you want to create and run the migrations?' );
	}
	
	
	protected function runMigrate ()
	{
		$bar = $this->output->createProgressBar ( count ( $this->migrations ) );
		foreach ( $this->migrations as $migration )
		{
			$this->line ( '' );
			
			$this->call ( 'finnegan:make-migration', [ 'name' => $migration ] );
			
			$bar->advance ();
			$this->line ( '' );
		}
		$this->line ( '' );
		
		$this->call ( 'migrate' );
	}
	
	
	protected function promptUserCreation ()
	{
		return $this->confirm ( 'Do you want to create an admin user?' );
	}
	
	
	protected function createAdminUser ()
	{
		$this->call ( 'finnegan:make-user', [ '--role' => 'developer' ] );
	}
	
}




