<?php
namespace Finnegan\Foundation\Console\Commands;


use Finnegan\Support\WizardCommand;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Arr;


class DatabaseConfig extends WizardCommand
{

	
	protected $signature   = 'finnegan:database-config
							  {--update-config : Whether the loaded config should be updated}';

	protected $description = 'Set the database configuration';

	protected $questions   = [
		'DB_HOST'     => [
			'text'    => 'Database host',
			'rules'   => 'required',
			'default' => 'localhost',
			'use_old' => true
		],
		'DB_DATABASE' => [
			'text'    => 'Database name',
			'rules'   => 'required',
			'use_old' => true
		],
		'DB_USERNAME' => [
			'text'    => 'Username',
			'rules'   => 'required',
			'default' => 'root',
			'use_old' => true
		],
		'DB_PASSWORD' => [
			'text'        => 'Password',
			'default'     => false,
			'allow_empty' => true,
			'use_old'     => true
		]
	];

	protected $files;


	public function __construct ( Factory $validator, Filesystem $files )
	{
		parent::__construct ( $validator );
		$this->files = $files;
	}


	public function fire ()
	{
		$data = $this->getInputData ();

		$this->saveConfig ( $data );

		if ( $this->option ( 'update-config' ) )
		{
			$this->updateLoadedConfig ( $data );
		}

		$this->info ( 'Database configured successfully.' );
	}
	
	
	protected function getDefaultValue ( $name, $question )
	{
		if ( Arr::get ( $question, 'use_old', false ) )
		{
			return env ( $name, parent::getDefaultValue ( $name, $question ) );
		}
		return parent::getDefaultValue ( $name, $question );
	}


	protected function saveConfig ( $data )
	{
		$path = $this->laravel->basePath () . DIRECTORY_SEPARATOR . '.env';

		if ( $this->files->exists ( $path ) )
		{
			foreach ( $data as $var => $value )
			{
				$search = "$var=" . env ( $var );
				$replace = "$var=" . $value;

				$config = $this->files->get ( $path );
				$this->files->put ( $path, str_replace ( $search, $replace, $config ) );
			}
		}
	}


	protected function updateLoadedConfig ( $data )
	{
		$name = $this->laravel [ 'config' ]->get ( 'database.default' );

		foreach ( $data as $var => $value )
		{
			$key = strtolower ( str_replace ( 'DB_', '', $var ) );
			$this->laravel [ 'config' ]->set ( "database.connections.$name.$key", $value );
		}
	}

}



