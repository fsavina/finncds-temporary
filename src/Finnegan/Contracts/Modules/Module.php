<?php
namespace Finnegan\Contracts\Modules;


use Illuminate\Contracts\Foundation\Application;


interface Module
{
	
	const DEFAULT_NAMESPACE                  = 'Finnegan\Foundation\Http\Controllers';
	
	const DEFAULT_CONTROLLER                 = 'BaseController';
	
	const DEFAULT_LOGIN_CONTROLLER           = 'Auth\\LoginController';
	
	const DEFAULT_PASSWORD_FORGOT_CONTROLLER = 'Auth\\ForgotPasswordController';
	
	const DEFAULT_PASSWORD_RESET_CONTROLLER  = 'Auth\\ResetPasswordController';


	/**
	 * Module constructor.
	 * @param string      $name
	 * @param Application $app
	 */
	public function __construct ( $name, Application $app );


	/**
	 * @return string
	 */
	public function name ();


	/**
	 * @param string $default
	 * @return string
	 */
	public function title ( $default = null );


	/**
	 * @param string $key
	 * @param mixed  $default
	 * @return mixed
	 */
	public function config ( $key, $default = false );


	/**
	 * @return boolean
	 */
	public function isSecure ();


	/**
	 * @param string $key
	 * @param string $glue
	 * @return string
	 */
	public function wrap ( $key, $glue = '_' );


	/**
	 * @param string $object
	 * @return boolean
	 */
	public function deny ( $object );


	/**
	 * @param string $object
	 * @return boolean
	 */
	public function allow ( $object );


	/**
	 * @param string $action
	 * @param array  $params
	 * @return string
	 */
	public function actionUrl ( $action, array $params = [ ] );


	/**
	 * @param string $action
	 * @return string
	 */
	public function wrapRoute ( $action );


	/**
	 * @return \Illuminate\Contracts\Foundation\Application
	 */
	public function getApplication ();


}