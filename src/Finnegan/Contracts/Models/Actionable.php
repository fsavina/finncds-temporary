<?php
namespace Finnegan\Contracts\Models;


interface Actionable
{

	public function actionUrl ( $action, array $params = [] );


	public function recordActionUrl ( $action, array $params = [] );

}