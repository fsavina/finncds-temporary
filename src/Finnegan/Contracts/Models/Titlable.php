<?php
namespace Finnegan\Contracts\Models;


interface Titlable
{

	/**
	 * @param string $key
	 * @param mixed  $default
	 * @return mixed
	 */
	public function config ( $key, $default = false );


	/**
	 * @return \Finnegan\Definitions\ModelDefinition
	 */
	public function definition ();
}