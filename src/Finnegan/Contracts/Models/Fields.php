<?php
namespace Finnegan\Contracts\Models;


interface Fields
{

	public function addCast ( $attribute, $cast );


	public function registerEvent ( $event, $callback );


	public function config ( $key, $default = false );


	public function hasAttribute ( $attribute );


	public function getAttribute ( $key );


	public function isDirty ( $attributes = null );


	public function getTable ();


	public function getKey ();


	public function addRelationField ( $name, $field );
}