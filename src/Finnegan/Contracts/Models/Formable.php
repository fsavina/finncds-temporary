<?php
namespace Finnegan\Contracts\Models;


interface Formable
{

	public function formConfig ();


	public function withSessionErrors ();
}