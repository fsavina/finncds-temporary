<?php
namespace Finnegan\Contracts\Models;


interface Migratable
{

	public function name ();


	public function getTable ();


	public function getKeyName ();


	public function getTableEngine ( $default = null );


	public function timestamps ();


	public function populateMigration ( $creator );


	public function populateMigrationOnSchemaReady ( $creator );


	public function config ( $key, $default = false );


	public function getMigrationName ();


}