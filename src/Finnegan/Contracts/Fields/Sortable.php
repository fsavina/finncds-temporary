<?php
namespace Finnegan\Contracts\Fields;


interface Sortable
{


	/**
	 * @return boolean
	 */
	public function isSortable ();

}