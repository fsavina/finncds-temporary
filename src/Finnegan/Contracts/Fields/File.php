<?php
namespace Finnegan\Contracts\Fields;


interface File
{
	
	public function url ( $filename = null );
	
	
	public function content ( $filename = null );
	
}


