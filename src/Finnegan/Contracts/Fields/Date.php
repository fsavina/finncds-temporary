<?php
namespace Finnegan\Contracts\Fields;


interface Date
{


	/**
	 * @return string
	 */
	public function mode ();


	/**
	 * @return bool
	 */
	public function dateMode ();


	/**
	 * @return bool
	 */
	public function timeMode ();
	
	
	/**
	 * @return string
	 */
	public function getFormFormat ();

}


