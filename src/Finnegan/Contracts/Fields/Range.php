<?php
namespace Finnegan\Contracts\Fields;


interface Range
{


	public function min ( $default = 0 );


	public function max ( $default = 100 );


	public function step ( $default = 1 );


	public function precision ( $default = 0 );

}