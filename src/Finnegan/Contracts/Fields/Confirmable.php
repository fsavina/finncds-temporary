<?php
namespace Finnegan\Contracts\Fields;


interface Confirmable
{


	/**
	 * @return boolean
	 */
	public function confirm ();

}