<?php
namespace Finnegan\Contracts\Fields;


interface Suggestions
{


	public function runSuggest ();


	public function currentSuggestions ();


	public function makeSuggestions ( array $ids );


	public function limit ( $default = false );


	public function allowInsert ( $default = false );

}