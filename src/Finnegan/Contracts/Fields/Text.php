<?php
namespace Finnegan\Contracts\Fields;


interface Text
{


	/**
	 * @return integer
	 */
	public function size ();

}