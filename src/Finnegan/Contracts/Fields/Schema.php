<?php
namespace Finnegan\Contracts\Fields;


interface Schema
{


	public function populateMigration ();


	public function populateOnSchemaReadyMigration ();

}