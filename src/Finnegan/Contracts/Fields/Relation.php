<?php
namespace Finnegan\Contracts\Fields;


interface Relation
{


	public function relationName ();


	/**
	 * @param boolean $forceReload
	 * @return \Finnegan\Models\Relations\BelongsToMany
	 */
	public function relation ( $forceReload = false );

}