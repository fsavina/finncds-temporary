<?php
namespace Finnegan\Contracts\Fields;


interface Image
{
	
	
	public function url ( $filename = null, $width = null, $height = null, $options = null );
	
	
	public function originalUrl ( $filename = null );
}