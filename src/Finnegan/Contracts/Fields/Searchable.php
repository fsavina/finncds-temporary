<?php
namespace Finnegan\Contracts\Fields;


use Finnegan\Contracts\Models\Driver;


interface Searchable
{


	const TEXT_SEARCH_TYPE = 'String';


	public function searchType ();


	public function search ( Driver $builder, $value );

}