<?php
namespace Finnegan\Contracts\Layout;


interface AssetManager
{

	const DEFAULT_CONTEXT = 'body';

	/**
	 * @param string|array $resource
	 * @param array        $params
	 * @return AssetManager
	 */
	public function requireResource ( $resource, $params = [ ] );


	/**
	 * @param string $context
	 * @return string
	 */
	public function render ( $context = self :: DEFAULT_CONTEXT );
}