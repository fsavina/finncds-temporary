<?php
namespace Finnegan\Contracts\Layout;


interface IconPresenter
{
	
	
	/**
	 * @param string $type
	 * @return string
	 */
	public function resolve ( $type );
	
	
	public function icon ( $icon );
	
}