<?php
namespace Finnegan\Contracts\Layout\Components;


interface Accordion extends Tabs
{


	/**
	 * @param $boolean
	 * @return Accordion
	 */
	public function allowAllClosed ( $boolean );


	/**
	 * @param $boolean
	 * @return Accordion
	 */
	public function multiExpand ( $boolean );

}