<?php
namespace Finnegan\Contracts\Layout\Components;


interface Tabs extends Component
{


	public function __construct ( $id = null );


	public function add ( $label, $content, $id = null, $active = false );


	public function id ( $id );


	public function render ();

}