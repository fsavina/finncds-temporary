<?php
namespace Finnegan\Contracts\Layout\Components;


interface Breadcrumbs extends Component
{
	
	
	public function __construct ( $items = [ ] );
	
	
	public function add ( $url, $caption = null );
	
	
	public function render ();
	
}