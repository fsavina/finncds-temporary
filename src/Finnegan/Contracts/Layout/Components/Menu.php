<?php
namespace Finnegan\Contracts\Layout\Components;


interface Menu extends Component
{


	/**
	 * Menu constructor.
	 * @param mixed $menu
	 */
	public function __construct ( $menu );


	/**
	 * @return Menu
	 */
	public function add ( array $item );


	/**
	 * @return Menu
	 */
	public function dropdown ();


	/**
	 * @return Menu
	 */
	public function drilldown ();


	/**
	 * @return Menu
	 */
	public function accordion ();


	/**
	 * @return Menu
	 */
	public function vertical ();

}