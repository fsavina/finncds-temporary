<?php
namespace Finnegan\Contracts\Layout\Components;


interface Component
{


	public function __toString ();

}