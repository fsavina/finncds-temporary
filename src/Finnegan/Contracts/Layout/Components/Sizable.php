<?php
namespace Finnegan\Contracts\Layout\Components;


interface Sizable
{
	
	/**
	 * @param int|string $width
	 * @return Sizable
	 */
	public function width ( $width );
	
	
	/**
	 * @param int|string $height
	 * @return Sizable
	 */
	public function height ( $height );
	
	
	/**
	 * @param int|string $width
	 * @param int|string $height
	 * @return Sizable
	 */
	public function size ( $width, $height );
	
}