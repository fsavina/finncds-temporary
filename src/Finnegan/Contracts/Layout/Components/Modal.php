<?php
namespace Finnegan\Contracts\Layout\Components;


interface Modal extends Component
{
	
	
	public function __construct ( $content, $anchor = null );
	
	
	/**
	 * @param string $id
	 * @return Modal
	 */
	public function id ( $id );
	
	
	/**
	 * @param string $content
	 * @return Modal
	 */
	public function content ( $content );
	
	
	/**
	 * @param string $anchor
	 * @return Modal
	 */
	public function anchor ( $anchor );
	
	
	/**
	 * @param bool $bool
	 * @return Modal
	 */
	public function showCloseButton ( $bool );
	
}