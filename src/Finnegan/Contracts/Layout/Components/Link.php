<?php
namespace Finnegan\Contracts\Layout\Components;


use Finnegan\Contracts\Models\Actionable;


interface Link extends Component
{


	public function __construct ( $caption = '', $url = '' );


	public function caption ( $caption );


	public function getCaption ();


	public function icon ( $icon );


	public function url ( $url );


	public function confirm ( $message );


	public function addClass ( $class );


	public function action ( $action );


	public function modelAction ( Actionable $model, $action );


	public function removeClass ( $class );


	public function renderIf ( $condition );

}