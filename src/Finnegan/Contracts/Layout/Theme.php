<?php
namespace Finnegan\Contracts\Layout;


interface Theme
{
	
	public function boot ();
	
	
	/**
	 * @return string
	 */
	public function getPresenterClass ();
	
	
	/**
	 * @return string
	 */
	public function getIconPresenterClass ();
	
	
	/**
	 * @return Presenter
	 */
	public function presenter ();
	
	
	/**
	 * @param string $view
	 * @param array  $params
	 * @return \Illuminate\Contracts\View\View
	 */
	public function view ( $view, $params = [ ] );
	
	
	/**
	 * @param string $view
	 * @return bool
	 */
	public function exists ( $view );
	
	
	/**
	 * @param string $layout
	 * @return string
	 */
	public function layout ( $layout );
	
	
	/**
	 * @param string $partial
	 * @return string
	 */
	public function partial ( $partial );
	
}