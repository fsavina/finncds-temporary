<?php
namespace Finnegan\Contracts\Layout;


use Finnegan\Contracts\Models\Titlable;
use Illuminate\Contracts\Pagination\Paginator;


interface Presenter
{
	
	
	/**
	 * Presenter constructor.
	 * @param IconPresenter $iconPresenter
	 */
	public function __construct ( IconPresenter $iconPresenter );
	
	
	/**
	 * @return IconPresenter
	 */
	public function icons ();
	
	
	/**
	 * @param string $message
	 * @param string $class
	 * @return string
	 */
	public function message ( $message, $class = '' );
	
	
	/**
	 * @param Titlable $model
	 * @param string   $context
	 * @param bool     $plain
	 * @return string
	 */
	public function title ( Titlable $model, $context = null, $plain = false );
	
	
	/**
	 * @param string $caption
	 * @param string $url
	 * @return Components\Link
	 */
	public function link ( $caption = '', $url = '' );
	
	
	/**
	 * @param string $caption
	 * @param string $url
	 * @return Components\Button
	 */
	public function button ( $caption = '', $url = '' );
	
	
	/**
	 * @param Paginator $paginator
	 * @return mixed
	 */
	public function pagination ( Paginator $paginator );
	
	
	/**
	 * @param string $id
	 * @return Components\Tabs
	 */
	public function tabs ( $id = null );
	
	
	/**
	 * @param string $id
	 * @return Components\Tabs
	 */
	public function accordion ( $id = null );
	
	
	/**
	 * @param array $items
	 * @return Components\Breadcrumbs
	 */
	public function breadcrumbs ( array $items = [ ] );
	
	
	/**
	 * @param mixed $embed
	 * @return Components\Sizable
	 */
	public function videoEmbed ( $embed = null );
	
	
	/**
	 * @param null $menu
	 * @return Components\Menu
	 */
	public function menu ( $menu = null );
	
	
	/**
	 * @param string $content
	 * @param string $anchor
	 * @return Components\Modal
	 */
	public function modal ( $content, $anchor = null );
	
	
	public function icon ( $icon );
	
	
	public function renderView ( $name, $data = [ ] );
	
}