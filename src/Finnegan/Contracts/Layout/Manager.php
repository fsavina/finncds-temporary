<?php
namespace Finnegan\Contracts\Layout;


interface Manager
{
	
	
	/**
	 * @return \Finnegan\Contracts\Layout\Presenter
	 */
	public function presenter ();
	
	
	/**
	 * @return \Finnegan\Contracts\Layout\AssetManager
	 */
	public function assetManager ();
	
	
	/**
	 * @return \Collective\Html\HtmlBuilder
	 */
	public function html ();
	
	
	/**
	 * @return \Collective\Html\FormBuilder
	 */
	public function form ();
	
	
	/**
	 * @param string $name
	 * @return \Finnegan\Layout\Menus\ModuleMenu
	 */
	public function menu ( $name = 'primary' );
	
	
	/**
	 * @return \Illuminate\View\Factory
	 */
	public function views ();
	
	
	/**
	 * @return Theme
	 */
	public function theme ();
	
	
}