<?php
namespace Finnegan\Contracts\Definitions;


use Finnegan\Definitions\Definition;


interface Validator
{
	
	public function check ( Definition $definition, array $params = [] );
	
}