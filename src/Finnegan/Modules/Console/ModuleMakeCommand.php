<?php
namespace Finnegan\Modules\Console;


use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;


class ModuleMakeCommand extends Command
{

	protected $signature = 'finnegan:make-module
							{name : The name of the module}
							{--crud : Whether the module requires is CRUD or not}}
							{--secure : Whether the module requires authentication}}
							{--role= : The minimum user role to access the module}';

	protected $description = 'Create a new module.';


	/**
	 * The filesystem instance.
	 * @var \Illuminate\Filesystem\Filesystem
	 */
	protected $files;


	public function __construct ( Filesystem $files )
	{
		parent::__construct();
		$this->files = $files;
	}


	public function fire (  )
	{
		$name = $this->parseName($this->getNameInput());

		$path = $this->getPath($name);

		if ( $this->alreadyExists ( $path ) )
		{
			$this->error ( 'Module already exists!' );
			return false;
		}

		$this->makeDirectory ( $path );
		$this->buildConfig ( $name, $path );

		$this->info('Module created successfully.');
		return true;
	}


	protected function getNameInput ()
	{
		return $this->argument ( 'name' );
	}


	protected function parseName ( $name )
	{
		return str_replace ( [ '_', ' ' ], '-', strtolower ( $name ) );
	}


	protected function getPath($name)
	{
		return $this->laravel[ 'path.config' ] . "/finnegan/modules/{$name}/";
	}


	protected function alreadyExists ( $path )
	{
		return $this->files->exists ( $path );
	}


	protected function makeDirectory ( $path )
	{
		if ( ! $this->files->isDirectory ( $path ) )
		{
			$this->files->makeDirectory ( $path, 0777, true, true );
		}
	}


	protected function buildConfig ( $name, $path )
	{
		$replace = [
			':title'  => Str::studly ( $name ),
			':prefix' => $name,
			':crud'   => $this->option ( 'crud' ) ? 'true' : 'false',
			':secure' => $this->option ( 'secure' ) ? 'true' : 'false',
			':role'   => ( $this->option ( 'secure' ) and ( $role = $this->option ( 'role' ) ) ) ? $role : 'admin'
		];

		foreach ( $this->files->files ( __DIR__ . '/stubs' ) as $file )
		{
			$stub = $this->files->get ( $file );
			$filename = $this->files->name ( $file );

			$stub = strtr ( $stub, $replace );

			$this->files->put ( "{$path}/{$filename}.php", $stub );
		}
	}


}




