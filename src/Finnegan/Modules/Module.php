<?php
namespace Finnegan\Modules;


use Finnegan\Contracts\Modules\Module as ModuleContract;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Str;


class Module implements ModuleContract
{

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var \Illuminate\Contracts\Foundation\Application
	 */
	protected $app;

	/**
	 * @var \Illuminate\Contracts\Config\Repository
	 */
	protected $config;


	/**
	 * Module constructor.
	 * @param string      $name
	 * @param Application $app
	 */
	public function __construct ( $name, Application $app )
	{
		$this->name = $name;
		$this->app = $app;
		$this->config = $app[ 'config' ];
	}
	
	
	/**
	 * Get the configuration value.
	 * @param string $key
	 * @param mixed  $default
	 * @return mixed
	 */
	public function config ( $key, $default = false )
	{
		return $this->config->get ( "finnegan.modules.{$this->name}.{$key}", $default );
	}


	/**
	 * Get the current module name.
	 * @return string
	 */
	public function name ()
	{
		return $this -> name;
	}
	
	
	/**
	 * Get the module title
	 * @param string $default
	 * @return string
	 */
	public function title ( $default = null )
	{
		$default = $default ?: Str :: studly ( $this -> name );
		return $this -> config ( 'module.title', $default );
	}
	
	
	/**
	 * Check if the module is secure
	 * @return boolean
	 */
	public function isSecure ()
	{
		return ( bool ) $this -> config ( 'auth.secure' );
	}


	/**
	 * Wrap the given string with the module name
	 * @param string $key
	 * @param string $glue
	 * @return string
	 */
	public function wrap ( $key, $glue = '_' )
	{
		return $this -> name . $glue . $key;
	}


	/**
	 * Check if the access to the given object is allowed for the current model
	 * @param string $object
	 * @return boolean
	 */
	public function allow ( $object )
	{
		return ! $this -> deny ( $object );
	}


	/**
	 * Check if the access to the given object is denied for the current model
	 * @param string $object
	 * @return boolean
	 */
	public function deny ( $object )
	{
		$only = $this -> config ( 'module.definitions.only' );
		if ( is_array ( $only ) and ! in_array ( $object, $only ) )
		{
			return true;
		}
		return in_array ( $object, ( array ) $this -> config ( 'module.definitions.exclude', [ ] ) );
	}


	/**
	 * Build the url for the given action
	 * @param string $action
	 * @param array  $params
	 * @return string
	 */
	public function actionUrl ( $action, array $params = [] )
	{
		$route = $this -> wrapRoute ( $action );

		if ( ! $this->app[ 'router' ]->has ( $route ) )
		{
			throw new \InvalidArgumentException( "Unknown route '$route'" );
		}

		return $this->app[ 'url' ]->route ( $route, $params );
	}


	/**
	 * @param $action
	 * @return string
	 */
	public function wrapRoute ( $action )
	{
		return $this -> wrap ( $action, '.' );
	}


	/**
	 * @return \Illuminate\Contracts\Foundation\Application
	 */
	public function getApplication ()
	{
		return $this->app;
	}
	
	
	public function __call ( $method, $parameters )
	{
		return call_user_func_array ( [ $this->app, $method ], $parameters );
	}
	
}





