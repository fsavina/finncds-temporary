<?php
namespace Finnegan\Modules;


use Finnegan\Contracts\Config\ConfigurationException;
use Finnegan\Contracts\Modules\Module as ModuleContract;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;


class ModulesManager
{
	
	/**
	 * @var Application
	 */
	protected $app;
	
	protected $modules = [ ];
	
	
	/**
	 * ModulesManager constructor.
	 * @param Application $app
	 */
	public function __construct ( Application $app )
	{
		$this->app = $app;
		
		if ( $app[ 'config' ]->has ( 'finnegan.modules' ) )
		{
			foreach ( $app[ 'config' ]->get ( 'finnegan.modules' ) as $module => $moduleConfig )
			{
				$this->register ( new Module ( $module, $this->app ) );
			}
		}
	}
	
	
	/**
	 * @return array
	 */
	public function modules ()
	{
		return Arr::except ( $this->modules, '*' );
	}
	
	
	/**
	 * @param string $name
	 * @return boolean
	 */
	public function exists ( $name )
	{
		return array_key_exists ( $name, $this->modules );
	}
	
	
	/**
	 * @param string $name
	 * @return ModuleContract
	 */
	public function get ( $name )
	{
		return Arr::get ( $this->modules, $name );
	}
	
	
	/**
	 * @param ModuleContract $module
	 * @return ModulesManager
	 */
	protected function register ( ModuleContract $module )
	{
		$this->modules[ $module->name () ] = $module;
		
		if ( $module->config ( 'module.default' ) )
		{
			$this->modules[ '*' ] = $module;
		}
		return $this;
	}
	
	
	/**
	 * @param string $prefix
	 * @return string
	 */
	public function resolvePrefix ( $prefix )
	{
		foreach ( $this->modules as $module )
		{
			if ( $module->config ( 'routing.prefix' ) == $prefix )
			{
				return $module->name ();
			}
		}
		return '*';
	}


	/**
	 * @param array   $pars
	 * @param Request $request
	 * @return ModuleContract
	 */
	public function resolve ( array $pars = [ ], Request $request = null )
	{
		$name = '';
		if ( isset( $pars[ 'name' ] ) )
		{
			$name = $pars[ 'name' ];
		} elseif ( $request and is_object ( $route = $request->route () ) )
		{
			$name = $this->resolvePrefix ( $route->getPrefix () );
		} elseif ( strlen ( $path = $request->path () ) )
		{
			$parts = explode ( '/', $path );
			$name = $this->resolvePrefix ( $parts[ 0 ] );
		}
		
		if ( ! strlen ( $name ) or ! $this->exists ( $name ) )
		{
			$name = '*';
		}

		$module = $this->get ( $name );
		if ( ( $locale = $module->config ( 'module.locale' ) ) and ( ! $this->app->isLocale ( $locale ) ) )
		{
			$this->app->setLocale ( $locale );
		}
		return $module;
	}
}