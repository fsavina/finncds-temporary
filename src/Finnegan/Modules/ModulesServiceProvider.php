<?php
namespace Finnegan\Modules;


use Finnegan\Contracts\Modules\Module as ModuleContract;
use Finnegan\Support\BaseServiceProvider;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Events\Dispatcher;
use Illuminate\Routing\Events\RouteMatched;


class ModulesServiceProvider extends BaseServiceProvider
{

	protected $defer = true;


	public function register ()
	{
		$this->shouldCompile ();
		
		$this->registerBindings ();

		$this->registerListeners ( $this->app[ 'events' ] );
		
		$this->commands ( Console\ModuleMakeCommand::class );
	}


	protected function registerBindings ()
	{
		$this->app->singleton ( 'modules.manager', ModulesManager::class );

		$this->app->singleton ( ModuleContract::class, function ( Application $app, $pars )
		{
			return $app[ 'modules.manager' ]->resolve ( $pars, $app[ 'request' ] );
		} );
	}


	protected function registerListeners ( Dispatcher $events )
	{
		$events->listen ( 'Illuminate\Routing\Events\RouteMatched', function ( RouteMatched $event )
		{
			$manager = $this->app[ 'modules.manager' ];

			$pars = [ 'name' => $manager->resolvePrefix ( $event->route->getPrefix () ) ];

			if ( $this->app->bound ( ModuleContract::class ) )
			{
				if ( $this->app->make ( ModuleContract::class )->name () == $pars[ 'name' ] )
				{
					// The bounded module is the right one, just wanted to be sure
					return;
				}
			}

			// The module is not bound yet or a wrong module has been bound before the route was resolved
			$module = $manager->resolve ( $pars, $event->request );

			$this->app->instance ( ModuleContract::class, $module );
		} );
	}


	public static function compiles ()
	{
		return [
			realpath ( __DIR__ . '/Module.php' ),
			realpath ( __DIR__ . '/ModulesManager.php' ),
		];
	}


	public function provides ()
	{
		return [ ModuleContract::class, 'modules.manager' ];
	}
}