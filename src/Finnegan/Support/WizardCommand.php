<?php
namespace Finnegan\Support;


use Illuminate\Console\Command;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;


abstract class WizardCommand extends Command
{

	/**
	 * @var \Illuminate\Validation\Validator
	 */
	protected $validator;

	protected $questions = [ ];


	public function __construct ( ValidationFactory $validator )
	{
		parent::__construct ();
		$this->validator = $validator->make ( [ ], [ ] );
	}


	protected function getInputData ( array $rules = null )
	{
		$output = [ ];

		foreach ( $this->questions as $name => $question )
		{
			$question = is_array ( $question ) ? $question : [ 'text' => $question ];

			if ( is_array ( $rules ) )
			{
				$this->validator->setRules ( Arr::only ( $rules, $name ) );
			} else
			{
				$this->validator->setRules ( [ $name => Arr::get ( $question, 'rules', [] ) ] );
			}

			do
			{
				$data = $this->promptQuestion ( $name, $question );

				$this->validator->setData ( $data );

				if ( empty( $data[ $name ] ) and $this->allowsEmptyValue ( $question ) )
				{
					$fails = false;
				} else
				{
					if ( $fails = $this->validator->fails () )
					{
						foreach ( $this->validator->errors ()->all () as $error )
						{
							$this->error ( $error );
						}
					}
				}
			} while ( $fails );

			foreach ( $data as $key => $value )
			{
				Arr::set ( $output, $key, $value );
			}
		}

		return $output;
	}


	protected function promptQuestion ( $name, $question )
	{
		$secret = Arr::get ( $question, 'secret', false );

		$default = $this->getDefaultValue ( $name, $question );

		if ( isset( $question[ 'choices' ] ) )
		{
			$multiple = Arr::get ( $question, 'multiple' );
			$data = [ $name => $this->choice ( $question [ 'text' ], $question [ 'choices' ], $default, null, $multiple ) ];
		} else
		{
			$data = [ $name => $this->askQuestion ( $question [ 'text' ], $secret, $default ) ];
		}

		if ( in_array ( 'confirmed', Arr::get ( $this->validator->getRules (), $name ) ) )
		{
			$confirm = Arr::get ( $question, 'confirm', 'Confirm ' . Str::studly ( $name ) );
			$data [ "{$name}_confirmation" ] = $this->askQuestion ( $confirm, $secret );
		}
		return $data;
	}


	protected function getDefaultValue ( $name, $question )
	{
		return Arr::get ( $question, 'default' );
	}


	protected function askQuestion ( $question, $secret = false, $default = null )
	{
		return $secret ? $this->secret ( $question ) : $this->ask ( $question, $default );
	}


	protected function allowsEmptyValue ( $question )
	{
		return Arr::get ( $question, 'allow_empty', false );
	}

}