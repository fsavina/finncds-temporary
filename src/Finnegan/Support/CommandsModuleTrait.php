<?php
namespace Finnegan\Support;


use Finnegan\Contracts\Modules\Module;


trait CommandsModuleTrait
{

	/**
	 * @var \Finnegan\Contracts\Modules\Module
	 */
	protected $module;


	protected function getModule ()
	{
		if ( is_null ( $this->module ) )
		{
			$this->module = $this->laravel->make ( Module::class, [ 'name' => 'admin' ] );
		}
		return $this->module;
	}

}