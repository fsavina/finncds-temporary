<?php
namespace Finnegan\Support;


use Illuminate\Support\Fluent;


/**
 * @property boolean unguarded
 * @property boolean unverified
 */
class LoadingContext extends Fluent
{
	
}