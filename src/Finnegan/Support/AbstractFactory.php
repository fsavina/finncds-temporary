<?php
namespace Finnegan\Support;


use Finnegan\Contracts\Modules\Module;


abstract class AbstractFactory
{

	
	/**
	 * The Module instance
	 * @var Module
	 */
	protected $module;

	/**
	 * The definition registry
	 * @var array
	 */
	protected $registry = [ ];


	/**
	 * @param Module $module
	 */
	public function __construct ( Module $module )
	{
		$this -> module = $module;
	}


	public function config ( $key, $default = null )
	{
		return $this->module->config ( $key, $default );
	}
	
	
	abstract public function load ( $mixed );

}


