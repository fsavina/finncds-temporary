<?php
namespace Finnegan\Support\Facades;


use Illuminate\Support\Facades\Facade;


/**
 * @see \Finnegan\Models\ModelsFactory
 */
class Models extends Facade
{

	/**
	 * Get the registered name of the component.
	 * @return string
	 */
	protected static function getFacadeAccessor ()
	{
		return 'models.factory';
	}
}
