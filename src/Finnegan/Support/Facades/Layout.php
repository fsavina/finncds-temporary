<?php
namespace Finnegan\Support\Facades;


use Illuminate\Support\Facades\Facade;


/**
 * @see \Finnegan\Layout\Manager
 */
class Layout extends Facade
{

	/**
	 * Get the registered name of the component.
	 * @return string
	 */
	protected static function getFacadeAccessor ()
	{
		return 'Finnegan\\Contracts\\Layout\\Manager';
	}
}
