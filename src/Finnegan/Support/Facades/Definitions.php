<?php
namespace Finnegan\Support\Facades;


use Illuminate\Support\Facades\Facade;


/**
 * @see \Finnegan\Definitions\DefinitionsFactory
 */
class Definitions extends Facade
{

	/**
	 * Get the registered name of the component.
	 * @return string
	 */
	protected static function getFacadeAccessor ()
	{
		return 'definitions.factory';
	}
}
