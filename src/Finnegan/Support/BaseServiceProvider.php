<?php
namespace Finnegan\Support;


use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;


abstract class BaseServiceProvider extends IlluminateServiceProvider
{

	protected function shouldCompile ()
	{
		$this->app[ 'config' ]->push ( 'compile.providers', get_class ( $this ) );
		return $this;
	}
	
	
	public static function sourcePath ()
	{
		return realpath ( __DIR__ . '/../../' ) . DIRECTORY_SEPARATOR;
	}
	
	
	public static function resourcesPath ()
	{
		return static::sourcePath () . 'resources/';
	}

}




