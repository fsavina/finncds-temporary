<?php

namespace Finnegan\Translation;


use Finnegan\Support\BaseServiceProvider;
use Illuminate\Translation\TranslationServiceProvider as LaravelTranslationServiceProvider;


class TranslationServiceProvider extends LaravelTranslationServiceProvider
{
	
	
	protected function registerLoader ()
	{
		$this->app->singleton ( 'translation.loader', function ( $app )
		{
			return new TranslationLoader( $app[ 'files' ], $app[ 'path.lang' ] );
		} );
	}
	
	
	public function boot ()
	{
		// Load core translations
		$this->loadTranslationsFrom ( BaseServiceProvider::resourcesPath () . 'lang', 'finnegan' );
	}
	
}


