<?php
namespace Finnegan\Translation;


use Illuminate\Config\Repository as ConfigContract;
use Illuminate\Contracts\Foundation\Application;


class TranslationsManager
{
	
	const STORAGE_PATH = 'app/finnegan/translations';
	
	/**
	 * @var Application
	 */
	protected $app;
	
	/**
	 * @var ConfigContract
	 */
	protected $config;
	
	
	public function __construct ( Application $app )
	{
		$this->app = $app;
		$this->config = $app[ 'config' ];
	}
	
	
	public function languages ()
	{
		return ( array ) $this->config->get ( 'finnegan.translations.languages' );
	}
	
	
	public function path ()
	{
		return $this->app[ 'path.storage' ] . '/' . static::STORAGE_PATH;
	}
	
}