<?php

namespace Finnegan\Translation;


use Illuminate\Support\Facades\Cache;
use Illuminate\Translation\FileLoader;


class TranslationLoader extends FileLoader
{
	
	public function load ( $locale, $group, $namespace = null )
	{
		if ( $namespace === 'finnegan.custom' )
		{
			return $this->loadFromDatabase ( $locale, $group );
		}
		return parent::load ( $locale, $group, $namespace );
	}
	
	
	protected function loadFromDatabase ( $locale, $group )
	{
		return Cache::remember (
			"locale.fragments.{$locale}.{$group}",
			config ( 'finnegan.translations.cache', 60 ),
			function () use ( $group, $locale )
			{
				return app ( 'models.factory' )->unguarded ( 'translations' )
											   ->withTranslation ()
											   ->select ( 'id', 'code', 'namespace' )
											   ->where ( 'namespace', $group )
											   ->get ()
											   ->map ( function ( $translation ) use ( $locale )
											   {
												   $key = $translation->code;
					
												   $text = $translation->{"translation:$locale"};
					
												   return compact ( 'key', 'text' );
											   } )
											   ->pluck ( 'text', 'key' )
											   ->toArray ();
			}
		);
	}
}