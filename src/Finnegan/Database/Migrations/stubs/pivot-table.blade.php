$table->unsignedInteger ( '{{ $relation->getForeignKeyRaw () }}' );
$table->unsignedInteger ( '{{ $relation->getOtherKeyRaw () }}' );

$table->primary ( [ '{{ $relation->getForeignKeyRaw () }}', '{{ $relation->getOtherKeyRaw () }}' ], 'primary_index' );

$table->foreign('{{ $relation->getForeignKeyRaw () }}')
    ->references ( '{{ $relation->getParent ()->getKeyName () }}' )
    ->on ( '{{ $relation->getParent ()->getTable () }}' )
    ->onDelete('cascade');

$table->foreign('{{ $relation->getOtherKeyRaw () }}')
    ->references ( '{{ $relation->getRelated ()->getKeyName () }}' )
    ->on ( '{{ $relation->getRelated ()->getTable () }}' )
    ->onDelete('cascade');

@if ( $relation->hasExtraFields () )
    @foreach( $relation->newPivot ()->fields() as $field )
        @if ( $field->config ( 'schema', true ) )
            {!! $field->populateMigration() !!}
        @endif
    @endforeach
@endif