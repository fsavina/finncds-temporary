$table->integer ( '{{ $parent->getForeignKey () }}', false, true );
$table->char ( 'locale', 2 );

$table->unique ( [ '{{ $parent->getForeignKey () }}', 'locale' ], $table->getTable () . '_unique' );
$table->index ( 'locale' );

$table->foreign ( '{{ $parent->getForeignKey () }}', $table->getTable () . '_foreign' )
    ->references ( '{{ $parent->getKeyName () }}' )
    ->on ( '{{ $parent->getTable () }}' )
    ->onDelete ( 'cascade' );