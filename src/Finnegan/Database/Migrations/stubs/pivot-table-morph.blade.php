$table->unsignedInteger ( '{{ $relation->getOtherKeyRaw () }}' );

$table->foreign('{{ $relation->getOtherKeyRaw () }}')
    ->references ( '{{ $relation->getRelated ()->getKeyName () }}' )
    ->on ( '{{ $relation->getRelated ()->getTable () }}' )
    ->onDelete('cascade');

$table->morphs( '{{ $relation->getMorphName() }}' );