@if( ($key = $model->getKeyName()) == 'id' )
    $table -> increments ( 'id' );
@elseif (strlen($key))
    $table -> primary ( '{{ $key }}' );
@endif