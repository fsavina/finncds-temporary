<?php
namespace Finnegan\Database\Migrations;


use Finnegan\Contracts\Models\Migratable;
use Illuminate\Support\Str;


class MigrationCreator extends \Illuminate\Database\Migrations\MigrationCreator
{

	const DEFAULT_TABLE_ENGINE = 'InnoDB';

	/**
	 * @var \Finnegan\Contracts\Models\Migratable
	 */
	protected $model;


	/**
	 * @param \Finnegan\Contracts\Models\Migratable $model
	 * @return MigrationCreator
	 */
	public function setModel ( Migratable $model )
	{
		$this->model = $model;
		return $this;
	}


	protected function getModelDriver ( $default = 'database' )
	{
		return $this->model->config ( 'storage.driver', $default );
	}


	protected function getStub ( $table, $create )
	{
		$driver = $this->getModelDriver ();

		$stub = "model-create-{$driver}.stub";

		return $this->files->get ( $this->getStubPath () . "/{$stub}" );
	}


	public function getStubPath ()
	{
		return __DIR__ . '/stubs';
	}


	protected function populateStub ( $name, $stub, $table )
	{
		$stub = parent::populateStub ( $name, $stub, $table );

		$method = 'populateStub' . Str::studly ( $this->getModelDriver () );
		return $this->$method( $stub );
	}


	protected function populateStubDatabase ( $stub )
	{
		$stub = str_replace ( 'DummyEngine', $this->model->getTableEngine ( static::DEFAULT_TABLE_ENGINE ), $stub );

		$path = $this->getStubPath() . '/primary-key.blade.php';
		$primaryKey = view ()->file ( $path, [ 'model' => $this->model ] )->render ();
		$stub = str_replace ( '/* key */', $primaryKey, $stub );

		$stub = str_replace ( '/* mixed */', $this->model->populateMigration ( $this ), $stub );

		$timestamps = $this->model->timestamps () ? '$table->timestamps();' . PHP_EOL : '';
		$stub = str_replace ( '/* timestamps */', $timestamps, $stub );

		$stub = str_replace ( '/* onSchemaReady */', $this->model->populateMigrationOnSchemaReady ( $this ), $stub );

		return $stub;
	}


	protected function populateStubFilesystem ( $stub )
	{
		$stub = str_replace ( 'DummyPath', $this->model->definition()->getStorageFilePath (), $stub );
		
		$disk = $this->model->definition()->get ('storage.file.disk');
		$stub = str_replace ( 'DummyDisk', $disk ? "'$disk'" : 'null', $stub );

		return $stub;
	}


	public function renderViewStub ( $name, $data = [] )
	{
		$path = __DIR__ . "/stubs/{$name}.blade.php";
		return view ()->file ( $path, $data )->render ();
	}

}
