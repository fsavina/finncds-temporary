<?php
namespace Finnegan\Database\Console;


use Finnegan\Contracts\Fields\Relation;
use Finnegan\Contracts\Models\Migratable;
use Finnegan\Database\Migrations\MigrationCreator;
use Finnegan\Models\ModelsFactory;
use Finnegan\Support\CommandsModuleTrait;
use Illuminate\Support\Composer;


class MigrateMakeCommand extends \Illuminate\Database\Console\Migrations\MigrateMakeCommand
{

	use CommandsModuleTrait;

	protected $signature   = 'finnegan:make-migration
	                          {name : The name of the model to be migrated.}
                              {--path= : The location where the migration file should be created.}
                              {--migrate : Run the migrate command after the creation}';

	protected $description = 'Create a new model migration file';


	public function __construct ( MigrationCreator $creator, Composer $composer )
	{
		parent::__construct ( $creator, $composer );
	}


	public function fire ()
	{
		$model = $this->loadModel ( $this->getInputModel () );

		$this->writeModelMigration ( $model );

		$this->writeRelationsMigration ( $model );

		$this->writeExtraTablesMigration ( $model );

		$this->composer->dumpAutoloads ();

		if ( $this->option ( 'migrate' ) )
		{
			$this->call ( 'migrate' );
		}
	}


	protected function getInputModel ()
	{
		return $this->input->getArgument ( 'name' );
	}


	protected function loadModel ( $name )
	{
		return ( new ModelsFactory( $this->getModule () ) )->forceLoad ( $name );
	}


	protected function writeModelMigration ( Migratable $model )
	{
		$path = $this->getMigrationPath ();

		$name = $model->getMigrationName ();
		$table = $create = $model->getTable ();

		$this->creator->setModel ( $model );

		$file = pathinfo ( $this->creator->create ( $name, $path, $table, $create ), PATHINFO_FILENAME );

		$this->line ( "<info>Created Migration:</info> $file" );
	}


	protected function writeRelationsMigration ( $model )
	{
		$relations = array_keys ( $model->config ( 'relations', [ ] ) );
		if ( count ( $relations ) )
		{
			// We wait a little bit to be sure that the migrations are created in the right order
			// In the end, what is a second compared to eternity?
			sleep ( 1 );
			foreach ( $relations as $relation )
			{
				$this->writeModelMigration ( $model->relation ( $relation ) );
			}
		}

		$relationFields = $model->fields()->ifInstanceOf ( Relation::class );
		if ( count ( $relationFields ) )
		{
			// We wait a little bit to be sure that the migrations are created in the right order
			// In the end, what is one second if compared to the eternity?
			sleep ( 1 );
			foreach ( $relationFields as $field )
			{
				$this->writeModelMigration ( $field->relation () );
			}
		}
	}


	protected function writeExtraTablesMigration ( Migratable $model )
	{
		if ( $model instanceof \Finnegan\Models\TranslatableFields )
		{
			// We wait a little bit to be sure that the migrations are created in the right order
			// In the end, what is one second if compared to the eternity?
			sleep ( 1 );
			$this->writeModelMigration ( $model->makeTranslationModel () );
		}
	}
}