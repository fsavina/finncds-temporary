<?php
namespace Finnegan\Database;


use Illuminate\Support\ServiceProvider;


class DatabaseServiceProvider extends ServiceProvider
{
	
	protected $defer = true;
	
	
	/**
	 * Bootstrap the application events.
	 * @return void
	 */
	public function boot ()
	{
	}
	
	
	/**
	 * Register the service provider.
	 * @return void
	 */
	public function register ()
	{
		$this->commands ( Console\MigrateMakeCommand::class );
	}
	
	
}
