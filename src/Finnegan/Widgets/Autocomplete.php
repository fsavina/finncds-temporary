<?php
namespace Finnegan\Widgets;


/**
 * @link http://loopj.com/jquery-tokeninput/
 */
class Autocomplete extends Input
{

	protected $contracts = [
		\Finnegan\Contracts\Fields\Suggestions::class
	];

	protected $assets = [
		'jquery.plugins.tokeninput'
	];
	

	protected function inputType ()
	{
		return 'text';
	}


	public function value ()
	{
		$value = parent :: value ();
		return is_array ( $value ) ? implode ( ',', $value ) : $value;
	}
	
	
	protected function attributes ( $except = [] )
	{
		$attributes = parent::attributes ( $except );
		if ( is_object ( $this->field ) )
		{
			$attributes [ 'data-suggestions-url' ] = $this->field->actionUrl ( 'suggest' );
		}
		return $attributes;
	}


	protected function limit ( $default = null )
	{
		if ( is_object ( $this -> field ) )
		{
			$default = $this -> field -> limit ( $default );
		}
		return $this -> config ( 'limit', $default );
	}


	protected function allowInsert ( $default = false )
	{
		if ( is_object ( $this -> field ) )
		{
			return $this -> field -> allowInsert ( $default );
		}
		return $this -> config ( 'insert', $default );
	}


	public function renderJs ()
	{
		$value = null;
		if ( is_object ( $this -> field ) )
		{
			if ( $this -> context() == static :: CONTEXT_FILTERS )
			{
				$filterBy = $this -> field -> model () -> getFilterBy ();
				$value = data_get ( $filterBy, $this -> field -> name () );
				$value = strlen ( $value ) ? $this -> field -> makeSuggestions ( explode ( ',', $value ) ) : [];
			} else
			{
				$value = $this -> field -> currentSuggestions ();
			}
		}

		return $this -> requireWidgetJs([
			'id' => $this -> id (),
			'config' => [
				'limit' => $this -> limit (),
				'value' => $value,
				'allowInsert' => ( $this -> context() != static :: CONTEXT_FILTERS ) ? $this -> allowInsert() : false
			]
		]);
	}

}



