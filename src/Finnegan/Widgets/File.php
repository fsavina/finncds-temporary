<?php
namespace Finnegan\Widgets;


use Illuminate\Support\Str;


class File extends Input
{

	protected $contracts = [
		\Finnegan\Contracts\Fields\File::class
	];


	protected function inputType ()
	{
		return 'file';
	}


	protected function attributes ( $except = [] )
	{
		$except[] = 'maxlength';
		$attributes = parent :: attributes ( $except );

		if ( $this -> editMode () and $this -> value () )
		{
			$attributes [ 'disabled' ] = 'disabled';
		}
		
		$mimes = [];
		$rules = $this -> rules ();
		foreach ( $rules as $rule )
		{
			if ( Str :: startsWith ( $rule, 'mimes' ) )
			{
				$mimes = explode ( ',', substr ( $rule, 6 ) );
			} elseif ( $rule == 'image' )
			{
				$mimes = [ 'jpg', 'png', 'bmp', 'gif', 'svg' ];
				$attributes [ 'data-is-image' ] = 'true';
			}
		}
		
		if ( count ( $mimes ) )
		{
			$attributes [ 'accept' ] = '.' . implode ( ',.', $mimes );
		}
		
		return $attributes;
	}


	public function render ()
	{
		return $this->renderView ( [
		   'required'   => $this->required (),
		   'editMode'   => ( $this->editMode () and $this->value () )
	    ] );
	}


	public function renderJs ()
	{
		return $this -> requireWidgetJs([
			'id'       => $this->id (),
			'required' => $this->required (),
			'editMode' => ( $this -> editMode () and $this -> value () )
		]);
	}


}


