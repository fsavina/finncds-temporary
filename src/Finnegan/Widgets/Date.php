<?php
namespace Finnegan\Widgets;


/**
 * http://xdsoft.net/jqplugins/datetimepicker/
 * http://html5doctor.com/using-modernizr-to-detect-html5-features-and-provide-fallbacks/
 */
class Date extends Input
{
	
	protected $contracts = [
		\Finnegan\Contracts\Fields\Date::class
	];

	protected $assets = [
		'jquery.plugins.datetimepicker'
	];


	protected function inputType ()
	{
		if ( $this -> config ( 'mode' ) == 'date' )
		{
			return 'date';
		}
		return self :: DEFAULT_TYPE;
	}
	
	
	public function render ()
	{
		if ( $this -> context () == static :: CONTEXT_FILTERS )
		{
			return $this -> renderRangeFilter ();
		}
		return parent :: render ();
	}


	/**
	 * Get the configuration options for the datepicker plugin
	 * @return array
	 */
	protected function datepickerOptions ()
	{
		return [
			'format'     => $this->config ( 'format' ),
			'lang'       => $this->config ( 'locale' ),
			'lazyInit'   => true,
			'datepicker' => $this->config ( 'dateMode' ),
			'timepicker' => $this->config ( 'timeMode' ),
			'step'       => $this->config ( 'step', 60 ),
			'minDate'    => $this->config ( 'minDate' ),
			'maxDate'    => $this->config ( 'maxDate' ),
			'formatDate' => $this->config ( 'dateFormat' ),
			'minTime'    => $this->config ( 'minTime' ),
			'maxTime'    => $this->config ( 'maxTime' ),
			'formatTime' => $this->config ( 'timeFormat' ),
		];
	}
	

	public function renderJs ()
	{
		$id = $this -> id ();
		if ( $this -> context () == static :: CONTEXT_FILTERS )
		{
			$id = [ $id . '_from', $id . '_to' ];
		}

		return $this -> requireWidgetJs([
			'id' => ( array ) $id,
			'mode' => $this -> config ( 'mode' ),
			'config' => $this -> datepickerOptions ()
		]);
	}
	
	
}



