<?php
namespace Finnegan\Widgets;


use Finnegan\Contracts\Fields\MultipleChoice;


class Checkbox extends Input
{


	protected $fieldContracts = [
		\Finnegan\Contracts\Fields\Options::class
	];


	protected function wrapFormElement ( $render, $attributes = [ ] )
	{
		$label = $this->label ();
		return $this->renderView ( compact ( 'render', 'attributes', 'label' ), 'checkbox-wrapper' );
	}


	public function render ()
	{
		if ( $this -> context () == static :: CONTEXT_FILTERS )
		{
			return $this -> renderFilter ();
		}

		$value = $this -> value ();

		$render = '';
		if ( $this -> isMultiple () )
		{
			$values = (array) $value;
			$options = $this -> options ();
			foreach ( $options as $key => $label )
			{
				$render .= $this -> renderOne (
					$this -> name () . '[]', $label, $key, in_array( $key, $values )
				);
			}
		} else
		{
			$value = ( ! is_null ( $value ) and (bool) $value );
			$render = $this -> renderOne ( $this -> name (), $this -> config ( 'description', '' ), 1, $value );
		}
		return $render;
	}
	
	
	protected function renderFilter ()
	{
		$filterBy = $this -> field -> model () -> getFilterBy ();

		return $this->renderView([
			'options' => $this->options (),
			'value'   => data_get ( $filterBy, $this -> name )
		], 'select');
	}
	
	
	/**
	 * Prepare the options array for the rendering
	 * @return array
	 */
	protected function options ()
	{
		$options = [ ];
		foreach ( $this -> field -> formOptions () as $key => $value )
		{
			$options [ $key ] = trans ( $value );
		}
		return $options;
	}
	
	
	/**
	 * Render one checkbox
	 * @param string $name
	 * @param string $label
	 * @param mixed $value
	 * @param boolean $checked
	 * @return string
	 */
	protected function renderOne ( $name, $label, $value, $checked )
	{
		if ( $this->config ( 'switch' ) and ! $this->isMultiple () )
		{
			$checkbox = $this->renderView ( compact ( 'name', 'value', 'checked' ) );
			return $this->renderView ( compact ( 'checkbox', 'label' ), 'checkbox-switch' );
		}
		return $this->renderView ( compact ( 'name', 'value', 'checked', 'label' ) );
	}
	
	
	protected function attributes ( $except = [] )
	{
		$isFilter = ( $this -> context() == static :: CONTEXT_FILTERS );

		$except[] = 'class';
		if ( $this -> isMultiple () or $isFilter )
		{
			$except[] = 'required';
		}
		$attributes = parent :: attributes ( $except );

		if ( $isFilter )
		{
			$attributes [ 'placeholder' ] = '----';
		}

		if (  $this -> config ( 'switch' ) and ! $this -> isMultiple () and !$isFilter )
		{
			$attributes [ 'class' ] = 'switch-input';
		}

		return $attributes;
	}


	protected function inputType ()
	{
		return 'checkbox';
	}
	
	
	/**
	 * Check if the field is a Set field
	 * @return boolean
	 */
	protected function isMultiple ()
	{
		return ( $this -> field instanceof MultipleChoice );
	}

}


