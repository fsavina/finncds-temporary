<?php
namespace Finnegan\Widgets;


class Password extends Input
{

	protected $contracts = [
		\Finnegan\Contracts\Fields\Password::class
	];


	protected function inputType ()
	{
		return 'password';
	}
	

	public function render ()
	{
		$render = parent :: render ();
		if ( $this -> editMode () )
		{
			return $this->renderView ( compact ( 'render' ) );
		}
		return $render;
	}


	protected function attributes ( $except = [] )
	{
		$attributes = parent :: attributes ( $except );
		if ( ! $this -> hasErrors() and $this -> editMode () )
		{
			unset ( $attributes [ 'required' ] );
			$attributes [ 'disabled' ] = 'disabled';
		}
		return $attributes;
	}


	public function containerClasses ()
	{
		$classes = parent :: containerClasses ();
		if ( $this -> config ( 'confirm' ) and ! $this -> hasErrors() and $this -> editMode () )
		{
			$classes [] = 'hide';
		}
		return $classes;
	}


	public function renderJs ()
	{
		if ( $this -> editMode () )
		{
			return $this -> requireWidgetJs([
				'id' => $this -> id (),
				'required' => $this -> required (),
				'hasErrors' => (bool) $this -> hasErrors ()
			]);
		}
		return '';
	}

}



