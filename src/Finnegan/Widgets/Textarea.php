<?php
namespace Finnegan\Widgets;


class Textarea extends Widget
{

	const ADVANCED_DEFAULT_MODE = 'standard';
	
	protected static $advancedModes = [ 'basic', 'standard', 'full' ];
	
	
	protected $config = [
		'attributes' => [
			'rows' => 5
		]
	];

	protected $contracts = [
		\Finnegan\Contracts\Fields\Text::class
	];


	public function render ()
	{
		return $this -> renderView ();
	}
	
	
	protected function attributes ( $except = [] )
	{
		$attributes = parent :: attributes ( $except );
		if ( $size = $this->config ( 'size' ) )
		{
			$attributes [ 'maxlength' ] = $size;
		}
		return $attributes;
	}
	

	protected function boot ()
	{
		parent :: boot ();
		if ( $this -> config ( 'advanced' ) )
		{
			$this -> requireAsset ( 'ckeditor.' . $this -> mode () );
		}
	}
	
	
	protected function mode ()
	{
		$mode = $this -> config ( 'mode', static :: ADVANCED_DEFAULT_MODE );
		return in_array ( $mode, static::$advancedModes ) ? $mode : static :: ADVANCED_DEFAULT_MODE;
	}
	

	/**
	 * @todo fixare in translatable mode
	 */
	public function renderJs ()
	{
		if ( $this->config ( 'advanced' ) )
		{
			return $this->requireWidgetJs ( [
				'id'     => [ $this->id () ],
				'config' => [
					'language' => $this->config ( 'locale' )
				]
			] );
		}
		return '';
	}

}


