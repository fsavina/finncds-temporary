<?php
namespace Finnegan\Widgets;


class Radio extends Checkbox
{
	
	
	protected function renderOne ( $name, $label, $value, $checked )
	{
		return $this->renderView ( compact ( 'value', 'checked', 'label' ), 'radio' );
	}


	protected function inputType ()
	{
		return 'radio';
	}
	

	protected function attributes ( $except = [] )
	{
		$except [] = 'id';
		return parent :: attributes ( $except );
	}

}



