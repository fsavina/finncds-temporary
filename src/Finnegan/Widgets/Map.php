<?php
namespace Finnegan\Widgets;


class Map extends Input
{

	protected $contracts = [
		\Finnegan\Contracts\Fields\Coordinates::class
	];


	protected function attributes ( $except = [] )
	{
		$attributes = parent :: attributes ( $except );
		$attributes [ 'readonly' ] = 'readonly';
		$attributes [ 'class' ] .= ' input-group-field';
		return $attributes;
	}


	public function render ()
	{
		return $this -> renderView ( [
			'inputType' => $this->inputType(),
			'mapHeight' => '250px'
		] );
	}


	protected function boot ()
	{
		parent :: boot ();
		$this->requireAsset ( 'google.maps', [ 'language' => $this->config ( 'locale' ) ] );
	}


	public function renderJs ()
	{
		return $this -> requireWidgetJs ([
			'id' => $this -> id (),
			'coordinates' => $this -> value (),
			'geolocate' => ( bool ) $this -> config ( 'geolocate' )
		]);
	}

}


