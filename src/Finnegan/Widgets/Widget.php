<?php
namespace Finnegan\Widgets;


use Illuminate\Support\Str;


abstract class Widget
	implements
		\Illuminate\Contracts\Support\Htmlable
{

	const DEFAULT_TYPE = 'input';

	const CONTEXT_OBJECT = 'object';

	const CONTEXT_FILTERS = 'filters';

	protected $name;

	protected $config = [ ];

	/**
	 * @var \Finnegan\Models\Fields\Field
	 */
	protected $field;

	protected $assets = [ ];

	protected $contracts = [ ];

	protected $context;


	protected function __construct ( $name, $config = [ ] )
	{
		if ( $name instanceof \Finnegan\Models\Fields\Field )
		{
			$this -> initFieldWidget( $name );
			$name = $name -> name();
		}

		$this->name = $name;
		$this->config = array_merge ( $this->config, $config );

		$this -> boot ();
	}
	
	
	abstract public function render ();


	protected function initFieldWidget ( $field )
	{
		$this->checkCompatibility ( $field );
		$this -> field = $field;
		$this -> config [ 'namespace' ] = $this -> field -> model() -> name();
	}


	protected function type ()
	{
		return strtolower ( ( new \ReflectionClass ( $this ) ) -> getShortName () );
	}


	protected function config ( $key, $default = false )
	{
		return array_get ( $this->config, $key, $default );
	}


	protected function boot ()
	{
		if ( is_array ( $this -> assets ) and count ( $this -> assets ) )
		{
			foreach ( $this -> assets as $asset )
			{
				$this -> requireAsset ( $asset );
			}
		}
	}


	public function context ()
	{
		return $this->context;
	}


	public function setContext ( $context )
	{
		$this->context = $context;
		if ( $context == static :: CONTEXT_FILTERS )
		{
			$this->config[ 'namespace' ] = 'search';
		}
		return $this;
	}


	protected function id ()
	{
		$id = $this->name;
		if ( $namespace = $this->config ( 'namespace' ) )
		{
			if ( is_object ( $this->field ) and $this->field->isTranslatable () and ( $locale = $this->field->locale () ) )
			{
				$namespace .= "_{$locale}";
			}
			$id = "{$namespace}_{$id}";
		}
		return $id;
	}


	public function name ( $name = null )
	{
		$name = $name ?: $this->name;
		if ( $namespace = $this->config ( 'namespace' ) )
		{
			if ( is_object ( $this->field ) and $this->field->isTranslatable () and ( $locale = $this->field->locale () ) )
			{
				$namespace .= "[{$locale}]";
			}
			$name = "{$namespace}[{$name}]";
		}
		return $name;
	}


	protected function attributes ( $except = [] )
	{
		$attributes = $this->config ( 'attributes', [ ] );

		$attributes = array_merge([
			'autocomplete' => 'off',
			'class' => $this -> hasErrors () ? 'is-invalid-input' : '',
			'id' => $this -> id ()
		], $attributes);

		if ( $this -> required () )
		{
			$attributes [ 'required' ] = 'required';
		}

		if ( $this -> config ( 'help' ) )
		{
			$attributes [ 'aria-describedby' ] = "{$attributes['id']}_help_text";
		}

		return array_except ( $attributes, $except );
	}


	protected function rules ()
	{
		return (array) $this->config ( 'rules', [ ] );
	}


	protected function hasErrors ()
	{
		return ( count ( $this->errors () ) > 0 );
	}


	protected function errors ()
	{
		return is_object ( $this->field ) ? $this -> field -> errors() : [];
	}


	public function containerClasses ()
	{
		$id = $this->name;
		if ( $namespace = $this->config ( 'namespace' ) )
		{
			$id = "{$namespace}-{$id}";
		}
		$id = str_replace ( '_', '-', $id );
		return [
			'finn-widget',
			"finn-widget-{$this -> type ()}",
			"{$id}-main-container"
		];
	}


	protected function wrapFormElement ( $render, $attributes = [ ] )
	{
		$label = $this->label ();
		return $this->renderView ( compact ( 'render', 'attributes', 'label' ), 'widget-wrapper' );
	}


	protected function label ( $label = null )
	{
		$label = $label ?: $this -> config ( 'label' );
		if ( $this -> required () )
		{
			return $this->renderRequiredLabel ( $label );
		}
		return $label;
	}


	protected function renderRequiredLabel ( $label )
	{
		return $label . ' <small>' . trans ( 'finnegan::general.required' ) . '</small>';
	}


	protected function value ()
	{
		
		return $this -> prepareValue (
			$this->layoutManager ()->form ()->getValueAttribute ( $this->name () )
		);
	}


	protected function prepareValue ( $value )
	{
		return is_object ( $this -> field ) ? $this -> field -> renderForForm ( $value ) : $value;
	}


	protected function required ()
	{
		return (bool) $this->config ( 'required' );
	}


	protected function editMode ()
	{
		return is_object($this -> field) ? $this -> field -> model () -> exists : false;
	}


	public function field ()
	{
		return $this -> field;
	}


	protected function renderView ( array $data = [], $name = null )
	{
		$name = $name ?: $this->type ();

		$data = array_merge ( [
			'widget'       => $this,
            'field'        => $this->field,
            'field_locale' => $this->field->isTranslatable () ? $this->field->locale () : null,
            'id'           => $this->id (),
            'name'         => $this->name (),
            'value'        => $this->value (),
            'attributes'   => $this->attributes (),
		], $data );

		return view ( "finnegan-widgets::$name", $data );
	}


	public function renderFormElement ()
	{
		if ( is_object ( $this->field ) and $this->field->isTranslatable () )
		{
			$presenter = $this->layoutManager ()->presenter ();

			$first = true;
			$tabs = $presenter->tabs ( $this->field->name () );
			$languages = $this->field->model()->getLocales();
			foreach ( $languages as $locale => $label )
			{
				$this->field->setLocale ( $locale );
				$tabs->add ( $label, $this->render (), $this->field->name () . '_' . $locale, $first );
				$first = false;
			}
			$render = $tabs->render ();
		} else
		{
			$render = $this -> render();
		}

		$render .= $this -> renderHelpText();
		$render .= $this -> renderErrors();

		return $this -> wrapFormElement ( $render, [
			'class' => $this -> hasErrors () ? 'is-invalid-label' : ''
		] );
	}


	protected function renderHelpText ()
	{
		if( $help = $this -> config ( 'help' ) )
		{
			return $this->renderView ( compact ( 'help' ), 'widget-help-text' );
		}
		return '';
	}


	protected function renderErrors ()
	{
		$errors = '';
		if ( $this -> hasErrors () )
		{
			foreach ( $this -> errors() as $error )
			{
				$errors .= "<span class=\"form-error is-visible\">{$error}</span>";
			}
		}
		return $errors;
	}


	public function renderJs ()
	{
		return '';
	}


	protected function requireWidgetJs ( $options = [] )
	{
		$objectName = Str::studly ( $this->type () );
		$this->requireAsset ( "assets/finnegan/js/widgets.js" );

		$options = json_encode ( $options );
		return "$(function(){ new FINNEGAN.Widgets.{$objectName}({$options}); });";
	}


	/**
	 * @param string                               $type
	 * @param string|\Finnegan\Models\Fields\Field $name
	 * @param array                                $config
	 * @return Widget
	 */
	public static function make ( $type, $name, $config = [ ] )
	{
		if ( ! class_exists ( $class = $type ) )
		{
			$class = static:: resolveType ( $type );
		}
		return new $class ( $name, $config );
	}


	protected static function resolveType ( $type )
	{
		return __NAMESPACE__ . '\\' . Str::studly ( $type );
	}


	public function toHtml ()
	{
		return $this->render ();
	}


	public function __toString ()
	{
		return $this->render ();
	}


	protected function checkCompatibility ( $field )
	{
		if ( is_array ( $this -> contracts ) and count ( $this -> contracts ) )
		{
			foreach ( $this -> contracts as $contract )
			{
				if ( ! $field instanceof $contract )
				{
					throw new \InvalidArgumentException ( '"' . $field -> name () . '" field is not compatible with the "' . get_class ( $this ) . '" widget' );
				}
			}
		}
	}
	
	
	/**
	 * @return \Finnegan\Contracts\Layout\Manager
	 */
	protected function layoutManager ()
	{
		return app ( 'Finnegan\Contracts\Layout\Manager' );
	}


	protected function requireAsset ( $asset, $params = [ ] )
	{
		$this->layoutManager ()->assetManager () ->requireResource ( $asset, $params );
	}



}