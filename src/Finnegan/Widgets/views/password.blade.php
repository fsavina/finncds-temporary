<div>
	{!! $presenter -> button ( '', '#' )
		-> translateCaption ( 'actions.change_value' )
		-> expanded ()
		-> id ( $id . '_toggle' )
		-> icon ( 'actions.edit' )
		-> render () !!}
	<div class="hide" id="{{ $id }}_container">
		{!! $render !!}

		{!! $presenter -> button ( '', '#' )
			-> translateCaption ( 'actions.cancel' )
			-> secondary ()
			-> expanded ()
			-> id ( $id . '_cancel' )
			-> icon ( 'actions.close' )
			-> render () !!}
	</div>
</div>