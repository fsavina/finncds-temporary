<fieldset{!! $layout->html()->attributes($attributes) !!}>
    <legend>{!! $label !!}</legend> {!! $render !!}
</fieldset>