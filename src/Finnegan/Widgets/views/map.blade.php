{!! $presenter -> inputGroup(
	$layout->form()->input($inputType, $name, $value, $attributes),
	$presenter -> button('','#') -> icon('map-marker') -> id($id . '_map_trigger')
) !!}
<div id="{{$id}}_map_container" class="hide" style="position:relative;">
	<div id="{{$id}}_map" style="height:{{$mapHeight}};margin:0 0 1rem"></div>
	<div id="{{$id}}_loading" class="hide" style="position:absolute;top:0;left:0;width:100%;height:{{$mapHeight}};background-color:#888;opacity:.4"></div>
	<div class="row">
		<div class="small-4 columns">
			{!! $presenter -> button('','#') -> translateCaption('general.ok') -> icon('actions.ok') -> success() -> id($id . '_ok') !!}
			{!! $presenter -> button('','#') -> translateCaption('actions.clear') -> icon('actions.close') -> secondary() -> id($id . '_clear') !!}
		</div>
		<div class="small-3 columns text-right">
			{!! $presenter -> button('','#') -> translateCaption('actions.my_position') -> warning() -> hide() ->id($id . '_geolocate') -> icon('location-arrow') !!}
		</div>
		<div class="small-5 columns">
			{!! $presenter -> inputGroup(
				$layout->form()->input ( 'search', null, null, [
					'placeholder' => trans ( 'finnegan::actions.find_address' ) . '...',
					'autocomplete' => 'off',
					'id' => $id . '_address',
					'class'=>'input-group-field'
				] ),
				$presenter -> button('','#') -> id($id . '_geocode') -> icon('search')
			) !!}
		</div>
	</div>
</div>