<div class="row">
	<div class="small-10 columns">
		<div class="slider"
			data-slider
			data-initial-start="{{ $value }}"
			data-start="{{ $min }}"
			data-end="{{ $max }}"
			data-step="{{ $step }}"
			data-decimal="{{ $precision }}"
		>
			<span aria-controls="{{ $id }}" class="slider-handle" data-slider-handle role="slider"></span>
			<span class="slider-fill" data-slider-fill></span>
		</div>
	</div>
	<div class="small-2 columns">
		{!! $layout->form()->input($inputType, $name, $value, $attributes) !!}
	</div>
</div>