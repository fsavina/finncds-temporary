<div class="switch">
	{!! $checkbox !!}
	<label class="switch-paddle" for="{{ $id }}">
		<span class="show-for-sr">{!! $label !!}</span>
	</label>
</div>
@if(strlen($label))<p class="help-text">{!! $label !!}</p>@endif