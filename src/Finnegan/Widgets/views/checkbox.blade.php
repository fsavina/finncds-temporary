@if (isset($label)) <label> @endif

{!! $layout->form()->checkbox ( $name, $value, $checked, $attributes ) !!}

@if (isset($label)) {!! $label !!}</label> @endif