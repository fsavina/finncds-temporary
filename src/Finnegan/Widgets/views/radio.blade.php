@if (isset($label)) <label> @endif

{!! $layout->form()->radio ( $name, $value, $checked, $attributes ) !!}

@if (isset($label)) {!! $label !!}</label> @endif