<div class="row">
    <div class="small-6 columns">{!! $render !!}</div>
    <div class="small-6 columns"><span id="{{ $id }}_confirm_label">{!! $confirm !!}</span></div>
</div>