@if(isset($confirm) and $confirm)
<div class="row">
	<div class="small-6 columns">{!! $layout->form()->input($inputType, $name, $value, $attributes) !!}</div>
	<div class="small-6 columns">{!! $confirm !!}</div>
</div>
@else
	{!! $layout->form()->input($inputType, $name, $value, $attributes) !!}
@endif