<div class="row">
	<div class="small-6 columns">{!! $layout->form()->input( $inputType, "{$name}[from]", $from['value'], $from['attributes'] ) !!}</div>
	<div class="small-6 columns">{!! $layout->form()->input( $inputType, "{$name}[to]", $to['value'], $to['attributes'] ) !!}</div>
</div>