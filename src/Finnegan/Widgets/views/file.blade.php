<div>
	@if($editMode and $field->isTranslatable())
		@php($field->setLocale($field_locale))
	@endif
	<div id="{{$id}}_preview">{!! $editMode ? $field -> render () : '' !!}</div>
	<div id="{{$id}}_input" class="{{ $editMode ? 'hide' : '' }}">
		{!! $layout->form()->file($name, $attributes ) !!}
		{!! $editMode ? $layout->form()->hidden ( $widget -> name ( $field -> name () . '_deleted' ), null, [ 'id' => $id . '_deleted' ] ) : '' !!}
	</div>
</div>
<div class="{{$id}}_button_container {{ !$editMode ? 'hide' : '' }}">
	@if($editMode)
	{!! $presenter -> button() -> action('replace') -> small() -> url('#') -> id($id . '_replace') -> icon('actions.edit') !!}
	@endif
	@if (!$required)
	{!! $presenter -> button() -> translateCaption('actions.discard') -> small() -> alert() -> url('#') -> id($id . '_delete') -> icon('actions.destroy') !!}
	@endif
</div>
@if($editMode)
<div class="hide" id="{{$id}}_cancel_container">
	{!! $presenter -> button() -> translateCaption('actions.cancel') -> small() -> secondary() -> url('#') -> id($id . '_cancel') -> icon('actions.close') !!}
</div>
@endif