<?php
namespace Finnegan\Widgets;


use Finnegan\Contracts\Fields\Confirmable;


class Input extends Widget
{

	const DEFAULT_TYPE = 'text';

	protected static $allowedTypes = [
		'text',
		'hidden',
		'password',
		'email',
		'url',
		'number',
		'range',
		'tel',
		'file',
		'date',
		'color'
	];
	
	protected static $rangeFilterTypes = [
		'number',
		'date'
	];


	public function render ()
	{
		if ( $this -> context == static :: CONTEXT_FILTERS
				and in_array ( $this -> inputType (), static :: $rangeFilterTypes ) )
		{
			return $this -> renderRangeFilter ();
		}

		return $this -> renderView ( [
			'inputType' => $this->inputType (),
			'confirm'   => $this -> confirm() ? $this -> renderConfirm() : false
		], 'input' );
	}


	protected function confirm (  )
	{
		return ( ( $this -> field instanceof Confirmable ) and $this -> field -> confirm () );
	}


	protected function renderConfirm ()
	{
		$attributes = $this -> attributes ();
		$attributes [ 'id' ] .= '_confirmation';

		return $this -> renderView ( [
			 'inputType'  => $this->inputType (),
			 'name'       => $this->name ( $this->field->name () . '_confirmation' ),
			 'attributes' => $attributes,
		], 'input' );
	}


	protected function label ( $label = null )
	{
		$render = parent :: label ();

		if ( $this -> confirm () )
		{
			$confirm = parent :: label ( trans ( 'finnegan::actions.confirm' ) );
			return $this->renderView ( compact('render', 'confirm'), 'input-confirm-label' );
		}

		return $render;
	}
	

	/**
	 * @return string
	 */
	protected function renderRangeFilter ()
	{
		$filterBy = $this -> field -> model () -> getFilterBy ();
		
		$from = data_get ( $filterBy, "{$this -> name}.from" );
		$to = data_get ( $filterBy, "{$this -> name}.to" );
		
		$fromAttributes = $toAttributes = $this -> attributes ();
		
		$fromAttributes [ 'placeholder' ] = trans ( 'finnegan::general.from' );
		$fromAttributes [ 'id' ] .= '_from';
		
		$toAttributes [ 'placeholder' ] = trans ( 'finnegan::general.to' );
		$toAttributes [ 'id' ] .= '_to';

		return $this -> renderView ( [
			 'inputType' => $this->inputType (),
			 'from'      => [
				 'attributes' => $fromAttributes,
				 'value'      => $from
			 ],
			 'to'      => [
				 'attributes' => $toAttributes,
				 'value'      => $to
			 ]
		 ], 'input-range-filter' );
	}


	/**
	 * Get the required input type
	 * @return string
	 */
	protected function inputType ()
	{
		$type = $this -> config ( 'inputType', self :: DEFAULT_TYPE );
		return in_array ( $type, static :: $allowedTypes ) ? $type : self :: DEFAULT_TYPE;
	}


	protected function attributes ( $except = [] )
	{
		$attributes = parent :: attributes ( $except );

		if ( $size = $this->config ( 'size' ) )
		{
			$attributes [ 'maxlength' ] = $size;
		}

		if ( $min = $this->config ( 'min' ) )
		{
			$attributes [ 'min' ] = $min;
		}

		if ( $max = $this->config ( 'max' ) )
		{
			$attributes [ 'max' ] = $max;
		}

		if ( $step = $this->config ( 'step' ) )
		{
			$attributes [ 'step' ] = $step;
		}

		return $attributes;
	}

}


