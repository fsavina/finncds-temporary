<?php
namespace Finnegan\Widgets;


class Slider extends Input
{

	protected $contracts = [
		\Finnegan\Contracts\Fields\Range::class
	];


	public function render ()
	{
		if ( $this->context () == static :: CONTEXT_FILTERS )
		{
			return parent:: render ();
		}
		
		return $this->renderView ( [
			'inputType' => $this->inputType (),
			'min'       => $this->config ( 'min' ),
			'max'       => $this->config ( 'max' ),
			'step'      => $this->config ( 'step' ),
			'precision' => $this->config ( 'precision' )
		] );
	}

}



