<?php
namespace Finnegan\Widgets;


class Select extends Widget
{


	protected $contracts = [
		\Finnegan\Contracts\Fields\Options::class
	];
	

	public function render ()
	{
		$value = $this -> value ();

		if ( is_object ( $this -> field ) and $this -> context() == static :: CONTEXT_FILTERS )
		{
			$filterBy = $this -> field -> model () -> getFilterBy ();
			$value = data_get ( $filterBy, $this -> name );
		}

		return $this->renderView([
			 'options' => $this->options (),
			 'value'   => $value
		]);
	}


	protected function attributes ( $except = [] )
	{
		if ( $this -> context() == static :: CONTEXT_FILTERS )
		{
			$except[] = 'required';
		}
		$attributes = parent :: attributes ( $except );

		if ( ! $this -> required () or ( $this -> context() == static :: CONTEXT_FILTERS ) )
		{
			$attributes [ 'placeholder' ] = '----';
		}
		return $attributes;
	}
	
	
	/**
	 * Prepare the options array for the rendering
	 * @return array
	 */
	protected function options ()
	{
		$options = [ ];
		if (is_object($this -> field))
		{
			foreach ( $this -> field -> formOptions () as $key => $value )
			{
				$options [ $key ] = trans ( $value );
			}
		} elseif ( $config = $this -> config ( 'options' ) )
		{
			foreach ( $config as $key => $value )
			{
				$options [ $key ] = trans ( $value );
			}
		}
		return $options;
	}
	

}


