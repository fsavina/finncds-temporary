<?php
namespace Finnegan\Layout\Components;


use Finnegan\Contracts\Layout\Components\Sizable;


class VideoEmbed extends AbstractComponent implements Sizable
{
	
	/**
	 * @var \Cohensive\Embed\Embed
	 */
	protected $embed = null;
	
	
	public function __construct ( $embed = null )
	{
		$this->embed ( $embed );
	}
	
	
	public function embed ( $embed )
	{
		$this->embed = is_string ( $embed ) ? app ( 'embed' )->make ( $embed ) : $embed;
		return $this;
	}
	
	
	public function width ( $width )
	{
		$this->embed->setAttribute ( 'width', $width );
		return $this;
	}
	
	
	public function height ( $height )
	{
		$this->embed->setAttribute ( 'height', $height );
		return $this;
	}
	
	
	public function size ( $width, $height )
	{
		$this->embed->setAttribute ( [
										 'width'  => $width,
										 'height' => $height
									 ] );
		return $this;
	}
	
	
	protected function reset ()
	{
		$this->embed = null;
	}
	
	
	public function render ()
	{
		$view = $this->embed->getHtml ();
		$this->reset ();
		return $view;
	}
}