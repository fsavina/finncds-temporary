<?php
namespace Finnegan\Layout\Components;


use Finnegan\Contracts\Layout\Components\Button as ButtonContract;


class Button extends Link implements ButtonContract
{
	
	protected $classes = [ 'button' ];
	
	
	public function render ()
	{
		if ( isset ( $this->attributes [ 'href' ] ) and strlen ( $this->attributes [ 'href' ] ) )
		{
			return parent:: render ();
		}
		unset ( $this->attributes [ 'href' ] );
		
		$layout = $this->getLayout ();
		
		$icon = $this->icon ? $layout->presenter ()->icon ( $this->icon ) . ' ' : '';
		
		$this->attributes [ 'class' ] = implode ( ' ', $this->classes );
		if ( ! isset ( $this->attributes [ 'type' ] ) )
		{
			$this->attributes [ 'type' ] = 'submit';
		}
		
		
		$render = $layout->form ()->button ( $icon . trans ( $this->caption ?: '' ), $this->attributes );
		$this->reset ();
		return $render->__toString ();
	}
	
	
	/**
	 * @return \Finnegan\Layout\Components\Button
	 */
	protected function reset ()
	{
		parent:: reset ();
		$this->classes = [ 'button' ];
		return $this;
	}
	
}



