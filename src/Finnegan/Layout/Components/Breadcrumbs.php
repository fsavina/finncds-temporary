<?php
namespace Finnegan\Layout\Components;


class Breadcrumbs extends AbstractComponent implements \Finnegan\Contracts\Layout\Components\Breadcrumbs
{
	
	protected $items = [ ];

	protected $containerClass = 'breadcrumbs';
	
	
	public function __construct ( $items = [ ] )
	{
		$this->items = $items;
	}
	
	
	/**
	 * @param $url
	 * @param $caption
	 * @return Breadcrumbs
	 */
	public function add ( $url, $caption = null )
	{
		if ( is_null ( $caption ) )
		{
			$this->items[] = $url;
		} else
		{
			$this->items[ $url ] = $caption;
		}
		return $this;
	}
	
	
	protected function reset ()
	{
		$this->items = [ ];
	}
	
	
	public function render ()
	{
		$view = $this->renderView ( 'breadcrumbs', [
			'items' => $this->items,
			'container_class' => $this->containerClass
		] );

		$this->reset ();

		return $view;
	}
	
}