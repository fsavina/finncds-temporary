<?php
namespace Finnegan\Layout\Components;


use Finnegan\Contracts\Layout\Components\Component;
use Finnegan\Contracts\Layout\Manager;
use Finnegan\Contracts\Layout\Presenter;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Contracts\Support\Renderable;


abstract class AbstractComponent implements Component, Htmlable, Renderable
{

	/**
	 * @var \Finnegan\Contracts\Layout\Presenter
	 */
	protected $presenter;


	public function setPresenter ( Presenter $presenter )
	{
		$this->presenter = $presenter;
		return $this;
	}


	/**
	 * @return \Finnegan\Contracts\Layout\Manager
	 */
	protected function getLayout ()
	{
		return app ( Manager::class );
	}


	/**
	 * @return \Collective\Html\HtmlBuilder
	 */
	protected function html ()
	{
		return $this->getLayout ()->html ();
	}


	public function toHtml ()
	{
		return $this->render ();
	}


	public function __toString ()
	{
		try
		{
			$render = $this->render ();
			return ( is_object ( $render ) ? $render->render () : $render );
		} catch ( \Exception $e )
		{
			return $e->getMessage ();
		}
	}
	
	
	public function renderView ( $name, $data = [ ] )
	{
		return $this->getLayout ()->views ()->file ( $this->getViewsPath () . "/$name.blade.php", $data );
	}
	
	
	protected function getViewsPath ()
	{
		return __DIR__ . "/views";
	}

}