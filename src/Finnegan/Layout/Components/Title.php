<?php

namespace Finnegan\Layout\Components;


use Finnegan\Contracts\Models\Titlable;


class Title extends AbstractComponent
{
	
	
	/**
	 * @var \Finnegan\Contracts\Models\Titlable
	 */
	protected $model;
	
	protected $context;
	
	protected $plain;
	
	
	public function __construct ( Titlable $model, $context = null, $plain = false )
	{
		$this->model = $model;
		$this->context = $context;
		$this->plain = (bool) $plain;
	}
	
	
	public function context ( $context )
	{
		$this->context = $context;
		return $this;
	}
	
	
	public function render ()
	{
		$icon = '';
		if ( ! $this->plain )
		{
			$icon = $this->model->config ( 'display.icon' );
			$icon = $icon ? $this->getLayout ()->presenter ()->icon ( $icon )->after ( ' ' ) : '';
		}
		
		$default = "finnegan::actions.{$this -> context}_title";
		if ( $this->context == 'list' )
		{
			$replace = [
				'plural' => $this->model->definition ()->plural ()
			];
		} else
		{
			if ( $this->context == 'form' )
			{
				$default .= $this->model->exists ? '_edit' : '_new';
			}
			$replace = [
				'singular' => $this->model->definition ()->singular ()
			];
			if ( $this->model->exists and $this->model->config ( 'display.fields' ) )
			{
				$replace [ 'singular' ] .= ': "' . $this->model . '"';
			}
		}
		
		$title = trans ( $this->model->config ( "display.{$this -> context}.title", $default ), $replace );
		
		if ( $this->plain )
		{
			$title = trim ( preg_replace ( '#<[^>]+>#', ' ', $title ) );
		}
		
		return $icon . $title;
	}
	
}



