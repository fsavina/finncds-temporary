<ul class="{{ $container_class }}">
    @foreach ( $items as $url => $caption )
        <li>
            @if(is_numeric($url))
                {!! $caption !!}
            @else
                <a href="{{ $url }}">{!! $caption !!}</a>
            @endif
        </li>
    @endforeach
</ul>