<?php
namespace Finnegan\Layout\Components;


use Finnegan\Contracts\Layout\Components\Link as LinkContract;
use Finnegan\Contracts\Models\Actionable;


/**
 * @method \Finnegan\Layout\Components\Link type
 * @method \Finnegan\Layout\Components\Link name
 * @method \Finnegan\Layout\Components\Link value
 * @method \Finnegan\Layout\Components\Link id
 * @method \Finnegan\Layout\Components\Link title
 */
class Link extends AbstractComponent implements LinkContract
{

	protected $caption;

	protected $title;

	protected $icon;

	protected $attributes = [ ];

	protected $classes = [ ];

	protected $attributesWhitelist = [
		'type',
		'name',
		'value',
		'id',
		'title',
		'target'
	];
	
	protected $classWhitelist = [ ];


	public function __construct ( $caption = '', $url = '' )
	{
		$this -> caption ( $caption );
		$this -> url ( $url );
	}
	

	/**
	 * @param string $method
	 * @param array $args
	 * @return \Finnegan\Layout\Components\Link
	 */
	public function __call ( $method, $args = [] )
	{
		if ( in_array ( $method, $this -> classWhitelist ) )
		{
			return $this -> addClass ( $method );
		}
		if ( in_array ( $method, $this -> attributesWhitelist ) )
		{
			return $this -> setAttribute ( $method, $args [ 0 ] );
		}
		return $this;
	}


	/**
	 * @param string $caption
	 * @return \Finnegan\Layout\Components\Link
	 */
	public function caption ( $caption )
	{
		$this->caption = is_object ( $caption ) ? $caption->__toString () : $caption;
		return $this;
	}


	/**
	 * @return string
	 */
	public function getCaption ()
	{
		return $this -> caption;
	}


	/**
	 * @param string $icon
	 * @return \Finnegan\Layout\Components\Link
	 */
	public function icon ( $icon )
	{
		$this -> icon = $icon;
		return $this;
	}


	/**
	 * @param string $url
	 * @return \Finnegan\Layout\Components\Link
	 */
	public function url ( $url )
	{
		$this -> attributes [ 'href' ] = $url;
		return $this;
	}


	/**
	 * @param Actionable $model
	 * @param $action
	 * @return \Finnegan\Layout\Components\Link
	 */
	public function modelAction ( Actionable $model, $action )
	{
		$this -> url ( $model -> recordActionUrl ( $action ) );
		$this -> action ( $action );
		return $this;
	}


	/**
	 * @param string $action
	 * @return \Finnegan\Layout\Components\Link
	 */
	public function action ( $action )
	{
		$this -> translateCaption ( "actions.$action" );
		$this -> icon ( "actions.$action" );
		return $this;
	}


	/**
	 * @param string $caption
	 * @return \Finnegan\Layout\Components\Link
	 */
	public function translateCaption ( $caption )
	{
		$this -> caption ( "finnegan::$caption" );
		return $this;
	}


	/**
	 * @param string $message
	 * @return \Finnegan\Layout\Components\Link
	 */
	public function confirm ( $message )
	{
		return $this -> setAttribute ( 'onclick', 'return confirm("' . trans ( $message ) . '");' );
	}
	
	
	/**
	 * @param string $class
	 * @return \Finnegan\Layout\Components\Link
	 */
	public function addClass ( $class )
	{
		if ( ! in_array ( $class, $this -> classes ) )
		{
			$this -> classes [] = $class;
		}
		return $this;
	}


	/**
	 * @param string $class
	 * @return \Finnegan\Layout\Components\Link
	 */
	public function removeClass ( $class )
	{
		$this -> classes = array_where ( $this -> classes, function  ( $key, $value ) use( $class ) {
			if ( is_array ( $class ) )
			{
				return ! in_array ( $value, $class );
			}
			return $value != $class;
		} );
		return $this;
	}


	/**
	 * @param string $name
	 * @param string $value
	 * @return \Finnegan\Layout\Components\Link
	 */
	public function setAttribute ( $name, $value )
	{
		$this -> attributes [ $name ] = $value;
		return $this;
	}
	
	
	/**
	 * @return \Finnegan\Layout\Components\Link
	 */
	protected function reset ()
	{
		$this -> caption = null;
		$this -> icon = null;
		$this -> attributes = [ ];
		$this -> classes = [ ];
		return $this;
	}


	public function renderIf ( $condition )
	{
		$render = ( $condition ) ? $this -> render () : '';
		$this -> reset ();
		return $render;
	}
	
	
	public function render ()
	{
		$layout = $this->getLayout ();
		$icon = $this -> icon ? $layout -> presenter() -> icon ( $this -> icon ) . ' ' : '';
		
		$this -> attributes [ 'class' ] = implode ( ' ', $this -> classes );
		
		$render = '<a' . $this->html()->attributes ( $this -> attributes ) . '>' . $icon . trans ( $this -> caption ? : '' ) . '</a>';
		
		$this -> reset ();
		
		return $render;
	}

}



