<?php
namespace Finnegan\Layout;


use Collective\Html\FormBuilder;
use Collective\Html\HtmlBuilder;
use Finnegan\Contracts\Layout\AssetManager;
use Finnegan\Contracts\Modules\Module as ModuleContract;
use Finnegan\Layout\Themes\Theme;
use Illuminate\View\Factory as ViewFactory;


class LayoutManager implements \Finnegan\Contracts\Layout\Manager
{
	
	/**
	 * @var \Finnegan\Contracts\Modules\Module
	 */
	protected $module;
	
	/**
	 * @var \Illuminate\View\Factory
	 */
	protected $views;
	
	/**
	 * @var \Finnegan\Contracts\Layout\Theme
	 */
	protected $theme;
	
	/**
	 * The AssetManager instance
	 * @var \Finnegan\Contracts\Layout\AssetManager
	 */
	protected $assetManager;
	
	/**
	 * @var \Collective\Html\HtmlBuilder
	 */
	protected $html;
	
	/**
	 * @var \Collective\Html\FormBuilder
	 */
	protected $form;
	
	
	public function __construct (
		ModuleContract $module,
		ViewFactory $views,
		AssetManager $assetManager,
		HtmlBuilder $html,
		FormBuilder $form
	)
	{
		$this->module = $module;
		$this->views = $views;
		$this->assetManager = $assetManager;
		$this->html = $html;
		$this->form = $form;

		$this->theme = Theme::make ( $this->module, $this, $this->views );

		$this->boot ();
	}
	
	
	protected function boot ()
	{
		$this->theme->boot ();

		$this->views->share ( [
								  'module' => $this->module,
								  'layout' => $this
							  ] );
	}
	
	
	/**
	 * @return \Finnegan\Contracts\Layout\AssetManager
	 */
	public function assetManager ()
	{
		return $this->assetManager;
	}
	
	
	/**
	 * @return \Illuminate\View\Factory
	 */
	public function views ()
	{
		return $this->views;
	}
	
	
	/**
	 * @return \Finnegan\Contracts\Layout\Theme
	 */
	public function theme ()
	{
		return $this->theme;
	}


	/**
	 * @return \Finnegan\Contracts\Layout\Presenter
	 */
	public function presenter ()
	{
		return $this->theme->presenter ();
	}
	
	
	/**
	 * @return \Collective\Html\HtmlBuilder
	 */
	public function html ()
	{
		return $this->html;
	}
	
	
	/**
	 * @return \Collective\Html\FormBuilder
	 */
	public function form ()
	{
		return $this->form;
	}
	
	
	/**
	 * Get the required menu instance
	 * @param string $name
	 * @return Menus\ModuleMenu
	 */
	public function menu ( $name = 'primary' )
	{
		return Menus\ModuleMenu:: make ( $name, $this->module );
	}
}



