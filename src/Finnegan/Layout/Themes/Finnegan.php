<?php
namespace Finnegan\Layout\Themes;


use Finnegan\Layout\Presenters\FontAwesome\FontAwesomeIconPresenter;
use Finnegan\Layout\Presenters\Foundation\FoundationPresenter;


class Finnegan extends Theme
{
	
	protected $namespace = 'finnegan';
	
	
	public function getPresenterClass ()
	{
		return FoundationPresenter::class;
	}
	
	
	public function getIconPresenterClass ()
	{
		return FontAwesomeIconPresenter::class;
	}
}