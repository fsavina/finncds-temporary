<?php
namespace Finnegan\Layout\Themes;


use Finnegan\Contracts\Modules\Module;
use Finnegan\Contracts\Layout\Manager;
use Finnegan\Contracts\Layout\Presenter;
use Finnegan\Contracts\Layout\IconPresenter;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\View\Factory as ViewFactory;


abstract class Theme implements \Finnegan\Contracts\Layout\Theme
{
	
	/**
	 * @var string
	 */
	protected $namespace;
	
	/**
	 * @var \Finnegan\Contracts\Layout\Manager
	 */
	protected $layout;
	
	/**
	 * @var \Finnegan\Contracts\Modules\Module
	 */
	protected $module;
	
	/**
	 * @var \Illuminate\View\Factory
	 */
	protected $views;
	
	/**
	 * @var \Finnegan\Contracts\Layout\Presenter
	 */
	protected $presenter;
	
	
	/**
	 * Theme constructor.
	 * @param Manager     $layout
	 * @param Module      $module
	 * @param ViewFactory $views
	 */
	public function __construct ( Manager $layout, Module $module, ViewFactory $views )
	{
		$this->layout = $layout;
		$this->module = $module;
		$this->views = $views;
		
		$this->presenter = $this->module->make ( $this->getPresenterClass (), [
			$this->module->make ( $this->getIconPresenterClass () )
		] );
	}
	
	
	public function boot ()
	{
		$assetManager = $this->layout->assetManager ();
		foreach ( $this->module->config ( 'layout.assets', [ ] ) as $resource )
		{
			$config = is_array ( $resource ) ? Arr::except ( $resource, 'ref' ) : [ ];
			$resource = is_array ( $resource ) ? Arr::get ( $resource, 'ref' ) : $resource;
			
			$assetManager->requireResource ( $resource, $config );
		}
		
		$this->views->share ( [
			  'theme'     => $this,
			  'presenter' => $this->presenter
		  ] );
	}
	
	
	public function getPresenterClass ()
	{
		return Presenter::class;
	}
	
	
	public function getIconPresenterClass ()
	{
		return IconPresenter::class;
	}
	
	
	/**
	 * @return \Finnegan\Contracts\Layout\Presenter
	 */
	public function presenter ()
	{
		return $this->presenter;
	}
	
	
	/**
	 * @param string $view
	 * @return string
	 */
	protected function wrap ( $view )
	{
		return "finnegan::{$this->namespace}.{$view}";
	}
	
	
	/**
	 * @param string $view
	 * @return string
	 */
	public function resolveView ( $view )
	{
		return $this->wrap ( $view );
	}
	
	
	/**
	 * @param string $view
	 * @return bool
	 */
	public function exists ( $view )
	{
		return $this->views->exists ( $this->wrap ( $view ) );
	}
	
	
	/**
	 * @param string $view
	 * @param array  $params
	 * @return \Illuminate\Contracts\View\View
	 */
	public function view ( $view, $params = [ ] )
	{
		return $this->views->make ( $this->wrap ( $view ), $params );
	}
	
	
	/**
	 * @param string $layout
	 * @return string
	 */
	public function layout ( $layout )
	{
		return $this->wrap ( "layouts.$layout" );
	}
	
	
	/**
	 * @param string $partial
	 * @return string
	 */
	public function partial ( $partial )
	{
		return $this->wrap ( "partials.$partial" );
	}
	
	
	/**
	 * @param string $name
	 * @return string
	 */
	protected static function resolve ( $name )
	{
		$class = Str::studly ( $name );
		return class_exists ( $class ) ? $class : __NAMESPACE__ . "\\{$class}";
	}
	
	
	/**
	 * @param Module      $module
	 * @param Manager     $layout
	 * @param ViewFactory $views
	 * @return Theme
	 */
	public static function make ( Module $module, Manager $layout, ViewFactory $views )
	{
		$theme = static::resolve ( $module->config ( 'layout.theme', 'finnegan' ) );
		return new $theme( $layout, $module, $views );
	}
	
}