<?php
namespace Finnegan\Layout\Presenters;


use Finnegan\Contracts\Layout\IconPresenter;
use Finnegan\Contracts\Layout\Presenter;
use Finnegan\Contracts\Models\Titlable;
use Illuminate\Support\Str;


abstract class AbstractPresenter implements Presenter
{
	
	/**
	 * @var IconPresenter
	 */
	protected $iconPresenter;
	
	
	public function __construct ( IconPresenter $iconPresenter )
	{
		$this->iconPresenter = $iconPresenter;
	}
	
	
	/**
	 * @return IconPresenter
	 */
	public function icons ()
	{
		return $this->iconPresenter;
	}
	
	
	public function icon ( $icon = '' )
	{
		return $this->iconPresenter->icon ( $icon );
	}
	
	
	protected function makeComponent ( $type, $args )
	{
		$class = Str::studly ( $type );
		$reflect = new \ReflectionClass( "Finnegan\\Layout\\Components\\{$class}" );
		$instance = $reflect->newInstanceArgs ( $args );
		return $instance->setPresenter ( $this );
	}
	
	
	/**
	 * @param string $caption
	 * @param string $url
	 * @return \Finnegan\Layout\Components\Link
	 */
	public function link ( $caption = '', $url = '' )
	{
		return $this->makeComponent ( __FUNCTION__, [ $caption, $url ] );
	}
	
	
	/**
	 * @param string $caption
	 * @param string $url
	 * @return \Finnegan\Layout\Components\Button
	 */
	public function button ( $caption = '', $url = '' )
	{
		return $this->makeComponent ( __FUNCTION__, [ $caption, $url ] );
	}
	
	
	/**
	 * @param Titlable $model
	 * @param string   $context
	 * @param bool     $plain
	 * @return \Finnegan\Layout\Components\Title
	 */
	public function title ( Titlable $model, $context = null, $plain = false )
	{
		return $this->makeComponent ( __FUNCTION__, [ $model, $context, $plain ] );
	}
	
	
	/**
	 * @param array $items
	 * @return \Finnegan\Layout\Components\Breadcrumbs
	 */
	public function breadcrumbs ( array $items = [ ] )
	{
		return $this->makeComponent ( __FUNCTION__, [ $items ] );
	}
	

	/**
	 * @param mixed $embed
	 * @return \Finnegan\Layout\Components\VideoEmbed
	 */
	public function videoEmbed ( $embed = null )
	{
		return $this->makeComponent ( __FUNCTION__, [ $embed ] );
	}
	
}



