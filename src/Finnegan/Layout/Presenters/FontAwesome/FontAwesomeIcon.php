<?php
namespace Finnegan\Layout\Presenters\FontAwesome;


use Khill\FontAwesome\FontAwesome;


/**
 * @link http://fortawesome.github.io/Font-Awesome/icons/
 */
class FontAwesomeIcon extends FontAwesome
	implements
	\Illuminate\Contracts\Support\Renderable,
	\Illuminate\Contracts\Support\Htmlable
{
	
	protected $before = '';
	
	protected $after  = '';
	
	
	public function __toString ()
	{
		return $this->before . parent:: __toString () . $this->after;
	}
	
	
	public function before ( $before )
	{
		$this->before = $before;
		return $this;
	}
	
	
	public function after ( $after )
	{
		$this->after = $after;
		return $this;
	}
	
	
	public function render ()
	{
		return $this->__toString ();
	}
	
	
	public function toHtml ()
	{
		return $this->__toString ();
	}
	
}