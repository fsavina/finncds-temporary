<?php
namespace Finnegan\Layout\Presenters\FontAwesome;


class FontAwesomeIconPresenter
	implements
	\Finnegan\Contracts\Layout\IconPresenter
{
	
	const FONTAWESOME_VERSION = '4.5.0';
	
	protected static $icons = [
		'logo' => 'skyatlas',
		
		'actions' => [
			'ok'        => 'check',
			'show'      => 'eye',
			'create'    => 'plus',
			'edit'      => 'pencil',
			'copy'      => 'copy',
			'translate' => 'globe',
			'save'      => 'save',
			'destroy'   => 'trash',
			'truncate'  => 'bomb',
			'close'     => 'remove',
			'sort-asc'  => 'sort-asc',
			'sort-desc' => 'sort-desc',
			'attach'    => 'paste',
			'search'    => 'search',
			'expand'    => 'expand',
			'rerun'     => 'repeat'
		],
		
		'files' => [
			'pdf' => 'file-pdf-o',
			'doc' => 'file-word-o',
			'odt' => 'file-word-o',
			'jpg' => 'file-image-o',
			'png' => 'file-image-o',
			'bmp' => 'file-image-o',
			'gif' => 'file-image-o',
			'zip' => 'file-archive-o',
			'rar' => 'file-archive-o'
		]
	];
	
	
	public function resolve ( $icon )
	{
		return array_get ( static::$icons, $icon, $icon );
	}
	
	
	public function icon ( $icon )
	{
		return new FontAwesomeIcon ( $this->resolve ( $icon ) );
	}
	
}



