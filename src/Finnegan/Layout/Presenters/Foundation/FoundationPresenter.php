<?php
namespace Finnegan\Layout\Presenters\Foundation;


use Finnegan\Contracts\Layout\Components\Button;
use Finnegan\Contracts\Layout\Presenter;
use Finnegan\Layout\Presenters\AbstractPresenter;
use Illuminate\Contracts\Pagination\Paginator;


class FoundationPresenter extends AbstractPresenter implements Presenter
{
	
	const FOUNDATION_VERSION = '6.1.1';
	
	/**
	 * @var Components\Button
	 */
	protected $button;
	
	
	/**
	 * @param string $caption
	 * @param string $url
	 * @return Components\Button
	 */
	public function button ( $caption = '', $url = '' )
	{
		if ( is_null ( $this->button ) )
		{
			$this->button = ( new Components\Button () )->setPresenter ( $this );
		}
		return $this->button->caption ( $caption )->url ( $url );
	}
	
	
	/**
	 * @param \Illuminate\Contracts\Pagination\Paginator $paginator
	 * @return Components\Pagination
	 */
	public function pagination ( Paginator $paginator )
	{
		return new Components\Pagination ( $paginator );
	}
	
	
	/**
	 * @param string $id
	 * @return Components\Tabs
	 */
	public function tabs ( $id = null )
	{
		return ( new Components\Tabs ( $id ) )->setPresenter ( $this );
	}
	
	
	/**
	 * @param string $id
	 * @return Components\Accordion
	 */
	public function accordion ( $id = null )
	{
		return ( new Components\Accordion ( $id ) )->setPresenter ( $this );
	}
	
	
	/**
	 * @param string $content
	 * @param string   $anchor
	 * @return Components\Modal
	 */
	public function modal ( $content, $anchor = null )
	{
		return new Components\Modal( $content, $anchor );
	}
	
	
	/**
	 * @return Components\Menu
	 */
	public function menu ( $menu = null )
	{
		return ( new Components\Menu ( $menu ) )->setPresenter ( $this );
	}
	
	
	public function message ( $content, $class = '' )
	{
		return $this->renderView ( 'callout', compact ( 'content', 'class' ) );
	}
	
	
	public function inputGroup ( $input, Button $button = null, $label = null )
	{
		if ( is_object ( $button ) )
		{
			$button->addClass ( 'input-group-button' );
		} else
		{
			$button = '';
		}
		return $this->renderView ( 'input-group', compact ( 'label', 'input', 'button' ) );
	}
	
	
	public function renderView ( $name, $data = [ ] )
	{
		return view ()->file ( $this->getViewsPath () . "/$name.blade.php", $data );
	}
	
	
	protected function getViewsPath ()
	{
		return __DIR__ . "/Components/views";
	}
}



