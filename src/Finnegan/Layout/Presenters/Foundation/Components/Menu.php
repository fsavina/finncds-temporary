<?php
namespace Finnegan\Layout\Presenters\Foundation\Components;


use Caffeinated\Menus\Item;
use Finnegan\Contracts\Layout\Components\Menu as MenuContract;


class Menu extends AbstractComponent implements MenuContract
{

	protected $menu;

	protected $dropdown = false;

	protected $accordion = false;

	protected $drilldown = false;

	protected $vertical = false;


	public function __construct ( $menu = null )
	{
		if ( is_array ( $menu ) or is_null ( $menu ) )
		{
			$menu = new \Finnegan\Layout\Menus\Menu( $menu );
		}
		$this -> menu = $menu;
	}


	public function add ( array $item, \Closure $callback = null )
	{
		$element = $this->menu->addArrayItem ( $item );

		if ( $callback instanceof \Closure )
		{
			$callback( $element );
		}

		return $this;
	}


	public function dropdown ()
	{
		$this -> accordion = $this -> drilldown = false;
		$this -> dropdown = true;
		return $this;
	}


	public function accordion ()
	{
		$this -> dropdown = $this -> drilldown = false;
		$this -> accordion = true;
		return $this -> vertical();
	}


	public function drilldown ()
	{
		$this -> dropdown = $this -> accordion = false;
		$this -> drilldown = true;
		return $this -> vertical();
	}


	public function vertical ()
	{
		$this -> vertical = true;
		return $this;
	}


	protected function reset (  )
	{
		$this -> dropdown = false;
		$this -> accordion = false;
		$this -> drilldown = false;
		$this -> vertical = false;
		return $this;
	}


	protected function containerClass ()
	{
		$class = [ 'menu' ];
		if ( $this->dropdown )
		{
			$class[] = 'dropdown';
		}
		if ( $this->vertical )
		{
			$class[] = 'vertical';
		}
		return implode ( ' ', $class );
	}


	public function render ()
	{
		$attributes = [
			'class' => $this->containerClass()
		];
		if ($this->dropdown)
		{
			$attributes ['data-dropdown-menu'] = '';
		} elseif ($this->accordion)
		{
			$attributes ['data-accordion-menu'] = '';
		} elseif ($this->drilldown)
		{
			$attributes ['data-drilldown'] = '';
		}

		$attributes = $this->html()->attributes ( $attributes );
		$render = "<ul{$attributes}>{$this -> renderItems ()}</ul>";
		$this -> reset();
		return $render;
	}


	protected function renderItems ()
	{
		$render = '';
		foreach ( $this->menu->roots () as $item )
		{
			$render .= $this -> renderItem ( $item );
		}
		return $render;
	}


	protected function renderItem ( Item $item )
	{
		$render = "<li{$item->attributes()}>";
		if ( $item -> link )
		{
			$render .= "<a href=\"{$item->url()}\">{$item->prependIcon()->title}</a>";
		} else
		{
			$render .= $item -> prependIcon () -> title;
		}

		$active = str_contains ( $item->attributes (), 'active' );
		$render .= $item -> hasChildren () ? $this -> renderSubmenu ( $item -> children (), $active ) : '';
		
		$render .= '</li>';
		
		if ( $item -> divider )
		{
			$render .= '<li' .$this->html()->attributes ( $item -> divider ) . '></li>';
		}
		return $render;
	}
	
	
	protected function renderSubmenu ( $items, $active = false )
	{
		$class = '';
		if ($this -> accordion)
		{
			$class .= ' nested';
		}
		if ( $active )
		{
			$class .= ' is-active';
		}
		$render = "<ul class=\"menu vertical{$class}\">";
		foreach ( $items as $item )
		{
			$render .= $this -> renderItem ( $item );
		}
		$render .= '</ul>';
		return $render;
	}

}