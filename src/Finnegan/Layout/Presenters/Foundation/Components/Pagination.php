<?php
namespace Finnegan\Layout\Presenters\Foundation\Components;


use Illuminate\Pagination\BootstrapThreePresenter;


class Pagination extends BootstrapThreePresenter
{


	protected function getDisabledTextWrapper ( $text )
	{
		return '<li class="disabled">' . $text . '</li>';
	}


	protected function getActivePageWrapper ( $text )
	{
		return '<li class="current">' . $text . '</li>';
	}


	protected function getDots ()
	{
		return '<li class="ellipsis" aria-hidden="true"></li>';
	}


	protected function getAvailablePageWrapper ( $url, $page, $rel = null )
	{
		$rel = is_null ( $rel ) ? '' : ' rel="' . $rel . '" class="arrow"';
		
		return '<li><a href="' . htmlentities ( $url ) . '"' . $rel . '>' . $page . '</a></li>';
	}

}



