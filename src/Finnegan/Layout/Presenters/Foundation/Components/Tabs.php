<?php
namespace Finnegan\Layout\Presenters\Foundation\Components;


use Finnegan\Contracts\Layout\Components\Tabs as TabsContract;


class Tabs extends AbstractComponent implements TabsContract
{

	protected $id;
	
	protected $tabs = [ ];
	
	
	public function __construct ( $id = null )
	{
		$this -> id = $id ?  : uniqid ();
	}
	
	
	public function add ( $label, $content, $id = null, $active = false )
	{
		$id = $id ?  : uniqid ();
		$this -> tabs [ $id ] = compact ( 'label', 'content', 'active' );
		return $this;
	}


	public function id ( $id )
	{
		$this -> id = $id;
		return $this;
	}


	public function render ()
	{
		$render = $this->renderView ( $this->getViewName (), $this->getViewData () );

		$this->reset ();
		
		return $render;
	}


	protected function getViewData ()
	{
		$id = $this->id;
		$tabs = $this->tabs;
		$html = $this->html ();

		return compact ( 'html', 'id', 'tabs' );
	}


	protected function getViewName (  )
	{
		return 'tabs';
	}
	
	
	protected function reset ()
	{
		$this -> id = null;
		$this -> tabs = [ ];
		return $this;
	}
	
}



