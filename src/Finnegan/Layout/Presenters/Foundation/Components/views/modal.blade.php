@if ($anchor)
    <a data-toggle="{{ $id }}_modal">{!! $anchor !!}</a>
@endif
<div class="reveal {{ implode(' ', $classes) }}" id="{{ $id }}_modal" data-reveal>
    {!! $content !!}
    @if($close_button)
        <button class="close-button" data-close aria-label="@lang('finnegan::actions.close')" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    @endif
</div>