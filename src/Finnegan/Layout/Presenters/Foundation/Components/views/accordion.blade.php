<ul class="accordion"
    data-accordion
    role="tablist"
    {!! $allowAllClosed ? 'data-allow-all-closed="true"' : '' !!}
    {!! $multiExpand ? 'data-multi-expand="true"' : '' !!}
>
    @foreach ( $tabs as $tabId => $tab )
        <li class="accordion-item {{ $tab['active'] ? 'is-active' : '' }}">
            <a href="#{{ $tabId }}" role="tab" class="accordion-title" id="{{ $tabId }}-heading" aria-controls="{{ $tabId }}">
                {!! $tab [ 'label' ] !!}
            </a>
            <div id="{{ $tabId }}" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="{{ $tabId }}-heading">
                {!! $tab [ 'content' ] !!}
            </div>
        </li>
    @endforeach
</ul>