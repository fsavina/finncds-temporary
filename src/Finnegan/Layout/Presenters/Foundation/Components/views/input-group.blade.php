<div class="input-group">
    @if($label) <span class="input-group-label">{!! $label !!}</span> @endif
    {!! $input !!}
    {!! $button !!}
</div>