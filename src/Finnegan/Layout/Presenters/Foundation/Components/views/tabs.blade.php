<ul class="tabs" data-tabs id="{{ $id }}-tabs">
    @foreach ( $tabs as $tabId => $tab )
        <li class="tabs-title {{ $tab['active'] ? 'is-active' : '' }}">
            {!! $html->link( "#{$tabId}", $tab ['label'], ( $tab['active'] ? ['aria-selected' => 'true'] : [] )) !!}
        </li>
    @endforeach
</ul>
<div class="tabs-content" data-tabs-content="{{ $id }}-tabs">
    @foreach ( $tabs as $tabId => $tab )
        <div class="tabs-panel {{ $tab['active'] ? 'is-active' : '' }}" id="{{ $tabId }}">
            {!! $tab['content'] !!}
        </div>
    @endforeach
</div>