<?php
namespace Finnegan\Layout\Presenters\Foundation\Components;


use Finnegan\Contracts\Layout\Components\Accordion as AccordionContract;


class Accordion extends Tabs implements AccordionContract
{


	protected $allowAllClosed = false;

	protected $multiExpand    = false;


	protected function getViewName ()
	{
		return 'accordion';
	}


	public function allowAllClosed ( $boolean )
	{
		$this->allowAllClosed = (bool) $boolean;
		return $this;
	}


	public function multiExpand ( $boolean )
	{
		$this->multiExpand = (bool) $boolean;
		return $this;
	}


	protected function getViewData ()
	{
		$data = parent::getViewData ();

		$data[ 'allowAllClosed' ] = $this->allowAllClosed;
		$data[ 'multiExpand' ] = $this->multiExpand;

		return $data;
	}


}



