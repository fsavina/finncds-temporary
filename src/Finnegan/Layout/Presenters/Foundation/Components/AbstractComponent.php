<?php
namespace Finnegan\Layout\Presenters\Foundation\Components;


use Finnegan\Layout\Components\AbstractComponent as StandardComponent;


abstract class AbstractComponent extends StandardComponent
{
	
	protected function getViewsPath ()
	{
		return __DIR__ . "/views";
	}

}