<?php
namespace Finnegan\Layout\Presenters\Foundation\Components;


/**
 * @method \Finnegan\Layout\Presenters\Foundation\Components\Button small
 * @method \Finnegan\Layout\Presenters\Foundation\Components\Button tiny
 * @method \Finnegan\Layout\Presenters\Foundation\Components\Button large
 * @method \Finnegan\Layout\Presenters\Foundation\Components\Button success
 * @method \Finnegan\Layout\Presenters\Foundation\Components\Button warning
 * @method \Finnegan\Layout\Presenters\Foundation\Components\Button secondary
 * @method \Finnegan\Layout\Presenters\Foundation\Components\Button alert
 * @method \Finnegan\Layout\Presenters\Foundation\Components\Button disabled
 * @method \Finnegan\Layout\Presenters\Foundation\Components\Button expanded
 * @method \Finnegan\Layout\Presenters\Foundation\Components\Button radius
 * @method \Finnegan\Layout\Presenters\Foundation\Components\Button round
 * @method \Finnegan\Layout\Presenters\Foundation\Components\Button hide
 * @method \Finnegan\Layout\Presenters\Foundation\Components\Button hollow
 */
class Button extends \Finnegan\Layout\Components\Button
{
	
	protected $classWhitelist = [
		'tiny',
		'small',
		'large',
		'expanded',
		
		'alert',
		'success',
		'warning',
		'secondary',
		'disabled',
		'hollow',
		
		'dropdown',
		'hide'
	];
	
}



