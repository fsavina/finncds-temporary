<?php
namespace Finnegan\Layout\Presenters\Foundation\Components;


use Finnegan\Contracts\Layout\Components\Modal as ModalContract;


class Modal extends AbstractComponent implements ModalContract
{
	
	protected $id;
	
	protected $content;
	
	protected $anchor;
	
	protected $closeButton = true;
	
	protected $classes = [];
	
	
	public function __construct ( $content, $anchor = null )
	{
		$this->content = $content;
		$this->anchor = $anchor;
	}
	
	
	public function render ()
	{
		$view = $this->renderView ( 'modal', [
			'id'           => $this->id ?: uniqid (),
			'content'      => $this->content,
			'anchor'       => $this->anchor,
			'classes'      => $this->classes,
			'close_button' => $this->closeButton
		] );
		
		$this->reset ();
		
		return $view;
	}
	
	
	public function id ( $id )
	{
		$this->id = $id;
		return $this;
	}
	
	
	public function content ( $content )
	{
		$this->content = $content;
		return $this;
	}
	
	
	public function anchor ( $anchor )
	{
		$this->anchor = $anchor;
		return $this;
	}
	
	
	public function showCloseButton ( $bool )
	{
		$this->closeButton = (bool) $bool;
		return $this;
	}
	
	
	public function addClass ( $class )
	{
		$this->classes[] = $class;
		return $this;
	}
	
	
	protected function reset ()
	{
		$this->id = null;
		$this->content = null;
		$this->anchor = null;
		$this->closeButton = true;
		$this->classes = [];
	}
}