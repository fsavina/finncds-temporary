<?php

namespace Finnegan\Layout\Menus;


use Finnegan\Contracts\Modules\Module;
use Finnegan\Definitions\ModelDefinition;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\Arr;


class ModuleMenu extends Menu
{
	
	protected $module;
	
	protected $config = [];
	
	
	public function __construct ( $name, Module $module )
	{
		$this->name = $name;
		$this->module = $module;
		
		$this->config = array_merge (
			$this->config,
			$module->config ( 'menus.config', [] ),
			$module->config ( "menus.instances.$name.config", [] )
		);
		
		parent:: __construct ( $name, $this->extractItems () );
	}
	
	
	/**
	 * @param string $name
	 * @param Module $module
	 * @return ModuleMenu
	 */
	public static function make ( $name, Module $module )
	{
		return new static ( $name, $module );
	}
	
	
	protected function configItems ()
	{
		return $this->module->config ( "menus.instances.{$this->name}.items", [] );
	}
	
	
	/**
	 * @return array
	 */
	protected function extractItems ()
	{
		$cacheTime = $this->cacheTime ();
		
		$cache = $this->module->make ( Repository::class );
		
		if ( $cacheTime and $cache->has ( $this->cacheName () ) )
		{
			return $cache->get ( $this->cacheName () );
		}
		
		$items = $this->resolveItems ( $this->configItems () );
		if ( $cacheTime )
		{
			$cache->put ( $this->cacheName (), $items, $cacheTime );
		}
		return $items;
	}
	
	
	/**
	 * @param array $config
	 * @return array
	 */
	protected function resolveItems ( array $config )
	{
		$items = [];
		foreach ( $config as $item )
		{
			if ( is_array ( $menuItem = $this->resolveItem ( $item ) ) )
			{
				if ( count ( $subItems = array_get ( $item, 'items', [] ) ) )
				{
					$menuItem[ 'submenu' ] = $this->resolveItems ( $subItems );
				}
				
				// If there is a submenu and is empty we skip the element
				if ( ! isset ( $menuItem [ 'submenu' ] ) or ( is_array ( $menuItem [ 'submenu' ] ) and count ( $menuItem [ 'submenu' ] ) ) )
				{
					$items [] = $menuItem;
				}
			}
		}
		
		return $items;
	}
	
	
	protected function resolveItem ( array $item )
	{
		$url = $label = $icon = $activeUrlPattern = null;
		
		if ( $model = Arr::get ( $item, 'model' ) )
		{
			if ( $this->module->deny ( $model ) )
			{
				// Model disabled for the current module
				return false;
			}
			
			if ( ! is_object ( $definition = $this->loadDefinition ( $model ) ) )
			{
				// Definition not available
				return false;
			}
			
			$action = Arr::get ( $item, 'action', 'index' );
			
			$gate = $this->module->make ( GateContract::class );
			$factory = $this->module->make ( 'models.factory' );
			
			if ( $gate->denies ( $action, $factory->load ( $definition ) ) )
			{
				// User not authorized to access the required action
				return false;
			}
			
			$params = Arr::get ( $item, 'params', [] );
			$params [ 'model' ] = $model;
			$url = $this->module->actionUrl ( $action, $params );
			
			$label = $definition->plural ();
			$icon = $definition->icon ();
			
			$activeUrlPattern = $this->module->actionUrl ( 'index', [ 'model' => $model ] ) . '/*';
			$activeUrlPattern = parse_url ( $activeUrlPattern, PHP_URL_PATH );
			
		} elseif ( $route = Arr::get ( $item, 'route' ) )
		{
			$action = is_array ( $route ) ? Arr::get ( $route, 'name' ) : $route;
			$params = is_array ( $route ) ? Arr::get ( $route, 'params', [] ) : [];
			
			$url = $this->module->actionUrl ( $action, $params );
			$activeUrlPattern = parse_url ( $url, PHP_URL_PATH );
		}
		
		if ( Arr::has ( $item, 'auth' ) )
		{
			$guard = $this->module->make ( Guard::class );
			if ( $role = Arr::get ( $item, 'auth.role' ) and ! $guard->user ()->achieve ( $role ) )
			{
				// User not authorized
				return false;
			}
			if ( $ability = Arr::get ( $item, 'auth.ability' ) and ! $guard->user ()->hasAbility ( $ability ) )
			{
				// User not authorized
				return false;
			}
		}
		
		return [
			'label'      => trans ( Arr::get ( $item, 'label', $label ) ),
			'url'        => Arr::get ( $item, 'url', $url ),
			'icon'       => Arr::get ( $item, 'icon', $icon ),
			'divider'    => Arr::get ( $item, 'divider', false ),
			'active-url' => Arr::get ( $item, 'active-url', $activeUrlPattern )
		];
	}
	
	
	/**
	 * @param string $model
	 * @return ModelDefinition
	 */
	protected function loadDefinition ( $model )
	{
		try
		{
			return $this->module->make ( 'definitions.factory', [ $this->module ] )->load ( $model );
		} catch ( \Exception $e )
		{
		}
		return false;
	}
	
	
	public function config ( $key, $default = false )
	{
		return array_get ( $this->config, $key, $default );
	}
	
	
	protected function cacheTime ()
	{
		$debug = $this->module->make ( 'config' )->get ( 'app.debug' );
		return $debug ? 0 : $this->config ( 'cacheTime' );
	}
	
	
	protected function cacheName ()
	{
		return "finnegan.menus.{$this->module->name()}.{$this->name}";
	}
	
	
}



