<?php
namespace Finnegan\Layout\Menus;


use Caffeinated\Menus\Builder;


class Menu extends Builder
{

	public function __construct ( $name = null, array $items = null )
	{
		if ( is_array ( $name ) )
		{
			$items = $name;
			$name = null;
		}

		if ( is_null ( $name ) )
		{
			$name = uniqid ();
		}

		if ( is_null ( $items ) )
		{
			$items = [ ];
		}

		parent::__construct ( $name, [ 'rest_base' => '' ], app ( 'html' ), app ( 'url' ) );

		$this->build ( $this, $items );
	}


	/**
	 * @param Menu|\Caffeinated\Menus\Item $menu
	 * @param array                        $items
	 */
	protected function build ( $menu, array $items )
	{
		foreach ( $items as $item )
		{
			$this->addArrayItem ( $item, $menu );
		}
	}


	public function addArrayItem ( array $item, $menu = null )
	{
		$menu = $menu ?: $this;

		$label = array_get ( $item, 'label', ':missing-label:' );

		$class = array_get ( $item, 'class', [ ] );
		$class = is_string ( $class ) ? explode ( ' ', $class ) : $class;

		$options = [ ];
		if ( $url = array_get ( $item, 'url', '' ) )
		{
			$options [ 'url' ] = $url;
		} else
		{
			$options [ 'raw' ] = true;
			$class [] = 'menu-text';
		}

		$menuItem = $menu->add ( $label, $options );

		if ( $icon = array_get ( $item, 'icon' ) )
		{
			$icons = app ( 'Finnegan\Contracts\Layout\Manager' )->presenter()->icons();
			$menuItem->icon ( $icons->resolve ( $icon ) );
		}
		if ( $image = array_get ( $item, 'image' ) )
		{
			$menuItem->prepend ( $this->html->image ( $image ) );
		}
		if ( array_get ( $item, 'divider' ) )
		{
			$menuItem->divide ();
		}
		if ( $activeUrl = array_get ( $item, 'active-url', false ) )
		{
			$menuItem->active ( $activeUrl );
		}

		if ( array_has ( $item, 'submenu' ) )
		{
			$class [] = 'has-submenu';
			$this->build ( $menuItem, array_get ( $item, 'submenu' ) );
		}
		$menuItem->attribute ( 'class', implode ( ' ', $class ) );
		return $menuItem;
	}

}