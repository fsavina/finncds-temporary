<?php
namespace Finnegan\Layout;


use Bkwld\Croppa\ServiceProvider;
use Caffeinated\Menus\MenusServiceProvider;
use Collective\Html\HtmlServiceProvider;
use Finnegan\Contracts\Layout\AssetManager;
use Finnegan\Contracts\Layout\IconPresenter;
use Finnegan\Contracts\Layout\Manager;
use Finnegan\Contracts\Layout\Presenter;
use Finnegan\Support\BaseServiceProvider;


class LayoutServiceProvider extends BaseServiceProvider
{
	
	
	protected $defer = true;


	public function register ()
	{
		$this -> app -> register ( MenusServiceProvider::class );
		$this -> app -> register ( HtmlServiceProvider::class );
		$this -> app -> register ( ServiceProvider::class );
		
		$this -> app -> singleton ( Manager::class, LayoutManager::class );
		$this -> app -> singleton ( AssetManager::class, Assets\AssetManager::class );

		$this -> app -> singleton ( Presenter::class, Presenters\Foundation\FoundationPresenter::class );
		$this -> app -> singleton ( IconPresenter::class, Presenters\FontAwesome\FontAwesomeIconPresenter::class );
	}


	public function boot ()
	{
		$this->loadViewsFrom ( static::resourcesPath () . 'themes', 'finnegan' );
		$this->loadViewsFrom ( static::sourcePath () . 'Finnegan/Widgets/views', 'finnegan-widgets' );

		$path = static::resourcesPath () . 'assets/';
		
		$this -> publishes ( [
			$path . 'vendor' => public_path ( 'assets/vendor' ),
			$path . 'js' => public_path ( 'assets/finnegan/js' ),
			$path . 'css' => public_path ( 'assets/finnegan/css' )
		], 'public' );
	}


	public function provides ()
	{
		return [
			Manager::class,
			Presenter::class,
			AssetManager::class
		];
	}

}


