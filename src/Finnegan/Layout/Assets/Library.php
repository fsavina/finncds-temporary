<?php
namespace Finnegan\Layout\Assets;


use Finnegan\Contracts\Layout\AssetManager as AssetManagerContract;
use Illuminate\Support\Arr;


class Library
{

	protected $name;

	protected $config;


	public function __construct ( $name )
	{
		$this -> name = $name;
		$this -> config = config ( "finnegan.assets.libraries.$name" );

		if ( ! $this -> config )
		{
			throw new \Exception ( "'$name' is not a valid asset library name" );
		}
	}


	public function register ( AssetManagerContract $manager, $params = [] )
	{
		if ( isset ( $this -> config [ 'requires' ] ) )
		{
			$manager -> requireResource ( $this -> config [ 'requires' ] );
		}

		if ( isset ( $this -> config [ 'css' ] ) )
		{
			$manager -> requireResource ( $this -> config [ 'css' ], ['type' => 'css'] );
		}

		if ( isset ( $this -> config [ 'js' ] ) )
		{
			$context = Arr::get ( $this -> config, 'context', AssetManagerContract :: DEFAULT_CONTEXT );
			$context = Arr::get ( $params, 'context', $context );
			
			$extraParams = [];
			if ( isset ( $this -> config [ 'params' ] ) )
			{
				$extraParams = Arr::only ( $params, ( array ) $this -> config [ 'params' ] );
			}
			
			if ( count ( $extraParams ) )
			{
				$urls = ( array ) $this -> config [ 'js' ];
				foreach ( $urls as $js )
				{
					$js .= ( ( strpos ( $js, '?' ) === false ) ? '?' : '&' ) . http_build_query ( $extraParams );
					$manager -> requireResource ( $js, [ 'type' => 'js', 'context' => $context ] );
				}
			} else
			{
				$manager -> requireResource ( $this -> config [ 'js' ], [
					'type' => 'js',
					'context' => $context
				] );
			}
		}
		
		if ( isset ( $this -> config [ 'script' ] ) )
		{
			$manager -> requireResource ( $this -> config [ 'script' ], ['type' => 'script'] );
		}
	}

}



