<?php

namespace Finnegan\Layout\Assets;


use Collective\Html\HtmlBuilder;
use Finnegan\Contracts\Modules\Module as ModuleContract;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;


/**
 * https://github.com/Stolz/Assets/blob/master/src/Laravel/config.php
 */
class AssetManager implements \Finnegan\Contracts\Layout\AssetManager
{
	
	/**
	 * @var array
	 */
	protected $registry = [];
	
	/**
	 * @var ModuleContract
	 */
	protected $module;
	
	/**
	 * @var HtmlBuilder
	 */
	protected $html;
	
	/**
	 * @var UrlGenerator
	 */
	protected $url;
	
	
	public function __construct ( ModuleContract $module, HtmlBuilder $html, UrlGenerator $url, Request $request )
	{
		$this->module = $module;
		$this->html = $html;
		$this->url = $url;
		$this->request = $request;
	}
	
	
	public function requireResource ( $resource, $params = [] )
	{
		$resources = ( array ) $resource;
		
		$moduleCdn = $this->module->config ( 'layout.cdn' );
		
		foreach ( $resources as $resource )
		{
			$type = isset ( $params [ 'type' ] ) ? $params [ 'type' ] : $this->guessType ( $resource );
			
			switch ( $type )
			{
				case 'lib':
					( new Library ( $resource ) )->register ( $this, $params );
					break;
				case 'css':
					$resource = Arr::get ( $params, 'version' ) ? $this->versioned ( $resource ) : $resource;
					
					if ( $cdn = Arr::get ( $params, 'cdn', $moduleCdn ) )
					{
						$resource = $this->assetUrlFromCdn ( $resource, $cdn );
					}
					
					$this->register ( 'head.css', $resource );
					break;
				case 'js':
					$resource = Arr::get ( $params, 'version' ) ? $this->versioned ( $resource ) : $resource;
					
					if ( $cdn = Arr::get ( $params, 'cdn', $moduleCdn ) )
					{
						$resource = $this->assetUrlFromCdn ( $resource, $cdn );
					}
				case 'script':
					$context = Arr::get ( $params, 'context', AssetManager :: DEFAULT_CONTEXT );
					$this->register ( "{$context}.{$type}", $resource );
					break;
				default:
					break;
			}
		}
		return $this;
	}
	
	
	protected function versioned ( $resources )
	{
		if ( ! file_exists ( public_path ( 'build/rev-manifest.json' ) ) )
		{
			// Missing manifest, nothing to do
			return $resources;
		}
		
		$resources = (array) $resources;
		
		$versioned = [];
		foreach ( $resources as $resource )
		{
			$versioned[] = elixir ( $resource );
		}
		
		return $versioned;
	}
	
	
	protected function register ( $key, $value )
	{
		$array = Arr::get ( $this->registry, $key, [] );
		
		if ( is_array ( $value ) )
		{
			$array = array_merge ( $array, $value );
		} else
		{
			array_push ( $array, $value );
		}
		
		Arr::set ( $this->registry, $key, array_unique ( $array ) );
	}
	
	
	protected function guessType ( $resource, $default = 'lib' )
	{
		$ext = pathinfo ( $resource, PATHINFO_EXTENSION );
		return in_array ( $ext, [ 'js', 'css' ] ) ? $ext : $default;
	}
	
	
	public function render ( $context = AssetManager :: DEFAULT_CONTEXT )
	{
		$render = [ 'css' => '', 'js' => '', 'script' => '' ];
		
		if ( isset ( $this->registry [ $context ] ) )
		{
			foreach ( $this->registry [ $context ] as $type => $resources )
			{
				switch ( $type )
				{
					case 'css':
						$render [ $type ] = $this->css ( $resources );
						break;
					case 'js':
						$render [ $type ] = $this->js ( $resources );
						break;
					case 'script':
						$render [ 'script' ] .= '<script>' . trim ( implode ( PHP_EOL, $resources ) ) . '</script>';
						break;
					default:
						break;
				}
			}
		}
		return implode ( PHP_EOL, $render );
	}
	
	
	protected function css ( $url )
	{
		if ( is_array ( $url ) )
		{
			$render = '';
			foreach ( $url as $css )
			{
				$render .= $this->css ( $css );
			}
			return $render;
		}
		
		if ( ! $this->url->isValidUrl ( $url ) )
		{
			$url = '/' . trim ( $url, '/' );
		}
		
		$attributes = [
			'href'  => $url,
			'media' => 'all',
			'type'  => 'text/css',
			'rel'   => 'stylesheet'
		];
		
		return new HtmlString( '<link' . $this->html->attributes ( $attributes ) . '>' . PHP_EOL );
	}
	
	
	protected function js ( $url )
	{
		if ( is_array ( $url ) )
		{
			$render = '';
			foreach ( $url as $js )
			{
				$render .= $this->js ( $js );
			}
			return $render;
		}
		
		if ( ! $this->url->isValidUrl ( $url ) )
		{
			$url = '/' . trim ( $url, '/' );
		}
		
		return new HtmlString( '<script' . $this->html->attributes ( [ 'src' => $url ] ) . '></script>' . PHP_EOL );
	}
	
	
	protected function assetUrlFromCdn ( $url, $cdn )
	{
		if ( is_array ( $url ) )
		{
			$assets = [];
			foreach ( $url as $item )
			{
				$assets[] = $this->assetUrlFromCdn ( $item, $cdn );
			}
			return $assets;
		} elseif ( $this->url->isValidUrl ( $url ) )
		{
			return $url;
		}
		return $this->url->assetFrom ( $cdn, $url );
	}
	
}



