<?php
namespace Finnegan\Models;


use Dimsav\Translatable\TranslatableServiceProvider;
use Finnegan\Support\BaseServiceProvider;


class ModelsServiceProvider extends BaseServiceProvider
{

	protected $defer = true;


	public function register ()
	{
		$this->shouldCompile ();

		$this -> app -> register ( TranslatableServiceProvider::class );
		$this -> app -> singleton ( 'models.factory', ModelsFactory::class );
		
		$this->commands ( Console\ModelMakeCommand::class );
	}


	public function provides ()
	{
		return [ 'models.factory' ];
	}
	
	
	public static function compiles ()
	{
		return [
			realpath ( __DIR__ . '/Model.php' ),
			realpath ( __DIR__ . '/ModelsFactory.php' ),
			realpath ( __DIR__ . '/Traits/ActionsTrait.php' ),
			realpath ( __DIR__ . '/Traits/DefinitionTrait.php' ),
			realpath ( __DIR__ . '/Traits/EventsTrait.php' ),
			realpath ( __DIR__ . '/Traits/FieldsTrait.php' ),
			realpath ( __DIR__ . '/Traits/FiltersTrait.php' ),
			realpath ( __DIR__ . '/Traits/FormTrait.php' ),
			realpath ( __DIR__ . '/Traits/RelationsTrait.php' ),
			realpath ( __DIR__ . '/Traits/SortingTrait.php' ),
			realpath ( __DIR__ . '/Scopes/SearchScope.php' ),
			realpath ( __DIR__ . '/Scopes/SortScope.php' ),
			realpath ( __DIR__ . '/Scopes/StatusScope.php' ),
			realpath ( __DIR__ . '/FieldCollection.php' ),
			realpath ( __DIR__ . '/Fields/Field.php' ),
		];
	}

}


