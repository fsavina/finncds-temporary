<?php
namespace Finnegan\Models;


use Finnegan\Translation\TranslationsManager;
use Illuminate\Support\Arr;


class Translatable extends Model
{


	/**
	 * @var TranslationsManager
	 */
	protected $manager;


	public function setup ( TranslationsManager $manager )
	{
		$this->manager = $manager;

		if ( ! $this->definitionInstance->hasComponent ( 'translatable' ) )
		{
			throw new \Exception ( 'Missing required component: translatable' );
		}
	}


	public function recordActionUrl ( $action, array $params = [ ] )
	{
		if ( $action == 'translate' )
		{
			$params = Arr::add ( $params, 'parent', $this->translates );
			return $this->actionUrl ( $action, $params );
		}
		return parent:: recordActionUrl ( $action, $params );
	}


	public function onCreated ()
	{
		if ( ! $this->translates and $this->exists )
		{
			$this->translates = $this->getKey ();
			$this->save ();
		}
	}


	public function onDeleting ()
	{
		if ( $this->exists )
		{
			$translations = $this->where ( 'translates', $this->getKey () )
								 ->where ( $this->primaryKey, '!=', $this->getKey () );

			if ( count ( $translations->get () ) )
			{
				if ( $this->getKey () != $this->translates )
				{
					$translates = $this->translates;
				} else
				{
					$translates = $translations->first ()->getKey ();
				}

				$translations->update ( [ 'translates' => $translates ] );
			}
		}
	}


	public function availableLocales ( $field = null, $excludeMine = false )
	{
		$locales = $this->manager->languages ();

		$locale = ( $this->exists and ! $excludeMine ) ? $this->locale : null;

		$translations = $this->where ( 'translates', $this->translates )->get ();
		foreach ( $translations as $translation )
		{
			if ( $translation->locale != $locale )
			{
				Arr::forget ( $locales, $translation->locale );
			}
		}

		return $locales;
	}

}



