<?php
namespace Finnegan\Models;


use Finnegan\Definitions\ModelDefinition;
use Finnegan\Support\LoadingContext;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;


class Model
	extends
		\Illuminate\Database\Eloquent\Model
	implements
		\Finnegan\Contracts\Models\Actionable,
		\Finnegan\Contracts\Models\Fields,
		\Finnegan\Contracts\Models\Formable,
		\Finnegan\Contracts\Models\Migratable,
		\Finnegan\Contracts\Models\Titlable
{

	use Traits\ActionsTrait,
		Traits\DefinitionTrait,
		Traits\EventsTrait,
		Traits\FieldsTrait,
		Traits\FiltersTrait,
		Traits\FormTrait,
		Traits\SchemaTrait,
		Traits\SortingTrait,
		Traits\RelationsTrait;
	

	const STATUS_DRAFT          = 0;

	const STATUS_PENDING_REVIEW = 1;

	const STATUS_PUBLISHED      = 2;

	const STATUS_TRASHED        = 3;
	
	const ACTION_DRAFT          = 'save_as_draft';
	
	const ACTION_REVIEW         = 'send_to_review';
	
	const ACTION_PUBLISH        = 'publish';
	
	
	/**
	 * Number of elements per page
	 * @var integer
	 */
	protected $perPage = 10;

	/**
	 * @var boolean
	 */
	public $timestamps = false;
	
	
	/**
	 * Model constructor.
	 * @param array $attributes
	 * @param bool  $bare
	 */
	public function __construct ( array $attributes = [ ], $bare = false, LoadingContext $context = null )
	{
		if ( ! $bare )
		{
			$this->setDefinition ( $this->loadDefinition ( $context ) );
		}
		parent:: __construct ( $attributes );
	}


	protected static function bare ( array $attributes = [ ] )
	{
		return new static ( $attributes, true );
	}


	/**
	 * @return \Finnegan\Definitions\ModelDefinition
	 */
	protected function loadDefinition ( LoadingContext $context = null )
	{
		$definition = $this->definition ?: static::resolveDefinitionName ();
		$factory = app ( 'definitions.factory' );
		return is_null ( $context ) ? $factory->load ( $definition ) : $factory->withContext ( $context, $definition );
	}


	protected function registerEvents ()
	{
		foreach ( get_class_methods ( $this ) as $method )
		{
			if ( substr ( $method, 0, 2 ) == 'on' )
			{
				$event = Str :: snake ( substr ( $method, 2 ) );
				$this -> registerEvent ( $event, [ $this, $method ] );
			}
		}
	}


	protected static function resolveDefinitionName ( $class = null )
	{
		$class = ( new \ReflectionClass( $class ?: get_called_class () ) )->getShortName ();
		return Str::snake ( Str::plural ( $class ) );
	}


	/**
	 * @param $id
	 * @return Model
	 */
	public function copyPreset ( $id )
	{
		$copy = $this -> newQueryWithoutScopes () -> findOrFail ( $id );
		$copy -> exists = false;

		foreach ( $this -> fields -> ifConfig('copyable', false) as $name => $field )
		{
			$copy -> $name = null;
		}
		$copy->syncOriginal ();
		return $copy;
	}
	

	public function timestamps ()
	{
		return (bool) $this->timestamps;
	}
	
	
	public function __call ( $method, $parameters )
	{
		if ( $this -> isRelation ( $method ) or $this -> isRelationField ( $method ) )
		{
			return $this -> relation ( $method );
		}
		return parent :: __call ( $method, $parameters );
	}


	/**
	 * Create a new Eloquent query builder for the model.
	 * @param \Illuminate\Database\Query\Builder $query
	 * @return Builder
	 */
	public function newEloquentBuilder ( $query )
	{
		return Drivers\Manager::getInstance ( $this )->driver ( $query );
	}


	public function newQuery ()
	{
		$builder = parent :: newQuery ();
		if ( $this->definition ()->hasComponent ( 'status' ) )
		{
			( new Scopes\StatusScope() )->apply ( $builder, $this );
		}
		return $builder;
	}


	/**
	 * Create a collection of models from plain arrays.
	 * @param array           $items
	 * @param string|null     $connection
	 * @param ModelDefinition $definition
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public static function hydrate ( array $items, $connection = null, ModelDefinition $definition = null )
	{
		$instance = static::bare ()->setDefinition ( $definition )->setConnection ( $connection );

		$items = array_map ( function  ( $item ) use( $instance ) {
			return $instance -> newFromBuilder ( $item );
		}, $items );

		return $instance -> newCollection ( $items );
	}


	/**
	 * @param array   $attributes
	 * @param boolean $exists
	 * @return \Finnegan\Models\Model
	 */
	public function newInstance ( $attributes = [], $exists = false )
	{
		$model = ( new static ( ( array ) $attributes, true ) )->setDefinition ( $this->definition () );
		$model -> exists = $exists;
		return $model;
	}
}




