<?php
namespace Finnegan\Models\Traits;


use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;


trait FormTrait
{

	/**
	 * The current model validator
	 * @var \Illuminate\Validation\Validator
	 */
	protected $validator;


	public function formConfig ()
	{
		$config = [ 'files' => true, 'url' => $this -> actionUrl ( 'store' ) ];
		if ( $this -> exists )
		{
			$config [ 'url' ] = $this -> recordActionUrl ( 'update' );
			$config [ 'method' ] = 'PUT';
		}
		return $config;
	}


	public function fromRequest ( Request $request, $action = 'store' )
	{
		$input = $request -> input ( $this -> name (), [ ] );
		$files = $request -> file ( $this -> name (), [ ] );
		if ( $this -> fromForm ( $input, $files ) )
		{
			return $this -> saveAs ( $action );
		}
		return false;
	}


	/**
	 * Validate incoming data and fill the current model
	 * @param array $input
	 * @param array $files
	 * @return boolean
	 */
	public function fromForm ( array $input, array $files = [] )
	{
		foreach ( $this -> formFields () as $name => $field )
		{
			if ( Arr::has ( $input, $name ) )
			{
				Arr::set ( $input, $name, $field -> filterRaw ( Arr::get ( $input, $name ) ) );
			}
		}
		
		$validator = $this -> validator ( $input, $files );
		if ( $validator -> passes () )
		{
			$input = $validator->valid () + $files;
			foreach ( $this -> formFields () as $name => $field )
			{
				$field -> setValue ( $field -> filter ( Arr::get ( $input, $name ) ) );
			}
			return true;
		}
		return false;
	}


	/**
	 * Get all the validation rules
	 * @return array
	 */
	public function rules ()
	{
		$rules = array ();
		foreach ( $this -> formFields () as $name => $field )
		{
			if ( $field -> isTranslatable () )
			{
				foreach ( $this->getLocales () as $locale => $label )
				{
					$rules[ "$locale.$name" ] = $field->rules ();
				}
			} else
			{
				$rules [ $name ] = $field -> rules ();
			}
		}
		return $rules;
	}


	/**
	 * Get the model validator instance
	 * @param array $data
	 * @param array $files
	 * @return \Illuminate\Validation\Validator
	 */
	public function validator ( array $data = null, array $files = null )
	{
		if ( is_null ( $this->validator ) )
		{
			$this->validator = Validator:: make ( [], $this->rules () );
		}
		if ( is_null ( $data ) )
		{
			$data = [];
		}
		if ( ! is_null ( $files ) )
		{
			$data += $files;
		}
		$this->validator->setData ( $data );
		return $this->validator;
	}


	protected function getErrorsBag ()
	{
		if ( Session :: has ( 'errors' ) )
		{
			return Session :: get ( 'errors' ) -> getBag ( $this -> name () );
		}
		return null;
	}


	public function withSessionErrors ()
	{
		$bag = $this -> getErrorsBag ();
		if ( is_object ( $bag ) and $bag -> any () )
		{
			foreach ( $bag -> getMessages () as $field => $message )
			{
				if ( $this -> hasField ( $field ) )
				{
					$this -> fields ( $field ) -> setErrors ( $message );
				}
			}
		}
		return $this;
	}

}


