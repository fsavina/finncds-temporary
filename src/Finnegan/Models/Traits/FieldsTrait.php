<?php

namespace Finnegan\Models\Traits;


use Finnegan\Models\Fields\Field;
use Finnegan\Models\FieldCollection;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Str;


trait FieldsTrait
{
	
	
	/**
	 * Current model field instances
	 * @var \Finnegan\Models\FieldCollection
	 */
	protected $fields;
	
	/**
	 * Registerd relation fields
	 * @var array
	 */
	protected $relationFields = [];
	
	
	/**
	 * Load the field instances
	 * @return \Finnegan\Models\Model
	 */
	protected function loadFields ()
	{
		$instances = [];
		
		$fields = $this->definitionInstance->fields ();
		
		$fillable = $guarded = $hidden = $visible = [];
		foreach ( $fields as $name => $config )
		{
			$instance = Field:: make ( $name, $this );
			if ( $this->definitionInstance->isUnguarded () or $instance->authorize () )
			{
				$instances [ $name ] = $instance;
				
				if ( $instance->config ( 'guarded' ) )
				{
					$guarded [] = $name;
				} elseif ( $instance->config ( 'fillable' ) )
				{
					$fillable [] = $name;
				}
				
				if ( $instance->config ( 'hidden' ) )
				{
					$hidden [] = $name;
				} elseif ( $instance->config ( 'visible' ) )
				{
					$visible [] = $name;
				}
			}
		}
		
		if ( count ( $fillable ) )
		{
			$this->fillable ( $fillable );
		} elseif ( count ( $guarded ) )
		{
			$this->guard ( $guarded );
		}
		
		if ( count ( $visible ) )
		{
			$this->setVisible ( $visible );
		} elseif ( count ( $hidden ) )
		{
			$this->setHidden ( $hidden );
		}
		
		$this->fields = FieldCollection:: make ( $instances );
		
		return $this;
	}
	
	
	public function newQueryWithoutScopes ()
	{
		$query = parent:: newQueryWithoutScopes ();
		foreach ( $this->fields->hasMethod ( 'newQuery' ) as $field )
		{
			$query = $field->newQuery ( $query );
		}
		return $query;
	}
	
	
	/**
	 * Check if the given attribute exists in the current model instance
	 * @param string $attribute
	 * @return boolean
	 */
	public function hasAttribute ( $attribute )
	{
		return array_key_exists ( $attribute, $this->attributes );
	}
	
	
	/**
	 * Get the list of the fields or a subset of them
	 * @param string|array $names
	 * @param boolean      $sort
	 * @return \Finnegan\Models\FieldCollection|\Finnegan\Models\Fields\Field
	 */
	public function fields ( $names = null, $sort = false )
	{
		if ( is_string ( $names ) )
		{
			return $this->fields->get ( $names, false );
		}
		if ( is_array ( $names ) and count ( $names ) )
		{
			return $this->fields->subset ( $names, $sort );
		}
		return $this->fields;
	}
	
	
	/**
	 * Check if the given field exists
	 * @param string $name
	 * @return boolean
	 */
	public function hasField ( $name )
	{
		return $this->fields->has ( $name );
	}
	
	
	/**
	 * Get the fields to show in the form
	 * @return \Finnegan\Models\FieldCollection
	 */
	public function formFields ()
	{
		return $this->fields->showInForm ();
	}
	
	
	protected function isRelationField ( $field )
	{
		return $this->fields->ifInstanceOf ( 'Finnegan\Contracts\Fields\Relation' )->has ( $field );
	}
	
	
	/**
	 * Register a new date field
	 * @param string $field
	 */
	public function addDateField ( $field )
	{
		$this->dates [] = $field;
	}
	
	
	/**
	 * Define a casting for an attribute
	 * @param string $attribute
	 * @param string $cast
	 */
	public function addCast ( $attribute, $cast )
	{
		$this->casts [ $attribute ] = $cast;
	}
	
	
	/**
	 * Register a new field with external relationship
	 * @param string $name
	 * @param string $field
	 */
	public function addRelationField ( $name, $field )
	{
		$this->relationFields [ $name ] = $field;
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see \Illuminate\Database\Eloquent\Model::getAttribute()
	 */
	public function getAttribute ( $key )
	{
		$result = parent:: getAttribute ( $key );
		
		if ( is_null ( $result ) and array_key_exists ( $key, $this->relationFields ) )
		{
			return $this->getRelationshipFromField ( $key );
		}
		
		return $result;
	}
	
	
	/**
	 * Load the required relationship from the registered field
	 * @param string $name
	 * @throws \LogicException
	 * @return mixed
	 */
	protected function getRelationshipFromField ( $name )
	{
		$field = $this->relationFields [ $name ];
		$relations = $this->fields ( $field )->related ();
		
		if ( ! $relations instanceof Relation )
		{
			throw new \LogicException( 'Relationship field "' . $field . '" must return an object of type '
									   . 'Illuminate\Database\Eloquent\Relations\Relation' );
		}
		
		return $this->relations[ $name ] = $relations->getResults ();
	}
	
	
	/**
	 * @param string $mode
	 * @return boolean
	 */
	public function saveAs ( $mode )
	{
		switch ( $mode )
		{
			case static :: ACTION_DRAFT :
				$this->status = static :: STATUS_DRAFT;
				break;
			case static :: ACTION_REVIEW :
				$this->status = static :: STATUS_PENDING_REVIEW;
				break;
			case static :: ACTION_PUBLISH :
				$this->status = static :: STATUS_PUBLISHED;
				break;
		}
		return $this->save ();
	}
	
	
	/**
	 * Check if the current record is published
	 * @return boolean
	 */
	public function isPublished ()
	{
		return ( $this->status == static :: STATUS_PUBLISHED );
	}
	
	
	/**
	 * Check if the current record is pending
	 * @return boolean
	 */
	public function isPendingReview ()
	{
		return ( $this->status == static :: STATUS_PENDING_REVIEW );
	}
	
	
	public function getSuggestions ( $term )
	{
		if ( strlen ( $term ) > 1 )
		{
			if ( $fields = $this->config ( 'system.suggestions' ) )
			{
				$fields = ( array ) $fields;
				
				$query = $this->newQuery ();
				foreach ( $fields as $field )
				{
					$query->where ( $field, 'like', "%{$term}%" );
				}
				return $query->get ();
			}
		}
		return false;
	}
	
	
	public function __toString ()
	{
		if ( $toString = $this->config ( 'display.fields' ) )
		{
			if ( is_string ( $toString ) )
			{
				if ( Str::contains ( $toString, '@' ) )
				{
					$callback = explode ( '@', $toString, 2 );
					if ( count ( $callback ) == 2 and is_callable ( $callback ) )
					{
						return call_user_func ( $callback, $this );
					}
				} elseif ( $this->hasField ( $toString ) )
				{
					return $this->fields ( $toString )->render ();
				}
			}
			if ( is_array ( $toString ) and count ( $toString ) )
			{
				$values = $this->fields ( $toString, true )->render ()->all ();
				return implode ( ' ', $values );
			}
		}
		return '#' . $this->getKey ();
	}
	
}


