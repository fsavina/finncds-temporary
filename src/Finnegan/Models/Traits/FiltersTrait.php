<?php
namespace Finnegan\Models\Traits;


use Finnegan\Contracts\Fields\Searchable;
use Finnegan\Contracts\Modules\Module as ModuleContract;
use Finnegan\Models\Scopes\SearchScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;


trait FiltersTrait
{


	/**
	 * The filtering manager instance
	 * @var \Finnegan\Models\Scopes\SearchScope
	 */
	protected $searcher;


	/**
	 * @param Builder        $builder
	 * @param ModuleContract $module
	 * @param Request        $request
	 * @return Builder
	 */
	public function scopeSearch ( Builder $builder, ModuleContract $module, Request $request )
	{
		$this->searcher = new SearchScope( $module, $request, $this );
		$this->searcher->apply ( $builder, $this );
		return $builder;
	}


	/**
	 * Get the current filtering rule
	 * @return array
	 */
	public function getFilterBy ()
	{
		if ( is_object ( $this -> searcher ) )
		{
			return $this -> searcher -> getFilterBy ();
		}
		return [];
	}


	/**
	 * @return \Finnegan\Models\FieldCollection
	 */
	public function filters ()
	{
		return $this->fields ()
					->ifInstanceOf ( Searchable::class )
					->ifConfig ( 'filter', true );
	}


	/**
	 * @return \Finnegan\Models\FieldCollection
	 */
	public function textFilters ()
	{
		return $this -> filters () -> filter ( function  ( Searchable $field )
		{
			return ( $field -> searchType () == Searchable :: TEXT_SEARCH_TYPE );
		} );
	}


	/**
	 * @return \Finnegan\Models\FieldCollection
	 */
	public function otherFilters ()
	{
		return $this -> filters () -> filter ( function  ( Searchable $field )
		{
			return ( $field -> searchType () != Searchable :: TEXT_SEARCH_TYPE );
		} );
	}


	public function hasTextSearch ()
	{
		$filters = $this -> filters ();
		foreach ( $filters as $filter )
		{
			if ( $filter -> searchType () == Searchable :: TEXT_SEARCH_TYPE )
			{
				return true;
			}
		}
		return false;
	}

}


