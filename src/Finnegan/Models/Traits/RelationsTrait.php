<?php
namespace Finnegan\Models\Traits;


use Finnegan\Models\Relations\BelongsToMany;
use Finnegan\Models\Relations\MorphToMany;
use Finnegan\Models\Relations\Pivot;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;


trait RelationsTrait
{
	
	
	public function getForeignKey ()
	{
		return snake_case ( $this -> name () ) . '_id';
	}


	public function hasMany($related, $foreignKey = null, $localKey = null)
	{
		$foreignKey = $foreignKey ?: $this->getForeignKey();

		if ( class_exists ( $related ) )
		{
			$instance = new $related( [ ], false, $this->definitionInstance->getContext () );
		} else
		{
			$instance = app ( 'models.factory' )->withContext ( $this->definitionInstance->getContext (), $related );
		}

		$localKey = $localKey ?: $this->getKeyName();

		return new HasMany($instance->newQuery(), $this, $instance->getTable().'.'.$foreignKey, $localKey);
	}
	
	
	public function belongsTo($related, $foreignKey = null, $otherKey = null, $relation = null)
	{
		// If no relation name was given, we will use this debug backtrace to extract
		// the calling method's name and use that as the relationship name as most
		// of the time this will be what we desire to use for the relationships.
		if (is_null($relation))
		{
			list(, $caller) = debug_backtrace(false, 2);
	
			$relation = $caller['function'];
		}
	
		// If no foreign key was supplied, we can use a backtrace to guess the proper
		// foreign key name by using the name of the relationship function, which
		// when combined with an "_id" should conventionally match the columns.
		if (is_null($foreignKey))
		{
			$foreignKey = snake_case($relation).'_id';
		}


		if ( class_exists ( $related ) )
		{
			$instance = new $related( [ ], false, $this->definitionInstance->getContext () );
		} else
		{
			$instance = app ( 'models.factory' )->withContext ( $this->definitionInstance->getContext (), $related );
		}

		// Once we have the foreign key names, we'll just create a new Eloquent query
		// for the related models and returns the relationship instance which will
		// actually be responsible for retrieving and hydrating every relations.
		$query = $instance->newQuery();
	
		$otherKey = $otherKey ?: $instance->getKeyName();
	
		return new BelongsTo($query, $this, $foreignKey, $otherKey, $relation);
	}


	public function joiningTable ( $related )
	{
		return $this -> definitionInstance -> relationPivotTable ( $related );
	}
	
	
	protected function isRelation ( $relation )
	{
		return is_array ( $this -> config ( "relations.$relation" ) );
	}
	
	
	/**
	 * @param string $relation
	 * @param bool $forceReload
	 * @return \Finnegan\Models\Relations\BelongsToMany|boolean
	 */
	public function relation ( $relation, $forceReload = false )
	{
		if ( isset ( $this -> relations [ $relation ] ) and ! $forceReload )
		{
			return $this -> relations [ $relation ];
		}

		if ( $morph = $this -> config ( "relations.{$relation}.morph" )
			or $morph = $this->config ( "fields.{$relation}.morph" ) )
		{
			return $this -> relations [ $relation ] = $this -> morphToMany ( $relation, $morph );
		}

		return $this -> relations [ $relation ] = $this -> belongsToMany ( $relation );
	}


	/**
	 * @return \Finnegan\Models\Relations\BelongsToMany
	 */
	public function belongsToMany($related, $table = null, $foreignKey = null, $otherKey = null, $relation = null)
	{
		// If no relationship name was passed, we will pull backtraces to get the
		// name of the calling function. We will use that function name as the
		// title of this relation since that is a great convention to apply.
		if ( is_null ( $relation ) )
		{
			$relation = $this->getBelongsToManyCaller ();

			if ( $relation == 'relation' )
			{
				// If the relation was loaded with the "relation" method we use the related model name
				$relation = $related;
			}
		}

		$config = $this -> config ( 'relations.' . $related, [] );

		// First, we'll need to determine the foreign key and "other key" for the
		// relationship. Once we have determined the keys we'll make the query
		// instances as well as the relationship instances we need for this.
		$foreignKey = $foreignKey ?  : $this -> getForeignKey ();
		$foreignKey = data_get ( $config, 'foreignKey', $foreignKey );

		$related = data_get ( $config, 'definition', $related );
		$instance = app ( 'models.factory' )->withContext ( $this->definitionInstance->getContext (), $related );

		$otherKey = $otherKey ?  : $instance -> getForeignKey ();
		$otherKey = data_get ( $config, 'otherKey', $otherKey );

		// If no table name was provided, we can guess it by concatenating the two
		// models using underscores in alphabetical order. The two model names
		// are transformed to snake case from their default CamelCase also.
		if (is_null($table)) {
			$table = $this->joiningTable($related);
		}

		// Now we're ready to create a new query builder for the related model and
		// the relationship instances for the relation. The relations will set
		// appropriate query constraint and entirely manages the hydrations.
		$query = $instance->newQuery();

		$relation = new BelongsToMany ( $query, $this, $table, $foreignKey, $otherKey, $relation );
		$relation -> setName ( $related );

		if ( $relation -> timestamps () )
		{
			$relation -> withTimestamps ();
		}
		if ( $relation -> hasExtraFields () )
		{
			$relation -> withPivot ( array_keys ( $relation -> extraFields () ) );
		}

		return $relation;
	}


	public function morphToMany($related, $name, $table = null, $foreignKey = null, $otherKey = null, $inverse = false)
	{
		$caller = $this->getBelongsToManyCaller();

		$config = $this -> config ( 'relations.' . $related, [] );

		// First, we will need to determine the foreign key and "other key" for the
		// relationship. Once we have determined the keys we will make the query
		// instances, as well as the relationship instances we need for these.
		$foreignKey = $foreignKey ?: $name.'_id';

		$instance = app ( 'models.factory' )->withContext ( $this->definitionInstance->getContext (), $related );

		$otherKey = $otherKey ?: $instance->getForeignKey();

		// Now we're ready to create a new query builder for this related model and
		// the relationship instances for this relation. This relations will set
		// appropriate query constraints then entirely manages the hydrations.
		$query = $instance->newQuery();

		$table = $table ?: Str::plural($name);
		$table = $instance -> definition () -> wrapTableName ($table);

		$relation = new MorphToMany(
			$query, $this, $name, $table, $foreignKey,
			$otherKey, $caller, $inverse
		);
		$relation -> setName ( $related );

		if ( $relation -> timestamps () )
		{
			$relation -> withTimestamps ();
		}

		return $relation;
	}


	public function getMorphClass ()
	{
		return $this -> name ();
	}
	
	
	/**
	 * @param string $relation
	 * @param bool $forceReload
	 * @return \Illuminate\Database\Eloquent\Collection>|boolean
	 */
	public function related ( $relation, $forceReload = false )
	{
		$relation = $this -> relation ( $relation, $forceReload );
		if ( is_object ( $relation ) )
		{
			return $relation -> getResults ();
		}
		return false;
	}
	
	
    public function newPivot ( BaseModel $parent, array $attributes, $table, $exists )
	{
		return new Pivot ( $parent, $attributes, $table, $exists );
	}

}


