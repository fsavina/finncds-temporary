<?php
namespace Finnegan\Models\Traits;


use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;


trait ActionsTrait
{

	protected static $bulkActions = [
		'destroy'
	];


	/**
	 * Build an action url for the current model
	 * @param string $action
	 * @param array  $params
	 * @return string
	 */
	public function actionUrl ( $action, array $params = [] )
	{
		$params = Arr::add ( $params, 'model', $this -> name () );
		return app ( 'Finnegan\Contracts\Modules\Module' ) -> actionUrl ( $action, $params );
	}


	/**
	 * Build an action url for the current record
	 * @param string $action
	 * @param array  $params
	 * @return string
	 */
	public function recordActionUrl ( $action, array $params = [] )
	{
		$params = Arr::add ( $params, 'id', $this -> getKey () );
		return $this -> actionUrl ( $action, $params );
	}
	
	
	/**
	 * Get the available bulk actions
	 * @return array
	 */
	public function bulkActions ()
	{
		$bulkActions = [];
		$actions = $this -> config ( 'auth.actions', [ ] );
		foreach ( $actions as $name => $config )
		{
			if ( in_array ( $name, static :: $bulkActions ) and Arr::get ( $config, 'bulk' ) and Gate::allows ( $name, $this ) )
			{
				$bulkActions [ $name ] = trans ( 'finnegan::actions.' . $name );
			}
		}
		return $bulkActions;
	}

}


