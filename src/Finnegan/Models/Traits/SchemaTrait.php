<?php
namespace Finnegan\Models\Traits;


trait SchemaTrait
{


	public function populateMigration ( $creator )
	{
		return $this->populateMigrationFields ( $creator ) .
		       $this->populateMigrationIndexes ( $creator );
	}


	protected function populateMigrationFields ( $creator )
	{
		$output = '';
		foreach ( $this -> fields -> schema () as $field )
		{
			if ( $field -> config ( 'schema', true ) )
			{
				$output .= $field -> populateMigration ();
			}
		}
		return $output;
	}


	protected function populateMigrationIndexes ( $creator )
	{
		$output = '';

		$indexes = $this -> config ( 'storage.table.indexes', [ ] );
		if ( is_array ( $indexes ) and count ( $indexes ) )
		{
			foreach ( $indexes as $index )
			{
				$columns = (array) data_get ( $index, 'columns', [ ] );
				$type = data_get ( $index, 'type', 'index' );
				$name = data_get ( $index, 'name', null );

				return $creator->renderViewStub ( 'table-index', compact ( 'type', 'columns', 'name' ) );
			}
		}

		return $output;
	}


	public function populateMigrationOnSchemaReady ( $creator )
	{
		$output = '';
		foreach ( $this -> fields -> schema () as $field )
		{
			if ( $field -> config ( 'schema', true ) )
			{
				$output .= $field -> populateOnSchemaReadyMigration ();
			}
		}
		return $output;
	}
	
	
	public function getMigrationName ()
	{
		return "create_{$this->getTable()}_table";
	}

}