<?php
namespace Finnegan\Models\Traits;


use Finnegan\Definitions\ModelDefinition;


trait DefinitionTrait
{
	
	/**
	 * The definition name
	 * @var string
	 */
	protected $definition;
	
	/**
	 * The definition instance
	 * @var \Finnegan\Definitions\ModelDefinition
	 */
	protected $definitionInstance;
	
	
	/**
	 * Set the definition for the current model
	 * @param \Finnegan\Definitions\ModelDefinition $definition
	 * @return \Finnegan\Models\Model
	 */
	public function setDefinition ( ModelDefinition $definition )
	{
		$this->definitionInstance = $definition;
		
		if ( is_null ( $this->definition ) )
		{
			$this->definition = $definition->name ();
		}
		
		$this->setTable ( $this->definitionInstance->table () );
		$this->setKeyName ( $this->definitionInstance->primaryKey ( $this->primaryKey ) );
		$this->setPerPage ( (int) $this->config ( 'display.list.items', $this->perPage ) );
		$this->timestamps = ( $this->definitionInstance->hasComponent ( 'timestamps' ) or $this->timestamps );
		
		$this->loadFields ();
		$this->registerEvents ();
		$this->registerDefinitionEvents ();
		
		if ( method_exists ( $this, 'setup' ) )
		{
			app ()->call ( [ $this, 'setup' ] );
		}
		
		return $this;
	}
	
	
	/**
	 * Get the model definition object
	 * @return \Finnegan\Definitions\ModelDefinition
	 */
	public function definition ()
	{
		return $this->definitionInstance;
	}
	
	
	/**
	 * Get the model name
	 * @return string
	 */
	public function name ()
	{
		return $this->definitionInstance ? $this->definitionInstance->name () : $this->definition;
	}
	
	
	public function getTableEngine ( $default = null )
	{
		return $this->config ( 'storage.table.engine', $default );
	}
	
	
	/**
	 * Retrieve a configuration value from the definition
	 * @param string $key
	 * @param mixed  $default
	 * @return mixed
	 */
	public function config ( $key, $default = false )
	{
		return $this->definitionInstance->get ( $key, $default );
	}
	
	
	final protected function registerDefinitionEvents ()
	{
		$events = $this->config ( 'events' );
		if ( is_array ( $events ) and count ( $events ) )
		{
			foreach ( $events as $event )
			{
				$this->registerEvent ( $event [ 'event' ], $event [ 'callback' ] );
			}
		}
	}
	
	
	/**
	 * Register event callback
	 * @param string       $event
	 * @param string|array $callback
	 */
	public function registerEvent ( $event, $callback )
	{
		if ( in_array ( $event, $this->getObservableEvents () ) and ! $this->isEventRegistered ( $event, $this->name () ) )
		{
			$callback = is_array ( $callback ) ? $callback : explode ( '@', $callback );
			if ( is_string ( $callback[ 0 ] ) and ( $callback[ 0 ] == '$this' ) )
			{
				$callback[ 0 ] = $this;
			}
			static:: registerModelEvent ( $event, $callback, 0, $this->name () );
		}
	}
	
}



