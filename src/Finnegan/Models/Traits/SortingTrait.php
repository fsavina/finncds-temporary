<?php
namespace Finnegan\Models\Traits;


use Finnegan\Contracts\Modules\Module as ModuleContract;
use Finnegan\Models\Scopes\SortScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;


trait SortingTrait
{

	/**
	 * The sorting manager instance
	 * @var \Finnegan\Models\Scopes\SortScope
	 */
	protected $sorter;


	/**
	 * @param Builder        $builder
	 * @param ModuleContract $module
	 * @param Request        $request
	 * @return Builder
	 */
	public function scopeSort ( Builder $builder, ModuleContract $module, Request $request )
	{
		$this->sorter = new SortScope( $module, $request, $this );
		$this->sorter->apply ( $builder, $this );
		return $builder;
	}


	/**
	 * Get the current sorting rule
	 * @return array
	 */
	public function getSortBy ()
	{
		if ( is_object ( $this->sorter ) )
		{
			return $this->sorter->getSortBy ();
		}
		return [ $this->getKeyName (), 'asc' ];
	}
}