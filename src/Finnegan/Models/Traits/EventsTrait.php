<?php
namespace Finnegan\Models\Traits;


use Illuminate\Support\Facades\Log;


trait EventsTrait
{
	
	protected static $registeredEvents = [];
	
	
	/**
	 * @param string $event
	 * @return bool
	 */
	protected static function isEventRegistered ( $event, $model )
	{
		return in_array ( static:: eventName ( $event, get_called_class (), $model ), static::$registeredEvents );
	}
	
	
	protected static function registerModelEvent ( $event, $callback, $priority = 0, $model = null )
	{
		if ( isset ( static:: $dispatcher ) )
		{
			$event = static:: eventName ( $event, get_called_class (), $model );
			static:: $dispatcher->listen ( $event, $callback, $priority );
			array_push ( static::$registeredEvents, $event );
		}
	}
	
	
	protected function fireModelEvent ( $event, $halt = true )
	{
		if ( ! isset ( static:: $dispatcher ) )
		{
			return true;
		}
		
		$event = static:: eventName ( $event, get_class ( $this ), $this->name () );
		$method = $halt ? 'until' : 'fire';
		return static:: $dispatcher->$method ( $event, $this );
	}
	
	
	protected static function eventName ( $event, $class, $model )
	{
		return "eloquent.{$event}: {$class}\\{$model}";
	}
}