<?php
namespace Finnegan\Models\Relations;


use Finnegan\Contracts\Models\Migratable;


class MorphToMany
	extends
		\Illuminate\Database\Eloquent\Relations\MorphToMany
	implements
		Migratable
{


	protected $name;


	public function setName ( $name )
	{
		$this -> name = $name;
	}


	public function config ( $key = null, $default = false )
	{
		if ( $key )
		{
			$key = "relations.{$this->name}.{$key}";
		} else
		{
			$key = "relations.{$this->name}";
			$default = [ ];
		}
		return $this -> parent -> config ( $key, $default );
	}


	public function name ()
	{
		return $this->name;
	}


	public function timestamps ()
	{
		return $this -> config ( 'timestamps' );
	}
	

	public function getKeyName ()
	{
		return false;
	}


	public function getTableEngine ( $default = null )
	{
		return $this -> parent -> getTableEngine ( $default );
	}


	public function populateMigration ( $creator )
	{
		return $creator->renderViewStub ( 'pivot-table-morph', [ 'relation' => $this ] );
	}


	public function populateMigrationOnSchemaReady ( $creator )
	{
		return '';
	}


	public function getOtherKeyRaw ()
	{
		return $this->otherKey;
	}


	public function getMorphName ()
	{
		return str_replace ( '_type', '', $this->getMorphType () );
	}


	public function getMigrationName ()
	{
		return "create_{$this->getMorphName()}_table";
	}
}