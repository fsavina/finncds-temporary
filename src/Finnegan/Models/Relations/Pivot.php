<?php
namespace Finnegan\Models\Relations;


use Illuminate\Support\Str;


class Pivot
	extends
		\Illuminate\Database\Eloquent\Relations\Pivot
	implements
		\Finnegan\Contracts\Models\Fields,
		\Finnegan\Contracts\Models\Formable,
		\Finnegan\Contracts\Models\Titlable
{
	

	use \Finnegan\Models\Traits\DefinitionTrait,
		\Finnegan\Models\Traits\FieldsTrait,
		\Finnegan\Models\Traits\FormTrait,
		\Finnegan\Models\Traits\ActionsTrait;

	/**
	 * @param \Finnegan\Definitions\ModelDefinition $definition
	 * @return \Finnegan\Models\Model
	 */
	public function init ( $definition )
	{
		return $this -> setDefinition ( $definition ) -> loadFields ();
	}


	protected function registerEvents ()
	{
		foreach ( get_class_methods ( $this ) as $method )
		{
			if ( substr ( $method, 0, 2 ) == 'on' )
			{
				$event = Str :: snake ( substr ( $method, 2 ) );
				$this -> registerEvent ( $event, [ $this, $method ] );
			}
		}
	}


	public function formConfig ()
	{
		$action = 'relationAttach';
		$params = [
			'model' => $this -> parent -> name (),
			'id' => $this -> parent -> getKey (),
			'relation' => $this -> name()
		];
		$config = [
			'files' => true
		];
		if ( $this -> exists )
		{
			$action = 'relationUpdate';
			$params [ 'relatedId' ] = $this -> getAttribute ( $this -> getOtherKey () );
			$config [ 'method' ] = 'PUT';
		}
		$config [ 'url' ] = $this -> actionUrl ( $action, $params );
		return $config;
	}

}



