<?php
namespace Finnegan\Models\Relations;


use Finnegan\Contracts\Models\Migratable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany as EloquentBelongsToMany;


class BelongsToMany extends EloquentBelongsToMany implements Migratable
{
	
	protected $name;
	
	protected $definitionInstance;
	
	
	public function setName ( $name )
	{
		$this->name = $name;
	}
	
	
	/**
	 * @return \Finnegan\Models\Relations\Pivot
	 */
	public function newPivot ( array $attributes = [ ], $exists = false )
	{
		$pivot = parent:: newPivot ( $attributes, $exists );
		return $pivot->init ( $this->definition () );
	}
	
	
	public function attachReverse ( $id, array $attributes = [ ] )
	{
		$query = $this->newPivotStatement ();

		$record[ $this->foreignKey ] = $id;

		$record[ $this->otherKey ] = $this->parent->getKey ();

		$timed = ( $this->hasPivotColumn ( $this->createdAt () ) ||
			$this->hasPivotColumn ( $this->updatedAt () ) );

		// If the record needs to have creation and update timestamps, we will make
		// them by calling the parent model's "freshTimestamp" method which will
		// provide us with a fresh timestamp in this model's preferred format.
		if ( $timed )
		{
			$record = $this->setTimestampsOnAttach ( $record );
		}

		$record = array_merge ( $record, $attributes );

		$query->insert ( $record );
	}
	
	
	protected function definition ()
	{
		if ( is_null ( $this->definitionInstance ) )
		{
			$this->definitionInstance = app ( 'definitions.factory' )->make ( $this->name, $this->config () );
		}
		return $this->definitionInstance;
	}
	
	
	public function config ( $key = null, $default = false )
	{
		if ( $key )
		{
			$key = "relations.{$this->name}.{$key}";
		} else
		{
			$key = "relations.{$this->name}";
			$default = [ ];
		}
		return $this->parent->config ( $key, $default );
	}
	
	
	public function name ()
	{
		return $this->getRelationName ();
	}
	
	
	public function timestamps ()
	{
		return $this->config ( 'timestamps' );
	}
	
	
	public function extraFields ()
	{
		return $this->config ( 'fields' );
	}
	
	
	public function hasExtraFields ()
	{
		$fields = $this->extraFields ();
		return ( is_array ( $fields ) and count ( $fields ) );
	}
	
	
	public function getKeyName ()
	{
		return false;
	}
	
	
	public function getTableEngine ( $default = null )
	{
		return $this->parent->getTableEngine ( $default );
	}
	
	
	public function populateMigration ( $creator )
	{
		return $creator->renderViewStub ( 'pivot-table', [ 'relation' => $this ] );
	}
	
	
	public function populateMigrationOnSchemaReady ( $creator )
	{
		return '';
	}
	
	
	public function getOtherKeyRaw ()
	{
		return $this->otherKey;
	}
	
	
	public function getForeignKeyRaw ()
	{
		return $this->foreignKey;
	}
	
	
	public function getMigrationName ()
	{
		$models = [ $this->related->name (), $this->parent->name () ];
		sort ( $models );
		$name = implode ( '_', $models );
		return "create_{$name}_table";
	}
}



