<?php

namespace Finnegan\Models\Fields;


use Carbon\Carbon;
use Finnegan\Contracts\Modules\Module;
use Finnegan\Filesystem\FilesystemServiceProvider;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;


class File
    extends
    Literal
    implements
    \Finnegan\Contracts\Fields\File
{
    
    use Traits\AcceptsFiles;
    
    
    protected $widgetConfig = [
        'type' => 'file'
    ];
    
    /**
     * @var \Illuminate\Filesystem\FilesystemAdapter
     */
    protected $disk;
    
    /**
     * @var Request
     */
    protected $request;
    
    /**
     * @var UrlGenerator
     */
    protected $url;
    
    
    public function boot ( Module $module, FilesystemManager $manager, Request $request, UrlGenerator $url )
    {
        $disk = $module->config ( 'module.disks.uploads', FilesystemServiceProvider::DEFAULT_DISK_NAME );
        $this->disk = $manager->disk ( $this->config ( 'disk', $disk ) );
        
        $this->request = $request;
        $this->url = $url;
    }
    
    
    /**
     * Sanitize the incoming file name and move it the destination path.
     * @param UploadedFile $file
     * @return string
     */
    public function filter ( $file )
    {
        if ( $this->mustDelete () )
        {
            $this->deleteFile ();
            $this->deleteValue ();
        }
        
        if ( $file and ( $file instanceof UploadedFile ) and $file->isValid () )
        {
            return $this->receiveFile ( $file );
        } elseif ( is_null ( $file ) and ! $this->value () )
        {
            $this->deleteValue ();
        }
        
        return $this->value ();
    }
    
    
    protected function deleteValue ()
    {
        if ( $this->isTranslatable () and $this->locale )
        {
            $this->model->setAttribute ( "{$this->name}:{$this->locale}", '' );
        } else
        {
            $this->setValue ( '' );
        }
        return $this;
    }
    
    
    /**
     * @return boolean
     */
    protected function mustDelete ()
    {
        $namespace = $this->model->name ();
        if ( $this->isTranslatable () )
        {
            $namespace .= ".{$this->locale ()}";
        }
        return ( bool ) $this->request->input ( $namespace . ".{$this->name}_deleted", false );
    }
    
    
    public function render ( $value = null, $escapeHtml = true )
    {
        if ( is_object ( $this->presenter ) )
        {
            return $this->presenter->render ();
        }
        
        $value = is_null ( $value ) ? $this->value () : $value;
        
        $layout = $this->layoutManager ();
        
        if ( strlen ( $value ) )
        {
            $icon = $layout->presenter ()->icon ( 'files.' . $this->extension () )->x2 () . ' ';
            return $icon . $this->htmlBuilder ()
                                ->link ( $this->url ( $value ), $this->shortName ( $value ), [
                                    'title'  => $value,
                                    'target' => '_blank'
                                ] );
        }
        
        return parent:: render ( '' );
    }
    
    
    protected function defaultValue ( $default = null )
    {
        return null;
    }
    
    
    public function rules ( array $rules = [] )
    {
        $rules = parent:: rules ( $rules );
        if ( in_array ( 'required', $rules ) and $this->model->exists )
        {
            if ( ( $key = array_search ( 'required', $rules ) ) !== false )
            {
                unset ( $rules [ $key ] );
            }
        }
        return $rules;
    }
    
    
    public function isRequired ()
    {
        $rules = $this->config ( 'rules', [] );
        $rules = ! is_array ( $rules ) ? explode ( '|', $rules ) : $rules;
        return in_array ( 'required', $rules );
    }
    
    
    /**
     * Get the public url to the file
     * @param string $filename
     * @return string
     */
    public function url ( $filename = null )
    {
        return $this->publicPath ( $filename );
    }
    
    
    public function content ( $filename = null )
    {
        $filename = $filename ?: $this->value ();
        
        if ( $this->disk->exists ( $path = $this->relativePath ( $filename ) ) )
        {
            return $this->disk->get ( $path );
        }
        return false;
    }
    
    
    /**
     * @param string $filename
     * @return Carbon
     */
    public function lastModified ( $filename = null )
    {
        $filename = $filename ?: $this->value ();
        
        if ( $this->disk->exists ( $path = $this->relativePath ( $filename ) ) )
        {
            return Carbon::createFromTimestamp ( $this->disk->lastModified ( $path ) );
        }
        return false;
    }
    
    
    public function searchType ()
    {
        return false;
    }
    
    
    public function isSortable ()
    {
        return false;
    }
    
    
    /**
     * Hook for the "deleted" model event
     */
    public function onDeleted ()
    {
        $this->deleteFile ();
    }
    
    
    public function getMigrationStubName ()
    {
        return 'file';
    }
    
}



