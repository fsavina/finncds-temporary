<?php
namespace Finnegan\Models\Fields;


use Finnegan\Contracts\Modules\Module;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Http\Request;


/**
 * https://github.com/BKWLD/croppa
 */
class Image
	extends
	File
	implements
	\Finnegan\Contracts\Fields\Image
{
	
	/**
	 * @var \Bkwld\Croppa\Helpers
	 */
	protected $croppa;
	
	
	public function boot ( Module $module, FilesystemManager $manager, Request $request, UrlGenerator $url )
	{
		parent::boot ( $module, $manager, $request, $url );
		$this->croppa = $module->make ( 'Bkwld\Croppa\Helpers' );
	}
	
	
	public function rules ( array $rules = [ ] )
	{
		$rules = parent:: rules ( $rules );
		$rules [] = 'image';
		return $rules;
	}
	
	
	public function url ( $filename = null, $width = null, $height = null, $options = null )
	{
		$filename = $filename ?: $this->value ();
		return $this->croppa->url ( $this->localPublicPath ( $filename ), $width, $height, $options );
	}
	
	
	public function originalUrl ( $filename = null )
	{
		$filename = $filename ?: $this->value ();
		return $this->publicPath ( $filename );
	}
	
	
	public function render ( $value = null, $escapeHtml = true )
	{
		return $this->renderSize ( is_null ( $value ) ? $this->value () : $value, 200, 200, [ 'resize' ] );
	}
	
	
	public function renderForList ( $value = null )
	{
		return $this->renderSize ( is_null ( $value ) ? $this->value () : $value, 100, 100 );
	}
	
	
	protected function renderSize ( $value = null, $width = null, $height = null, $options = null )
	{
		$value = is_null ( $value ) ? $this->value () : $value;
		if ( strlen ( $value ) )
		{
			$img = $this->htmlBuilder ()
						->image ( $this->url ( $value, $width, $height, $options ), null, [ 'class' => 'thumbnail' ] );
			
			$modal = $this->layoutManager ()->presenter ()->modal ( "<img src=\"{$this->originalUrl ()}\">", $img );
			$modal->showCloseButton ( false );
			return $modal;
		}
		return '';
	}
	
	
	protected function deleteFile ()
	{
		if ( $filename = $this->value () )
		{
			$path = public_path ( $this->localPublicPath ( $filename ) );
			$this->croppa->reset ( $path );
		}
		return parent::deleteFile ();
	}
	
}



