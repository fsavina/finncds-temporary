<?php
namespace Finnegan\Models\Fields;




use Finnegan\Contracts\Models\Driver;


class Numeric
	extends
		Field
	implements
		\Finnegan\Contracts\Fields\Range,
		\Finnegan\Contracts\Fields\Schema,
		\Finnegan\Contracts\Fields\Sortable,
		\Finnegan\Contracts\Fields\Searchable
{

	const DEFAULT_NUMBER_TYPE = 'integer';
	
	protected static $allowedNumberTypes = [
		'integer',
		'decimal'
	];

	protected $rules = [
		'numeric'
	];

	protected $widgetConfig = [
		'inputType' => 'number'
	];
	
	
	/**
	 * Get the configured number type among
	 * @return string
	 */
	public function numberType ()
	{
		return $this -> config ( 'numberType', self :: DEFAULT_NUMBER_TYPE );
	}
	
	
	protected function widgetConfig ()
	{
		$config = parent :: widgetConfig ();
		
		if ( ! isset( $config [ 'attributes' ] ) )
		{
			$config [ 'attributes' ] = [ ];
		}
		
		$config [ 'attributes' ] [ 'precision' ] = $this -> precision ();
		$config [ 'attributes' ] [ 'min' ] = $this -> min ();
		$config [ 'attributes' ] [ 'step' ] = $this -> step ();

		if ( ! is_null ( $this -> max () ) )
		{
			$config [ 'attributes' ] [ 'max' ] = $this -> max ();
		}
		
		switch ( $this -> numberType () )
		{
			case 'decimal':
				$config [ 'attributes' ] [ 'pattern' ] = $this -> decimalRegex ();
				break;
		}

		return $config;
	}
	

	/**
	 * Get the minimum allowed value
	 * @param integer $default
	 * @return integer
	 */
	public function min ( $default = 0 )
	{
		return $this -> config ( 'min', $default );
	}


	/**
	 * Get the maximum allowed value
	 * @param int $default
	 * @return int
	 */
	public function max ( $default = 100 )
	{
		switch ( $this -> numberType () )
		{
			case 'decimal':
				$decimalDigits = $this -> decimalDigits ();
				$digits = $this -> totalDigits () - $decimalDigits;
				$default = str_pad ( '', $digits, '9' ) . '.' . str_pad ( '', $decimalDigits, '9' );
				break;
		}
		return $this -> config ( 'max', $default );
	}
	
	
	/**
	 * Get the incrementing step
	 * @param integer $default
	 * @return integer|string
	 */
	public function step ( $default = 1 )
	{
		switch ( $this -> numberType () )
		{
			case 'decimal':
				$step = '0.' . str_pad ( '1', $this -> decimalDigits (), '0', STR_PAD_LEFT );
				break;
			case 'integer':
			default:
				$step = $default;
				break;
		}
		return $this -> config ( 'step', $step );
	}
	
	
	/**
	 * Get the value precision
	 * @param integer $default
	 * @return integer
	 */
	public function precision ( $default = 0 )
	{
		switch ( $this -> numberType () )
		{
			case 'decimal':
				$precision = $this -> decimalDigits ();
				break;
			case 'integer':
			default:
				$precision = $default;
				break;
		}
		return $this -> config ( 'precision', $precision );
	}
	
	
	/**
	 * Get the number of total digits for decimal number type
	 * @param integer $default
	 * @return integer
	 */
	public function totalDigits ( $default = 10 )
	{
		return (int) $this -> config ( 'totalDigits', $default );
	}
	
	
	/**
	 * Get the number of decimal digits for decimal number type
	 * @param integer $default
	 * @return integer
	 */
	public function decimalDigits ( $default = 2 )
	{
		return (int) $this -> config ( 'decimalDigits', $default );
	}
	
	
	/**
	 * Get regex for decimal input validation
	 * @return string
	 */
	protected function decimalRegex()
	{
		return '[0-9]+([\.|,][0-9]{1,' . $this -> decimalDigits () . '})?';
	}
	
	
	public function rules ( array $rules = [] )
	{
		$rules [] = 'min:' . $this -> min ();
		if ( ! is_null ( $this -> max () ) )
		{
			$rules [] = 'max:' . $this -> max ();
		}
		switch ( $this -> numberType () )
		{
			case 'decimal':
				$rules [] = 'regex:/' . $this -> decimalRegex () . '/';
				break;
			case 'integer':
			default:
				$rules [] = 'integer';
				break;
		}
		
		return parent :: rules ( $rules );
	}
	
	
	public function searchType ()
	{
		return 'Numeric';
	}
	
	
	/**
	 * @todo search on translatable field
	 */
	public function search ( Driver $builder, $value )
	{
		if ( is_array ( $value ) and isset ( $value [ 'from' ] ) and isset ( $value [ 'to' ] ) )
		{
			if ( strlen ( $value [ 'from' ] ) and strlen ( $value [ 'to' ] ) )
			{
				$builder -> whereBetween ( $this -> name (), array_values ( $value ) );
			} elseif ( strlen ( $value [ 'from' ] ) )
			{
				$builder -> where ( $this -> name (), '>=', $value [ 'from' ] );
			} elseif ( strlen ( $value [ 'to' ] ) )
			{
				$builder -> where ( $this -> name (), '<=', $value [ 'to' ] );
			}
		}
	}

}



