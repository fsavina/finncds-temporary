$table->string('{{ $field->name() }}', {{ $field->size() }}){!! !$field->isRequired () ? '->nullable ()' : '' !!};

@if($field->unique())
    $table->unique( '{{ $field->name() }}' );
@endif