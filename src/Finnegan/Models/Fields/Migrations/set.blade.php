@if ( $schema = $field->config ( 'schema' ) )
    <?php
    $schema = (array) $schema;
    $method = $schema [ 0 ];
    $schema [ 0 ] = $field->name();
    ?>

    $table->{{ $method }} ('{!! implode("', '", $schema) !!}')->nullable();

@elseif ( $field->config ( 'multiple' ) )

    $table->string('{{ $field->name() }}')->nullable();

@else

    $table -> integer ( '{{ $field->name() }}', false, true )->nullable();

    @if ($field->config('model'))
        <?php $model = $field->externalModel() ?>
        $table->foreign ( '{{ $field->name() }}' )
            ->references ( '{{ $model->getKeyName () }}' )
            ->on ( '{{ $model->getTable () }}' );
    @else
        $table -> index ( '{{ $field->name() }}' );
    @endif

@endif
