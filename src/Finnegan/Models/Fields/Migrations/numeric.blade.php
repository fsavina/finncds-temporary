@if($field -> numberType() == 'decimal')
    $table->decimal ( '{{ $field->name() }}', {{ $this -> totalDigits () }}, {{ $this -> decimalDigits () }});
@else
    $table -> integer ( '{{ $field->name() }}', false, {{ ( $field->config ( 'min', 0 ) >= 0 ) ? 'true' : 'false' }} );
@endif
