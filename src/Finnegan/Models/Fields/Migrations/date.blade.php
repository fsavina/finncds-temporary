$table->{{ $field->getBlueprintTableMethod() }}('{{ $field->name() }}')
    {!! $field->config ( 'nullable' ) ? '->nullable ()' : '' !!};
