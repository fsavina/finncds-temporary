<?php

namespace Finnegan\Models\Fields;


use Illuminate\Support\Str;


class Text
	extends
	Literal
	implements
	\Finnegan\Contracts\Fields\Text,
	\Finnegan\Contracts\Fields\Schema,
	\Finnegan\Contracts\Fields\Sortable,
	\Finnegan\Contracts\Fields\Searchable
{
	
	protected $size         = 0;
	
	protected $widgetConfig = [
		'type' => 'textarea'
	];
	
	
	public function filter ( $value )
	{
		return ! $this->config ( 'html' ) ? strip_tags ( $value ) : $value;
	}
	
	
	public function render ( $value = null, $escapeHtml = true )
	{
		$render = parent:: render ( $value, ! $this->config ( 'html' ) );
		return nl2br ( $render );
	}
	
	
	public function renderForList ( $value = null )
	{
		$text = strip_tags ( $this->render ( $value ) );
		//$more = app ( 'Finnegan\Contracts\Layout\Presenter' ) -> modal ( $this-> render ( $value ), $this -> name );
		return Str::words ( $text, 20 );
	}
	
	
	protected function widgetConfig ()
	{
		return array_merge (
			[
				'advanced' => $this->config ( 'html' ),
				'locale'   => $this->appLocale ()
			],
			parent::widgetConfig ()
		);
	}
	
	
	protected function getMigrationStubName ()
	{
		return 'text';
	}
	
}


