<?php
namespace Finnegan\Models\Fields;


use Finnegan\Contracts\Models\Driver;
use Illuminate\Support\Facades\DB;


/**
 * @todo rendere Searchable con ricerca per posizione+raggio di ricerca
 */
class Coordinates
	extends
		Field
	implements
		\Finnegan\Contracts\Fields\Coordinates,
		\Finnegan\Contracts\Fields\Schema
{

	protected $widgetConfig = [
		'type' => 'map'
	];


	public function newQuery ( Driver $query )
	{
		$query -> addSelect ( '*', DB :: raw ( " AsText({$this->name}) as {$this->name} " ) );
		return $query;
	}


	public function setValue ( $value )
	{
		if ( ! is_array ( $value ) )
		{
			parent :: setValue ( $value );
			return;
		}
		$value = implode ( ' ', array_values ( $value ) );
		$this -> model -> setAttribute ( $this -> name, DB :: raw ( "GeomFromText('POINT($value)')" ) );
	}
	
	
	protected static function makeValue ( $values )
	{
		return [
			'latitude' => ( float ) $values [ 0 ],
			'longitude' => ( float ) $values [ 1 ]
		];
	}


	public function filter ( $value )
	{
		$value = trim ( strip_tags ( $value ) );
		if ( ! strlen ( $value ) )
		{
			return null;
		}
		return static :: makeValue ( explode ( ',', $value ) );
	}


	public function value ()
	{
		$value = parent :: value ();
		if ( is_string ( $value ) )
		{
			if ( preg_match ( '/^[a-zA-Z]+\((-?[0-9\.]+) (-?[0-9\.]+)\)$/', $value, $matches ) )
			{
				$value = static :: makeValue ( array_slice ( $matches, 1, 2 ) );
				$this -> model -> setAttribute ( $this -> name, $value );
			}
		}
		return $value;
	}


	public function render ( $value = null, $escapeHtml = true )
	{
		$value = ! is_null ( $value ) ? $value : $this -> value ();
		if ( is_null ( $value ) )
		{
			return parent :: render ( $value );
		}

		if ( is_array ( $value ) )
		{
			return ( new Presenters\MapPresenter ( $this ) )->render ();
		}
		return '';
	}


	public function renderForForm ( $value = null )
	{
		$value = $value ?  : $this -> value ();
		if ( is_null ( $value ) )
		{
			return parent :: renderForForm ( $value );
		}
		return is_array ( $value ) ? implode ( ',', array_values ( $value ) ) : '';
	}


	public function populateMigration (  )
	{
		return '';
	}


	public function populateOnSchemaReadyMigration ()
	{
		return $this->renderMigrationStub ( $this->getMigrationStubName () );
	}
	
	
	protected function widgetConfig ()
	{
		return array_merge ( parent:: widgetConfig (), [
			'locale' => $this->appLocale ()
		] );
	}

}