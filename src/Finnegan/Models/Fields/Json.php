<?php
namespace Finnegan\Models\Fields;


use Illuminate\Support\Str;


if ( ! defined ( 'JSON_PRETTY_PRINT' ) )
{
	define ( 'JSON_PRETTY_PRINT', 128 );
}


class Json
	extends
		Field
	implements
		\Finnegan\Contracts\Fields\Schema,
		\Finnegan\Contracts\Fields\Json,
		\Finnegan\Contracts\Fields\Text,
		\Illuminate\Contracts\Support\Jsonable
{

	protected $castAs = 'array';

	protected $widgetConfig = [
			'type' => 'textarea'
	];


	public function size ()
	{
		return false;
	}


	public function filter ( $value )
	{
		return json_decode ( $value, true );
	}


	public function render ( $value = null )
	{
		$value = ! is_null ( $value ) ? $value : $this -> value ();
		if ( is_null ( $value ) )
		{
			return parent :: render ( '' );
		}
		
		$render = json_encode ( $value, JSON_PRETTY_PRINT );
		$render = str_replace ( '    ', '&nbsp;&nbsp;&nbsp;&nbsp;', $render );
		$render = nl2br ( $render );
		return $render;
	}


	public function renderForList ( $value = null )
	{
		$text = strip_tags ( $this -> render ( $value ) );
		return Str :: words ( $text, 20 );
	}


	public function renderForForm ( $value = null )
	{
		$value = $value ?  : $this -> value ();
		if ( is_null ( $value ) )
		{
			return $value;
		}
		return json_encode ( $value, JSON_PRETTY_PRINT );
	}


	protected function getMigrationStubName (  )
	{
		return 'text';
	}


	public function toJson ( $options = 0 )
	{
		return $this->value ();
	}

}


