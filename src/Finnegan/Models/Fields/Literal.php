<?php

namespace Finnegan\Models\Fields;


use Finnegan\Contracts\Fields\Searchable;
use Finnegan\Contracts\Models\Driver;


class Literal
	extends
	Field
	implements
	\Finnegan\Contracts\Fields\Text,
	\Finnegan\Contracts\Fields\Schema,
	\Finnegan\Contracts\Fields\Confirmable,
	\Finnegan\Contracts\Fields\Sortable,
	\Finnegan\Contracts\Fields\Searchable
{

	protected $size = 255;


	/**
	 * @return integer
	 */
	public function size ()
	{
		return $this->config ( 'size', $this->size );
	}


	public function filter ( $value )
	{
		return ! $this->config ( 'html' ) ? strip_tags ( $value ) : $value;
	}


	public function render ( $value = null, $escapeHtml = true )
	{
		if ( is_object ( $this->presenter ) )
		{
			return $this->presenter->render ();
		}
		$value = parent:: render ( $value );
		return ( $escapeHtml and ! $this->config ( 'html' ) ) ? e ( $value ) : $value;
	}


	public function rules ( array $rules = [] )
	{
		if ( $size = $this->size () )
		{
			$rules [] = 'max:' . $size;
		}
		if ( $this->unique () )
		{
			$rules [] = $this->buildUniqueRule ();
		}
		return parent:: rules ( $rules );
	}


	protected function buildUniqueRule ()
	{
		$rule = [
			$this->model->getTable (),
			$this->name ()
		];
		if ( $this->model->exists )
		{
			$rule [] = $this->model->getKey ();
		}
		return 'unique:' . implode ( ',', $rule );
	}


	protected function widgetConfig ()
	{
		$config = parent:: widgetConfig ();
		if ( $size = $this->size () )
		{
			$config [ 'size' ] = $size;
		}
		return $config;
	}


	public function searchType ()
	{
		return Searchable :: TEXT_SEARCH_TYPE;
	}


	public function search ( Driver $builder, $value )
	{
		if ( strlen ( $value ) )
		{
			if ( $this->config ( 'translatable' ) )
			{
				$builder->orWhereHas ( 'translations', function ( $builder ) use ( $value )
				{
					$builder->where ( $this->name (), 'like', "%$value%" );
				} );
			} else
			{
				$builder->orWhere ( $this->name (), 'like', "%$value%" );
			}
		}
	}


	final public function unique ()
	{
		$unique = property_exists ( $this, 'unique' ) ? $this->unique : false;
		return $this->config ( 'unique', $unique );
	}


	public function confirm ()
	{
		return in_array ( 'confirmed', $this->rules () );
	}


	protected function getMigrationStubName ()
	{
		return 'literal';
	}

}
