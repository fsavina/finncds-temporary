<?php
namespace Finnegan\Models\Fields;


use Finnegan\Contracts\Fields\Sortable;
use Illuminate\Support\Str;


class Slug extends Literal implements Sortable
{

	
	/**
	 * Hook method called before saving the current record
	 */
	public function onSaving ()
	{
		$value = '';
		if ( $this -> model -> exists )
		{
			$value = trim ( $this -> value () );
		}
		if ( ! strlen ( $value ) )
		{
			$value = $this -> getSourceValue ();
		}
		$this -> setValue ( $this -> getUniqueSlug ( $value ) );
	}
	
	
	/**
	 * Get the current value from the source field
	 * @return string|boolean
	 */
	protected function getSourceValue ()
	{
		$value = false;
		if ( $source = $this -> config ( 'source' ) )
		{
			if ( is_array ( $source ) )
			{
				$values = $this -> model -> fields ( $source ) -> values () -> all();
				$value = trim ( implode ( $this -> config ( 'glue', ' ' ), $values ) );
			}
			if ( is_string ( $source ) and $this -> model -> hasField ( $source ) )
			{
				$value = $this -> model -> fields( $source ) -> value ();
			}
		}
		return $value;
	}
	
	
	/**
	 * Generate a unique slug for the given value
	 * @param string $value
	 * @return string
	 */
	protected function getUniqueSlug ( $value )
	{
		$value = Str :: slug ( $value );
		$i = 1;
		
		do
		{
			$query = $this -> model -> newQueryWithoutScopes ();
			$query -> where ( $this -> name, '=', $value );
			if ( $this -> model -> exists )
			{
				$query -> where ( $this -> model -> getKeyName (), '!=', $this -> model -> getKey () );
			}
			if ( $count = $query -> count () )
			{
				$value .= '-' . $i ++;
			}
		} while ( $count );
		
		return $value;
	}
	
	
	public function showInForm ()
	{
		return $this -> model -> exists;
	}

}



