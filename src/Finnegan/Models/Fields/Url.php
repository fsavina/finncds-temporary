<?php
namespace Finnegan\Models\Fields;


class Url extends Literal
{

	protected $rules = [ 'url' ];

	protected $widgetConfig = [ 'inputType' => 'url' ];

}

