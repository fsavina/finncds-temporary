<?php
namespace Finnegan\Models\Fields;


use Finnegan\Contracts\Models\Driver;


class Boolean
	extends
		Field
	implements
		\Finnegan\Contracts\Fields\Options,
		\Finnegan\Contracts\Fields\Schema,
		\Finnegan\Contracts\Fields\Sortable,
		\Finnegan\Contracts\Fields\Searchable
{

	protected static $defaultOptions = [
		'0' => 'finnegan::general.no',
		'1' => 'finnegan::general.yes'
	];
	
	protected $rules = [
		'boolean'
	];

	protected $widgetConfig = [
		'type' => 'checkbox'
	];


	/**
	 * Get the available options
	 * @return array
	 */
	public function options ()
	{
		return $this -> config ( 'options', self :: $defaultOptions );
	}


	public function formOptions ()
	{
		return $this->options ();
	}


	public function render ( $value = null )
	{
		$value = is_null ( $value ) ? $this -> value () : $value;
		$options = $this -> options ();
		
		$render = isset ( $options [ $value ] ) ? $options [ $value ] : false;
		return e ( trans ( $render ) );
	}


	public function filter ( $value )
	{
		return (bool) $value;
	}
	
	
	public function searchType ()
	{
		return 'Boolean';
	}


	public function isRequired (  )
	{
		return ( parent :: isRequired () or in_array ( 'accepted', $this -> rules () ) );
	}
	
	
	/**
	 * @todo search on translatable field
	 */
	public function search ( Driver $builder, $value )
	{
		if ( strlen($value) )
		{
			$builder -> where ( $this -> name (), $value );
		}
	}

}



