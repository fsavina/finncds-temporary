<?php
namespace Finnegan\Models\Fields;


use Carbon\Carbon;
use Finnegan\Contracts\Models\Driver;


class Date
	extends
		Field
	implements
		\Finnegan\Contracts\Fields\Date,
		\Finnegan\Contracts\Fields\Schema,
		\Finnegan\Contracts\Fields\Sortable,
		\Finnegan\Contracts\Fields\Searchable
{

	const DEFAULT_MODE = 'date';
	
	const DATE_FORMAT = 'm/d/Y';
	
	const DATETIME_FORMAT = 'm/d/Y H:i';
	
	const TIME_FORMAT = 'H:i';
	
	protected $widgetConfig = [
		'type' => 'date',
	];
	
	protected static $allowedModes = [
		'date',
		'datetime',
		'timestamp',
		'time'
	];
	
	
	public function boot ()
	{
		if ( $this -> mode () != 'time' )
		{
			$this -> model -> addDateField ( $this -> name );
		}
	}
	
	
	/**
	 * Get the configured date mode
	 * @return string
	 */
	public function mode ()
	{
		$mode = $this -> config ( 'mode', self :: DEFAULT_MODE );
		return in_array ( $mode, static :: $allowedModes ) ? $mode : self :: DEFAULT_MODE;
	}
	
	
	/**
	 * Render the given date object according to the configured format
	 * @param Carbon $date
	 * @return string
	 */
	public function format ( Carbon $date )
	{
		switch ( $this -> mode () )
		{
			case 'datetime':
			case 'timestamp':
				$format = $this -> config ( 'format', self :: DATETIME_FORMAT );
				break;
			case 'time':
				$format = $this -> config ( 'format', self :: TIME_FORMAT );
				break;
			case 'date':
			default:
				$format = $this -> config ( 'format', self :: DATE_FORMAT );
				break;
		}
		return ($format == 'diffForHumans') ? $date->diffForHumans() : $date -> format ( $format );
	}
	
	
	public function render ( $value = null )
	{
		$value = is_null ( $value ) ? $this -> value () : $value;
		return ( $value instanceof Carbon ) ? $this -> format ( $value ) : $value;
	}
	
	
	public function filter ( $value )
	{
		return Carbon :: parse ( $value );
	}
	
	
	public function value ()
	{
		$value = parent :: value ();
		if ( $this -> mode () == 'time' )
		{
			$value = Carbon :: parse ( $value );
		}
		return $value;
	}
	
	
	/**
	 * Check if the field requires a date value
	 * @return boolean
	 */
	public function dateMode ()
	{
		return in_array ( $this -> mode (), [ 'date', 'datetime', 'timestamp' ] );
	}
	
	
	/**
	 * Check if the field requires a time value
	 * @return boolean
	 */
	public function timeMode ()
	{
		return in_array ( $this -> mode (), [ 'datetime', 'timestamp', 'time' ] );
	}
	
	
	public function renderForForm ( $value = null )
	{
		$value = parent :: renderForForm ( $value );
		return ( $value instanceof Carbon ) ? $value -> format ( $this -> getFormFormat () ) : $value;
	}
	
	
	/**
	 * Get the format for the form value
	 * @param string $mode
	 * @return string
	 */
	public function getFormFormat ( $mode = null )
	{
		switch ( $mode ?: $this -> mode () )
		{
			case 'datetime':
			case 'timestamp':
				return 'Y-m-d H:i';
			case 'time':
				return 'H:i';
			case 'date':
			default:
				return 'Y-m-d';
		}
	}


	protected function widgetConfig ()
	{
		$config = array_merge ( parent:: widgetConfig (), [
			'mode'       => $this->mode (),
			'dateMode'   => $this->dateMode (),
			'timeMode'   => $this->timeMode (),
			'format'     => $this->getFormFormat (),
			'minDate'    => $this->minDate (),
			'maxDate'    => $this->maxDate (),
			'minTime'    => $this->minTime (),
			'maxTime'    => $this->maxTime (),
			'dateFormat' => $this->getFormFormat ( 'date' ),
			'timeFormat' => $this->getFormFormat ( 'time' ),
			'locale'     => $this->appLocale ()
		] );
		return $config;
	}
	
	
	protected function defaultValue ( $default = null )
	{
		$value = $this -> config ( 'default', $default );
		return $value ? Carbon :: parse ( $value ) : '';
	}
	
	
	public function minDate ( $format = 'date' )
	{
		if ( $this -> dateMode () and $min = $this -> config ( 'min' ) )
		{
			return Carbon :: parse ( $min ) -> format ( $this -> getFormFormat ( $format ) );
		}
		return false;
	}
	
	
	public function maxDate ( $format = 'date' )
	{
		if ( $this -> dateMode () and $max = $this -> config ( 'max' ) )
		{
			return Carbon :: parse ( $max ) -> format ( $this -> getFormFormat ( $format ) );
		}
		return false;
	}
	
	
	public function minTime ( $format = 'time' )
	{
		if ( $this -> timeMode () and $min = $this -> config ( 'min' ) )
		{
			return Carbon :: parse ( $min ) -> format ( $this -> getFormFormat ( $format ) );
		}
		return false;
	}
	
	
	public function maxTime ( $format = 'time' )
	{
		if ( $this -> timeMode () and $max = $this -> config ( 'max' ) )
		{
			return Carbon :: parse ( $max ) -> format ( $this -> getFormFormat ( $format ) );
		}
		return false;
	}
	
	
	public function rules ( array $rules = [] )
	{
		$rules = parent :: rules ( $rules );
		
		$rules [] = 'date_format:' . $this -> getFormFormat ();
		
		if ( $this -> dateMode () and $min = $this -> config ( 'min' ) )
		{
			$min = Carbon :: parse ( $min );
			if ( $this -> timeMode () )
			{
				$min -> subMinute ();
			} else
			{
				$min -> subDay ();
			}
			$rules [] = 'after:' . $min -> format ( $this -> getFormFormat () );
		}
		
		if ( $this -> dateMode () and $max = $this -> config ( 'max' ) )
		{
			$max = Carbon :: parse ( $max );
			if ( $this -> timeMode () )
			{
				$max -> addMinute ();
			} else
			{
				$max -> addDay ();
			}
			$rules [] = 'before:' . $max -> format ( $this -> getFormFormat () );
		}
		
		return $rules;
	}
	
	
	public function searchType ()
	{
		return 'Date';
	}
	
	
	/**
	 * @todo search on translatable field
	 */
	public function search ( Driver $builder, $value )
	{
		if ( is_array ( $value ) and isset ( $value [ 'from' ] ) and isset ( $value [ 'to' ] ) )
		{
			if ( strlen ( $value [ 'from' ] ) )
			{
				$value [ 'from' ] = with ( Carbon :: parse ( $value [ 'from' ] ) ) -> getTimestamp ();
			}
			if ( strlen ( $value [ 'to' ] ) )
			{
				$value [ 'to' ] = with ( Carbon :: parse ( $value [ 'to' ] ) ) -> getTimestamp ();
			}

			if ( strlen ( $value [ 'from' ] ) and strlen ( $value [ 'to' ] ) )
			{
				$builder -> whereBetween ( $this -> name (), array_values ( $value ) );
			} elseif ( strlen ( $value [ 'from' ] ) )
			{
				$builder -> where ( $this -> name (), '>=', $value [ 'from' ] );
			} elseif ( strlen ( $value [ 'to' ] ) )
			{
				$builder -> where ( $this -> name (), '<=', $value [ 'to' ] );
			}
		}
	}


	public function getBlueprintTableMethod ()
	{
		switch ( $mode = $this -> mode () )
		{
			case 'datetime':
				return 'dateTime';
			default:
				return $mode;
		}
	}

}




