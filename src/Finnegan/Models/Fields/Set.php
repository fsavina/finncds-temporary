<?php
namespace Finnegan\Models\Fields;


use Finnegan\Contracts\Auth\AuthorizationException;
use Finnegan\Contracts\Models\Driver;
use Finnegan\Models\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;


class Set extends Field implements
	\Finnegan\Contracts\Fields\Options,
	\Finnegan\Contracts\Fields\MultipleChoice,
	\Finnegan\Contracts\Fields\Suggestions,
	\Finnegan\Contracts\Fields\Schema,
	\Finnegan\Contracts\Fields\Sortable,
	\Finnegan\Contracts\Fields\Searchable
{

	protected $widgetConfig = [
		'type' => 'select'
	];
	
	
	/**
	 * @var \Finnegan\Models\Model
	 */
	protected $externalModel;
	
	/**
	 * @var array
	 */
	protected $externalOptions;


	public function boot ()
	{
		if ( $this -> config ( 'multiple' ) )
		{
			$this -> model -> addCast ( $this -> name, 'array' );
		}
		if ( $config = $this -> modelConfig () )
		{
			$this -> model -> addRelationField ( $this -> externalModel () -> name (), $this -> name );
		}
	}
	
	
	/**
	 * Get related model
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function related ()
	{
		if ( $config = $this -> modelConfig () )
		{
			return $this -> model -> belongsTo ( $this -> externalModel () -> name (), $this -> name );
		}
		return null;
	}
	
	
	/**
	 * Get the available options
	 * @return array
	 */
	public function options ()
	{
		$options = $this -> config ( 'options' );
		if ( is_string ( $options ) or is_array ( $options ) )
		{
			$options = $this -> parseDynamicOptions ( $options );
		} elseif ( $this -> config ( 'model' ) )
		{
			$options = $this -> loadModelOptions ();
		}
		return is_array ( $options ) ? $options : [];
	}


	public function formOptions ()
	{
		$options = $this -> config ( 'formOptions' );

		if (!$options)
		{
			return $this->options();
		}

		if ( is_string ( $options ) or is_array ( $options ) )
		{
			$options = $this -> parseDynamicOptions ( $options );
		} elseif ( $this -> config ( 'model' ) )
		{
			$options = $this -> loadModelOptions ();
		}
		return is_array ( $options ) ? $options : [];
	}


	protected function parseDynamicOptions ( $options )
	{
		if ( is_string ( $options ) and starts_with ( $options, 'config:' ) )
		{
			return Config :: get ( str_replace ( 'config:', '', $options ) );
		} elseif ( $callback = $this -> parseCallback ( $options ) )
		{
			return call_user_func_array ( $callback, [ $this ] );
		}
		return $options;
	}


	protected function parseCallback ( $callback )
	{
		if ( is_string ( $callback ) and Str::contains ( $callback, '@' ) )
		{
			$callback = explode ( '@', $callback );
		}
		
		if ( is_array ( $callback ) and ( count ( $callback ) == 2 ) and isset( $callback[ 0 ] ) and $callback[ 0 ] == '$this' )
		{
			$callback[ 0 ] = $this->model;
		}
		
		if ( is_array ( $callback ) and ( count ( $callback ) == 2 ) and is_callable ( $callback ) )
		{
			return $callback;
		}
		return false;
	}


	/**
	 * Get the external model instance
	 * @return \Finnegan\Models\Model
	 */
	public function externalModel ()
	{
		if ( is_null ( $this->externalModel ) and ( $config = $this->modelConfig () ) )
		{
			if ( $config [ 'name' ] == '$this' )
			{
				$this->externalModel = $this->model;
			} else
			{
				$this->externalModel = app ( 'models.factory' )->withContext (
					$this->model->definition ()->getContext (), $config [ 'name' ]
				);
			}
		}
		return $this->externalModel;
	}
	
	
	/**
	 * Get the external model configuration
	 * @return array|boolean
	 */
	protected function modelConfig ()
	{
		if ( $config = $this -> config ( 'model' ) )
		{
			$config = is_array ( $config ) ? $config : [ 'name' => $config ];
			return $config;
		}
		return false;
	}
	
	
	/**
	 * Load options from the configured model
	 * @return array
	 */
	protected function loadModelOptions ()
	{
		if ( ! is_array ( $this -> externalOptions ) and ( $config = $this -> modelConfig () ) )
		{
			$model = $this -> externalModel ();
			
			if ( ! isset ( $config [ 'fields' ] ) )
			{
				$config [ 'fields' ] = [ ];
			}
			if ( ! is_array ( $config [ 'fields' ] ) )
			{
				$config [ 'fields' ] = [ $config [ 'fields' ] ];
			}
			
			$columns = [ '*' ];
			if ( count ( $config [ 'fields' ] ) )
			{
				$columns = array_merge (
					[ $model -> getKeyName () ],
					$config [ 'fields' ]
				);
			}
			$items = $model -> newQueryWithoutScopes () -> get ( $columns );
			
			$options = [];
			foreach ( $items as $item )
			{
				$options [ $item -> getKey () ] = $this -> renderItemLabel ( $item, $config [ 'fields' ] );
			}
			$this -> externalOptions = $options;
		}
		return $this -> externalOptions;
	}
	
	
	/**
	 * Render a single element label from an external object
	 * @param Model $item
	 * @param array $fields
	 * @return string
	 */
	protected function renderItemLabel ( Model $item, $fields = [] )
	{
		if ( count ( $fields ) )
		{
			$render = $item -> fields ( $fields ) -> render ();
			return implode ( ' ', $render -> all () );
		}
		return $item -> __toString ();
	}
	
	
	protected function widgetConfig ()
	{
		$config = parent :: widgetConfig();
		if ( $this -> config ( 'multiple' ) )
		{
			$config [ 'type' ] = 'checkbox';
		}
		return $config;
	}
	
	
	public function render ( $value = null )
	{
		if ( is_object ( $this -> presenter ) )
		{
			return $this -> presenter -> render ();
		}

		$value = is_null ( $value ) ? $this -> value () : $value;
		$options = $this -> options ();
		if ( is_array ( $value ) )
		{
			$render = '';
			foreach ( $value as $key )
			{
				$render .= '<div>' . $this -> renderOne ( $key, $options [ $key ] ) . '</div>';
			}
			return $render;
		}
		return isset ( $options [ $value ] ) ? $this -> renderOne ( $value, $options [ $value ] ) : parent :: render ( false );
	}
	
	
	protected function renderOne ( $value, $label )
	{
		if ( $this -> modelConfig () )
		{
			return $this->htmlBuilder()-> link ( $this -> externalModel () -> actionUrl ( 'show', [ 'id' => $value ] ), $label );
		}
		return e ( trans ( $label ) );
	}
	
	
	public function filterRaw ( $value )
	{
		if ( $this -> config ( 'multiple' ) and is_string ( $value ) and strlen ( $value ) )
		{
			return explode ( ',', $value );
		}
		return parent :: filterRaw ( $value );
	}


	public function filter ( $value )
	{
		return ( is_array ( $value ) or strlen ( $value ) ) ? $value : null;
	}
	
	
	public function rules ( array $rules = [] )
	{
		$rules = parent :: rules ( $rules );
		
		if ( $this -> config ( 'multiple' ) )
		{
			$rules [] = 'array';
		}
		
		if ( $model = $this -> externalModel () )
		{
			$rules [] = 'exists:' . $model -> getTable () . ',' . $model -> getKeyName ();
		} else
		{
			$rules [] = 'in:' . implode ( ',', array_keys ( $this -> formOptions () ) );
		}
		
		return $rules;
	}
	
	
	public function searchType ()
	{
		return $this -> config ( 'multiple' ) ? false : 'Set';
	}
	
	
	/**
	 * @todo search on translatable field
	 */
	public function search ( Driver $builder, $value )
	{
		if ( strlen ( $value ) )
		{
			$builder -> where ( $this -> name (), $value );
		}
	}
	
	
	public function authorize ()
	{
		$check = parent :: authorize ();
		if ( $check and $this -> config ( 'model' ) )
		{
			try
			{
				$this -> externalModel ();
			} catch ( AuthorizationException $e )
			{
				return false;
			}
		}
		return $check;
	}
	
	
	public function runSuggest ( $params = [] )
	{
		$term = mb_strtolower ( $params [ 'q' ] );
		
		$results = [ ];
		if ( strlen ( $term ) > 1 )
		{
			$options = $this -> options ();
			foreach ( $options as $key => $label )
			{
				if ( str_contains ( mb_strtolower ( $label ), $term ) )
				{
					$results [] = static :: makeSuggestion ( $key, $label );
				}
			}
		}
		return $results;
	}
	
	
	public function limit ( $default = null )
	{
		if ( $this -> config ( 'multiple' ) )
		{
			return ( $limit = $this -> config ( 'limit' ) ) ? ( int ) $limit : $default;
		}
		return 1;
	}


	public function allowInsert ( $default = false )
	{
		return false;
	}
	
	
	public function currentSuggestions ()
	{
		$value = $this -> value ();
		
		$values = [ ];
		if ( $value )
		{
			$options = $this -> options ();
			foreach ( ( array ) $value as $key )
			{
				if (isset($options [ $key ]))
				{
					$values [] = static :: makeSuggestion ( $key, $options [ $key ] );
				}
			}
		}
		return $values;
	}


	public function makeSuggestions ( array $ids )
	{
		$values = [ ];
		if ( count ( $ids ) )
		{
			$options = $this -> options ();
			foreach ( $ids as $key )
			{
				$values [] = static :: makeSuggestion ( $key, $options [ $key ] );
			}
		}
		return $values;
	}

	
	protected static function makeSuggestion ( $id, $label )
	{
		return [
			'id' => $id,
			'name' => $label
		];
	}
	
	
	public function isSortable ()
	{
		return ! $this -> config ( 'multiple' );
	}
	
	
}



