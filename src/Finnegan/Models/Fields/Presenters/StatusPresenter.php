<?php
namespace Finnegan\Models\Fields\Presenters;


use Finnegan\Models\Model;


class StatusPresenter extends Presenter
{

	public function render ()
	{
		$value = $this->field->value ();
		$options = $this->field->options ();

		if ( isset( $options[ $value ] ) )
		{
			$type = '';
			switch ( $value )
			{
				case Model::STATUS_PUBLISHED:
					$type = 'success';
					break;
				case Model::STATUS_DRAFT:
					$type = 'info';
					break;
				case Model::STATUS_PENDING_REVIEW:
					$type = 'warning';
					break;
				case Model::STATUS_TRASHED:
					$type = 'alert';
					break;
			}
			$label = trans($options[$value]);
			return "<span class=\"{$type} label\">{$label}</span>";
		}
		return '';
	}

}