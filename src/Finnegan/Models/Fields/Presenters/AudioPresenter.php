<?php

namespace Finnegan\Models\Fields\Presenters;


class AudioPresenter extends Presenter
{
	
	public function render ()
	{
		$url = $this->field->value () ? $this->field->url () : false;
		
		if ( $url )
		{
			$source = $this->html ()->tag ( 'source', '', [
				'src'  => $url,
				'type' => 'audio/mpeg'
			] );
			
			return $this->html ()->tag ( 'audio', $source->toHtml (), [ 'controls' => true ] );
		}
		return '';
	}
	
}