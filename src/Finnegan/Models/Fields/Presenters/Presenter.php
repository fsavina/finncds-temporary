<?php
namespace Finnegan\Models\Fields\Presenters;


use Finnegan\Models\Fields\Field;


abstract class Presenter
	implements
		\Illuminate\Contracts\Support\Renderable,
		\Illuminate\Contracts\Support\Htmlable
{

	/**
	 * @var Field
	 */
	protected $field;


	public function __construct ( Field $field, $value = null )
	{
		$this -> field = $field;
	}


	public function toHtml ()
	{
		return $this -> render ();
	}


	public function __toString ()
	{
		return $this -> render ();
	}


	protected function html ()
	{
		return $this->field->htmlBuilder();
	}
}