<?php
namespace Finnegan\Models\Fields\Presenters;


class MapPresenter extends Presenter
{

	public function render ()
	{
		$value = implode ( ',', array_values ( $this->field->value () ) );
		$params = [
			'center'  => $value,
			'zoom'    => 7,
			'size'    => '600x300',
			'markers' => '|' . $value
		];
		$url = 'https://maps.googleapis.com/maps/api/staticmap?' . http_build_query ( $params );
		return $this->html()->image ( $url );
	}

}