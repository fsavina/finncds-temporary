<?php
namespace Finnegan\Models\Fields\Presenters;


class TagsPresenter extends Presenter
{

	public function render ()
	{
		$render = '';
		if ( count ( $relations = $this->field->relation ( true )->get () ) )
		{
			foreach ( $relations as $related )
			{
				$render .= "<a href=\"{$related -> recordActionUrl ( 'show' )}\"><span class=\"label secondary\">{$related}</span></a> ";
			}
		}
		return $render;
	}

}