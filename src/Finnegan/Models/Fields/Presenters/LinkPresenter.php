<?php

namespace Finnegan\Models\Fields\Presenters;


use Finnegan\Contracts\Fields\File;


class LinkPresenter extends Presenter
{
	
	public function render ()
	{
		$title = $this->field->value ();
		
		$url = ( $this->field instanceof File ) ? $this->field->url () : $title;
		if ( filter_var ( $url, FILTER_VALIDATE_EMAIL ) )
		{
			$url = "mailto:$url";
		}
		
		return strlen ( $url ) ? $this->html ()->link ( $url, $title )->__toString () : '';
	}
	
}