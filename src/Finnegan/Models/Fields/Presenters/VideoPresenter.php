<?php
namespace Finnegan\Models\Fields\Presenters;


use Finnegan\Contracts\Fields\File;


/**
 * Class VideoPresenter
 *
 * Note: Requires the "cohensive/embed" package to work
 */
class VideoPresenter extends LinkPresenter
{
	
	public function render ()
	{
		if ( ! $this->embedderExists () )
		{
			throw new \Exception( '"cohensive/embed" package not found' );
		}

		$url = ( $this->field instanceof File ) ? $this->field->url () : $this->field->value ();
		return strlen ( $url ) ? $this->parseUrl ( $url ) : '';
	}
	
	
	protected function parseUrl ( $url )
	{
		$factory = app ( 'embed' );
		$url = app ( 'url' )->asset ( $url );
		if ( $embed = $factory->make ( $url )->parseUrl () )
		{
			return $this->field->layoutManager ()->presenter ()->videoEmbed ( $embed );
		}
		return parent::render ();
	}
	
	
	protected function embedderExists ()
	{
		return class_exists ( 'Cohensive\Embed\EmbedServiceProvider' );
	}
	
}