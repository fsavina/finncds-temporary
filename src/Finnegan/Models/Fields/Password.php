<?php
namespace Finnegan\Models\Fields;


use Illuminate\Contracts\Hashing\Hasher;


class Password
	extends
		Literal
	implements
		\Finnegan\Contracts\Fields\Password
{

	protected $widgetConfig = [
		'type' => 'password'
	];

	/**
	 * @var Hasher
	 */
	protected $hash;


	public function boot ( Hasher $hash )
	{
		$this->hash = $hash;
	}


	public function render ( $value = null, $escapeHtml = true )
	{
		$value = is_null ( $value ) ? $this -> value () : $value;
		return parent :: render ( strlen ( $value ) ? '********' : '', $escapeHtml );
	}


	public function filter ( $value )
	{
		if ( ! strlen ( $value ) )
		{
			return $this->model->exists ? $this->value () : null;
		}
		return $this->hash->make ( $value );
	}
	
	
	public function renderForForm ( $value = null )
	{
		return '';
	}


	public function rules ( array $rules = [] )
	{
		$rules = parent :: rules ( $rules );
		if ( in_array ( 'required', $rules ) and $this -> model () -> exists )
		{
			if ( ( $key = array_search ( 'required', $rules ) ) !== false )
			{
				unset ( $rules [ $key ] );
			}
		}
		return $rules;
	}
	
	
	public function isRequired ()
	{
		$rules = $this -> config ( 'rules', [ ] );
		$rules = ! is_array ( $rules ) ? explode ( '|', $rules ) : $rules;
		return in_array ( 'required', $rules );
	}
	
	
	public function searchType ()
	{
		return false;
	}
	
	
	public function isSortable ()
	{
		return false;
	}

}



