<?php
namespace Finnegan\Models\Fields;


use Finnegan\Contracts\Models\Fields as FieldsContract;
use Finnegan\Widgets\Widget;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;


abstract class Field
{

	const DEFAULT_TYPE = 'Literal';
	

	/**
	 * The field name
	 * @var string
	 */
	protected $name;

	/**
	 * The Model instance
	 * @var \Finnegan\Models\Model
	 */
	protected $model;
	
	
	/**
	 * Define how to cast the value on the model
	 * @var string
	 */
	protected $castAs;
	
	/**
	 * The default validation rules
	 * @var array
	 */
	protected $rules = [];

	/**
	 * @var Presenters\Presenter
	 */
	protected $presenter;

	/**
	 * The widget instance
	 * @var \Finnegan\Widgets\Widget
	 */
	protected $widget;
	
	/**
	 * The default widget configuration
	 * @var array
	 */
	protected $widgetConfig = [];
	
	/**
	 * The errors list
	 * @var array
	 */
	protected $errors = [];
	
	protected $locale;
	

	/**
	 * @param string $name
	 * @param mixed $model
	 */
	final protected function __construct ( $name, FieldsContract $model )
	{
		$this->name = $name;
		$this->model = $model;

		$this->setUp ();
		if (method_exists($this, 'boot'))
		{
			app ()->call ( [ $this, 'boot' ] );
		}
	}
	
	
	public function setUp ()
	{
		if ( is_object ( $this -> model ) )
		{
			if ( strlen ( $this -> castAs ) )
			{
				$this -> model -> addCast ( $this -> name, $this -> castAs );
			}
				
			foreach ( get_class_methods ( $this ) as $method )
			{
				if ( substr ( $method, 0, 2 ) == 'on' )
				{
					$event = Str :: snake ( substr ( $method, 2 ) );
					$this -> model -> registerEvent ( $event, [ $this, $method ] );
				}
			}

			if ( count ( $events = (array) $this->config ( 'events', [ ] ) ) )
			{
				foreach ( $events as $event )
				{
					$callback = data_get ( $event, 'callback' );
					$callback = is_array ( $callback ) ? $callback : explode ( '@', $callback );
					if ( $callback[ 0 ] == '$this' )
					{
						$callback[ 0 ] = $this->model;
					}
					$event = data_get ( $event, 'event' );
					$this->model->registerEvent ( $event, $callback );
				}
			}
		}

		if ( $presenter = $this->config ( 'presenter' ) )
		{
			if ( ! class_exists ( $presenter ) )
			{
				$presenter = Str::studly ( $presenter );
				$presenter = "Finnegan\\Models\\Fields\\Presenters\\{$presenter}Presenter";
			}
			$this->presenter = new $presenter( $this );
		}
	}


	/**
	 * Load a new Field instance
	 * @param string $name
	 * @param mixed $model
	 * @return \Finnegan\Models\Fields\Field
	 */
	public static function make ( $name, $model )
	{
		$type = $model -> config ( "fields.$name.type", self :: DEFAULT_TYPE );
		$className = self :: resolveClassName ( $type );
		return new $className ( $name, $model );
	}


	/**
	 * Convert type to the corresponding complete class name
	 * @param string $type
	 * @return string
	 */
	protected static function resolveClassName ( $type )
	{
		if ( class_exists ( $type ) )
		{
			return $type;
		}
		return __NAMESPACE__ . '\\' . Str :: studly ( $type );
	}


	/**
	 * Get the field name
	 * @return string
	 */
	public function name ()
	{
		return $this -> name;
	}


	/**
	 * Get the model instance
	 * @return \Finnegan\Models\Model
	 */
	public function model ()
	{
		return $this -> model;
	}


	/**
	 * Get the required configuration value
	 * @param string $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function config ( $key, $default = false )
	{
		return $this -> model -> config ( "fields.{$this -> name}.{$key}", $default );
	}


	/**
	 * Get the field label
	 * @return string
	 */
	public function label ()
	{
		$default = Str :: title ( str_replace ( '_', ' ', $this -> name ) );
		return trans ( $this -> config ( 'label', $default ) );
	}


	/**
	 * Get the current field value or its default value
	 * @return mixed
	 */
	public function value ()
	{
		if ( ! $this -> isTranslatable () )
		{
			if ( $this -> model -> hasAttribute ( $this -> name ) )
			{
				return $this -> model -> getAttribute ( $this -> name );
			}
		} else
		{
			$name = $this -> name;
			if ( $this -> isTranslatable () )
			{
				$name .= ':' . $this -> locale ();
			}
			return $this -> model -> getAttribute ( $name );
		}

		return $this -> defaultValue ();
	}


	public function isEmpty ()
	{
		$name = $this->name;

		if ( $this->isTranslatable () )
		{
			$name .= ':' . $this->locale ();
		}

		return ! $this->model->getAttribute ( $name );
	}


	public function setLocale ( $locale )
	{
		$this -> locale = $locale;
		return $this;
	}


	public function locale ()
	{
		return $this->locale ?: $this->appLocale ();
	}
	
	
	protected function appLocale ()
	{
		return App::getLocale ();
	}
	
	
	/**
	 * Get the field default value, if available
	 * @param mixed $default
	 * @return mixed
	 */
	protected function defaultValue ( $default = null )
	{
		return $this -> config ( 'default', $default );
	}
	
	
	/**
	 * Render the current field value for visualization
	 * @return string
	 */
	final public function __toString ()
	{
		return $this -> render ();
	}
	
	
	/**
	 * Render the current field value for visualization
	 * @param mixed $value
	 * @return string
	 */
	public function render ( $value = null )
	{
		$value = is_null ( $value ) ? $this -> value () : $value;
		return strlen ( $value ) ? $value : '&nbsp;';
	}
	
	
	/**
	 * Render the current field value for the list page
	 * @param mixed $value
	 * @return string
	 */
	public function renderForList ( $value = null )
	{
		return $this -> render ( $value );
	}


	/**
	 * Render the current field value for the form widget
	 * @param mixed $value
	 * @return string
	 */
	public function renderForForm ( $value = null )
	{
		return is_null ( $value ) ? $this -> value () : $value;
	}

	
	/**
	 * Check if the field has to be shown in a form
	 * @return boolean
	 */
	public function showInForm ()
	{
		return (bool) $this -> config ( 'form', true );
	}

	
	/**
	 * Check if the field has to be shown in the preview
	 * @return boolean
	 */
	public function showInPreview ()
	{
		return (bool) $this -> config ( 'view', true );
	}
	
	
	/**
	 * Set the current field value
	 * @param mixed $value
	 * @return Field
	 */
	public function setValue ( $value )
	{
		$this -> model -> setAttribute ( $this -> name, $value );
		return $this;
	}
	

	/**
	 * Filter incoming value
	 * @param mixed $value
	 * @return mixed
	 */
	public function filter ( $value )
	{
		return $value;
	}
	
	
	/**
	 * Filter incoming value (before the validation)
	 * @param mixed $value
	 * @return mixed
	 */
	public function filterRaw ( $value )
	{
		return $value;
	}
	
	
	public function isDirty ()
	{
		return $this -> model -> isDirty ( $this -> name );
	}
	
	
	public function isTranslatable ()
	{
		return ( bool ) $this -> config ( 'translatable' );
	}


	/**
	 * @param string $action
	 * @param array  $params
	 * @return string
	 */
	public function actionUrl ( $action, array $params = [ ] )
	{
		$params [ 'field' ] = $this->name;
		$params [ 'action' ] = $action;

		return $this->model->actionUrl ( 'field', $params );
	}
	

	/**
	 * Get the widget instance
	 * @return Widget
	 */
	public function widget ()
	{
		if ( is_null ( $this -> widget ) )
		{
			$config = $this -> widgetConfig();
			$type = data_get ( $config, 'type', Widget::DEFAULT_TYPE );
			$this -> widget = Widget :: make ( $type, $this, $config );
		}
		return $this -> widget;
	}


	/**
	 * Get the widget configuration for the current field.
	 * @return array
	 */
	protected function widgetConfig ()
	{
		$config = $this->config ( 'widget', [ ] );
		if ( is_string ( $config ) )
		{
			$config = [ 'type' => $config ];
		}
		$config = array_merge ( $this->widgetConfig, $config );
		if ( ! array_key_exists ( 'label', $config ) )
		{
			$config[ 'label' ] = $this->label ();
		}
		if ( $this->isRequired () )
		{
			$config[ 'required' ] = true;
		}
		$config [ 'rules' ] = $this -> rules ();
		return $config;
	}
	
	
	/**
	 * Get the field validation rules
	 * @param array $rules
	 * @return array
	 */
	public function rules ( array $rules = [] )
	{
		$configRules = $this -> config ( 'rules', [ ] );
		$configRules = ! is_array ( $configRules ) ? explode ( '|', $configRules ) : $configRules;
		return array_merge ( $this -> rules, $rules, $configRules );
	}
	
	
	/**
	 * Check if the field is required
	 * @return boolean
	 */
	public function isRequired ()
	{
		return in_array ( 'required', $this -> rules () );
	}
	
	
	/**
	 * Check if the user is allowed to access the current field
	 * @return boolean
	 */
	public function authorize ()
	{
		$role = $this -> config ( 'auth.role' );
		return ( ! $role or ! Auth :: check () or Auth :: user () -> achieve ( $role ) );
	}
	
	
	/**
	 * Check if the current field is sortable
	 * @return boolean
	 */
	public function isSortable ()
	{
		return true;
	}
	
	
	/**
	 * Set the field errors
	 * @param array $errors
	 * @return \Finnegan\Models\Fields\Field
	 */
	public function setErrors ( $errors )
	{
		$this -> errors = ( array ) $errors;
		return $this;
	}


	/**
	 * Check if the field has errors
	 * @return boolean
	 */
	public function hasErrors ()
	{
		return ( bool ) count ( $this -> errors );
	}


	/**
	 * Get the the field errors
	 * @return array
	 */
	public function errors ()
	{
		return $this -> errors;
	}


	/**
	 * @return \Finnegan\Contracts\Layout\Manager
	 */
	public function layoutManager ()
	{
		return app ( 'Finnegan\Contracts\Layout\Manager' );
	}


	public function htmlBuilder ()
	{
		return $this->layoutManager ()->html ();
	}


	public function populateMigration ()
	{
		return $this->renderMigrationStub ( $this->getMigrationStubName () );
	}


	protected function renderMigrationStub ( $name )
	{
		$path = __DIR__ . "/Migrations/{$name}.blade.php";
		return view ()->file ( $path, [ 'field' => $this ] )->render ();
	}


	protected function getMigrationStubName ()
	{
		return Str::snake ( ( new \ReflectionClass( $this ) )->getShortName () );
	}


	public function populateOnSchemaReadyMigration ()
	{
		return '';
	}

}





