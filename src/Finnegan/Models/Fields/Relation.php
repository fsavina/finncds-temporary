<?php
namespace Finnegan\Models\Fields;


use Finnegan\Contracts\Models\Driver;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Support\Facades\DB;


class Relation
	extends
		Field
	implements
		\Finnegan\Contracts\Fields\Options,
		\Finnegan\Contracts\Fields\MultipleChoice,
		\Finnegan\Contracts\Fields\Searchable,
		\Finnegan\Contracts\Fields\Suggestions,
		\Finnegan\Contracts\Fields\Relation
{

	/**
	 * @var Gate
	 */
	protected $gate;

	protected $relation;

	protected $relationsIds;
	
	protected $options;

	protected $widgetConfig = [
		'type' => 'autocomplete'
	];


	public function boot ( Gate $gate )
	{
		$this->gate = $gate;
	}


	public function setValue ( $value )
	{
		$this -> relationsIds = ( array ) $value;
	}
	
	
	public function filterRaw ( $value )
	{
		$value = trim ( strip_tags ( $value ) );
		$values = [];
		if ( is_string ( $value ) and strlen ( $value ) )
		{
			foreach ( explode ( ',', $value ) as $tag )
			{
				if ( is_numeric ( $tag ) )
				{
					$values [] = $tag;
				} elseif ( $this -> allowInsert () and ( $new = $this->addNewItem ( $tag ) ) )
				{
					$values [] = $new;
				}
			}
		}
		return $values;
	}
	
	
	protected function addNewItem ( $item )
	{
		$model = $this -> relation () -> getRelated ();
		if ( $fields = $model->config ( 'system.suggestions' ) )
		{
			$fields = ( array ) $fields;
			$instance = $model -> newInstance();
			$instance->setAttribute ( $fields [ 0 ], $item );
			$instance->save ();
			return $instance->getKey ();
		}
		return false;
	}


	public function onSaved ()
	{
		if ( is_array ( $this -> relationsIds ) )
		{
			$this -> relation () -> sync ( $this -> relationsIds );
		}
	}


	public function onDeleted ()
	{
		$this -> relation () -> sync ( [ ] );
	}


	public function value ()
	{
		if ( is_null ( $this -> relationsIds ) )
		{
			$relations = $this -> relation ( true ) -> get ();
			
			$this -> relationsIds = [ ];
			foreach ( $relations as $related )
			{
				$this -> relationsIds [] = $related -> getKey ();
			}
		}
		return $this -> relationsIds;
	}
	
	
	public function limit ( $default = false )
	{
		if ( $limit = $this -> config ( 'limit' ) )
		{
			return ( int ) $limit;
		}
		return $default;
	}


	public function allowInsert ( $default = false )
	{
		if ( $this -> config ( 'insert' ) )
		{
			return $this->gate->allows ( 'create', $this -> relation () -> getRelated () );
		}
		return $default;
	}


	/**
	 * Get the relation instance
	 * @param bool $forceReload
	 * @return \Finnegan\Models\Relations\BelongsToMany|\Finnegan\Models\Relations\MorphToMany
	 */
	public function relation ( $forceReload = false )
	{
		if ( ( $forceReload or is_null ( $this -> relation ) ) and ( $config = $this -> modelConfig () ) )
		{
			if ( $morph = $this -> config ( 'morph' ) )
			{
				$this -> relation = $this -> model -> morphToMany ( $config [ 'name' ], $morph );
			} else
			{
				$this -> relation = $this -> model -> belongsToMany ( $config [ 'name' ] );
			}
		}
		return $this -> relation;
	}
	
	
	/**
	 * Get the related model configuration
	 * @return array|boolean
	 */
	protected function modelConfig ()
	{
		$config = $this -> config ( 'model' );
		if ( ! $config )
		{
			// If no related model is defined, we'll use the field name
			$config = $this -> name;
		}
		return is_array ( $config ) ? $config : [ 'name' => $config ];
	}
	
	
	public function relationName ()
	{
		$config = $this -> modelConfig ();
		return $config [ 'name' ];
	}

	
	public function rules ( array $rules = [] )
	{
		$rules = parent :: rules ( $rules );
		
		$rules [] = 'array';
		
		$relatedModel = $this -> relation () -> getRelated ();
		$rules [] = 'exists:' . $relatedModel -> getTable () . ',' . $relatedModel -> getKeyName ();
		
		return $rules;
	}
	
	
	public function render ( $value = null )
	{
		if ( is_object ( $this -> presenter ) )
		{
			return $this -> presenter -> render ();
		}

		$relations = $this -> relation ( true ) -> get ();
		
		$render = '';
		if ( count ( $relations ) )
		{
			foreach ( $relations as $related )
			{
				$render .= '<div><a target="_blank" href="' . $related -> recordActionUrl ( 'show' ) . '">' . $related . '</a></div>';
			}
		}
		return $render;
	}
	
	
	public function options ()
	{
		if ( is_null ( $this -> options ) )
		{
			$this -> options = [];
			if ( $config = $this -> modelConfig () )
			{
				$relation = $this -> relation ();
				$availables = $relation -> getRelated () -> get ();
				
				$this -> options = [ ];
				foreach ( $availables as $available )
				{
					$this -> options [ $available -> getKey () ] = $available -> __toString ();
				}
			}
		}
		return $this -> options;
	}


	public function formOptions ()
	{
		return $this -> options ();
	}
	

	public function runSuggest ( $params = [] )
	{
		$term = $params [ 'q' ];
		
		$results = [ ];
		if ( strlen ( $term ) > 1 )
		{
			$model = $this -> relation () -> getRelated ();
			$suggestions = $model -> getSuggestions ( $term );
			if ( $suggestions and count ( $suggestions ) )
			{
				foreach ( $suggestions as $item )
				{
					$results [] = static :: makeSuggestion ( $item );
				}
			}
		}
		return $results;
	}
	
	
	public function currentSuggestions ()
	{
		$values = [ ];
		foreach ( $this -> relation ( true ) -> get () as $relation )
		{
			$values [] = static :: makeSuggestion ( $relation );
		}
		return $values;
	}

	
	protected static function makeSuggestion ( $item )
	{
		return [
			'id' => $item -> getKey (),
			'name' => $item -> __toString ()
		];
	}


	public function makeSuggestions ( array $ids )
	{
		$values = [];
		if ( count ( $ids ) )
		{
			foreach ( $this -> relation() -> getRelated() -> findMany ( $ids ) as $item )
			{
				$values [] = static :: makeSuggestion ( $item );
			}
		}
		return $values;
	}


	public function searchType ()
	{
		return 'Autocomplete';
	}
	
	
	/**
	 * @todo search on translatable field
	 */
	public function search ( Driver $builder, $value )
	{
		if ( strlen ( $value ) )
		{
			$relation = $this -> relation ();
			$parentKey = $relation -> getParent () -> getQualifiedKeyName ();

			$ids = ! is_array ( $value ) ? explode ( ',', $value ) : $value;
			$totalIds = count($ids);

			$builder -> join ( $relation -> getTable (),function($join) use ($relation, $parentKey) {
				$join -> on ( $parentKey, '=', $relation -> getForeignKey () );

				if ( $relation instanceof \Finnegan\Models\Relations\MorphToMany )
				{
					$join -> where ( $relation -> getMorphType(), '=', DB::raw ( $relation -> getMorphClass () ) );
				}
			} ) -> whereIn ( $relation -> getOtherKey (), $ids )
				-> groupBy ( $parentKey )
				-> havingRaw ( "COUNT({$parentKey}) = {$totalIds}" );
		}
	}

}



