<?php
namespace Finnegan\Models\Fields;


class Email extends Literal
{

	protected $rules = [ 'email' ];

	protected $widgetConfig = [ 'inputType' => 'email' ];

	protected $unique = true;

}



