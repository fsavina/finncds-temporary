<?php
namespace Finnegan\Models\Fields;


use Illuminate\Support\Traits\Macroable;


class Macro extends Field
{

	use Macroable;


	public function showInForm ()
	{
		return false;
	}


	public function render ( $value = null )
	{
		$function = $this->config ( 'macro' );

		$params = [];
		if ( ! is_string ( $function ) )
		{
			$params = data_get ( $function, 'params', [ ] );
			$function = data_get ( $function, [ 'name' ] );
		}

		if ( ! $this->hasMacro ( $function ) )
		{
			if ( ! method_exists ( $this, $method = "{$function}Macro" ) )
			{
				throw new \InvalidArgumentException( "Macro function not supported [{$function}]" );
			}
			$function = $method;
			$params = $this->checkAndPrepareParams ( $method, $params );
		}
		return call_user_func_array ( [ $this, $function ], $params );
	}


	protected function checkAndPrepareParams ( $method, $rawParams )
	{
		$reflector = new \ReflectionClass( $this );

		$params = [ ];
		foreach ( $reflector->getMethod ( $method )->getParameters () as $parameter )
		{
			if ( isset( $rawParams[ $parameter->getName () ] ) )
			{
				$params[ $parameter->getName () ] = $rawParams[ $parameter->getName () ];
			} elseif ( ! $parameter->isOptional () )
			{
				throw new \InvalidArgumentException( "Missing macro required parameter [{$parameter->getName ()}]" );
			}
		}
		return $params;
	}


	protected function countRelationsMacro ( $relationType, $model, $name = null )
	{
		$types = [ 'hasMany', 'belongsTo', 'belongsToMany', 'morphToMany', 'morphedByMany' ];

		if ( ! in_array ( $relationType, $types ) )
		{
			throw new \InvalidArgumentException( "Relation type not supported [{$relationType}]" );
		}

		$params = is_null ( $name ) ? compact ( 'model' ) : compact ( 'model', 'name' );
		return with ( call_user_func_array ( [ $this->model, $relationType ], $params ) )->count ();
	}

}