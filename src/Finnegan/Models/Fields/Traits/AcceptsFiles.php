<?php
namespace Finnegan\Models\Fields\Traits;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use League\Flysystem\Adapter\Local;


trait AcceptsFiles
{
	
	
	protected function receiveFile ( UploadedFile $file )
	{
		$filename = $file->getClientOriginalName ();
		$filename = $this->sanitizeFilename ( $filename );
		$filename = $this->getUniqueFilename ( $filename, $this->relativePath () );
		
		$this->disk->putFileAs ( $this->relativePath (), $file, $filename, 'public' );
		return $filename;
	}
	
	
	/**
	 * @param string $filename
	 * @return string
	 */
	protected function extension ( $filename = null )
	{
		$filename = is_null ( $filename ) ? $this->value () : $filename;
		return pathinfo ( $filename )[ 'extension' ];
	}
	
	
	/**
	 * @param string $filename
	 * @return string
	 */
	protected function basename ( $filename = null )
	{
		$filename = is_null ( $filename ) ? $this->value () : $filename;
		return pathinfo ( $filename )[ 'filename' ];
	}
	
	
	/**
	 * Get a short name of the file name for rendering
	 * @param string $filename
	 * @return string
	 */
	protected function shortName ( $filename )
	{
		$limit = 20;
		$short = $this->basename ( $filename );
		if ( Str:: length ( $short ) <= 30 )
		{
			return $filename;
		}
		$short = Str:: limit ( $short, $limit ) . mb_substr ( $short, -5, null, 'UTF-8' );
		return $short . '.' . $this->extension ( $filename );
	}
	
	
	/**
	 * Get a sanitized version of the file name.
	 * @param string $filename
	 * @return string
	 */
	protected function sanitizeFilename ( $filename )
	{
		$extension = strtolower ( $this->extension ( $filename ) );
		$filename = $this->basename ( $filename );
		$filename = Str:: slug ( $filename );
		return "{$filename}.{$extension}";
	}
	
	
	/**
	 * Get a unique file name for the upload path
	 * @param string $filename
	 * @param string $path
	 * @return string
	 */
	protected function getUniqueFilename ( $filename, $path )
	{
		$basename = $this->basename ( $filename );
		$extension = '.' . $this->extension ( $filename );
		
		$i = 1;
		$newName = $basename;
		while ( $this->disk->exists ( $path . $newName . $extension ) )
		{
			$newName = $basename . '-' . $i++;
		}
		return $newName . $extension;
	}
	
	
	/**
	 * Get the path relative to the Flysystem disk root
	 * @param string $filename
	 * @return string
	 */
	protected function relativePath ( $filename = '' )
	{
		return 'vendor/finnegan/' . $this->model->name () . "/{$this->name}/" . $filename;
	}
	
	
	/**
	 * Get the upload directory path. Create the directory if it's missing.
	 * @return string
	 * @throws \Exception
	 */
	public function uploadPath ()
	{
		return $this->createDirectoryIfMissing ( $this->relativePath () );
	}
	
	
	protected function createDirectoryIfMissing ( $path )
	{
		if ( ! $this->disk->exists ( $path ) )
		{
			$this->disk->makeDirectory ( $path );
		}
		return $path;
	}
	
	
	/**
	 * Get the public base path
	 * @param string $filename
	 * @return string
	 */
	protected function publicPath ( $filename = null )
	{
		$filename = $filename ?: $this->value ();
		
		if ( $this->disk->getDriver ()->getAdapter () instanceof Local )
		{
			return $this->localPublicPath ( $filename );
		}
		
		return $this->disk->url ( $this->relativePath ( $filename ) );
	}
	
	
	protected function localPublicPath ( $filename = null )
	{
		return '/uploads/' . $this->relativePath ( $filename );
	}
	
	
	/**
	 * Delete the current file
	 * @return boolean
	 */
	protected function deleteFile ()
	{
		$path = $this->uploadPath () . DIRECTORY_SEPARATOR;
		if ( $filename = $this->value () and $this->disk->exists ( $path . $filename ) )
		{
			return $this->disk->delete ( $path . $filename );
		}
		return true;
	}
	
}