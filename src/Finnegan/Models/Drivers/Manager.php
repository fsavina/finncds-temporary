<?php
namespace Finnegan\Models\Drivers;


use Finnegan\Models\Model;
use Illuminate\Support\Str;


class Manager
{


	protected static $instance;

	protected $model;


	/**
	 * Manager constructor.
	 * @param Model $model
	 */
	public function __construct ( Model $model )
	{
		$this->model = $model;
	}


	/**
	 * @param Model $model
	 * @return Manager
	 */
	public function setModel ( $model )
	{
		$this->model = $model;
		return $this;
	}


	public function driver ( $query, $driver = null )
	{
		$driver = $driver ?: $this->getDefaultDriver ();

		$method = 'create' . Str::studly ( $driver ) . 'Driver';
		if ( method_exists ( $this, $method ) )
		{
			return $this->$method( $query );
		}

		throw new \InvalidArgumentException( "Driver [$driver] not supported." );
	}


	/**
	 * @return string
	 */
	public function getDefaultDriver ()
	{
		return $this->model->config ( 'storage.driver', 'database' );
	}


	/**
	 * @return DatabaseDriver
	 */
	public function createDatabaseDriver ( $query )
	{
		return new DatabaseDriver( $query );
	}


	/**
	 * @return FilesystemDriver
	 */
	public function createFilesystemDriver ( $query )
	{
		return new FilesystemDriver( $query );
	}


	/**
	 * @param Model $model
	 * @return Manager
	 */
	public static function getInstance ( Model $model )
	{
		if ( is_null ( static:: $instance ) )
		{
			static:: $instance = new static ( $model );
		} else
		{
			static::$instance->setModel ( $model );
		}
		return static:: $instance;
	}

}