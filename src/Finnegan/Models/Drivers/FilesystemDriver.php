<?php
namespace Finnegan\Models\Drivers;


use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;


class FilesystemDriver extends DatabaseDriver
{

	/**
	 * @var Filesystem
	 */
	protected $disk;

	protected $source;


	protected function path ()
	{
		return $this->model->definition ()->getStorageFilePath ();
	}


	/**
	 * @return Filesystem
	 */
	protected function disk ()
	{
		if ( is_null ( $this->disk ) )
		{
			$disk = $this->model->definition ()->get ( 'storage.file.disk' );
			$this->disk = app ( 'filesystem' )->disk ( $disk );
		}
		return $this->disk;
	}


	protected function loadSource ()
	{
		if ( is_null ( $this->source ) )
		{
			$this->source = json_decode ( $this->disk ()->get ( $this->path () ), true );
		}
		return $this->source;
	}


	protected function storeSource ( array $source = null )
	{
		$this->source = $source ?: $this->source;
		$this->disk ()->put ( $this->path (), json_encode ( $this->source ) );
	}


	public function find ( $id, $columns = [ '*' ] )
	{
		if ( is_array ( $id ) )
		{
			return $this->findMany ( $id, $columns );
		}

		$source = $this->loadSource ();
		if ( ! isset( $source[ $id ] ) )
		{
			return null;
		}

		return $this->model->newFromBuilder ( $this->filterColumns ( $source[ $id ], $columns ) );
	}


	public function findMany ( $ids, $columns = [ '*' ] )
	{
		if ( empty( $ids ) )
		{
			return $this->model->newCollection ();
		}

		$models = [ ];
		foreach ( array_intersect_key ( $this->loadSource (), array_flip ( $ids ) ) as $model )
		{
			$models[] = $this->filterColumns ( $model, $columns );
		}
		return $this->model->hydrate ( $models, null, $this->model );
	}


	protected function filterColumns ( array $item, $columns = [ '*' ] )
	{
		if ( $columns != [ '*' ] )
		{
			return array_intersect_key ( $item, array_flip ( (array) $columns ) );
		}
		return $item;
	}


	public function update ( array $values )
	{
		$source = $this->loadSource ();

		$item = array_get ( $source, $this->model->getKey (), [ ] );
		foreach ( $values as $column => $value )
		{
			$item[ $column ] = $value;
		}

		array_set ( $source, $this->model->getKey (), $item );
		$this->storeSource ( $source );
		return 1;
	}


	public function insert ( array $values )
	{
		$source = $this->loadSource ();

		$id = count ( $source ) ? ( max ( array_keys ( $source ) ) + 1 ) : 1;
		array_set ( $source, $id, array_merge ( [ 'id' => $id ], $values ) );

		$this->storeSource ( $source );
		return $id;
	}


	public function insertGetId ( array $values, $sequence = null )
	{
		return $this->insert ( $values );
	}


	public function delete ()
	{
		$source = $this->loadSource ();

		if ( array_has ( $source, $this->model->getKey () ) )
		{
			array_forget ( $source, $this->model->getKey () );
		}

		$this->storeSource ( $source );
	}


	public function paginate ( $perPage = null, $columns = [ '*' ], $pageName = 'page', $page = null )
	{
		$source = $this->loadSource ();
		$total = count ( $source );

		$page = $page ?: Paginator::resolveCurrentPage ( $pageName );
		$perPage = $perPage ?: $this->model->getPerPage ();

		$models = $this->model->hydrate (
			array_slice ( $source, ( $page - 1 ) * $perPage, $perPage ),
			null,
			$this->model
		);

		return new LengthAwarePaginator( $models, $total, $perPage, $page, [
			'path'     => Paginator::resolveCurrentPath (),
			'pageName' => $pageName,
		] );
	}


	public function truncate ()
	{
		$this->source = new \stdClass();
		$this->storeSource ();
	}


}