<?php
namespace Finnegan\Models\Drivers;


use Finnegan\Contracts\Models\Driver as DriverContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;


class DatabaseDriver extends Builder implements DriverContract
{

	/**
	 * Get the hydrated models without eager loading.
	 * @param array $columns
	 * @return \Illuminate\Database\Eloquent\Model[]
	 */
	public function getModels ( $columns = [ '*' ] )
	{
		$results = $this->query->get ( $columns );

		$connection = $this->model->getConnectionName ();
		
		return $this->model->hydrate ( $results->all (), $connection, $this->model->definition () )->all ();
	}


	public function truncate ()
	{
		DB::table ( $this->model->getTable () )->truncate ();
	}

}