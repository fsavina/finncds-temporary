<?php

namespace Finnegan\Models;


use Finnegan\Translation\TranslationsManager;
use Illuminate\Database\Eloquent\Relations\HasMany;


/**
 * https://github.com/dimsav/laravel-translatable
 */
class TranslatableFields extends Model
{
    
    use \Dimsav\Translatable\Translatable;
    
    public    $translatedAttributes = [];
    
    protected $translationModel     = 'Finnegan\Models\Translation';
    
    /**
     * @var TranslationsManager
     */
    protected $manager;
    
    
    public function setup ( TranslationsManager $manager )
    {
        $this->manager = $manager;
        
        foreach ( $this->fields ()->isTranslatable () as $field )
        {
            $this->translatedAttributes [] = $field->name ();
        }
    }
    
    
    public function fromForm ( array $input, array $files = [] )
    {
        $validator = $this->validator ( $input, $files );
        if ( $validator->passes () )
        {
            $input = $validator->valid ();
            if ( count ( $files ) )
            {
                foreach ( array_dot ( $files ) as $key => $file )
                {
                    array_set ( $input, $key, $file );
                }
            }
            
            foreach ( $this->formFields () as $name => $field )
            {
                if ( ! $field->isTranslatable () )
                {
                    $field->setValue ( $field->filter ( array_get ( $input, $name ) ) );
                }
            }
            if ( $this->definitionInstance->hasTranslatableFields () )
            {
                foreach ( $this->getLocales () as $locale => $label )
                {
                    $translation = $this->getTranslationOrNew ( $locale );
                    $translationsInput = data_get ( $input, $locale, [] );
                    
                    foreach ( $this->fields ()->isTranslatable () as $name => $field )
                    {
                        $field->setLocale ( $locale );
                        $translation->$name = $field->filter ( data_get ( $translationsInput, $name ) );
                    }
                }
            }
            return true;
        }
        return false;
    }
    
    
    /**
     * @return \Finnegan\Models\Translation
     */
    public function makeTranslationModel ()
    {
        $modelName = $this->getTranslationModelName ();
        return ( new $modelName ( [], true ) )->setDefinition ( $this->definitionInstance )->setParent ( $this );
    }
    
    
    /**
     * @param string $locale
     * @return \Finnegan\Models\Translation
     */
    public function getNewTranslation ( $locale )
    {
        $translation = $this->makeTranslationModel ();
        $translation->setAttribute ( $this->getLocaleKey (), $locale );
        $this->translations->add ( $translation );
        return $translation;
    }
    
    
    public function hasMany ( $related, $foreignKey = null, $localKey = null )
    {
        if ( $related == Translation::class )
        {
            $foreignKey = $foreignKey ?: $this->getForeignKey ();
            
            $instance = $this->makeTranslationModel ();
            
            $localKey = $localKey ?: $this->getKeyName ();
            return new HasMany ( $instance->newQuery (), $this, $instance->getTable () . '.' . $foreignKey, $localKey );
        }
        
        return parent::hasMany ( $related, $foreignKey, $localKey );
    }
    
    
    public function populateMigrationFields ( $creator )
    {
        $output = '';
        foreach ( $this->fields->schema () as $field )
        {
            if ( $field->config ( 'schema', true ) and ! $field->isTranslatable () )
            {
                $output .= $field->populateMigration ();
            }
        }
        return $output;
    }
    
    
    public function truncate ()
    {
        $this->deleteAllTranslations ();
        
        $db = app ( 'db' );
        $db->unprepared ( 'set foreign_key_checks=0;' );
        parent:: truncate ();
        $db->unprepared ( 'set foreign_key_checks=1;' );
    }
    
    
    protected function deleteAllTranslations ()
    {
        $this->makeTranslationModel ()->truncate ();
    }
    
    
    public function getLocales ()
    {
        return $this->manager->languages ();
    }
    
}



