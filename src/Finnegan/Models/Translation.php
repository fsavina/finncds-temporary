<?php
namespace Finnegan\Models;


use Finnegan\Definitions\ModelDefinition;
use Finnegan\Models\Fields\Field;
use Illuminate\Support\Str;


class Translation extends Model
{

	/**
	 * The Translatable parent model
	 * @var \Finnegan\Models\TranslatableFields
	 */
	protected $parent;


	public function name ()
	{
        return Str::singular ( is_null ( parent::name () ) ? '' : parent::name () ) . '_translations';
	}


	/**
	 * Set the Translatable parent model
	 * @param \Finnegan\Models\TranslatableFields $parent
	 * @return Translation
	 */
	public function setParent ( TranslatableFields $parent )
	{
		$this -> parent = $parent;
		return $this;
	}
	

	public function setDefinition ( ModelDefinition $definition )
	{
		parent::setDefinition ( $definition );
		$this->setTable ( $this->definition ()->translationsTable () );
		$this->timestamps = false;
		return $this;
	}
	
	
	protected function loadFields ()
	{
		$instances = [];
		$fields = $this->definition ()->fields ();
		foreach ( $fields as $name => $config )
		{
			if ( isset( $config[ 'translatable' ] ) and $config[ 'translatable' ] )
			{
				$instance = Field:: make ( $name, $this );
				
				if ( $this->definition ()->isUnguarded () or $instance->authorize () )
				{
					$instances [ $name ] = $instance;
					$this->fillable [] = $name;
				}
			}
		}
		$this->fields = FieldCollection:: make ( $instances );
		return $this;
	}
	
	
	public function populateMigration ( $creator )
	{
		$output = parent::populateMigrationFields ( $creator );

		$output .= $creator->renderViewStub ( 'translation-model', [ 'parent' => $this->parent ] );

		return $output;
	}
	
	
	public function newQuery ()
	{
		$builder = $this->newQueryWithoutScopes();
		
		foreach ($this->getGlobalScopes() as $identifier => $scope) {
			$builder->withGlobalScope($identifier, $scope);
		}
		
		return $builder;
	}

}



