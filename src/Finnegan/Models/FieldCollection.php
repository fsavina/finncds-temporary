<?php
namespace Finnegan\Models;


use Finnegan\Contracts\Fields\Schema;
use Finnegan\Models\Fields\Field;
use Illuminate\Support\Collection;


/**
 * @method FieldCollection showInForm
 * @method FieldCollection showInPreview
 * @method FieldCollection isTranslatable
 */
class FieldCollection extends Collection
{

	protected static $passthru = ['showInPreview', 'showInForm', 'isTranslatable'];


	/**
	 * Get a subset of the current collection
	 * @param array $keys
	 * @param boolean $sort
	 * @return \Finnegan\Models\FieldCollection
	 */
	public function subset ( array $keys, $sort = false )
	{
		$items = $this->only ( $keys );
		if ( $sort )
		{
			$items -> sortByKeys ( $keys );
		}
		return $items;
	}


	/**
	 * Sort the collection according to the given list of keys
	 * @param array $keys
	 * @return \Finnegan\Models\FieldCollection
	 */
	public function sortByKeys ( array $keys )
	{
		$items = [ ];
		foreach ( $keys as $key )
		{
			if ( $this -> has ( $key ) )
			{
				$items [ $key ] = $this -> get ( $key );
			}
		}
		$this -> items = $items;
		return $this;
	}


	/**
	 * Filter the collection according to a configuration value
	 * @param string $key
	 * @param mixed $value
	 * @return \Finnegan\Models\FieldCollection
	 */
	public function ifConfig ( $key, $value )
	{
		return $this -> filter ( function  ( Field $field ) use( $key, $value )
		{
			return $field -> config ( $key, null ) === $value;
		} );
	}


	/**
	 * Filter the collection according to a configuration value
	 * @param string $key
	 * @param mixed $value
	 * @return \Finnegan\Models\FieldCollection
	 */
	public function ifConfigNot ( $key, $value )
	{
		return $this -> filter ( function  ( Field $field ) use( $key, $value )
		{
			return $field -> config ( $key, null ) !== $value;
		} );
	}
	
	
	/**
	 * Filter the collection by field contract
	 * @param string $contract
	 * @return \Finnegan\Models\FieldCollection
	 */
	public function ifInstanceOf ( $contract )
	{
		return $this -> filter ( function  ( Field $field ) use( $contract )
		{
			return ( $field instanceof $contract );
		} );
	}
	
	
	/**
	 * Filter the collection by method presence
	 * @param string $method
	 * @return \Finnegan\Models\FieldCollection
	 */
	public function hasMethod ( $method )
	{
		return $this -> filter ( function  ( Field $field ) use( $method )
		{
			return method_exists ( $field, $method );
		} );
	}
	

	/**
	 * Get only the schema fields
	 * @return \Finnegan\Models\FieldCollection
	 */
	public function schema ()
	{
		return $this -> ifInstanceOf ( Schema::class );
	}


	/**
	 * Get all the values for the fields in the current collection
	 * @return \Finnegan\Models\FieldCollection
	 */
	public function values ()
	{
		return $this -> map ( function  ( Field $field )
		{
			return $field -> value ();
		} );
	}


	public function render ()
	{
		return $this -> map ( function  ( Field $field )
		{
			return $field -> render ();
		} );
	}


	public function __call ( $method, $parameters )
	{
		if ( in_array ( $method, static :: $passthru ) )
		{
			return $this->filterByFieldMethod ( $method, $parameters );
		}
		return parent:: __call ( $method, $parameters );
	}


	protected function filterByFieldMethod ( $method, $parameters = [ ] )
	{
		return $this->filter ( function ( Field $field ) use ( $method, $parameters )
		{
			return call_user_func_array ( [ $field, $method ], $parameters );
		} );
	}

}




