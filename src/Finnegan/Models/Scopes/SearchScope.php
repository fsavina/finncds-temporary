<?php
namespace Finnegan\Models\Scopes;


use Finnegan\Contracts\Modules\Module;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Http\Request;


class SearchScope implements Scope
{

	/**
	 * @var Module
	 */
	protected $module;

	/**
	 * @var \Illuminate\Session\SessionManager
	 */
	protected $session;

	/**
	 * @var \Illuminate\Http\Request
	 */
	protected $request;

	/**
	 * @var Model
	 */
	protected $model;

	/**
	 * @var array
	 */
	protected $filterBy;


	/**
	 * Filterer constructor.
	 * @param Module $module
	 * @param Request   $request
	 * @param Model     $model
	 */
	public function __construct ( Module $module, Request $request, Model $model )
	{
		$this->module = $module;
		$this->request = $request;
		$this->model = $model;
		$this->session = $module->make ( 'session' );
	}


	/**
	 * Apply the scope to a given Eloquent query builder.
	 * @param  \Illuminate\Database\Eloquent\Builder $builder
	 * @param  \Illuminate\Database\Eloquent\Model   $model
	 */
	public function apply ( Builder $builder, Model $model )
	{
		$filterBy = $this -> getFilterBy ();
		if ( is_array ( $filterBy ) and count ( $filterBy ) )
		{
			$this -> applyFilters ( $builder, $filterBy );
		}
	}


	protected function applyFilters ( Builder $builder, $filterBy )
	{
		$filters = $this -> model -> filters ();
		foreach ( $filterBy as $filter => $value )
		{
			if ( $filter === '_text' and strlen ( $value ) )
			{
				$textFilters = $this -> model -> textFilters ();
				if ( count ( $textFilters ) )
				{
					$value = str_replace ( ' ', '%', trim ( $value ) );
					$builder -> where ( function ( Builder $builder ) use( $textFilters, $value )
					{
						foreach ( $textFilters as $field )
						{
							$field -> search ( $builder, $value );
						}
					} );
				}
			} elseif ( $filters -> has ( $filter ) )
			{
				$field = $filters -> get ( $filter );
				$field -> search ( $builder, $value );
			}
		}
	}


	/**
	 * @return array
	 */
	public function getFilterBy ()
	{
		if ( is_null ( $this -> filterBy ) )
		{
			$sessionName = $this -> module -> wrap ( $this -> model -> name () . '_search' );

			$filterBy = [ ];
			if ( $this -> request -> has ( 'search' ) )
			{
				$filterBy = $this -> request -> get ( 'search' );
				if ( is_string ( $filterBy ) and ( $filterBy == 'reset' ) )
				{
					$this -> session -> forget ( $sessionName );
					$filterBy = [ ];
				} elseif ( is_array ( $filterBy ) and count ( $filterBy ) )
				{
					$this -> session -> put ( $sessionName, $filterBy );
				} else
				{
					$filterBy = [ ];
				}
			} elseif ( $this -> session -> has ( $sessionName ) )
			{
				$filterBy = $this -> session -> get ( $sessionName );
			}
			$this -> filterBy = $filterBy;
		}
		return $this -> filterBy;
	}
}