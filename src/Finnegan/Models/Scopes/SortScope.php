<?php
namespace Finnegan\Models\Scopes;


use Finnegan\Contracts\Modules\Module;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Http\Request;


class SortScope implements Scope
{

	/**
	 * @var Module
	 */
	protected $module;

	/**
	 * @var \Illuminate\Session\SessionManager
	 */
	protected $session;

	/**
	 * @var \Illuminate\Http\Request
	 */
	protected $request;

	/**
	 * @var Model
	 */
	protected $model;

	/**
	 * @var array
	 */
	protected $sortBy;


	/**
	 * Sorter constructor.
	 * @param Module  $module
	 * @param Request $request
	 * @param Model   $model
	 */
	public function __construct ( Module $module, Request $request, Model $model )
	{
		$this->module = $module;
		$this->request = $request;
		$this->model = $model;
		$this->session = $module->make ( 'session' );
	}


	public function apply ( Builder $builder, Model $model )
	{
		$sortBy = $this -> getSortBy ();
		if ( is_array ( $sortBy ) and ( count ( $sortBy ) == 2 ) )
		{
			$builder -> orderBy ( $sortBy [ 0 ], $sortBy [ 1 ] );
		}
	}


	/**
	 * @return array
	 */
	public function getSortBy ()
	{
		if ( is_null ( $this -> sortBy ) )
		{
			$sessionName = $this -> module -> wrap ( $this -> model -> name () . '_sort' );

			$sortBy = (array) $this -> model -> config ( 'display.list.sort' );
			if ( $this -> request -> has ( 'sort' ) )
			{
				$sortBy = [
					$this -> request -> input ( 'sort' ),
					$this -> request -> input ( 'descasc', 'asc' )
				];
				$this -> session -> put ( $sessionName, $sortBy );
			} elseif ( $this -> session -> has ( $sessionName ) )
			{
				$sortBy = $this -> session -> get ( $sessionName );
			}

			if ( ! isset ( $sortBy [ 0 ] ) or ! strlen ( $sortBy [ 0 ] ) )
			{
				$sortBy = [ $this -> model -> getKeyName (), 'asc' ];
				$this -> session -> forget ( $sessionName );
			}
			if ( ! $this -> model -> hasField ( $sortBy [ 0 ] ) and ( $sortBy [ 0 ] != $this -> model -> getKeyName () ) )
			{
				$sortBy [ 0 ] = $this -> model -> getKeyName ();
				$this -> session -> forget ( $sessionName );
			}
			if ( ! isset ( $sortBy [ 1 ] ) or ! strlen ( $sortBy [ 1 ] ) )
			{
				$sortBy [ 1 ] = 'asc';
			}
			$this -> sortBy = $sortBy;
		}
		return $this -> sortBy;
	}

}