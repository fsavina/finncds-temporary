<?php
namespace Finnegan\Models\Console;


use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;


class ModelMakeCommand extends GeneratorCommand
{

	protected $name        = 'finnegan:make-model';

	protected $description = 'Create a new model class';

	protected $type        = 'Model';


	protected function getStub ()
	{
		return __DIR__ . '/stubs/model.stub';
	}


	protected function buildClass ( $name )
	{
		return parent::buildClass ( $name );
	}


	protected function parseName ( $name )
	{
		$name = Str::studly ( Str::singular ( $name ) );
		return parent::parseName ( $name );
	}
}