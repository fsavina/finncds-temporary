<?php
namespace Finnegan\Models;


use Finnegan\Definitions\Loaders\Loader;
use Finnegan\Definitions\ModelDefinition;
use Finnegan\Support\AbstractFactory;
use Finnegan\Support\LoadingContext;


class ModelsFactory extends AbstractFactory
{
	
	const DEFAULT_MODEL = 'Model';
	
	
	/**
	 * Load a new model instance
	 * @param string|ModelDefinition $definition
	 * @param array                  $attributes
	 * @return Model
	 */
	public function load ( $definition, array $attributes = [ ] )
	{
		if ( is_string ( $definition ) or ( $definition instanceof Loader ) )
		{
			$definition = $this->definitionsFactory ()->load ( $definition );
		}
		
		if ( ! ( $definition instanceof ModelDefinition ) )
		{
			throw new \InvalidArgumentException();
		}
		
		$className = $this->resolveClassName ( $definition );
		return ( new $className ( $attributes, true ) )->setDefinition ( $definition );
	}
	
	
	/**
	 * @param LoadingContext $context
	 * @param string         $definition
	 * @param array          $attributes
	 * @return Model
	 */
	public function withContext ( LoadingContext $context, $definition, array $attributes = [ ] )
	{
		$definition = $this->definitionsFactory ()->withContext ( $context, $definition );
		return $this->load ( $definition, $attributes );
	}
	
	
	/**
	 * @param string $definition
	 * @param array  $attributes
	 * @return Model
	 */
	public function unguarded ( $definition, array $attributes = [ ] )
	{
		$definition = $this->definitionsFactory ()->unguarded ( $definition );
		return $this->load ( $definition, $attributes );
	}
	
	
	/**
	 * @param string $definition
	 * @param array  $attributes
	 * @return Model
	 */
	public function unverified ( $definition, array $attributes = [ ] )
	{
		$definition = $this->definitionsFactory ()->unverified ( $definition );
		return $this->load ( $definition, $attributes );
	}
	
	
	/**
	 * @param string $definition
	 * @param array  $attributes
	 * @return Model
	 */
	public function forceLoad ( $definition, array $attributes = [ ] )
	{
		$definition = $this->definitionsFactory ()->forceLoad ( $definition );
		return $this->load ( $definition, $attributes );
	}
	
	
	/**
	 * @return \Finnegan\Definitions\DefinitionsFactory
	 */
	public function definitionsFactory ()
	{
		return $this->module->make ( 'definitions.factory', [ $this->module ] );
	}
	
	
	/**
	 * Resolve the model class name
	 * @param \Finnegan\Definitions\ModelDefinition $definition
	 * @return string
	 */
	protected function resolveClassName ( ModelDefinition $definition )
	{
		$className = $definition->getCustomModel ( static :: DEFAULT_MODEL );
		return class_exists ( $className ) ? $className : __NAMESPACE__ . "\\$className";
	}
	
}




