<?php
namespace Finnegan\Definitions;


use Finnegan\Contracts\Auth\AuthorizationException;
use Finnegan\Contracts\Modules\Module;
use Finnegan\Support\AbstractFactory;
use Finnegan\Support\LoadingContext;
use Illuminate\Contracts\Auth\Guard as GuardContract;
use Illuminate\Contracts\Cache\Repository as CacheContract;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;


class DefinitionsFactory extends AbstractFactory
{

	const DEFAULT_TYPE = 'model';

	const CACHE_TIME = 3600;
	
	/**
	 * @var CacheContract
	 */
	protected $cache;
	
	/**
	 * @var GuardContract
	 */
	protected $auth;

	/**
	 * Whether or not the user authorization should be checked
	 * @var boolean
	 */
	protected $guard = true;

	/**
	 * Whether or not the definitions should be verified
	 * @var boolean
	 */
	protected $verify = true;
	
	
	public function __construct ( Module $module )
	{
		$app = $module->getApplication ();

		if ( ! $app[ 'config' ]->get ( 'app.debug' ) )
		{
			$this->cache = $app->make ( CacheContract::class );
		}
		
		$this->auth = $app->make ( GuardContract::class );

		parent::__construct ( $module );
	}


	/**
	 * @param string|Loaders\Loader $definition
	 * @return Definition
	 */
	public function unguarded ( $definition )
	{
		return $this->withContext ( new LoadingContext( [ 'unguarded' => true ] ), $definition );
	}


	/**
	 * @param string|Loaders\Loader $definition
	 * @return Definition
	 */
	public function unverified ( $definition )
	{
		return $this->withContext ( new LoadingContext( [ 'unverified' => true ] ), $definition );
	}


	public function forceLoad ( $definition )
	{
		return $this->withContext ( new LoadingContext( [
			'unguarded'  => true,
			'unverified' => true
		] ), $definition );
	}


	public function withContext ( LoadingContext $context, $definition )
	{
		$this->applyContext ( $context );

		$definition = $this->load ( $definition );

		$this->resetContext ();

		return $definition;
	}


	protected function applyContext ( LoadingContext $context )
	{
		$this->guard = ! $context->unguarded;
		$this->verify = ! $context->unverified;
		return $this;
	}


	protected function resetContext ()
	{
		$this->guard = $this->verify = true;
		return $this;
	}


	/**
	 * Load a new definition
	 * @param string|Loaders\Loader $definition
	 * @return Definition
	 * @throws AuthorizationException
	 */
	public function load ( $definition )
	{
		if ( is_string ( $definition ) )
		{
			if ( isset ( $this -> registry [ $definition ] ) )
			{
				// Load the definition from the registry
				$definition = $this -> registry [ $definition ];
				$this -> verify = false;
			} elseif ( $cached = $this->loadFromCache ( $definition ) )
			{
				$definition = $cached;
				$this -> verify = false;
			} else
			{
				$loader = Loaders\Loader :: make ( $definition, [
					'folders' => $this->config ( 'module.definitions.folders', [ ] )
				] );
			}
		} elseif ( $definition instanceof Loaders\Loader )
		{
			$loader = $definition;
		} else
		{
			throw new \InvalidArgumentException ( 'Unknown definition type' );
		}

		$context = new LoadingContext( [
			'unguarded' => ! $this->guard,
			'unverified' => ! $this->verify
		] );

		if ( isset ( $loader ) )
		{
			// Create, initialize and validate the new definition
			$definition = $this->make ( $loader );

			// Context must be set before the init procedure
			$definition->setContext ( $context );

			$definition->init ( $this );
			$definition->validate ( $this );
		} else
		{
			$definition->setContext ( $context );
		}


		if ( $this->guard and ! $definition->authorize ( $this->auth ) )
		{
			throw new AuthorizationException ( '"' . $definition -> name () . '" definition: access forbidden to the current user' );
		}
		
		if ( $this->verify and $definition->mustVerify () )
		{
			$definition->verify ( $this );
		}
		
		// Save the definition in the cache, if required
		if ( $definition->get ( 'system.cache', true ) )
		{
			$this->saveToCache ( $definition );
		}
		
		// Register the definition
		if ( ! isset ( $this->registry [ $definition->name () ] ) and $this->guard )
		{
			$this->registry [ $definition->name () ] = $definition;
		}

		return $definition;
	}


	/**
	 * Make a new Definition instance
	 * @param string|Loaders\Loader $name
	 * @param array $data
	 * @return Definition
	 */
	public function make ( $name, $data = null )
	{
		$loader = null;
		if ( $name instanceof Loaders\Loader )
		{
			$loader = $name;
			$data = $loader -> parse ();
			$name = $loader -> name ();
		}
		
		$type = Arr::get ( $data, 'type', static :: DEFAULT_TYPE );
		$className = __NAMESPACE__ . '\\' . Str :: studly ( $type ) . 'Definition';
		
		return new $className ( $name, $data, $loader );
	}
	
	
	/**
	 * @param $definition
	 * @return bool|Definition
	 */
	protected function loadFromCache ( $definition )
	{
		$key = $this->wrapCacheKey ( $definition );
		if ( $this->cache and $this->cache->has ( $key ) )
		{
			return $this->make ( $definition, $this->cache->get ( $key ) );
		}
		return false;
	}


	/**
	 * @param Definition $definition
	 */
	protected function saveToCache ( Definition $definition )
	{
		if ( $this->cache )
		{
			$key = $this->wrapCacheKey ( $definition->name () );
			$this->cache->put ( $key, $definition->toArray (), static::CACHE_TIME );
		}
	}
	
	
	/**
	 * @param string $key
	 * @return string
	 */
	public function wrapCacheKey ( $key )
	{
		return 'finnegan.definitions.' . md5 ( $key );
	}
	
	
	/**
	 * @return CacheContract
	 */
	public function cache ()
	{
		return $this->cache;
	}
	
	
	/**
	 * @return GuardContract
	 */
	public function auth ()
	{
		return $this->auth;
	}
	
	
	/**
	 * @param $driver
	 * @return \Finnegan\Contracts\Definitions\Validator
	 */
	public function validator ( $driver )
	{
		return $this->module->make ( 'definitions.validators' )->driver ( $driver );
	}

}




