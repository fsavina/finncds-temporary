<?php
namespace Finnegan\Definitions\Loaders;


use Illuminate\Support\Arr;


abstract class Loader
{

	protected $identifier;


	/**
	 * Loader constructor.
	 * @param string $identifier
	 */
	protected function __construct ( $identifier )
	{
		$this -> identifier = $identifier;
		$this -> boot ();
	}


	/**
	 * Get the definition name
	 * @return string
	 */
	abstract public function name ();


	/**
	 * Get the last modified timestamp
	 * @return integer
	 */
	abstract public function lastModified ();


	/**
	 * Parse the definition
	 * @return array
	 */
	abstract public function parse ();


	protected function boot ()
	{}


	/**
	 * Get the unique definition identifier
	 * @return string
	 */
	public function identifier ()
	{
		return $this -> identifier;
	}


	/**
	 * Create a new loader instance according to the given identifier
	 * @param mixed $identifier
	 * @param array $config
	 * @return Loader
	 */
	public static function make ( $identifier, $config = [] )
	{
		if ( is_string ( $identifier ) )
		{
			// $identifier is a definition name or a file path
			return new File ( $identifier, Arr::get ( $config, 'folders', [ ] ) );
		}
		
		throw new \InvalidArgumentException ( 'Unrecognized object identifier: ' . $identifier );
	}

}



