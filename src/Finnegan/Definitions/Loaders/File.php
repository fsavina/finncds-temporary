<?php
namespace Finnegan\Definitions\Loaders;


use Symfony\Component\Yaml\Yaml;


class File extends Loader
{

	protected $folders;
	
	protected $name;
	
	protected $type;
	
	protected $isComponent = false;

	protected static $allowedExtensions = [
		'json',
		'php',
		'yaml'
	];


	/**
	 * File constructor.
	 * @param string $identifier
	 * @param array  $folders
	 */
	protected function __construct ( $identifier, $folders = [] )
	{
		$this->folders = (array) $folders;
		parent::__construct ( $identifier );
	}

	
	protected function boot ()
	{
		if ( file_exists ( $this -> identifier ) )
		{
			$this -> name = pathinfo ( $this -> identifier, PATHINFO_FILENAME );
			$this -> type = $this -> guessType ( $this -> identifier );
		} elseif ( file_exists ( base_path ( $this -> identifier ) ) )
		{
			$this -> identifier = base_path ( $this -> identifier );
			$this -> name = pathinfo ( $this -> identifier, PATHINFO_FILENAME );
			$this -> type = $this -> guessType ( $this -> identifier );
		} else
		{
			$this -> name = $this -> identifier;
			if ( starts_with ( $this -> name, 'component:' ) )
			{
				$this -> name = str_replace ( 'component:', '', $this -> name );
				$this -> isComponent = true;
			}
			
			$this -> identifier = $this -> resolveName ( $this -> name );
			$this -> type = $this -> guessType ( $this -> identifier );
		}
	}
	

	/**
	 * @param string $path
	 * @return string mixed
	 * @throws \DomainException
	 */
	protected function guessType ( $path )
	{
		$ext = pathinfo ( $path, PATHINFO_EXTENSION );
		if ( ! in_array ( $ext, static :: $allowedExtensions ) )
		{
			throw new \DomainException ( "Wrong file format, '{$ext}' extension is not allowed." );
		}
		return $ext;
	}
	

	/**
	 * @return string
	 */
	public function name ()
	{
		return $this -> name;
	}
	

	/**
	 * @return int
	 */
	public function lastModified ()
	{
		return filemtime ( $this -> identifier );
	}


	/**
	 * Resolve the definition name to a file path
	 * @param string $name
	 * @return string
	 * @throws \Exception
	 */
	protected function resolveName ( $name )
	{
		$name = self :: filterName ( $name );
		
		foreach ( $this -> folders as $folder )
		{
			if ( ! $this -> isComponent and $path = $this -> parseFolder ( $folder, $name ) )
			{
				return $path;
			} elseif ( $this -> isComponent and ( $path = $this -> parseFolder ( $folder, "components/$name" ) ) )
			{
				return $path;
			}
		}
		
		throw new \Exception ( "Missing definition file: $name" );
	}
	
	
	protected function parseFolder ( $folder, $name )
	{
		foreach ( static :: $allowedExtensions as $ext )
		{
			$path = rtrim ( base_path ( $folder ), '/' ) . '/' . $name . '.' . $ext;
			if ( file_exists ( $path ) )
			{
				return $path;
			}
		}
		return false;
	}


	/**
	 * Filter the given definition name
	 * @param string $name
	 * @return string
	 */
	protected static function filterName ( $name )
	{
		$name = strtolower ( $name );
		$name = str_replace ( '- ', '_', $name );
		$name = preg_replace ( '/[^a-z0-9_\/]/', '', $name );
		return $name;
	}


	/**
	 * Parse the given definition file
	 * @throws \Exception
	 * @return array
	 */
	public function parse ()
	{
		$method = 'parse' . title_case ( $this -> type );
		return call_user_func_array ( [ $this, $method ], [ $this -> identifier ] );
	}


	/**
	 * Parse the given JSON file
	 * @param string $filename
	 * @throws \Exception
	 * @return array
	 */
	protected function parseJson ( $filename )
	{
		$json = json_decode ( file_get_contents ( $filename ), true );
		if ( ! is_array ( $json ) )
		{
			$msg = "Empty or malformed definition file: $filename";
			if ( function_exists ( 'json_last_error_msg' ) and ( json_last_error () != JSON_ERROR_NONE ) )
			{
				$msg = json_last_error_msg () . " in $filename";
			}
			throw new \Exception ( $msg );
		}
		return $json;
	}


	/**
	 * Parse the given PHP file
	 * @param string $filename
	 * @return array
	 */
	protected function parsePhp ( $filename )
	{
		return ( array ) require $filename;
	}
	
	
	/**
	 * Parse the given YAML file
	 * @param string $filename
	 * @return array
	 */
	protected function parseYaml ( $filename )
	{
		return Yaml :: parse ( file_get_contents ( $filename ) );
	}

}



