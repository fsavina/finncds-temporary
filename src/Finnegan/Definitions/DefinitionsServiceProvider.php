<?php
namespace Finnegan\Definitions;


use Finnegan\Support\BaseServiceProvider;


class DefinitionsServiceProvider extends BaseServiceProvider
{
	
	protected $defer = true;
	
	
	public function register ()
	{
		$this->shouldCompile ();
		
		$this->app->singleton ( 'definitions.factory', DefinitionsFactory::class );
		
		$this->app->singleton ( 'definitions.validators', function ( $app )
		{
			return new Validators\ValidatorManager( $app );
		} );
		
		$this->commands ( Console\MakeDefinitionCommand::class );
	}
	
	
	public function provides ()
	{
		return [
			'definitions.factory',
			'definitions.validators'
		];
	}
	
	
	public static function compiles ()
	{
		return [
			realpath ( __DIR__ . '/DefinitionsFactory.php' ),
			realpath ( __DIR__ . '/Definition.php' ),
			realpath ( __DIR__ . '/ModelDefinition.php' ),
			realpath ( __DIR__ . '/ComponentDefinition.php' ),
			realpath ( __DIR__ . '/Validators/StorageValidator.php' ),
			realpath ( __DIR__ . '/Validators/JsonSchemaValidator.php' ),
			realpath ( __DIR__ . '/Loaders/Loader.php' )
		];
	}
	
}


