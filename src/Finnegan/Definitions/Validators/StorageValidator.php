<?php
namespace Finnegan\Definitions\Validators;


use Finnegan\Contracts\Definitions\ValidationException;
use Finnegan\Contracts\Definitions\Validator;
use Finnegan\Definitions\Definition;
use Finnegan\Definitions\ModelDefinition;
use Illuminate\Database\DatabaseManager;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;


/**
 * The Definition validating system
 */
class StorageValidator implements Validator
{

	/**
	 * @var \Illuminate\Database\Schema\Builder
	 */
	protected $schema;

	/**
	 * @var FilesystemManager
	 */
	protected $storage;


	public function __construct ( DatabaseManager $db, FilesystemManager $storage )
	{
		$this->schema = $db->connection ()->getSchemaBuilder ();
		$this->storage = $storage;
	}
	
	
	/**
	 * Run the validation on the given definition
	 * @param Definition $definition
	 * @param array      $params
	 * @return ModelDefinition
	 */
	public function check ( Definition $definition, array $params = [ ] )
	{
		$driver = Str::studly ( $definition->get ( 'storage.driver', 'database' ) );
		$method = "check{$driver}Storage";

		if ( ! method_exists ( $this, $method ) )
		{
			throw new \InvalidArgumentException( "Unknown validator method: $method" );
		}

		return $this->$method( $definition );
	}


	/**
	 * @param ModelDefinition $definition
	 * @return ModelDefinition
	 */
	protected function checkFilesystemStorage ( ModelDefinition $definition )
	{
		$definitionName = $definition->name ();
		$path = $definition->getStorageFilePath ();
		$disk = $definition->get ( 'storage.file.disk' );

		if ( ! $this->storage->disk ( $disk )->exists ( $path ) )
		{
			$this->signalError ( "The storage file '$path' doesn't exist in '{$definitionName}'" );
		}
		return $definition;
	}


	/**
	 * @param \Finnegan\Definitions\ModelDefinition $definition
	 * @return \Finnegan\Definitions\ModelDefinition
	 */
	protected function checkDatabaseStorage ( ModelDefinition $definition )
	{
		$definitionName = $definition->name ();

		if ( ! $this->schema->hasTable ( $table = $definition->table () ) )
		{
			$this->signalError ( "The table '$table' doesn't exist in the database in '{$definitionName}'" );
		}

		if ( $definition->hasTranslatableFields () and ! $this->schema->hasTable ( $translationsTable = $definition->translationsTable () ) )
		{
			$this->signalError ( "The translations table '$translationsTable' doesn't exist in the database in '{$definitionName}'" );
		}

		return $this->validateFields ( $definition );
	}


	/**
	 * Validate the fields definition
	 * @param \Finnegan\Definitions\ModelDefinition $definition
	 * @return \Finnegan\Definitions\ModelDefinition
	 */
	protected function validateFields ( ModelDefinition $definition )
	{
		$table = $definition->table ();

		foreach ( $definition->fields () as $name => $config )
		{
			if ( Arr::get ( $config, 'column', true ) )
			{
				if ( Arr::get ( $config, 'translatable', false ) )
				{
					if ( ! $this->schema->hasColumn ( $translationsTable = $definition->translationsTable (), $name ) )
					{
						$this->signalError ( "The column '$name' is missing in '$translationsTable' table" );
					}
				} elseif ( ! $this->schema->hasColumn ( $table, $name ) )
				{
					$this->signalError ( "The column '$name' is missing in '$table' table" );
				}
			}
		}

		return $definition;
	}


	/**
	 *
	 * @param string $message
	 * @throws \Exception
	 */
	protected function signalError ( $message )
	{
		throw new ValidationException ( $message );
	}

}



