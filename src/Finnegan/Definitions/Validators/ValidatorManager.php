<?php
namespace Finnegan\Definitions\Validators;


use Illuminate\Support\Manager;


class ValidatorManager extends Manager
{
	
	public function getDefaultDriver ()
	{
		return 'json_schema';
	}
	
	
	protected function createJsonSchemaDriver ()
	{
		return new JsonSchemaValidator( $this->app[ 'files' ] );
	}
	
	
	protected function createStorageDriver ()
	{
		return new StorageValidator( $this->app[ 'db' ], $this->app[ 'filesystem' ] );
	}
}