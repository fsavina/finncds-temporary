<?php
namespace Finnegan\Definitions\Validators;


use Finnegan\Contracts\Definitions\Validator as ValidatorContract;
use Finnegan\Definitions\Definition;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use JsonSchema\RefResolver;
use JsonSchema\Uri\UriRetriever;
use JsonSchema\Validator;


class JsonSchemaValidator implements ValidatorContract
{

	
	/**
	 * The JSON Schema validator
	 * @var \JsonSchema\Validator
	 */
	protected $validator;
	
	/**
	 * @var Filesystem
	 */
	protected $files;
	
	/**
	 * The JSON Schema registry
	 * @var array
	 */
	protected $registry = [ ];
	
	
	public function __construct ( Filesystem $filesystem )
	{
		RefResolver::$maxDepth = 15;
		
		$this->files = $filesystem;
		
		$this->validator = new Validator ();
	}
	
	
	/**
	 * @param Definition $definition
	 * @param array      $params
	 * @return Definition
	 */
	public function check ( Definition $definition, array $params = [] )
	{
		foreach ( $params['schemas'] as $schema )
		{
			$this -> validator -> check ( json_decode ( $definition -> toJson () ), $this->retrieveSchema ( $schema ) );
			
			if ( ! $this -> validator -> isValid () )
			{
				$errors = '';
				foreach ( $this -> validator -> getErrors () as $error )
				{
					$errors .= sprintf ( "[%s] %s\n", $error [ 'property' ], $error [ 'message' ] );
				}
				throw new \InvalidArgumentException ( "'{$definition -> name()}' definition is not valid:\n{$errors}" );
			}
			$this -> validator -> reset ();
		}
		return $definition;
	}


	/**
	 * Retrieve a JSON Schema by name
	 * @param string $schema
	 * @return object
	 */
	protected function retrieveSchema ( $schema )
	{
		if ( ! isset ( $this->registry [ $schema ] ) )
		{
			$source = realpath ( __DIR__ . "/../Schemas/$schema.json" );
			$resolved = __DIR__ . "/../Schemas/$schema.resolved.json";
			
			$path = $this->getSchemaPath ( $source, $resolved );

			$retriever = new UriRetriever ();
			$jsonSchema = $retriever->retrieve ( 'file://' . $path );

			if ( $path == $source )
			{
				$jsonSchema = $this->saveResolvedSchema ( $schema, $retriever, $jsonSchema, $resolved );
			}

			$this->registry [ $schema ] = $jsonSchema;
		}
		return $this->registry [ $schema ];
	}
	
	
	protected function getSchemaPath ( $source, $resolved )
	{
		if ( $this->files->exists ( $resolved ) )
		{
			if ( $this->files->lastModified ( $resolved ) >= $this->files->lastModified ( $source ) )
			{
				return $resolved;
			}
		}
		return $source;
	}


	protected function saveResolvedSchema ( $schema, UriRetriever $retriever, $json, $path )
	{
		$resolver = new RefResolver( $retriever );
		$resolver->resolve ( $json, 'file://' . __DIR__ . '/../Schemas/' );

		$json->id = 'Finnegan/' . Str::studly ( $schema );
		unset( $json->definitions );

		$this->files->put ( $path, json_encode ( $json, JSON_PRETTY_PRINT ) );

		return $json;
	}

}



