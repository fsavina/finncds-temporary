<?php
namespace Finnegan\Definitions\Console;


use Finnegan\Foundation\Auth\User;
use Finnegan\Support\WizardCommand;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Symfony\Component\Finder\Finder;


class MakeDefinitionCommand extends WizardCommand
{


	protected $signature = 'finnegan:make-definition
							{name : The definition name}';


	protected $description = 'Create a new definition.';

	protected $questions   = [
		'components' => [
			'text'  => 'Include components (comma separated)',
			'choices' => [],
			'multiple' => true,
			'rules' => 'required'
		],
		'auth.role' => [
			'text'  => 'Minimum authorization role (optional)',
			'allow_empty' => true,
		]
	];

	/**
	 * @var Filesystem
	 */
	protected $files;


	public function __construct ( Factory $validator, Filesystem $files )
	{
		parent::__construct ( $validator );
		$this->files = $files;
	}


	public function fire ()
	{
		$this->init ();

		$this->writeDefinition ( [] /*$this->getInputData ()*/ );

		$this->info ( 'Definition created successfully.' );

		$this->call ( 'finnegan:make-model', [ 'name' => $this->getInputName () ] );
	}


	protected function init ()
	{
		$this->questions[ 'components' ][ 'choices' ] = $this->getAvailableComponents ();

		$this->questions[ 'auth.role' ][ 'choices' ] = $this->getAvailableRoles ();
	}


	protected function getAvailableComponents ()
	{
		$components = Finder::create ()
							->files ()->depth ( 0 )->sortByName ()
							->in ( resource_path ( 'definitions/components' ) );

		$names = [ 'NONE' ];
		foreach ( $components as $component )
		{
			$names[] = $component->getBasename ( '.' . $component->getExtension () );
		}
		return $names;
	}


	protected function getAvailableRoles ()
	{
		$names = [ 'NONE' ];
		foreach ( User::roles () as $role => $config )
		{
			$names[] = $role;
		}
		return $names;
	}


	protected function getInputName ()
	{
		return $this->argument ( 'name' );
	}


	protected function getPath ()
	{
		return resource_path ( 'definitions' ) . DIRECTORY_SEPARATOR;
	}


	protected function writeDefinition ( array $data )
	{
		$path = $this->getPath ();

		$name = $this->getInputName ();
		$filename = Str::plural ( $name ) . '.json';

		$this->files->put ( $path . $filename, json_encode ( $data, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT ) );
	}

}