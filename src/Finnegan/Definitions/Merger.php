<?php
namespace Finnegan\Definitions;


use Illuminate\Support\Arr;


class Merger
{

	public static function merge ( $target, $source, $overwrite = false )
	{
		if ( is_scalar ( $target ) and is_scalar ( $source ) )
		{
			return $overwrite ? $source : $target;
		} elseif (is_array($target) and is_array($source))
		{
			if ( ! count ( $target ) )
			{
				return $source;
			}
			if ( ! count ( $source ) )
			{
				return $target;
			}
			if ( ! Arr::isAssoc ( $target ) and ! Arr::isAssoc ( $source ) )
			{
				return array_unique ( array_merge ( $target, $source ) );
			} elseif ( Arr::isAssoc ( $target ) and Arr::isAssoc ( $source ) )
			{
				foreach ( $source as $key => $value )
				{
					if ( ! isset( $target[ $key ] ) )
					{
						$target[ $key ] = $value;
						continue;
					}
					$target[ $key ] = static:: merge ( $target[ $key ], $value, $overwrite );
				}
				return $target;
			}
		}

		throw new \InvalidArgumentException( 'Merge incompatibility' );
	}

}