<?php
namespace Finnegan\Definitions;


use Finnegan\Support\LoadingContext;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Arr;


abstract class Definition implements Jsonable, Arrayable, \JsonSerializable
{

	/**
	 * The definition name
	 * @var string
	 */
	protected $name;

	/**
	 * The definition loader
	 * @var Loaders\Loader
	 */
	protected $loader;

	/**
	 * @var LoadingContext
	 */
	protected $context;

	/**
	 * The definition data
	 * @var array
	 */
	protected $data;

	/**
	 * The JSON schemas for the validation
	 * @var array
	 */
	protected $schemas = [];


	/**
	 * @param string $name
	 * @param array  $data
	 * @param Loaders\Loader $loader
	 */
	final public function __construct ( $name, array $data, Loaders\Loader $loader = null )
	{
		$this -> name = $name;
		$this -> data = $data;
		$this -> loader = $loader;
	}


	/**
	 * Initialize the current definition
	 * @param DefinitionsFactory $factory
	 */
	abstract public function init ( DefinitionsFactory $factory );
	
	
	/**
	 * Perform extra validation checks
	 * @param DefinitionsFactory $factory
	 */
	abstract public function verify ( DefinitionsFactory $factory );


	/**
	 * @return bool
	 */
	public function mustVerify ()
	{
		return false;
	}


	/**
	 * @return bool
	 */
	public function mustValidate ()
	{
		return ( count ( $this->schemas ) and $this->get ( 'system.validate', true ) );
	}
	
	
	/**
	 * Validate the definition against the given schema(s)
	 * @param DefinitionsFactory $factory
	 * @return Definition
	 */
	public function validate ( DefinitionsFactory $factory )
	{
		if ( $this->mustValidate () )
		{
			$lastModified = $this->loader->lastModified ();
			
			$key = $factory->wrapCacheKey ( "valid.{$this->name}" );
			
			if ( ! ( $cache = $factory->cache () ) or ( $cache->get ( $key ) != $lastModified ) )
			{
				$factory->validator ( 'json_schema' )->check ( $this, [ 'schemas' => $this->schemas ] );
				
				if ( $cache )
				{
					$cache->forever ( $key, $lastModified );
				}
			}
		}
		return $this;
	}


	/**
	 * @param LoadingContext $context
	 * @return Definition
	 */
	public function setContext ( LoadingContext $context )
	{
		$this->context = $context;
		return $this;
	}


	/**
	 * @return LoadingContext
	 */
	public function getContext ()
	{
		return $this->context;
	}


	/**
	 * Check if the definition is unguarded
	 * @return boolean
	 */
	public function isUnguarded ()
	{
		return $this->context->unguarded;
	}


	/**
	 * Check if the definition is unverified
	 * @return boolean
	 */
	public function isUnverified ()
	{
		return $this->context->unverified;
	}


	/**
	 * Check if the user is authorized to use the definition
	 * @param Guard $guard
	 * @return bool
	 */
	public function authorize ( Guard $guard )
	{
		$role = $this -> get ( 'auth.role' );
		return ( ! $role or ! $guard->check () or $guard->user ()->achieve ( $role ) );
	}
	
	
	/**
	 * Get the name of the definition
	 * @return string
	 */
	public function name ()
	{
		return $this -> name;
	}
	
	
	/**
	 * Get a value from the definition data
	 * @param string $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function get ( $key, $default = false )
	{
		return Arr::get ( $this -> data, $key, $default );
	}


	/**
	 * Convert to array data
	 * @return array
	 */
	public function toArray ()
	{
		return $this -> data;
	}


	/**
	 * Convert to jsonable data
	 * @param mixed $options
	 * @return string
	 */
	public function toJson ( $options = 0 )
	{
		return json_encode ( $this -> toArray (), $options );
	}


	/**
	 * Convert the object into something JSON serializable.
	 * @return array
	 */
	public function jsonSerialize ()
	{
		return $this -> toArray ();
	}

}



