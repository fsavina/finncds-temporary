<?php
namespace Finnegan\Definitions;


class ComponentDefinition extends Definition
{

	/**
	 * The JSON schemas for the validation
	 * @var array
	 */
	protected $schemas = [
		'component'
	];


	public function init ( DefinitionsFactory $factory )
	{
	}


	public function verify ( DefinitionsFactory $factory )
	{
	}

}




