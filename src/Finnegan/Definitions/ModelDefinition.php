<?php
namespace Finnegan\Definitions;


use Finnegan\Contracts\Auth\AuthorizationException;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;


class ModelDefinition extends Definition
{

	const DEFAULT_TABLE_NAMESPACE = 'finn_';

	/**
	 * The list of the system tables
	 * @var array
	 */
	protected static $systemTables = [ 'migrations' ];

	/**
	 * The table namespace for the definition
	 * @var string
	 */
	protected $tableNamespace;

	/**
	 * The JSON schemas for the validation
	 * @var array
	 */
	protected $schemas = [ 'model' ];


	/**
	 * @param \Finnegan\Definitions\DefinitionsFactory $factory
	 */
	public function init ( DefinitionsFactory $factory )
	{
		if ( is_null ( $this->context ) )
		{
			throw new \Exception('No loading context set');
		}

		if ( $namespace = $factory -> config ( 'module.table_namespace' ) )
		{
			$this -> tableNamespace = $namespace;
		}

		$this -> loadIncludes ( $factory )
			-> loadComponents ( $factory );
			//-> loadAuthExtensions ( $factory );
	}


	/**
	 * Extend the current definition with the included definitions
	 * @param \Finnegan\Definitions\DefinitionsFactory $factory
	 * @return \Finnegan\Definitions\ModelDefinition
	 * @throws \Exception
	 */
	protected function loadIncludes ( DefinitionsFactory $factory )
	{
		if ( isset ( $this -> data [ 'include' ] ) )
		{
			$includes = ( array ) $this -> data [ 'include' ];

			foreach ( $includes as $include )
			{
				$included = $factory -> load ( $include );

				$data = $included -> toArray ();

				$table = Arr::get ( $data, 'storage.table' );
				if ( ! $table )
				{
					Arr::set ( $data, 'storage.table', $included->table () );
				} elseif ( is_array ( $table ) and ! Arr::get ( $table, 'name' ) )
				{
					Arr::set ( $data, 'storage.table.name', $included->table () );
				}

				$this -> merge ( $data );
			}
		}
		return $this;
	}


	/**
	 * Extend the current definition with the required components
	 * @param \Finnegan\Definitions\DefinitionsFactory $factory
	 * @return \Finnegan\Definitions\ModelDefinition
	 */
	protected function loadComponents ( DefinitionsFactory $factory )
	{
		if ( isset ( $this -> data [ 'components' ] ) )
		{
			$components = ( array ) $this -> data [ 'components' ];

			foreach ( $components as $componentName )
			{
				try {
					$component = $factory -> load ( "component:$componentName" );
				} catch (AuthorizationException $e)
				{
					continue;
				}
				$this -> merge ( Arr::get ( $component -> toArray (), 'component', [ ] ) );
			}
		}
		return $this;
	}



	/**
	 * Check if the current definition has a component
	 * @param string $component
	 * @return boolean
	 */
	public function hasComponent ( $component )
	{
		return in_array ( $component, ( array ) $this -> get ( 'components', [ ] ) );
	}
	
	
	/**
	 * Overwrite specific user role configurations
	 * @param DefinitionsFactory $factory
	 * @return ModelDefinition
	 */
	/*protected function loadAuthExtensions ( DefinitionsFactory $factory )
	{
		if ( ! $this -> isUnguarded() )
		{
			$guard = $factory->auth ();

			$if = $this -> get ( 'auth.if' );
			if ( is_array ( $if ) and count ( $if ) and $guard->check () )
			{
				foreach ( $if as $roleConfig )
				{
					if ( ! $guard->user ()->achieve ( $roleConfig [ 'role' ] ) )
					{
						continue;
					}
					$this -> merge ( $roleConfig [ 'extend' ], true );
				}
			}
		}
		return $this;
	}*/


	public function mustVerify ()
	{
		return $this -> get ( 'storage.verify', true );
	}


	public function verify ( DefinitionsFactory $factory )
	{
		$factory->validator ( 'storage' )->check ( $this );
	}
	
	
	/**
	 * Get the namespace for the current model table
	 * @return string
	 */
	protected function tableNamespace ()
	{
		return $this -> tableNamespace ?  : static :: DEFAULT_TABLE_NAMESPACE;
	}


	/**
	 * Get the table name
	 * @return string
	 * @throws AuthorizationException
	 */
	public function table ()
	{
		$table = $this -> get ( 'storage.table', $this -> name () );
		
		$table = is_string ( $table ) ? $table : Arr::get ( $table, 'name', $this -> name () );
		$table = $this -> wrapTableName ( $table );
		
		if ( static :: isSystemTable ( $table ) and ! $this -> get ( 'storage.table.unguard' ) )
		{
			throw new AuthorizationException ( "The '$table' table is a system table. Set storage.table.unguard=true on your definition to gain access." );
		}
		return $table;
	}
	
	
	/**
	 * Apply the given namespace to the table name
	 * @param string $table
	 * @return string
	 */
	public function wrapTableName ( $table )
	{
		$namespace = $this -> tableNamespace ();
		if ( $this -> get ( 'storage.table.namespace', true ) and ! starts_with ( $table, $namespace ) )
		{
			$table = "{$namespace}{$table}";
		}
		return $table;
	}
	
	
	/**
	 * Check if the given table is a system table
	 * @param string $table
	 * @return boolean
	 */
	protected static function isSystemTable ( $table )
	{
		return in_array ( $table, static :: $systemTables );
	}
	
	
	/**
	 * Get the relation pivot table name
	 * @param string $relation
	 * @return string
	 */
	public function relationPivotTable ( $relation )
	{
		$table = $this -> get ( "relations.{$relation}.table" );
		if ( strlen ( $table ) )
		{
			return $this -> wrapTableName ( $table );
		}
		
		$base = Str :: snake ( $this -> name () );
		
		// If there's no definition the relation name is the related object
		$related = $this -> get ( "relations.{$relation}.definition", $relation );
		$related = snake_case ( $related );
		
		$models = [ $related, $base ];
		sort ( $models );
		
		return $this -> wrapTableName ( strtolower ( implode ( '_', $models ) ) );
	}
	
	
	/**
	 * Get the translations table name
	 * @return string
	 */
	public function translationsTable ()
	{
		$table = Str :: singular ( $this -> name ) . '_translations';
		return $this -> wrapTableName ( $table );
	}
	
	
	/**
	 * Get the primary key name, if available
	 * @param mixed $default
	 * @return string
	 */
	public function primaryKey ( $default = false )
	{
		return $this -> get ( 'storage.key', $default );
	}
	
	
	/**
	 * Get the fields list
	 * @return array
	 */
	public function fields ()
	{
		return $this -> get ( 'fields', [] );
	}
	
	
	/**
	 * Prepend a field to the model definition
	 * @param string $name
	 * @param array $config
	 * @param boolean $overwrite
	 */
	public function prependField ( $name, $config, $overwrite = false )
	{
		$fields = $this -> fields ();
		if ( ! isset ( $fields [ $name ] ) or $overwrite )
		{
			$newFields = [ $name => $config ];
			foreach ( $fields as $name => $config )
			{
				$newFields [ $name ] = $config;
			}
			$this -> data [ 'fields' ] = $newFields;
		}
	}
	
	
	/**
	 * Append a field to the model definition
	 * @param string $name
	 * @param array $config
	 * @param boolean $overwrite
	 */
	public function appendField ( $name, $config, $overwrite = false )
	{
		$fields = $this -> fields ();
		if ( ! isset ( $fields [ $name ] ) or $overwrite )
		{
			$fields [ $name ] = $config;
			$this -> data [ 'fields' ] = $fields;
		}
	}
	
	
	/**
	 * Check if the model has at least a translatable field
	 * @return boolean
	 */
	public function hasTranslatableFields ()
	{
		foreach ( $this -> fields () as $field )
		{
			if ( Arr::get ( $field, 'translatable', false ) )
			{
				return true;
			}
		}
		return false;
	}


	/**
	 * @param mixed $default
	 * @return string
     */
	public function getCustomModel ($default = null )
	{
		if ( $this -> hasTranslatableFields () )
		{
			$default = 'TranslatableFields';
		}
		return $this -> get ( 'system.model', $default );
	}


	/**
	 * Get the icon name, if available
	 * @param mixed $default
	 * @return string
	 */
	public function icon ( $default = false )
	{
		return $this -> get ( 'display.icon', $default );
	}
	
	
	/**
	 * Get the plural name if available or try to generate it
	 * @return string
	 */
	public function plural ()
	{
		$default = Str :: title ( Str :: plural ( $this -> name ) );
		$default = str_replace ( '_', ' ', $default );
		return trans ( $this -> get ( 'display.plural', $default ) );
	}


	/**
	 * Get the singular name if available or try to generate it
	 * @return string
	 */
	public function singular ()
	{
		$default = Str :: title ( Str :: singular ( $this -> name ) );
		$default = str_replace ( '_', ' ', $default );
		return trans ( $this -> get ( 'display.singular', $default ) );
	}


	/**
	 * Merge the given data with the current definition
	 * @param array   $data
	 * @param boolean $overwrite
	 * @return \Finnegan\Definitions\Definition
	 */
	protected function merge ( array $data, $overwrite = false )
	{
		unset ( $data [ 'type' ] );
		$this->data = Merger::merge ( $this->data, $data, $overwrite );
	}


	/**
	 * @return bool
     */
	public function sortable ()
	{
		return $this -> hasComponent ( 'sortable' );
	}


	/**
	 * @param string $default
	 * @return bool|string
     */
	public function sortablePriorityField ( $default = 'priority' )
	{
		if ( $sortable = $this -> get ( 'system.sortable' ) )
		{
			return is_array ( $sortable ) ? Arr::get ( $sortable, 'field', $default ) : $default;
		}
		return false;
	}


	public function getStorageFilePath ()
	{
		$path = $this->get ( 'storage.file' );
		if ( is_array ( $path ) )
		{
			$path = Arr::get ( $path, 'path' );
		}
		return $path ?: "finnegan/{$this->name}.json";
	}

}




