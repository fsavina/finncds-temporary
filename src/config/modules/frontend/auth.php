<?php

return [

	/**
	 * Set the module as secure (the user needs to be authenticated to access this module).
	 */
	'secure'     => false,

	/**
	 * The minimum role required to access this module.
	 * (Available roles: developer, admin, editor, author, contributor, subscriber)
	 */
	'role'       => 'contributor',

	/**
	 * Set the Authorization controller.
	 */
	'controller' => 'Auth\\LoginController',

	/**
	 * The Password management configuration.
	 */
	'password'   => [
		/**
		 * Enable the Password reset system.
		 */
		'reset'      => false,
		
		/**
		 * Set the Password management controllers.
		 */
		'controllers' => [
			
			'forgot' => 'Auth\\ForgotPasswordController',
			
			'reset' => 'Auth\\ResetPasswordController'
		
		]
	]

];