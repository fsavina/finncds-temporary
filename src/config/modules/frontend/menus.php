<?php

return [

	/**
	 * The general configuration, shared by all the menus.
	 */
	'config'    => [

		/**
		 * The default caching time.
		 */
		'cacheTime' => 60

	],
	
	/**
	 * The list of menus for the Module.
	 */
	'instances' => [

		/**
		 * The primary menu name.
		 */
		'primary' => [

			/**
			 * The list of menu items.
			 */
			'items' => [
				[
					/**
					 * The menu item label.
					 */
					'label' => 'Home',

					/**
					 * The menu item url.
					 */
					'url'   => '/'
				]
			]
		]
	]

];