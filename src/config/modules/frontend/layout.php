<?php

return [

	/**
	 * The main view theme for the Module.
	 */
	'theme'  => 'welcome',

	/**
	 * The list of required asset libraries.
	 * (See the main assets.php config file for the complete list of available libraries and their dependencies)
	 */
	'assets' => [ ]

];