<?php

return [

	/**
	 * Define a URL prefix for the whole Module routing system.
	 * ('false' to disable URL prefixing)
	 */
	'prefix'     => false,

	/**
	 * The controllers namespace.
	 */
	'namespace'  => 'App\\Http\\Controllers',

	/**
	 * The default controller name.
	 */
	'controller' => 'Controller',

	/**
	 * Enable the CRUD routing system
	 */
	'crud'       => false,

	/**
	 * The list of the extra routes.
	 */
	'routes'     => []

];