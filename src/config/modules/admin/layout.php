<?php

return [

	/**
	 * The main view theme for the Module.
	 */
	'theme'  => 'finnegan',

	/**
	 * The list of required asset libraries.
	 * (See the main assets.php config file for the complete list of available libraries and their dependencies)
	 */
	'assets' => [ 'modernizr', 'font-awesome.local', 'finnegan' ],
	
	/**
	 * The sidebar configuration.
	 */
	'sidebar' => [
		
		/**
		 * Show the current version number.
		 */
		'show_version' => true,
	
	],
	
	/**
	 * The footer configuration.
	 */
	'footer'  => [
		
		/**
		 * The credits text.
		 */
		'credits' => 'Powered by FinneganCDS // <a href="http://www.hce.it/en/" target="_blank">hce.it</a>'
	
	],

];