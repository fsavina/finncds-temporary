<?php

return [

	/**
	 * The Module title.
	 */
	'title'       => 'FinneganCDS',

	/**
	 * Set this Module as the default one.
	 * Note: There should be only one default module in the system.
	 */
	'default'     => false,
	
	/**
	 * Set the default locale.
	 */
	'locale'      => 'en',
	
	/**
	 * The Module definitions configuration.
	 */
	'definitions' => [

		/**
		 * The list of folders from where the Module is supposed to load the definitions.
		 */
		'folders' => [
			'resources/definitions'
		],

		/**
		 * White list of definitions to which this Module as access.
		 */
		//'only' => ['some_definition'],

		/**
		 * Black list of definitions to which this Module don't have access.
		 */
		//'except' => ['some_definition'],
	]

];