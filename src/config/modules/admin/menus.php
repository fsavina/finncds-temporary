<?php

return [

	/**
	 * The general configuration, shared by all the menus.
	 */
	'config'    => [

		/**
		 * The default caching time.
		 */
		'cacheTime' => 60

	],

	/**
	 * The list of menus for the Module.
	 */
	'instances' => [

		/**
		 * The primary menu name.
		 */
		'primary'   => [

			/**
			 * The list of menu items.
			 */
			'items' => [
				[
					/**
					 * The menu item label.
					 */
					'label' => 'Fields testing',

					/**
					 * The menu item icon.
					 */
					'icon'  => 'bomb',

					/**
					 * The menu item url.
					 */
					'url'   => '#',

					/**
					 * The submenu items.
					 */
					'items' => [
						[ 'model' => 'test_strings' ],
						[ 'model' => 'test_numbers' ],
						[ 'model' => 'test_dates' ],
						[ 'model' => 'test_selects' ],
						[ 'model' => 'test_files' ],
						[ 'model' => 'test_translatables' ],
						[ 'model' => 'test_sortables' ]
					]
				],
				[
					'label' => 'finnegan::general.content',
					'icon'  => 'folder-open',
					'url'   => '#',
					'items' => [
						[ 'model' => 'pages' ],
						[ 'model' => 'posts' ],
						[ 'model' => 'events' ]
					]
				],
				[
					'label' => 'finnegan::general.categorization',
					'icon'  => 'tags',
					'url'   => '#',
					'items' => [
						[ 'model' => 'categories' ],
						[ 'model' => 'tags' ]
					]
				],
				[
					'label' => 'finnegan::general.configuration',
					'url'   => '#',
					'icon'  => 'wrench',
					'items' => [
						[ 'model' => 'translations' ],
						[
							'route' => 'settings',
							'label' => 'Settings',
							'icon'  => 'cog',
							'auth'  => [
								'role' => 'admin'
							]
						],
					]
				],
				[ 'model' => 'users' ]
			]
		],

		/**
		 * The secondary menu name.
		 */
		'developer' => [
			'items' => [
				[
					/**
					 * The menu item label.
					 */
					'label' => 'Modules',

					/**
					 * The menu item icon.
					 */
					'icon'  => 'cubes',

					/**
					 * The menu item route.
					 */
					'route' => 'modulesList'
				],
				[
					'label' => 'Definition wizard',
					'icon'  => 'magic',
					'route' => 'definitionWizard'
				],
				[
					'label' => 'View configuration',
					'icon'  => 'cog',
					'route' => 'configViewer'
				],
				[
					'label' => 'Routes list',
					'icon'  => 'compass',
					'route' => 'routesList'
				],
			]
		]
	]

];