<?php

return [

	/**
	 * Define a URL prefix for the whole Module routing system.
	 * ('false' to disable URL prefixing)
	 */
	'prefix'     => 'finnegan',

	/**
	 * The controllers namespace.
	 */
	'namespace'  => 'Finnegan\\Foundation\\Http\\Controllers',

	/**
	 * The default controller name.
	 */
	'controller' => 'CrudController',

	/**
	 * Enable the CRUD routing system
	 */
	'crud'       => true,
	
	/**
	 * Enable the Settings routes
	 */
	'settings'   => true,

	/**
	 * The list of the extra routes.
	 */
	'routes'     => [
		
		'modules-list' => [
			'as'   => 'modulesList',
			'uses' => 'DevelopController@modulesList'
		],
		
		'definition-wizard' => [
			'as'     => 'definitionWizard',
			'uses'   => 'DevelopController@definitionWizard',
			'method' => [ 'get', 'post' ]
		],

		'config-viewer' => [
			'as'   => 'configViewer',
			'uses' => 'DevelopController@configViewer'
		],
		
		'routes-list' => [
			'as'   => 'routesList',
			'uses' => 'DevelopController@routesList'
		],

	]

];