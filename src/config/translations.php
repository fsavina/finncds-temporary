<?php

return [

	/**
	 * The list of the available languages.
	 */
	'languages'  => [
		'en' => 'English'
	],
	
	/**
	 * The translation cache time (in minutes)
	 */
	'cache' => ( env ( 'APP_ENV' ) == 'production' ) ? 60 : 0,

	/**
	 * The translation namespaces for the custom translation system.
	 */
	'namespaces' => [
		'general'  => 'finnegan::general.general',
		'messages' => 'finnegan::general.messages',
		'actions'  => 'finnegan::general.actions',
		'auth'     => 'finnegan::general.auth'
	]

];