<?php

return [
	'roles' => [
		'developer' => [ ],

		'admin' => [
			'abilities' => [ 'edit_other_profiles', 'manage_settings' ]
		],

		'editor' => [
			'abilities' => [ 'publish_others_objects', 'edit_others_objects' ]
		],

		'author'      => [
			'abilities' => [ 'publish' ]
		],

		'contributor' => [ ],

		'subscriber' => [
			'abilities' => [ 'subscribe' ]
		]
	]
];