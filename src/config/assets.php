<?php
return [
	'libraries' => [
		'modernizr' => [
			'js'      => 'assets/vendor/modernizr/modernizr.js',
			'context' => 'head'
		],
		
		'jquery' => [
			'js' => 'assets/vendor/jquery/dist/jquery.min.js',
			
			'plugins' => [
				'ui' => [
					'js'       => '//code.jquery.com/ui/1.11.4/jquery-ui.min.js',
					'requires' => [ 'jquery' ]
				],
				
				'datetimepicker' => [
					'js'       => 'https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.4.5/jquery.datetimepicker.min.js',
					'css'      => 'https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.4.5/jquery.datetimepicker.min.css',
					'requires' => [ 'jquery' ]
				],
				
				'tokeninput' => [
					'js'       => 'assets/vendor/jquery-tokeninput/build/jquery.tokeninput.min.js',
					'css'      => 'assets/vendor/jquery-tokeninput/styles/token-input.css',
					'requires' => [ 'jquery' ]
				]
			]
		],
		
		'foundation' => [
			'cdn' => [
				'js'       => 'https://cdn.jsdelivr.net/foundation/6.2.0/foundation.min.js',
				'css'      => 'https://cdn.jsdelivr.net/foundation/6.2.0/foundation.min.css',
				'requires' => [ 'jquery' ],
				'script'   => '$(document).foundation();'
			],
			
			'local' => [
				'js'       => 'assets/vendor/foundation-sites/dist/foundation.min.js',
				'css'      => 'assets/vendor/foundation-sites/dist/foundation.min.css',
				'requires' => [ 'jquery' ],
				'script'   => '$(document).foundation();'
			]
		],
		
		'ckeditor' => [
			'basic'    => [ 'js' => 'https://cdn.ckeditor.com/4.5.7/basic/ckeditor.js' ],
			'standard' => [ 'js' => 'https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js' ],
			'full'     => [ 'js' => 'https://cdn.ckeditor.com/4.5.7/full/ckeditor.js' ]
		],
		
		'font-awesome' => [
			'cdn' => [
				'css' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'
			],
			
			'local' => [
				'css' => 'assets/vendor/fontawesome/css/font-awesome.min.css'
			]
		],
		
		'google' => [
			'maps' => [
				'js'     => 'https://maps.googleapis.com/maps/api/js',
				'params' => [ 'language' ]
			]
		],
		
		'finnegan' => [
			'js'       => 'assets/finnegan/js/finnegan.js',
			'css'      => 'assets/finnegan/css/finnegan.css',
			'requires' => [ 'jquery', 'foundation.local' ]
		]
	]
];