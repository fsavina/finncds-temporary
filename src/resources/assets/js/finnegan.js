var FINNEGAN = FINNEGAN || {};

FINNEGAN.Widgets = {};

FINNEGAN.init = function(){
	var $body = $('body');

	if ($body.hasClass('finn-list'))
	{
		this._initFilters();
		this._initBulkActions();

		if ($('.finn-items', '.finn-list').hasClass('finn-sortable'))
		{
			this._initSortable();
		}
	}/* else if ($body.hasClass('finn-form'))
	 {
	 this._initForm();
	 }*/
};

FINNEGAN._initFilters = function(){
	$('#toggle-list-filters', '.finn-list').on('click',function(e){
		e.preventDefault();
		$('#list-filters').toggleClass('hide');
	});
};

FINNEGAN._initSortable = function(){
	var $list = $('.finn-items', '.finn-list');
	$list.find('tbody').sortable({
		cursor:'move',
		placeholder:'ui-sortable-highlight',

		stop:function() {
			var priorityField = $list.data('priority-field');

			var ids = [];
			var min = 0;
			var max = 0;

			$('tr', '.finn-items tbody').each(function(){
				ids.push($(this).data('id'));
				var priority = parseInt( $(this).find('.' + priorityField).text() );
				if (priority < min || min == 0) min = priority;
				if (priority > max || max == 0) max = priority;
			});

			$.ajax({
				url:$list.data('sortable-url'),
				method:"POST",
				data:{
					"_token":$list.data('token'),
					items:ids,
					min:min,
					max:max
				},
				success:function(result){
					if (result.success)
					{
						var i = min;
						$list.find('tbody tr').each(function(){
							$(this).find('.' + priorityField).text(i++);
						});
					}
				}
			});
		},
		helper:function(e, tr) {
			var $originals = tr.children();
			var $helper = tr.clone();
			$helper.children().each(function(index){
				$(this).width($originals.eq(index).width())
			});
			return $helper;
		}
	});
};

FINNEGAN._initBulkActions = function(){
	var items = $('.bulk-record', '.finn-items');

	$('a, button, input', '.finn-list tbody tr').not('[data-toggle]').on('click',function (e){
		e.stopPropagation();
	});

	$('tr', '.finn-list tbody').on('click',function (){
		var checkbox = $(this).find('.bulk-record');
		checkbox.prop('checked', !checkbox.is(':checked'));
	});

	$('#bulk-select-all', '.finn-list').on('click',function(){
		if (this.checked)
		{
			items.prop('checked', true);
		} else
		{
			items.prop('checked', false);
		}
	});

	$('#bulk-form', '.finn-list').on('submit',function (e){
		var selected = 0;
		var $form = $(this);
		items.filter(':checked').each(function(){
			$form.append('<input type="hidden" name="' + $form.data('model') + '_bulk[records][]" value="' + $(this).val() + '">');
			selected++;
		});
		if (!selected)
		{
			e.preventDefault();
		}
		return true;
	});
};

FINNEGAN._initForm = function () {
	$('form','.finn-form').on('change',function(){
		var invalid = $(this).find(':invalid');
		if (invalid.length && (!$(invalid[0]).is(':visible')))
		{
			invalid.parents('.tabs-content').find('.is-active').removeClass('is-active');
			invalid.parents('.tabs-panel').addClass('is-active');
		}
	});
};

$(function(){
	FINNEGAN.init();
});