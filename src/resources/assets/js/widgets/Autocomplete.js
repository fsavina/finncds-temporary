FINNEGAN.Widgets.Autocomplete = function (options) {
    this.options = options;
    this.init();
};

FINNEGAN.Widgets.Autocomplete.prototype.init = function () {
    var config = this.options.config;
    var id = this.options.id;
    var $el = $('#' + id);

    $el.tokenInput($el.data('suggestions-url'), {
        jsonContainer: 'data',
        preventDuplicates: true,
        allowFreeTagging: config.allowInsert,
        prePopulate: config.value,
        tokenLimit: config.limit,
        minChars: 2
    });
};