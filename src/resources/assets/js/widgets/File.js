FINNEGAN.Widgets.File = function(options) {
	this.options = options;
	this.initListeners();
};

FINNEGAN.Widgets.File.prototype.id = function() {
	return this.options.id;
};

FINNEGAN.Widgets.File.prototype.required = function() {
	return this.options.required;
};

FINNEGAN.Widgets.File.prototype.updatePreview = function (url) {
	var id = this.id();
	var preview = $('#' + id + '_preview');

	if (!preview.find('img').length)
	{
		preview.append('<img class="thumbnail"/>');
	} else if (!this.oldValue)
	{
		this.oldValue = preview.find('img').attr('src');
	}
	this.setPreview(url);

	$('.' + id + '_button_container').removeClass('hide');
	if (!this.options.editMode)
	{
		$('#' + id + '_input').addClass('hide');
	}
};

FINNEGAN.Widgets.File.prototype.setPreview = function (url) {
	$('#' + this.id() + '_preview')
		.removeClass('finn-removed')
		.find('img').attr('src', url);
};

FINNEGAN.Widgets.File.prototype.isImage = function () {
	return $('#' + this.id()).data('is-image');
};

FINNEGAN.Widgets.File.prototype.initListeners = function() {
	var $this = this;
	var id = $this.id();

	if (this.isImage())
	{
		$('#' + id).on('change',function(){
			if (this.files && this.files[0] && this.files[0].type.match('image.*'))
			{
				var reader = new FileReader();
				reader.onload = function (e) {
					$this.updatePreview(e.target.result);
				};
				reader.readAsDataURL(this.files[0]);
			}
		});
	}


	$('#' + id + '_replace').click(function(e) {
		e.preventDefault();
		
		$('.' + id + '_button_container').addClass('hide');
		$('#' + id + '_preview').addClass('finn-removed');
		$('#' + id + '_input, #' + id + '_cancel_container').removeClass('hide');
		$('#' + id + '_deleted').val(1);
		if ($this.required())
		{
			$('#' + id).attr('required','required');
		}
		$('#' + id).removeAttr('disabled');
	});


	$('#' + id + '_delete').click(function(e) {
		e.preventDefault();
		
		$('.' + id + '_button_container').addClass('hide');

		if ($this.options.editMode)
		{
			$('#' + id + '_preview').addClass('finn-removed');
			$('#' + id + '_deleted').val(1);
		} else
		{
			$('#' + id + '_preview img').remove();
			$('#' + id + '_input').removeClass('hide');
			$('#' + id).val('');
		}

		$('#' + id + '_cancel_container').removeClass('hide');
		$('#' + id).removeAttr('disabled');
	});


	$('#' + id + '_cancel').click(function(e) {
		e.preventDefault();
		
		$('.' + id + '_button_container').removeClass('hide');
		if ($this.isImage() && $this.oldValue)
		{
			$this.setPreview($this.oldValue);
		} else
		{
			$('#' + id + '_preview').removeClass('finn-removed');
		}
		$('#' + id + '_input, #' + id + '_cancel_container').addClass('hide');
		$('#' + id + '_deleted').val('');
		$('#' + id).val('').attr('disabled','disabled');
		$('#' + id).removeAttr('required');
	});
};


