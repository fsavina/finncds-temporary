FINNEGAN.Widgets.Textarea = function (options) {
    this.options = options;
    this.init();
};

FINNEGAN.Widgets.Textarea.prototype.init = function () {
    for (var id in this.options.id) {
        CKEDITOR.replace(this.options.id[id], this.options.config);
    }
};
