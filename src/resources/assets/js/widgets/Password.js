FINNEGAN.Widgets.Password = function(options) {
	this.options = options;
	this.initListeners();
};

FINNEGAN.Widgets.Password.prototype.id = function() {
	return this.options.id;
};

FINNEGAN.Widgets.Password.prototype.required = function() {
	return this.options.required;
};

FINNEGAN.Widgets.Password.prototype.initListeners = function() {
	var $this = this;
	var id = $this.id();

	$('#' + id + '_confirm_label').addClass('hide');

	$('#' + id + '_toggle, #' + id + '_cancel').on('click', function(e){
		e.preventDefault();
		$this.editToggle();
	});
	
	if (this.options.hasErrors)
	{
		this.editToggle(true);
	}
};

FINNEGAN.Widgets.Password.prototype.editToggle = function(forceShow){
	var id = this.id();
	var containers = $('#' + id + '_container');
	var labels = $('#' + id + '_confirm_label');
	var input = $('#' + id + ', #' + id + '_confirmation');
	var errors = $('.' + id + '_main_container').find('.error');
	
	if (forceShow || containers.hasClass('hide'))
	{
		$('#' + id + '_toggle').hide();
		if (this.required())
		{
			input.attr('required', 'required');
		}
		input.removeAttr('disabled');
		
		containers.add(labels).removeClass('hide');
		input.first().focus();
	} else
	{
		errors.remove();
		containers.add(labels).addClass('hide');
		$('#' + id + '_toggle').show();
		if (this.required())
		{
			input.removeAttr('required');
		}
		input.attr('disabled', 'disabled').val('');
	}
};