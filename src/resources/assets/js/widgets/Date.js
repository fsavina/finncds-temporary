FINNEGAN.Widgets.Date = function (options) {
    this.options = options;
    this.init();
};

FINNEGAN.Widgets.Date.prototype.init = function () {
    var $this = this;

    var fallback = true;
    if ((this.options.mode == 'date') && ($('[type="date"]').prop('type') == 'date' )) {
        fallback = false;
    }

    if (fallback) {
        for (var i in this.options.id) {
            $('#' + this.options.id[i]).datetimepicker(this.options.config);
        }
    }
};