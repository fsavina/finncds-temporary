FINNEGAN.Widgets.Map = function(options) {
	this.options = options;
	this.initialized = false;
	this.map = null;
	this.marker = null;
	this.geocoder = null;
	this.loading = false;

	if (typeof google == 'undefined')
	{
		alert('The Google Maps library is missing or unreachable');
		return;
	}

	this.initListeners();
};

FINNEGAN.Widgets.Map.prototype.id = function() {
	return this.options.id;
};

FINNEGAN.Widgets.Map.prototype.setLoading = function(loading) {
	if (loading)
	{
		$('#' + this.id() + '_loading').removeClass('hide');
	} else
	{
		$('#' + this.id() + '_loading').addClass('hide');
	}
	this.loading = loading;
};

FINNEGAN.Widgets.Map.prototype.initListeners = function() {
	var $this = this;
	$('#' + this.id() + '_map_trigger').click(function(e) {
		e.preventDefault();
		$this.toggleMap();
	});

	$('#' + this.id() + '_ok').click(function(e) {
		e.preventDefault();
		$this.toggleMap();
	});

	$('#' + this.id() + '_clear').click(function(e) {
		e.preventDefault();
		$this.clearValue();
	});

	if (navigator.geolocation)
	{
		$('#' + this.id() + '_geolocate').removeClass('hide').click(function(e) {
			e.preventDefault();
			$this.geolocateUser();
		});
	}

	$('#' + this.id() + '_geocode').click(function(e) {
		e.preventDefault();
		var address = $('#' + $this.id() + '_address').val();
		if(address.length)
		{
			$this.geocode(address);
		}
	});

	$('#' + this.id() + '_address').on('keyup keypress', function(e) {
		var code = e.keyCode || e.which;
		if (code == 13)
		{
			var address = $(this).val();
			if (address.length)
			{
				$this.geocode(address);
			}
			e.preventDefault();
			return false;
		}
	});
};

FINNEGAN.Widgets.Map.prototype.toggleMap = function() {
	$('#' + this.id() + '_map_container').toggleClass('hide');
	if (!this.initialized) {
		this.startMap();
	}
};

FINNEGAN.Widgets.Map.prototype.startMap = function() {
	var $this = this;

	this.map = new google.maps.Map(document.getElementById(this.id() + '_map'),{
		zoom : 7,
		streetViewControl : false,
		mapTypeControl : false,
		scrollwheel : false
	});

	this.marker = new google.maps.Marker({
		position : this.map.getCenter(),
		map : this.map,
		title : 'Drag me!',
		draggable : true
	});
	
	var position = this.options.coordinates;
	if (position == null)
	{
		this.setMarker(0, 0);
		if (this.options.geolocate && navigator.geolocation)
		{
			$this.geolocateUser();
		}
	}else if (typeof position == 'string')
	{
		this.geocode(position);
	} else
	{
		this.setMarkerAndValue(position.latitude, position.longitude);
	}

	google.maps.event.addListener(this.map, 'click', function(event) {
		$this.marker.setPosition(event.latLng);
		$this.setValue(event.latLng.lat(), event.latLng.lng());
	});

	this.marker.addListener('dragend', function(event) {
		$this.setValue(event.latLng.lat(), event.latLng.lng());
	});

	this.initialized = true;
};

FINNEGAN.Widgets.Map.prototype.clearValue = function() {
	$('#' + this.id()).val('');
};

FINNEGAN.Widgets.Map.prototype.setValue = function(lat, lng) {
	$('#' + this.id()).val(lat + ',' + lng);
};

FINNEGAN.Widgets.Map.prototype.setMarker = function(lat, lng) {
	this.map.setCenter({ lat:lat, lng:lng });
	this.marker.setPosition({ lat:lat, lng:lng });
};

FINNEGAN.Widgets.Map.prototype.setMarkerAndValue = function(lat, lng){
	this.setMarker(lat, lng);
	this.setValue(lat, lng);
};

FINNEGAN.Widgets.Map.prototype.geocode = function(address) {
	this.setLoading(true);
	var $this = this;
	if (this.geocoder == null)
	{
		this.geocoder = new google.maps.Geocoder();
	}
	this.geocoder.geocode({'address': address}, function(results, status) {
		$this.setLoading(false);
		if (status === google.maps.GeocoderStatus.OK)
		{
			var location = results[0].geometry.location;
			$this.setMarkerAndValue(location.lat(), location.lng());
		} else
		{
			alert('Geocode was not successful for the following reason: ' + status);
		}
	});
};

FINNEGAN.Widgets.Map.prototype.geolocateUser = function() {
	if (navigator.geolocation)
	{
		this.setLoading(true);
		var $this = this;
		navigator.geolocation.getCurrentPosition(function(position) {
			$this.setLoading(false);
			$this.setMarkerAndValue(
				position.coords.latitude,
				position.coords.longitude
			);
		});
	}
};

