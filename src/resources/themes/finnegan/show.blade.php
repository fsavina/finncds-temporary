@extends($theme->layout('default','finnegan'))

@section('page-title', $presenter->title($model, 'show', true) . ' - ')
@section('body-class', 'finn-show')

@section('inner-title', $presenter->title($model, 'show'))

@section('model-actions')
	@include($theme->partial('show.actions'))
@endsection

@section('content')
	@include($theme->partial('messages'))

	<div class="fields">
		<?php $showEmptyFields = $model->config('display.show.showEmptyFields'); ?>
		@foreach($model -> fields () -> showInPreview() as $field)
			@if ($showEmptyFields or !$field->isEmpty())
				@include($theme->partial('show.field'))
			@endif
		@endforeach

		@if ($model->config('relations') and Gate::allows('relations',$model))
			@include($theme->partial('show.relations'))
		@endif
	</div>
@endsection
