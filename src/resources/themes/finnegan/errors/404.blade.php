@extends($theme->layout(Auth::check()?'sidebar':'basic'))

@section('content')
<div class="row text-center">
	<div class="small-10 medium-5 small-centered columns">
		<div class="callout alert secondary" style="margin-top:30%">
			<h3 style="margin-bottom:40px">Something went wrong</h3>
			<div>{{ $message }}</div>
		</div>
	</div>
</div>
@endsection