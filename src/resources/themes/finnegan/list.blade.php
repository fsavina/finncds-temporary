@extends($theme->layout('default','finnegan'))

@section('page-title', $presenter->title($model, 'list',true) . ' - ')
@section('body-class', 'finn-list')

@section('inner-title', $presenter->title($model, 'list'))

@section('model-actions')
	@if(isset($actions['general']))
		@foreach ($actions['general'] as $action)
			{!! $action -> renderButton($model) !!}
		@endforeach
	@endif

	@if (count($items) and (isset($filters) and (count($filters['*']) or count($filters['text']) )) )
		{!! $presenter -> button() -> action('search') -> url('#') -> id('toggle-list-filters') !!}
	@endif
@endsection

@section('content')
	@include($theme->partial('list.filters'))

	@include($theme->partial('messages'))

	@if (count($items))
		<table class="finn-items
			{{ $model->definition()->sortable() ? 'finn-sortable' : '' }}
			{{ count($model -> bulkActions()) ? 'hover' : '' }}"
		   @if ($model->definition()->sortable())
			data-sortable-url="{{$model->actionUrl('sort')}}"
			data-priority-field="field-{{$model->definition()->sortablePriorityField()}}"
			data-token="{{ csrf_token() }}"
		   @endif
		>
			<thead>
				@include($theme->partial('list.header'))
			</thead>
			<tbody>
				@foreach($items as $item)
					@include($theme->partial('list.item'))
				@endforeach
			</tbody>
			<tfoot></tfoot>
		</table>
		@include($theme->partial('list.bulk'))
	@else
		<p>@lang('finnegan::messages.list_empty')</p>
	@endif

	<div class="text-center">{{ $items->links($layout->theme()->resolveView('partials.pagination')) }}</div>
@endsection

@section('extra-js')
	@foreach ($filters['*'] as $filter)
		@if ($filter -> searchType())
			{!! $filter -> widget() -> setContext ( 'filters' ) -> renderJs() !!}
		@endif
	@endforeach
@endsection