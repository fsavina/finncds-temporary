@extends($theme->layout('basic'))

@section('body-class', 'finn-login')

@section('content')
<div class="row">
	<div class="small-10 medium-7 large-5 small-centered columns">
		<div class="callout secondary large">
			<h2 class="text-center" style="margin-bottom:40px">
				<a href="{{$module->actionUrl('home')}}">{!! $presenter->icon('logo') !!} {{$module->title('FinneganCMS')}}</a>
			</h2>
			
			@if (isset($errors) and count($errors))
				<div class="callout alert">
				@foreach ($errors->all() as $error)
					<p>{!! $presenter->icon('warning') !!} {{ $error }}</p>
				@endforeach
				</div>
			@endif

			@if (session('status'))
				{!! $presenter -> message( session('status'), 'success' ) !!}
			@else
				{!! $layout->form()->open(['method'=>'post']) !!}

				{!! $presenter -> inputGroup( $layout->form()->email('email', old('email'), ['class'=>'input-group-field']), null, $presenter->icon('at')->fixedWidth() ) !!}

				{!! $presenter -> button('finnegan::auth.send_password_reset_link') -> large() -> expanded() !!}

				<div class="text-center">{!! $presenter -> link('finnegan::auth.back_to_login_page') -> url($module->actionUrl('login')) -> icon('arrow-circle-left') -> secondary() -> expanded() !!}</div>

				{!! $layout->form()->close() !!}
			@endif

		</div>
	</div>
</div>
@endsection

@section('extra-js')
	$(function(){
		$('input[name=email]').focus();
	});
@endsection