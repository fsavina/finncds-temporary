@extends($theme->layout('basic'))

@section('body-class', 'finn-login')

@section('content')
<div class="row">
	<div class="small-10 medium-7 large-5 small-centered columns">
		<div class="callout secondary large">
			<h2 class="text-center" style="margin-bottom:40px">
				<a href="{{$module->actionUrl('home')}}">{!! $presenter->icon('logo') !!} {{$module->title('FinneganCDS')}}</a>
			</h2>
			
			@if (isset($errors) and count($errors))
				<div class="callout alert">
				@foreach ($errors->all() as $error)
					<p>{!! $presenter->icon('warning') !!} {{ $error }}</p>
				@endforeach
				</div>
			@endif

			{!! $layout->form()->open(['method'=>'post']) !!}
			
				{!! $presenter -> inputGroup( $layout->form()->email('email', old('email'), ['class'=>'input-group-field']), null, $presenter->icon('at')->fixedWidth() ) !!}
				
				{!! $presenter -> inputGroup( $layout->form()->password('password', ['class'=>'input-group-field']), null, $presenter->icon('lock')->fixedWidth() ) !!}
				
				{!! $presenter -> button('finnegan::auth.login') -> large() -> expanded() !!}

				<div class="clearfix">
					<label class="float-left">{!! $layout->form()->checkbox('remember',1, old('remember'), ['class'=>'inline'] ) !!}@lang('finnegan::auth.remember_me')</label>
					@if( $module->config('auth.password.reset'))
						<a class="float-right" href="{{ $module->actionUrl('password_email') }}">@lang('finnegan::auth.forgot_your_password')</a>
					@endif
				</div>
			{!! $layout->form()->close() !!}
		</div>
	</div>
</div>
@endsection

@section('extra-js')
	$(function(){
		$('input[name=email]').focus();
	});
@endsection