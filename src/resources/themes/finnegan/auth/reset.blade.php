@extends($theme->layout('basic'))

@section('body-class', 'finn-login')

@section('content')
<div class="row">
	<div class="small-10 medium-7 large-5 small-centered columns">
		<div class="callout secondary large">
			<h2 class="text-center" style="margin-bottom:40px">
				<a href="{{$module->actionUrl('home')}}">{!! $presenter->icon('logo') !!} {{$module->title('FinneganCMS')}}</a>
			</h2>
			
			@if (isset($errors) and count($errors))
				<div class="callout alert">
				@foreach ($errors->all() as $error)
					<p>{!! $presenter->icon('warning') !!} {{ $error }}</p>
				@endforeach
				</div>
			@endif

			{!! $layout->form()->open(['method'=>'post', 'url'=>$module->actionUrl('password_reset_post')]) !!}
				{!! $layout->form()->hidden ('token', $token) !!}
			
				{!! $presenter -> inputGroup( $layout->form()->email('email', old('email'), ['class'=>'input-group-field']), null, $presenter->icon('at')->fixedWidth() ) !!}

				{!! $presenter -> inputGroup( $layout->form()->password('password', ['class'=>'input-group-field']), null, $presenter->icon('lock')->fixedWidth() ) !!}
				{!! $presenter -> inputGroup( $layout->form()->password('password_confirmation', ['class'=>'input-group-field']), null, $presenter->icon('lock')->fixedWidth() ) !!}

				{!! $presenter -> button('finnegan::auth.reset_password') -> large() -> expanded() !!}

			{!! $layout->form()->close() !!}
		</div>
	</div>
</div>
@endsection

@section('extra-js')
	$(function(){
		$('input[name=email]').focus();
	});
@endsection