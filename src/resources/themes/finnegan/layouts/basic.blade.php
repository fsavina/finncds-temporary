<!doctype html>
<html class="no-js" lang="{{App::getLocale()}}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>@yield('page-title'){{$module->title('FinneganCDS')}}</title>
	{!! $layout -> assetManager () -> render('head') !!}
</head>
<body class="finnegan finn-layout-basic finn-module-{{$module->name()}} @yield('body-class')">
	<div id="finn-main-container">
		@yield('content')
	</div>
	
	{!! $layout -> assetManager () -> render('body') !!}
	<script>@yield('extra-js')</script>
</body>
</html>