<!doctype html>
<html class="no-js" lang="{{App::getLocale()}}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>@yield('page-title'){{$module->title('FinneganCDS')}}</title>
	{!! $layout -> assetManager () -> render('head') !!}
</head>
<body class="finnegan finn-layout-sidebar finn-module-{{$module->name()}} @yield('body-class')">
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
			<div class="off-canvas position-left reveal-for-large" id="finn-main-sidebar" data-off-canvas>
				@include($theme->partial('sidebar-left'))
			</div>
			@if (Auth::check() and Auth::user()->achieve('developer'))
			<div class="off-canvas position-right" id="finn-tools-sidebar" data-off-canvas data-position="right">
				@include($theme->partial('sidebar-right'))
			</div>
			@endif
			<div class="off-canvas-content" data-off-canvas-content>
				@include($theme->partial('header'))

				<div id="finn-main-content">
					@yield('open-main-content')

					@if(isset($breadcrumbs)) {!! $breadcrumbs !!} @endif

					<div id="finn-page-header" class="row">
						<div class="small-4 columns"><h4>@yield('inner-title')</h4></div>
						<div class="small-8 columns text-right">@yield('model-actions')</div>
					</div>

					<div id="finn-page-body" class="row">@yield('content')</div>

					@yield('close-main-content')
				</div>

				@include($theme->partial('footer'))
			</div>
		</div>
	</div>
	
	{!! $layout -> assetManager () -> render('body') !!}
	<script>@yield('extra-js')</script>
</body>
</html>