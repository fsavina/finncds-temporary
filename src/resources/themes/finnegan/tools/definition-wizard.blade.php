@extends($theme->layout('default'))

@section('page-title', 'Definition wizard - ')
@section('body-class', 'finn-json-editor')

@section('inner-title', $presenter->icon('magic') . ' Definition wizard')

@section('model-actions')
    {!! $presenter -> button()->action('save')->success() !!}
@endsection

@section('open-main-content', $layout->form()->open(['method'=>'post', 'id'=>'json-editor']) )

@section('content')
    <div class="callout">
        <label>
            {!! $layout->form()->text('name','',[
                'id'=>'name',
                'pattern'=>'[a-z_]{3,}',
                'required'=>'required',
                'placeholder'=>'The definition name'
            ]) !!}
        </label>
        <p class="help-text">The definition will be saved in "{{ realpath(base_path('config/finnegan/definitions')) }}\&laquo;filename&raquo;.json"</p>
    </div>
    <div id="json-editor-holder" class="callout"></div>
@endsection

@section('close-main-content', $layout->form()->close() )

@section('extra-js')
$(function(){
    JSONEditor.defaults.theme = 'foundation5';
    JSONEditor.defaults.iconlib = 'fontawesome4';
    JSONEditor.defaults.language = 'en';

    var editor = new JSONEditor(document.getElementById('json-editor-holder'),{
        schema: {!! file_get_contents(\Finnegan\Support\BaseServiceProvider::sourcePath() . 'Finnegan/Definitions/Schemas/model.json') !!}
    });

    $('#json-editor').submit(function(e){
        if (editor.validate().length)
        {
            e.preventDefault();
        }
    });
});
@endsection
