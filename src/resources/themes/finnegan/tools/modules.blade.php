@extends($theme->layout('default'))

@section('page-title', $title . ' - ')
@section('body-class', 'finn-modules')

@section('inner-title', $presenter->icon($icon) . ' ' . $title)

@section('model-actions')
    {!! $presenter->button('Create module', '#')->setAttribute('data-open', 'create-module')->icon('actions.create')->success() !!}
@endsection

@section('content')
    @include($theme->partial('messages'))

    @foreach($modules as $mod)
        <div class="box is-active" id="{{$id = uniqid()}}" data-toggler=".is-active">
            <a class="title" data-toggle="{{$id}}">
                {{ $mod->name() }}
                @if ($mod->config ( 'module.default' ))
                    <small>(Default module)</small>
                @endif
            </a>
            <div class="content">
                <table>
                    <tbody>
                        <tr>
                            <td>Route prefix</td>
                            <td>/{{ $mod->config ( 'routing.prefix' ) }}</td>
                        </tr>
                        @if ($mod->isSecure ())
                            <tr>
                                <td>Secure</td>
                                <td>{!! $mod->isSecure () ? '<span class="label success">YES</span>' : '<span class="label alert">NO</span>' !!}</td>
                            </tr>
                            <tr>
                                <td>User Role</td>
                                <td>{{ $mod->config('auth.role') }}</td>
                            </tr>
                        @else
                            <tr>
                                <td>Secure</td>
                                <td><span class="label alert">NO</span></td>
                            </tr>
                        @endif
                        <tr>
                            <td>CRUD Routes</td>
                            <td>{!! $mod->config ( 'routing.crud' ) ? '<span class="label success">YES</span>' : '<span class="label alert">NO</span>' !!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    @endforeach

    <div class="reveal" id="create-module" data-reveal>
        {!! Form::open() !!}
            <label>
                Name: {!! Form::input('text', 'module', '', ['required'=>'required']) !!}
            </label>
            <label>{!! Form::checkbox('crud', 1) !!} Crud</label>
            <label>{!! Form::checkbox('secure', 1) !!} Secure</label>

            <div class="text-right">
                {!! $presenter->button('Create')->success() !!}
            </div>
        {!! Form::close() !!}
        <button class="close-button" data-close aria-label="Close" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endsection