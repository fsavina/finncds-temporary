@extends($theme->layout('default'))

@section('page-title', $title . ' - ')
@section('body-class', 'finn-dumper')

@section('inner-title', $presenter->icon($icon) . ' ' . $title)

@section('content')
    {!! (new Illuminate\Support\Debug\Dumper)->dump($dump) !!}
@endsection