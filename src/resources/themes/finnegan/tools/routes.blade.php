@extends($theme->layout('default'))

@section('page-title', $title . ' - ')
@section('body-class', 'finn-routes')

@section('inner-title', $presenter->icon($icon) . ' ' . $title )

@section('model-actions')
    @if ($cache)
        Cache status: <span class="label success">enabled</span>
        {!! $presenter->button()->url('?cmd=refresh')->translateCaption('actions.refresh_cache')->icon('refresh')->success() !!}
        {!! $presenter->button()->url('?cmd=dump')->translateCaption('actions.dump_cache')->icon('eraser')->alert() !!}
    @else
        Cache status: <span class="label warning">disabled</span>
        {!! $presenter->button()->url('?cmd=refresh')->translateCaption('actions.cache_routes')->icon('plug')->success() !!}
    @endif
@endsection

@section('content')
    @include($theme->partial('messages'))

    <table>
        <thead>
            <tr>
                <th>URI</th>
                <th>Methods</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($routes as $route)
                <tr>
                    <td>{{ $route->uri() }}</td>
                    <td>{{ implode('|', $route->methods()) }}</td>
                    <td>{{ $route->getName() }}</td>
                    <td>
                        <abbr title="{{ $route->getActionName() }}">
                            {{ \Illuminate\Support\Arr::last(explode('\\', $route->getActionName())) }}
                        </abbr>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection