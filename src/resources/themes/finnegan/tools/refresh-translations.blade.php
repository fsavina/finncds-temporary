@extends($theme->layout('default'))

@section('page-title', $title . ' - ')
@section('body-class', 'finn-dumper')

@section('inner-title', $presenter->icon($icon) . ' ' . $title)

@section('content')
    {!! $presenter -> message(trans('finnegan::messages.refresh_translations_completed'), 'success small') !!}
    <p>{!! $presenter->button()->action('rerun')->url($module->actionUrl('refreshTranslations')) !!}</p>
@endsection