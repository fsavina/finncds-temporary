@extends($theme->layout('default'))

@section('page-title', $title . ' - ')
@section('body-class', 'finn-dumper')

@section('inner-title', $presenter->icon($icon) . ' ' . $title)

@section('model-actions')
    @if ($cache)
        Cache status: <span class="label success">enabled</span>
        {!! $presenter->button()->url('?cmd=refresh')->translateCaption('actions.refresh_cache')->icon('refresh')->success() !!}
        {!! $presenter->button()->url('?cmd=dump')->translateCaption('actions.dump_cache')->icon('eraser')->alert() !!}
    @else
        Cache status: <span class="label warning">disabled</span>
        {!! $presenter->button()->url('?cmd=refresh')->translateCaption('actions.cache_config')->icon('plug')->success() !!}
    @endif
@endsection

@section('content')
    @include($theme->partial('messages'))

    @foreach($dump as $key => $section)
        <div class="box" id="{{$id = uniqid()}}" data-toggler=".is-active">
            <a class="title" data-toggle="{{$id}}">{{ $key }}</a>
            <div class="content">
                {!! (new Illuminate\Support\Debug\Dumper)->dump($section) !!}
            </div>
        </div>
    @endforeach
@endsection