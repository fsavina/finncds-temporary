@extends($theme->layout('default','finnegan'))

@section('body-class', 'finn-home')

@section('content')
<div class="row">
	<div class="text-center" style="margin-top:90px">
		<h1>{!! $presenter -> icon( $module->config ('module.icon', 'logo') )->lg() !!}<br>Welcome to {{ $module->title('FinneganCDS') }}</h1>
	</div>
</div>
@endsection
