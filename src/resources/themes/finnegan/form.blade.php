@extends($theme->layout('default','finnegan'))

@section('page-title', $presenter->title($model, 'form', true) . ' - ')
@section('body-class', 'finn-form' . ($model->config('display.form.groups')?' finn-groups-form':''))

@section('inner-title', $presenter->title($model, 'form'))

@section('model-actions')
	@include($theme->partial('form.actions'))
@endsection

@section('open-main-content', $layout->form()->model( $model, $model -> formConfig() ))

@section('content')
	@include($theme->partial('messages'))
	@include($theme->partial($model->config('display.form.groups')?'form.groups':'form.basic'))

	@if (isset($hidden) and is_array($hidden) and count($hidden))
		@foreach ($hidden as $name => $value)
		{!! $layout->form()->hidden($name, $value) !!}
		@endforeach
	@endif
@endsection

@section('close-main-content', $layout->form()->close() )

@section('extra-js')
	@foreach ($model -> formFields() as $name => $field)
        @if ($field->isTranslatable())
            @foreach ( $model->getLocales() as $locale => $label )
                {!! $field->setLocale ( $locale )->widget ()->renderJs () !!}
            @endforeach
        @else
		    {!! $field->widget ()->renderJs () !!}
        @endif
	@endforeach
@endsection
