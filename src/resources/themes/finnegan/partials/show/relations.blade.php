@foreach ($model->config('relations') as $name => $config)
	<?php
	$relation = $model -> relation ( $name );
	$relations = $relation -> getResults();
	$availables = $relation -> getRelated () -> get ();

	$attached = [];
	foreach ($relations as $related)
	{
		$attached[] = $related -> getKey ();
	}
	
	$options = [];
	foreach ($availables as $available)
	{
		if (in_array($available->getKey(), $attached))
		{
			continue;
		}
		$options[ $available->getKey () ] = trim ( preg_replace ( '#<[^>]+>#', ' ', $available->__toString () ) );
	}
	?>
	
	<div class="row field relation" id="relation-{{ $name }}">
		<div class="small-3 columns name">Related: {{ $relation -> getRelated ()->definition()->plural() }}</div>
		<div class="small-9 columns render">
			@if (count($options) and Gate::allows('relationAttach',$model))
				@if($relation->hasExtraFields())
					{!! $layout->form()->open(['method'=>'get','url'=>$model->recordActionUrl('relationCreate', ['relation'=>$name])]) !!}
				@else
					{!! $layout->form()->open(['url'=>$model->recordActionUrl('relationAttach', ['relation'=>$name])]) !!}
				@endif
					<div class="row">
						<div class="small-9 columns">{!! $layout->form()->select('related_id', $options) !!}</div>
						<div class="small-3 columns text-right">{!! $presenter -> button() -> action('attach')->expanded() -> success()->addClass('attach') !!}</div>
					</div>
				{!! $layout->form()->close() !!}
			@endif
			@if (count($relations))
			<table class="related-records"><tbody>
				@foreach ($relations as $related)
					<tr>
						@can('show',$related)
							<td><a href="{{$related->recordActionUrl('show')}}">{!! $related !!}</a></td>
						@else
							<td>{!! $related !!}</td>
						@endcan
						<td class="text-right actions" width="200">
							@can('relationEdit',$model)
								{!! $presenter -> button() -> small() -> success() -> icon('actions.edit') -> url($model->recordActionUrl('relationEdit', ['relation'=>$name,'relatedId'=>$related->getKey()])) -> renderIf($relation->hasExtraFields()) !!}
							@endcan
							@can('relationDetach',$model)
							{!! $layout->form()->open( ['method'=>'delete', 'style'=>'display:inline', 'url'=>$model->recordActionUrl('relationDetach', ['relation'=>$name,'relatedId'=>$related->getKey()]) ]) !!}
								{!! $presenter -> button() -> small() -> alert() -> icon('actions.destroy') -> confirm('finnegan::messages.delete_confirm') !!}
							{!! $layout->form()->close() !!}
							@endcan
						</td>
					</tr>
				@endforeach
			</tbody></table>
			@endif
		</div>
	</div>
@endforeach