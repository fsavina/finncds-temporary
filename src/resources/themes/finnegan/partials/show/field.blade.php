<div class="row field">
	<div class="small-3 columns name">{!! $field->label() !!}</div>
	<div class="small-9 columns render">
		@if ($field->isTranslatable())
			<?php
			$first = true;
			$tabs = $presenter->tabs( $field->name() );
			foreach($field->model()->getLocales() as $locale => $label)
			{
				$tabs -> add ( $label, $field -> setLocale($locale) -> render(), $field -> name () . '_' . $locale, $first );
				$first = false;
			}
			?>
			{!! $tabs->render() !!}
		@else
			<div>{!! $field->render() !!}</div>
		@endif
	</div>
</div>