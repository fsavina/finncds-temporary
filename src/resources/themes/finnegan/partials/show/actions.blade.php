@if(isset($actions['record']))
    @foreach ($actions['record'] as $action)
        {!! $action -> renderButton($model) !!}
    @endforeach
@endif
@can('index',$model)
{!! $presenter -> button() -> translateCaption('actions.back') -> icon('arrow-circle-left') -> secondary() -> url($model->actionUrl('index')) !!}
@endcan