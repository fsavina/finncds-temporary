<?php
$sorting = $model -> config('display.list.sorting', true);
if($sorting)
{
	$sortBy = $model -> getSortBy();
	$sortIcon = ($sortBy[1] == 'desc') ? 'actions.sort-desc' : 'actions.sort-asc';
}
?>
<tr>
	@if (count($model->bulkActions()))
	<th class="text-center" width="50">{!! $layout->form()->checkbox( null, null, null, ['style'=>'margin:0;vertical-align:middle', 'id'=>'bulk-select-all', 'autocomplete'=>'off']) !!}</th>
	@endif
	@if ($model -> config ( 'display.list.id', true ))
		<th class="text-center" width="50">
			@if($sorting)
			<a href="?sort={{$model->getKeyName()}}{{$sortBy[0] == $model->getKeyName() ? ( ( $sortBy[1] == 'asc' ) ? '&descasc=desc' : '' ) : ''}}">ID{!! (($sortBy[0] == $model->getKeyName()) ? ' ' . $presenter->icon($sortIcon) : '') !!}</a>
			@else
			ID
			@endif
		</th>
	@endif
	@foreach ($model -> fields() -> ifConfig ( 'list', true ) as $field)
		@if ($sorting and $field instanceof \Finnegan\Contracts\Fields\Sortable and $field -> isSortable ())
		<th><a href="?sort={{$field->name()}}{{$sortBy[0] == $field->name() ? ( ( $sortBy[1] == 'asc' ) ? '&descasc=desc' : '' ) : ''}}">{{$field->label()}}{!! (($sortBy[0] == $field->name()) ? ' ' . $presenter->icon($sortIcon) : '') !!}</a></th>
		@else
		<th>{{$field->label()}}</th>
		@endif
	@endforeach
	<th>&nbsp;</th>
</tr>