@if (count($bulkActions = $model -> bulkActions()))
	{!! $layout->form()->open(['url'=>$model->actionUrl('bulk'), 'method'=>'post', 'id'=>'bulk-form', 'data-model'=>$model->name()]) !!}
		<div class="row">
			<div class="small-2 columns">{!! $layout->form()->select($model->name().'_bulk[action]', $bulkActions, null, [
				'autocomplete' => 'off',
				'required' => 'required',
				'placeholder' => trans('finnegan::actions.choose_action')
			]) !!}</div>
			<div class="small-10 columns">{!! $presenter -> button() -> icon('truck') -> translateCaption('actions.apply') -> warning() -> confirm('finnegan::messages.bulk_apply_confirm') !!}</div>
		</div>
	{!! $layout->form()->close() !!}
@endif
