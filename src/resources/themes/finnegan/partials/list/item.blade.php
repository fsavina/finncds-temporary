<tr data-id="{{ $item->getKey() }}">
	@if (count($item->bulkActions()))
		<td class="text-center">{!! $layout->form()->checkbox( null, $item->getKey(), false, ['style'=>'margin:0;vertical-align:middle', 'class'=>'bulk-record', 'autocomplete'=>'off']) !!}</td>
	@endif
	@if ($item -> config ( 'display.list.id', true ))
		<td class="text-center">
			@can ('view',$item)
				{!! $presenter -> link($item->getKey(), $item->recordActionUrl('show')) !!}
			@else
				@can('edit',$item)
					{!! $presenter -> link($item->getKey(), $item->recordActionUrl('edit')) !!}
				@else
					{{$item->getKey()}}
				@endcan
			@endcan
		</td>
	@endif
	@foreach ($item -> fields() -> ifConfig ( 'list', true ) as $field)
		<td class="field-{{ $field->name()  }}">{!! $field->renderForList() !!}</td>
	@endforeach
	<td class="text-right actions">
		@if(isset($actions['record']))
			@foreach ($actions['record'] as $action)
				{!! $action -> renderButton($item, ['icon-only'=>true, 'basic'=>true]) !!}
			@endforeach
		@else
			&nbsp;
		@endif
	</td>
</tr>