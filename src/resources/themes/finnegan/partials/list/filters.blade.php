<?php
$filterBy = $model -> getFilterBy();
$hide = (is_array($filterBy) and count($filterBy)) ? '':'hide';

$filterElements = [];
if (isset($filters['text']) and count($filters['text']))
{
	$textFieldsLabels = [];
	foreach ($filters['text'] as $textFilter)
	{
		$textFieldsLabels[]=$textFilter->label();
	}
	$label = trans('finnegan::actions.text_search') . ' (' . implode(', ', $textFieldsLabels) . ')';
	$filterElements[]='<label>' . $label . ' ' . $layout->form()->input('search', 'search[_text]', isset($filterBy['_text'])?$filterBy['_text']:'') . '</label>';
}
foreach ($filters['*'] as $filter)
{
	if ($filter -> searchType())
	{
		$widget = $filter -> widget() -> setContext ( 'filters' );
		$filterElements[]='<label>'.$filter->label().' '.$widget->render().'</label>';
	}
}
?>
@if (count($filterElements))
	<?php $columnWidth = floor ( 12 / count ( $filterElements ) ); ?>
	{!! $layout->form()->open(['method'=>'get', 'id'=>'list-filters', 'class'=>$hide]) !!}
	<div class="callout">
		<h5>@lang('finnegan::actions.search_filters')</h5>
		
		<div class="row">
			@foreach ($filterElements as $element)
			<div class="small-{{$columnWidth}} columns">{!! $element !!}</div>
			@endforeach
		</div>
		<div class="row">
			<div class="small-12 columns text-right">
				{!! $presenter -> button('finnegan::actions.reset','?search=reset') -> small() -> warning() ->icon('actions.close') !!}
				{!! $presenter -> button('finnegan::actions.apply') -> small() -> icon('filter') !!}
			</div>
		</div>
	</div>
	{!! $layout->form()->close() !!}
@endif