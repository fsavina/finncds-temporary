@if ($paginator->hasPages())
    <ul class="pagination" role="navigation">
        <!-- Previous Page Link -->
        @if ($paginator->onFirstPage())
            <li class="pagination-previous disabled"><span>@lang('finnegan::general.previous')</span></li>
        @else
            <li class="pagination-previous"><a href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('finnegan::general.previous')</a></li>
            @endif

                    <!-- Pagination Elements -->
            @foreach ($elements as $element)
                    <!-- "Three Dots" Separator -->
            @if (is_string($element))
                <li><span>{{ $element }}</span></li>
                @endif

                        <!-- Array Of Links -->
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="current"><span>{{ $page }}</span></li>
                        @else
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                            @endif
                            @endforeach
                            @endif
                            @endforeach

                                    <!-- Next Page Link -->
                            @if ($paginator->hasMorePages())
                                <li class="pagination-next"><a href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('finnegan::general.next')</a></li>
                            @else
                                <li class="pagination-next disabled"><span>@lang('finnegan::general.next')</span></li>
                            @endif
    </ul>
@endif