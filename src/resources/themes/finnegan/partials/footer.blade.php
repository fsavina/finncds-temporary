@if ($credits = $module->config('layout.footer.credits', '&nbsp;'))
    <footer class="text-right">{!! $credits !!}</footer>
@endif