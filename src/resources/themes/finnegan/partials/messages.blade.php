@if (Session::has('message'))
	<div class="row columns">
		{!! $presenter -> message(Session::get('message'), 'primary small') !!}
	</div>
@endif