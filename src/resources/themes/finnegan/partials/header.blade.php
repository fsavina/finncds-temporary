<div class="top-bar" id="finn-top-bar">
	<div class="top-bar-left">
		<a class="hide-for-large" data-toggle="finn-main-sidebar">{!! $presenter->icon('bars')->x3() !!}</a>
		<a class="title hide-for-large" href="{{ $module->actionUrl('home') }}">
			{{ $module->title('FinneganCDS') }}
			@if($module->config('layout.sidebar.show_version', true))
				<small class="version">v{{ \Finnegan\Foundation\Http\Kernel::VERSION }}</small>
			@endif
		</a>
		{!! $presenter->button('finnegan::actions.view_site','/')->hollow()->target('_blank')->addClass('show-for-large')->icon('external-link') !!}
	</div>
	@if ( $module->isSecure () and Auth :: check () )
	<div class="top-bar-right">
		<ul class="menu dropdown" data-dropdown-menu>
			<li class="has-submenu">
				<a class="user">
					@if( with($user=Auth :: user ())->avatar )
						<img src="{{ $user->fields('avatar')->url(null,25,25) }}">
					@else
						{!! $presenter->icon('user')->x2() !!}
					@endif
					{{ $user }}
				</a>
				<ul class="menu vertical">
					<li>{!! $presenter->link('finnegan::auth.edit_profile', Auth::user()->recordActionUrl('edit'))->icon('edit') !!}</li>
					<li>
						{!! $layout->form()->open(['url'=>$module->actionUrl('logout')]) !!}
							{!! $presenter->button('finnegan::auth.logout')->icon('sign-out')->expanded()->small() !!}
						{!! $layout->form()->close() !!}
					</li>
				</ul>
			</li>
			@if($user->achieve('developer'))
				<li><a data-open="finn-tools-sidebar">{!! $presenter->icon('wrench') !!} Developer tools</a></li>
			@endif
		</ul>
	</div>
	@endif
</div>
