<div class="row">
    <div class="small-8 columns">
    @foreach($model->config('display.form.groups') as $group)
        <?php
        if (data_get($group,'sidebar')) continue;
        $active = data_get($group, 'active', false) ? 'is-active' : '';
        $fields = $model->fields(data_get($group,'fields',[]))->showInForm();
        ?>
        @if(count($fields))
        <div class="box {{ $active }}" id="{{$id = uniqid()}}" data-toggler=".is-active">
            <a class="title" data-toggle="{{$id}}">@lang(data_get($group, 'label'))</a>
            <div class="content">
                @foreach($fields as $field)
                    @include($theme->partial('form.widget'), ['widget'=>$field->widget()])
                @endforeach
            </div>
        </div>
        @endif
    @endforeach
    </div>
    <div class="small-4 columns">
        @foreach($model->config('display.form.groups') as $label => $group)
            <?php
            if (!data_get($group,'sidebar')) continue;
            $active = data_get($group, 'active', false) ? 'is-active' : '';
            $fields = $model->fields(data_get($group,'fields',[]))->showInForm();
            ?>
            @if(count($fields))
            <div class="box {{ $active }}" id="{{$id = uniqid()}}" data-toggler=".is-active">
                <a class="title" data-toggle="{{$id}}">@lang(data_get($group, 'label'))</a>
                <div class="content">
                    @foreach($model->fields(data_get($group,'fields',[])) as $field)
                        @include($theme->partial('form.widget'), ['widget'=>$field->widget()])
                    @endforeach
                </div>
            </div>
            @endif
        @endforeach
    </div>
</div>