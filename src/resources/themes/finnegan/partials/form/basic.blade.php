<div class="row columns" id="basic-form">
    @foreach ($model -> formFields() as $field)
        @include($theme->partial('form.widget'), ['widget'=>$field->widget()])
    @endforeach
</div>