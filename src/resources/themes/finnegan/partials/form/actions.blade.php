@if ($module->isSecure() and $model -> definition() -> hasComponent ( 'status' ) and Auth::check())
    @if ( Auth::user()->achieve('author'))
        @if (!$model -> isPublished())
            {!! $presenter -> button() -> action( 'publish') -> success() -> name('save-action') -> value('publish') -> icon('actions.save') !!}
        @endif
    @else
        {!! $presenter -> button() -> action( $model -> isPendingReview() ? 'update' : 'send_to_review') -> warning() -> name('save-action') -> value('send_to_review') -> icon('actions.save') !!}
    @endif

    @if(!$model->exists)
        {!! $presenter -> button() -> action('save_as_draft') -> info() -> name('save-action') -> value('save_as_draft') -> icon('actions.save') !!}
    @else
        {!! $presenter -> button() -> action('save') !!}
    @endif
@else
    {!! $presenter -> button() -> action('save') -> success() !!}
@endif

@if(!isset($backUrl))
    @can ('index',$model)
        <?php $backUrl=$model->actionUrl('index'); ?>
    @else
        @can ('show',$model)
           <?php $backUrl=$model->recordActionUrl('show'); ?>
        @endcan
    @endcan
@endif

@if(isset($backUrl))
    {!! $presenter -> button() -> translateCaption('actions.cancel') -> secondary() -> icon('actions.close') -> url($backUrl) !!}
@endif