<a href="{{ $module->actionUrl ( 'home' ) }}" class="logo title">
    {!! $presenter -> icon( $module->config ('module.icon', 'logo') )->lg() !!}
    {{ $module->title('FinneganCDS') }}
    @if($module->config('layout.sidebar.show_version', true))
        <small class="version">v{{ \Finnegan\Foundation\Http\Kernel::VERSION }}</small>
    @endif
</a>
@if (!$module->isSecure() or Auth::check())
    {!! $presenter -> menu( $layout->menu('primary') )->accordion() !!}
@endif