
@lang('finnegan::auth.click_here_to_reset_your_password'): {{ $module->actionUrl('password_reset', ['token'=>$token]) }}