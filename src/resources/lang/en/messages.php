<?php

return [
	
	
	'list_empty'                     => 'The list is empty',
	'store_completed'                => 'The object was created correctly.',
	'update_completed'               => 'The object was updated correctly.',
	'publish_completed'              => 'The object was published correctly.',
	'save_as_draft_completed'        => 'The object was saved correctly.',
	'send_to_review_completed'       => 'The object was sent correctly.',
	'delete_confirm'                 => 'Permanently delete this element?',
	'delete_completed'               => 'The object was deleted permanently.|The objects were deleted permanently.',
	'truncate_confirm'               => 'Permanently delete all the elements?',
	'truncate_completed'             => 'All the objects were deleted permanently.',
	'relation_missing'               => 'The required relation is missing.',
	'relation_attach_completed'      => 'The relation was created correctly.',
	'relation_detach_completed'      => 'The relation was deleted permanently.',
	'relation_edit_completed'        => 'The relation was updated correctly.',
	'refresh_translations_completed' => 'The translations cache has been refreshed.',
	'bulk_apply_confirm'             => 'Apply the action to the selected records?'

];