<?php

return [
	'roles' => [
		'developer'   => 'Developer',
		'superadmin'  => 'Superadmin',
		'admin'       => 'Admin',
		'editor'      => 'Editor',
		'author'      => 'Author',
		'contributor' => 'Contributor',
		'subscriber'  => 'Subscriber',
	],

	'login'        => 'Login',
	'logout'       => 'Logout',
	'edit_profile' => 'Edit profile',

	'remember_me'              => 'Remember Me',
	'forgot_your_password'     => 'Forgot your password?',
	'back_to_login_page'       => 'Back to login page',
	'send_password_reset_link' => 'Send password reset link',

	'click_here_to_reset_your_password' => 'Click here to reset your password',
	'reset_password'                    => 'Reset password'
];