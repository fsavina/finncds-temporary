<?php

return [
	'no'  => 'No',
	'yes' => 'Yes',

	'ok' => 'Ok',
	
	'from' => 'From',
	'to'   => 'To',
	
	'previous' => 'Previous',
	'next'     => 'Next',
	
	'status'         => 'Status',
	'draft'          => 'Draft',
	'pending_review' => 'Pending',
	'published'      => 'Published',
	'trashed'        => 'Trashed',

	'author'         => 'Author',
	'category'       => 'Category',
	'required'       => 'Required',
	'content'        => 'Content',
	'configuration'  => 'Configuration',
	'categorization' => 'Categorization',
	'settings'       => 'Settings',
	'edit_settings'  => 'Edit settings',

	'general'  => 'General',
	'messages' => 'Messages',
	'actions'  => 'Actions',
	'auth'     => 'Auth',
];