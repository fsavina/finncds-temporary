<?php

return [
	
	
	'list_empty'                     => 'La lista è vuota',
	'store_completed'                => 'L\'oggetto è stato creato correttamente.',
	'update_completed'               => 'L\'oggetto è stato aggiornato correttamente.',
	'publish_completed'              => 'L\'oggetto è stato pubblicato correttamente.',
	'save_as_draft_completed'        => 'L\'oggetto è stato salvato correttamente.',
	'send_to_review_completed'       => 'L\'oggetto è stato inviato correttamente.',
	'delete_confirm'                 => 'Vuoi cancellare questo elemento permanentemente?',
	'delete_completed'               => 'L\'oggetto è stato cancellato permanentemente.|Gli oggetti sono stati cancellati permanentemente.',
	'truncate_confirm'               => 'Vuoi cancellare tutti gli elementi permanentemente?',
	'truncate_completed'             => 'Tutti gli oggetti sono stati cancellati permanentemente.',
	'relation_missing'               => 'Impossibile trovare la relazione richiesta.',
	'relation_attach_completed'      => 'La relazione è stata creata correttamente.',
	'relation_detach_completed'      => 'La relazione è stata cancellata permanentemente.',
	'relation_edit_completed'        => 'La relazione è stata aggiornata correttamente.',
	'refresh_translations_completed' => 'La cache delle traduzione è stata aggiornata.',
	'bulk_apply_confirm'             => 'Applicare l\'azione ai record selezionati?'

];