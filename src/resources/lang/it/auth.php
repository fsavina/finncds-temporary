<?php

return [
	'roles' => [
		'developer'   => 'Developer',
		'superadmin'  => 'Superadmin',
		'admin'       => 'Admin',
		'editor'      => 'Editor',
		'author'      => 'Author',
		'contributor' => 'Contributor',
		'subscriber'  => 'Subscriber',
	],

	'login'        => 'Login',
	'logout'       => 'Logout',
	'edit_profile' => 'Modifica profilo',

	'remember_me'              => 'Ricordami',
	'forgot_your_password'     => 'Password dimenticata?',
	'back_to_login_page'       => 'Torna alla pagina di login',
	'send_password_reset_link' => 'Invia di link di reset password',

	'click_here_to_reset_your_password' => 'Clicca qui per resettare la tua password',
	'reset_password'                    => 'Resetta password'
];