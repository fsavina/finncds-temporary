<?php

return [
	'no'  => 'No',
	'yes' => 'Si',

	'ok' => 'Ok',
	
	'from' => 'Da',
	'to'   => 'A',
	
	'previous' => 'Indietro',
	'next'     => 'Avanti',
	
	'status'         => 'Status',
	'draft'          => 'Bozza',
	'pending_review' => 'In attesa',
	'published'      => 'Pubblicato',
	'trashed'        => 'Cestinato',

	'author'         => 'Autore',
	'category'       => 'Categoria',
	'required'       => 'Obbligatorio',
	'content'        => 'Contenuto',
	'configuration'  => 'Configurazione',
	'categorization' => 'Categorizzazione',
	'settings'       => 'Configurazione',
	'edit_settings'  => 'Modifica configurazione',

	'general'  => 'Generale',
	'messages' => 'Messaggi',
	'actions'  => 'Azioni',
	'auth'     => 'Autenticazione',
];