<?php
namespace Finnegan\Tests;


use Carbon\Carbon;
use Finnegan\Contracts\Fields\Searchable;
use Illuminate\Support\Facades\App;


class FieldTest extends TestCase
{


	public function setUp ()
	{
		parent :: setUp ();
			$this -> beginDatabaseTransaction();
	}


	protected function loadModel ()
	{
		return $this->modelsFactory ()->load ( $this->loadTestDefinition () );
	}


	protected function loadFieldInstance ( $name, $model = null )
	{
		$model = $model ?: $this->loadModel();
		return $model->fields ( $name );
	}


	public function testBasic ()
	{
		$name = 'basic';
		$model = $this->loadModel ();
		$field = $this->loadFieldInstance ( $name, $model );
		
		// general
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Field', $field );
		$this->assertSame ( $model, $field->model () );
		$this->assertEquals ( $name, $field->name () );
		$this->assertInternalType ( 'boolean', $field->isSortable () );
		$this->assertInternalType ( 'boolean', $field->authorize () );

		// widget
		$this->assertInternalType ( 'boolean', $field->showInForm () );
		$this->assertInstanceOf ( 'Finnegan\Widgets\Widget', $field->widget () );
	}


	public function testInternalValue ()
	{
		$value = 'foobar';
		$field = $this->loadFieldInstance ( 'basic' );

		$field->setValue ( $value );
		$this->assertEquals ( $value, $field->value () );
		$this->assertEquals ( $value, $field->filter ( $value ) );
		$this->assertEquals ( $value, $field->filterRaw ( $value ) );
		$this->assertInternalType ( 'boolean', $field->isDirty () );
	}


	public function testRendering ()
	{
		$field = $this->loadFieldInstance ( 'basic' );

		$this->assertEquals ( 'Basic', $field->label () );
		$this->assertInternalType ( 'boolean', $field->showInPreview () );

		$this->assertInternalType ( 'string', $field->render () );
		$this->assertEquals ( $field->render (), $field->__toString () );
		$this->assertEquals ( $field->render (), $field->renderForList () );
		$this->assertEquals ( $field->value (), $field->renderForForm () );
	}


	public function testValidation ()
	{
		$field = $this->loadFieldInstance ( 'basic' );
		$this->assertInternalType ( 'array', $field->rules () );
	}


	public function testErrors ()
	{
		$field = $this->loadFieldInstance ( 'basic' );
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Field', $field->setErrors ( [ ] ) );
		$this->assertInternalType ( 'boolean', $field->hasErrors () );
		$this->assertInternalType ( 'array', $field->errors () );
	}


	public function testTranslatable ()
	{
		$field = $this -> loadFieldInstance ( 'basic' );
		$this -> assertInternalType ( 'boolean', $field -> isTranslatable () );

		$this -> assertEquals ( App::getLocale(), $field -> locale () );

		$this -> assertInstanceOf ( 'Finnegan\Models\Fields\Field', $field -> setLocale('it') );
		$this -> assertEquals ( 'it', $field -> locale () );
	}


	public function testString ()
	{
		$field = $this -> loadFieldInstance ( 'string' );
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Literal', $field );
		$this -> assertEquals ( 100, $field -> size () );
		$this -> assertEquals ( 'foobar', $field -> filter ( '<i>foobar</i>' ) );

		$this->assertInternalType ( 'string', $field->render () );
		$this->assertEquals ( Searchable :: TEXT_SEARCH_TYPE, $field->searchType () );
		$this->assertInternalType ( 'boolean', $field->confirm () );
		$this -> assertInstanceOf ( 'Finnegan\Widgets\Input', $field -> widget () );
	}


	public function testUrl ()
	{
		$field = $this -> loadFieldInstance ( 'url' );
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Url', $field );
		$this -> assertInArray ( 'url', $field -> rules () );

		$this -> assertStringStartsWith('<a href=',$field->setValue('http://www.foobar.com')->render());
	}


	public function testEmail ()
	{
		$field = $this -> loadFieldInstance ( 'email' );
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Email', $field );
		$this -> assertInArray ( 'email', $field -> rules () );

		$this -> assertContains('mailto:',$field->setValue('foo@bar.com')->render());
	}


	public function testPassword ()
	{
		$field = $this -> loadFieldInstance ( 'password' );
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Password', $field );

		$this -> assertInternalType ( 'array', $field -> rules () );
		$this -> assertInternalType ( 'boolean', $field -> isRequired() );
		$this -> assertInternalType ( 'string', $field -> filter ('foobar') );

		$this -> assertInternalType ( 'string', $field -> setValue('foobar') -> render () );
		$this -> assertEquals('', $field->renderForForm());

		$this -> assertFalse($field->searchType());
		$this -> assertFalse($field->isSortable());
	}


	public function testBoolean ()
	{
		$field = $this -> loadFieldInstance ( 'boolean' );
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Boolean', $field );
		$this -> assertInArray ( 'boolean', $field -> rules () );
		$this -> assertInternalType ( 'array', $field -> options () );

		$this->assertInternalType ( 'string', $field->render () );
		$this->assertInternalType ( 'string', $field-> searchType() );
		$this->assertInternalType ( 'boolean', $field->isRequired () );
	}


	public function testNumeric ()
	{
		$field = $this -> loadFieldInstance ( 'numeric' );
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Numeric', $field );
		$this -> assertInArray ( 'numeric', $field -> rules () );
		$this -> assertInArray ( $field -> numberType (), [ 'integer', 'decimal' ] );

		$this->assertInternalType ( 'string', $field-> searchType() );
	}


	public function testJson ()
	{
		$field = $this -> loadFieldInstance ( 'json' );
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Json', $field );

		$this -> assertInternalType ( 'array', $field -> filter ('{"foo":"bar"}') );

		$field->setValue(['foo'=>'bar']);
		$this->assertInternalType ( 'string', $field->render () );
		$this->assertInternalType ( 'string', $field->renderForList () );
		$this->assertInternalType ( 'string', $field->renderForForm () );
	}


	public function testText ()
	{
		$field = $this -> loadFieldInstance ( 'text' );
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Text', $field );

		$field->setValue('<i>foobar</i>');
		$this->assertEquals ( '<i>foobar</i>', $field->filter('<i>foobar</i>') );
		$this->assertInternalType ( 'string', $field->render () );
		$this->assertInternalType ( 'string', $field->renderForList () );
	}


	public function testFile ()
	{
		$field = $this -> loadFieldInstance ( 'file' );
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\File', $field );

		$this -> assertFalse($field->searchType());
		$this -> assertFalse($field->isSortable());

		$uploadedFile = \Mockery::mock ( 'Symfony\Component\HttpFoundation\File\UploadedFile' );
		$uploadedFile->shouldReceive ( 'isValid' )->andReturn ( true )->once ();
		$uploadedFile->shouldReceive ( 'getClientOriginalName' )->andReturn ( 'foobar.pdf' )->once ();
		$uploadedFile->shouldReceive ( 'move' )->with ( $field->uploadPath (), 'foobar.pdf' )->once ();
		$this->assertEquals ( 'foobar.pdf', $field->filter ( $uploadedFile ) );

		$field -> setValue('foobar.pdf');
		$this->assertInternalType ( 'string', $field->render () );
		$this->assertInternalType ( 'string', $field->url () );
		$this->assertInternalType ( 'string', $field->uploadPath () );
		$this->assertInternalType ( 'array', $field->rules () );
	}


	public function testImage ()
	{
		$field = $this -> loadFieldInstance ( 'image' );
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Image', $field );

		$this->assertInternalType ( 'array', $field->rules () );
		$this->assertInArray ( 'image', $field->rules () );

		$field -> setValue('foobar.jpg');
		$this->assertInternalType ( 'string', $field->render () );
		$this->assertInternalType ( 'string', $field->renderForList () );
		$this->assertContains ( '<img', $field->render () );
		$this->assertInternalType ( 'string', $field->url () );
	}


	public function testSet ()
	{
		$field = $this -> loadFieldInstance ( 'set' );
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Set', $field );

		$this->assertInternalType ( 'array', $field->options () );
		$this->assertInternalType ( 'array', $field->rules () );
		$this->assertInternalType ( 'boolean', $field->isSortable () );
		$this->assertEquals ( 'Set', $field->searchType () );

		$this->assertEquals ( 1, $field->limit () );
		$this->assertInternalType ( 'array', $field->runSuggest (['q'=>'fo']) );

		$this->assertInternalType ( 'string', $field->setValue('foo')->render() );
	}


	public function testSetModelMultiple ()
	{
		$field = $this -> loadFieldInstance ( 'model' );

		$this->assertTrue($field->authorize());
		$this->assertInstanceOf ( 'Finnegan\Models\Model', $field->externalModel() );
		$this->assertInternalType ( 'array', $field->options () );
		$this->assertInstanceOf ( 'Illuminate\Database\Eloquent\Relations\BelongsTo', $field->related() );

		$this->assertInArray ( 'array', $field->rules () );
		$this->assertFalse ( $field->searchType () );
		$this->assertFalse ( $field->isSortable () );
		$this->assertNull ( $field->limit () );
		$this->assertEquals ( ['foo','bar'], $field->filterRaw('foo,bar') );
	}


	public function testDate ()
	{
		$field = $this->loadFieldInstance ( 'date' );
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Date', $field );

		$this->assertInternalType ( 'array', $field->rules () );
		$this->assertEquals ( 'Date', $field->searchType () );

		$this->assertInternalType ( 'string', $field->mode () );
		$this->assertInternalType ( 'string', $field->format (Carbon::now()) );
		$this->assertInternalType ( 'string', $field->getFormFormat () );
		$this->assertInstanceOf ( 'Carbon\Carbon', $field->filter ('today') );

		$this -> assertTrue($field->dateMode());
		$this -> assertFalse($field->timeMode());

		$field -> setValue(Carbon::now());
		$this->assertInternalType ( 'string', $field->render () );
		$this->assertInternalType ( 'string', $field->renderForForm () );
	}


	public function testRelation ()
	{
		$field = $this->loadFieldInstance ( 'relation' );
		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Relation', $field );

		$this->assertInternalType ( 'array', $field->options () );
		$this->assertInternalType ( 'array', $field->runSuggest (['q'=>'fo']) );

		$this->assertEquals ( 'Autocomplete', $field->searchType () );
		$this->assertInternalType ( 'array', $field->rules () );
		$this->assertInArray ( 'array', $field->rules () );

		$this->assertEquals ( [], $field->filterRaw('foo,bar') );
	}
	
	
	public function testSlug ()
	{
		$source = $this -> loadFieldInstance ( 'string' );
		$source -> setValue('Foo Bar');
		
		$field = $this -> loadFieldInstance ( 'slug', $source->model() );
		$field -> onSaving ();
		$this -> assertEquals ( 'foo-bar', $field -> value () );

		$this -> assertFalse($field->showInForm());
		$field->model ()->exists = true;
		$this -> assertTrue($field->showInForm());
	}
	
	

}



