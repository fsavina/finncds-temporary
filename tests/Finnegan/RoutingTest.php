<?php
namespace Finnegan\Tests;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


class RoutingTest extends TestCase
{


	protected $user;


	public function setUp ()
	{
		parent::setUp ();
		$this->user = $this->createTestUser ();
	}


	public function testHomeRoute ()
	{
		$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'home' ) ) );
	}


	public function testCrudRoutes ()
	{
		if ( $this->module->config ( 'module.crud' ) )
		{
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'index' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'show' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'create' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'store' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'edit' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'update' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'destroy' ) ) );

			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'truncate' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'bulk' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'field' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'copy' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'sort' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'translate' ) ) );

			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'relationAttach' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'relationCreate' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'relationDetach' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'relationEdit' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'relationUpdate' ) ) );
		}
	}


	public function testAuthRoutes ()
	{
		if ( $this->module->isSecure () )
		{
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'login' ) ) );
			$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'logout' ) ) );

			if ( $this->module->config ( 'auth.password.reset' ) )
			{
				$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'password_email' ) ) );
				$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'password_reset' ) ) );
				$this->assertTrue ( Route::has ( $this->module->wrapRoute ( 'password_reset_post' ) ) );
			}
		}
	}


	public function testRedirectToLogin ()
	{
		if ( $this->module->isSecure () )
		{
			$this->get ( $this->module->actionUrl ( 'home' ) );
			$this->assertRedirectedTo ( $this->module->actionUrl ( 'login' ) );
		}
	}


	public function testModuleHomeIsOk ()
	{
		if ( $this->module->isSecure () )
		{
			if ( Auth::login($this->user) )
			{
				$this->visit ( $this->module->actionUrl ( 'home' ) )->assertResponseOk ();

				$view = app ( 'Finnegan\Contracts\Layout\Manager' )->resolveView ( 'home' );
				$this->assertEquals ( $view, $this->response->getOriginalContent ()->name () );

				$this->get ( $this->module->actionUrl ( 'login' ) )
					 ->assertRedirectedTo ( $this->module->actionUrl ( 'home' ) );

				Auth::logout ();
			}
		} else
		{
			$this->visit ( $this->module->actionUrl ( 'home' ) )->assertResponseOk ();
		}
	}


	public function testLogoutRedirect ()
	{
		if ( Auth::login($this->user) )
		{
			$this->get ( $this->module->actionUrl ( 'logout' ) );
			$this->assertRedirectedTo ( $this->module->actionUrl ( 'login' ) );
		}
	}
}



