<?php
namespace Finnegan\Tests;


use Finnegan\Definitions\Definition;
use Finnegan\Definitions\DefinitionsFactory;
use Finnegan\Definitions\LoadingContext;
use Finnegan\Definitions\ModelDefinition;


class DefinitionTest extends TestCase
{

	
	/**
	 * @var \Finnegan\Definitions\ModelDefinition
	 */
	protected $definition;


	public function setUp ()
	{
		parent :: setUp ();
		$this -> definition = $this -> loadTestDefinition ();
	}
	
	
	public function testFactory ()
	{
		$factory = $this->definitionsFactory ();
		$this->assertInstanceOf ( DefinitionsFactory::class, $factory );
		
		$this->assertInstanceOf ( Definition::class, $factory->unguarded ( $this->getTestDefinitionPath () ) );

		$this->assertInstanceOf ( Definition::class, $factory->forceLoad ( $this->getTestDefinitionPath () ) );

		$this->assertInstanceOf ( Definition::class, $factory->make ( 'test_name', [ ] ) );
	}


	public function testLoading ()
	{
		$this -> assertInstanceOf ( Definition::class, $this -> definition );
		$this -> assertInstanceOf ( ModelDefinition::class, $this -> definition );
	}

	
	public function testBasicDefinition ()
	{
		$this -> assertEquals ( static :: TEST_DEFINITION, $this -> definition -> name () );

		$this -> assertInstanceOf ( Definition::class, $this -> definition -> validate ($this->definitionsFactory ()) );
		$this -> assertInternalType ( 'boolean', $this -> definition -> authorize ($this->definitionsFactory ()->auth()) );
		$this -> assertInstanceOf ( Definition::class, $this->definition->setContext ( new LoadingContext([
			 'unguarded'=>true
		]) ) );
		$this -> assertTrue ( $this -> definition -> isUnguarded () );
		
		$this -> assertInternalType ( 'array', $this -> definition -> toArray () );
		$this -> assertInternalType ( 'array', $this -> definition -> jsonSerialize () );
		$this -> assertInternalType ( 'string', $this -> definition -> toJson () );

		$this -> assertInternalType ( 'array', $this -> definition -> get ( 'fields' ) );
		$this -> assertEquals ( 'foobar', $this -> definition -> get ( 'missing.key', 'foobar' ) );
	}
	
	
	public function testModelDefinition ()
	{
		// table
		$this -> assertInternalType ( 'string', $this -> definition -> table () );
		$this -> assertEquals ( 'test_table', $this -> definition -> get ( 'storage.table' ) );

		$this -> assertEquals ( 'finn_foobar', $this -> definition -> wrapTableName ( 'foobar' ) );
		$this -> assertEquals ( 'finn_test_definition_translations', $this -> definition -> translationsTable () );

		// fields
		$this -> assertEquals ( 'id', $this -> definition -> primaryKey ( 'id' ) );
		$this -> assertInternalType ( 'array', $this -> definition -> fields () );
		$this -> assertFalse ( $this -> definition -> hasTranslatableFields () );
		$this -> assertNull ( $this -> definition -> getCustomModel () );

		// components
		$this -> assertTrue ( $this -> definition -> hasComponent ( 'status' ) );
		$this -> assertInternalType ( 'boolean', $this -> definition -> sortable () );
		$this -> assertFalse ( $this -> definition -> sortablePriorityField () );

		// relations
		$this -> assertEquals ( 'finn_foobar_test_definition', $this -> definition -> relationPivotTable ( 'foobar' ) );

		// display
		$this -> assertEquals ( 'foobar', $this -> definition -> icon ( 'foobar' ) );
		$this -> assertEquals ( 'Test Definition', $this -> definition -> singular () );
		$this -> assertEquals ( 'Test Definitions', $this -> definition -> plural () );
	}
	
}

