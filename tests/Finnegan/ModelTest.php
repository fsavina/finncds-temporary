<?php
namespace Finnegan\Tests;


use Finnegan\Models\Model;


class ModelTest extends TestCase
{

	/**
	 * @var Model
	 */
	protected $model;


	public function setUp ()
	{
		parent:: setUp ();
		$this->model = $this->modelsFactory ()->load ( $this->loadTestDefinition () );
	}


	public function testConstants ()
	{
		$this->assertEquals ( 0, Model::STATUS_DRAFT );
		$this->assertEquals ( 1, Model::STATUS_PENDING_REVIEW );
		$this->assertEquals ( 2, Model::STATUS_PUBLISHED );

		$this->assertEquals ( 'save_as_draft', Model::ACTION_DRAFT );
		$this->assertEquals ( 'send_to_review', Model::ACTION_REVIEW );
		$this->assertEquals ( 'publish', Model::ACTION_PUBLISH );
	}


	public function testBasicFeatures ()
	{
		$this->assertInstanceOf ( 'Finnegan\Models\Model', $this->model );

		$this->assertInstanceOf ( 'Finnegan\Contracts\Models\Fields', $this->model );
		$this->assertInstanceOf ( 'Finnegan\Contracts\Models\Formable', $this->model );
		$this->assertInstanceOf ( 'Finnegan\Contracts\Models\Migratable', $this->model );
		$this->assertInstanceOf ( 'Finnegan\Contracts\Models\Titlable', $this->model );

		$this->assertInstanceOf ( 'Finnegan\Models\FieldCollection', $this->model->fields () );

		$this->assertInternalType ( 'string', $this->model->__toString () );
		$this->assertInternalType ( 'array', $this->model->getSortBy () );

		$this->assertInternalType ( 'boolean', $this->model->timestamps () );

		$this->assertInstanceOf ( 'Finnegan\Models\Model', $this->model->newInstance () );
	}


	public function testDefinitionTrait ()
	{
		$this->assertInstanceOf ( 'Finnegan\Definitions\ModelDefinition', $this->model->definition () );
		$this->assertEquals ( TestCase :: TEST_DEFINITION, $this->model->name () );

		$this->assertInternalType ( 'array', $this->model->config ( 'fields' ) );
	}
	
	
	public function testActionsTrait ()
	{
		$this->assertInternalType ( 'string', $this->model->actionUrl ( 'index' ) );
		$this->assertInternalType ( 'string', $this->model->recordActionUrl ( 'show' ) );

		$this->assertInternalType ( 'array', $this->model->bulkActions () );
	}


	public function testFormTrait ()
	{
		$this->assertInternalType ( 'array', $this->model->formConfig () );
		$this->assertInstanceOf ( 'Illuminate\Validation\Validator', $this->model->validator () );
		$this->assertInternalType ( 'boolean', $this->model->fromForm ( [ ] ) );
	}


	public function testFieldsTrait ()
	{
		$subset = [ 'basic', 'string' ];
		$fields = $this->model->fields ();

		$this->assertInstanceOf ( 'Finnegan\Models\Fields\Field', $this->model->fields ( $subset[ 0 ] ) );
		$this->assertEquals ( $fields->subset ( $subset ), $this->model->fields ( $subset ) );

		$this->assertTrue ( $this->model->hasField ( $subset[ 0 ] ) );
		$this->assertFalse ( $this->model->hasField ( 'foobar' ) );

		$this->assertInstanceOf ( 'Finnegan\Models\FieldCollection', $this->model->formFields () );

		$this->assertInternalType ( 'boolean', $this->model->isPublished () );
		$this->assertInternalType ( 'boolean', $this->model->isPendingReview () );
	}


	public function testFiltersTrait ()
	{
		$this->assertInternalType ( 'array', $this->model->getFilterBy () );
		$this->assertInstanceOf ( 'Finnegan\Models\FieldCollection', $this->model->filters () );
		$this->assertInstanceOf ( 'Finnegan\Models\FieldCollection', $this->model->textFilters () );
		$this->assertInstanceOf ( 'Finnegan\Models\FieldCollection', $this->model->otherFilters () );

		$this->assertInternalType ( 'boolean', $this->model->hasTextSearch () );
	}


	public function testRelationsTrait ()
	{
		$this->assertEquals ( TestCase :: TEST_DEFINITION . '_id', $this->model->getForeignKey () );
		$this->assertEquals ( 'finn_foobar_test_definition', $this->model->joiningTable ('foobar') );

		$this->assertInternalType ( 'string', $this->model->getMorphClass () );
	}


}


