<?php
namespace Finnegan\Tests;


use Finnegan\Auth\Roles\Role;
use Finnegan\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;


class AuthTest extends TestCase
{
	
	protected $user;


	public function setUp ()
	{
		parent :: setUp();
		$this->user = $this -> createTestUser();
	}
	
	
	public function testRoles ()
	{
		$role = Role :: make ( 'author' );

		$this -> assertInternalType ( 'array', User :: roles () );
		$this -> assertInstanceOf ( 'Finnegan\Auth\Roles\Role', $role );
		
		$this -> assertTrue ( $role -> is ( 'author' ) );
		$this -> assertTrue ( $role -> achieve ( 'contributor' ) );
		$this -> assertFalse ( $role -> achieve ( 'admin' ) );
	}


	public function testAuthentication ()
	{
		Auth::login($this->user);

		$user = Auth:: user ();
		$role = $user->role ();
		$this->assertInstanceOf ( 'Finnegan\Foundation\Auth\User', $user );
		$this->assertInstanceOf ( 'Finnegan\Auth\Roles\Role', $role );
		$this->assertEquals ( $user->achieve ( 'admin' ), $role ->achieve ( 'admin' ) );
		$this->assertTrue ( $user->achieve ( 'admin' ) );

		$this->assertTrue ( $role->is ( 'admin' ) );
		$this->assertTrue ( $role->achieve ( 'author' ) );
		$this->assertFalse ( $role->achieve ( 'developer' ) );

		Auth :: logout ();
	}

}



