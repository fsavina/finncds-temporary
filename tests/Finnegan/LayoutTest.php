<?php
namespace Finnegan\Tests;


use Finnegan\Contracts\Layout\Manager;


class LayoutTest extends TestCase
{
	

	/**
	 * @var Manager
	 */
	protected $manager;


	public function setUp ()
	{
		parent:: setUp ();
		$this->manager = app ( 'Finnegan\Contracts\Layout\Manager' );
	}
	
	
	public function testManager ()
	{
		$this->assertInstanceOf ( 'Finnegan\Contracts\Layout\Manager', $this->manager );

		$this->assertInstanceOf ( 'Finnegan\Contracts\Layout\Presenter', $this->manager->presenter () );
		$this->assertInstanceOf ( 'Finnegan\Contracts\Layout\AssetManager', $this->manager->assetManager () );

		$this->assertInternalType ( 'string', $this->manager->theme () );
		$this->assertTrue ( $this->manager->viewExists ( 'home' ) );

		$this->assertInternalType ( 'string', $this->manager->resolveView ( 'home' ) );
		$this->assertInternalType ( 'string', $this->manager->resolvePartial ( 'list.item' ) );
		$this->assertInternalType ( 'string', $this->manager->resolveLayout ( 'default' ) );

		$this->assertInstanceOf ( 'Finnegan\Layout\Menus\Menu', $this->manager->menu () );
	}


	public function testPresenter ()
	{
		$presenter = $this->manager->presenter ();

		$this->assertInternalType ( 'string', $presenter->menu($this->manager->menu ())->render() );

		$components = [
			$presenter->message ( 'foobar' ),
			$presenter->link ( 'foobar' ),
			$presenter->inputGroup ( $this->manager->form()->input ( 'text', 'foobar' ) ),
			$presenter->tabs (),
			$presenter->accordion (),
			$presenter->modal ( 'foobar' )
		];

		foreach ( $components as $component )
		{
			$render = $component->render ();
			$this->assertInternalType ( 'string', is_object ( $render ) ? $render->__toString () : $render );
		}
	}


	public function testAssetManager ()
	{
		$assetManager = $this->manager->assetManager ();

		$this->assertInstanceOf ( 'Finnegan\Contracts\Layout\AssetManager', $assetManager->requireResource ( 'finnegan' ) );

		$render = $assetManager->render ();
		$this->assertInternalType ( 'string', $render );
		$this->assertContains ( 'finnegan.js', $render );
	}


	/**
	 * @expectedException \Exception
	 */
	public function testMissingLibrary ()
	{
		$this->manager->assetManager ()->requireResource ( 'foobar' );
	}

}



