<?php
namespace Finnegan\Tests;


class ModuleTest extends TestCase
{


	public function testLoading ()
	{
		$this -> assertInstanceOf ( 'Finnegan\Modules\Module', $this -> module );
		$this -> assertInstanceOf ( 'Finnegan\Contracts\Modules\Module', $this -> module );
	}


	public function testConfig ()
	{
		$this -> assertEquals ( $this -> moduleName, $this -> module -> name () );
		$this -> assertEquals ( "{$this -> moduleName}_foobar", $this -> module -> wrap ( 'foobar', '_' ) );
		
		$this -> assertInternalType ( 'array', $this -> module -> config ( 'module' ) );
		$this -> assertInternalType ( 'string', $this -> module -> title () );
		$this -> assertEquals ( 'FooBar', $this -> module -> title ( 'FooBar' ) );

		$this -> assertInternalType ( 'boolean', $this -> module -> isSecure () );

		$this -> assertInternalType ( 'string', $this -> module -> actionUrl ( 'home' ) );

		$this -> assertTrue ( $this -> module -> allow ( 'foobar' ) );
		$this -> assertFalse ( $this -> module -> deny ( 'foobar' ) );
	}

}


