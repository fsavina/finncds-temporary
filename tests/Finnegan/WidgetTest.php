<?php
namespace Finnegan\Tests;


use Finnegan\Support\BaseServiceProvider;
use Finnegan\Widgets\Widget;
use Symfony\Component\Finder\Finder;


class WidgetTest extends TestCase
{


	public function testBasicWidget ()
	{
		$name = 'foobar';
		$widget = Widget::make ( 'input', $name, [ ] );
		
		$this->assertInstanceOf ( 'Finnegan\Widgets\Widget', $widget );

		$this->assertEquals ( $name, $widget->name () );
		$this->assertInternalType ( 'array', $widget->containerClasses () );

		$this->assertInstanceOf ( 'Finnegan\Widgets\Widget', $widget->setContext ( Widget::CONTEXT_FILTERS ) );
		$this->assertEquals ( Widget::CONTEXT_FILTERS, $widget->context () );
		$this->assertEquals ( "search[{$name}]", $widget->name () );

		$this->assertEquals ( $widget->render (), $widget->toHtml () );

		$this->assertNull ( $widget->field () );
	}


	public function testAllWidgetTypes ()
	{
		$path = BaseServiceProvider::sourcePath () . 'Finnegan/Widgets';

		$files = Finder::create ()->files ()->in ( $path )->name ( '*.php' )->notName ( 'Widget.php' )->depth ( 0 );
		foreach ( $files as $file )
		{
			$type = $file->getBasename ( '.php' );

			$widget = Widget::make ( $type, 'foobar', [ ] );

			$this->assertInstanceOf ( "Finnegan\\Widgets\\{$type}", $widget );
			$this->assertInternalType ( 'string', $widget->renderFormElement ()->render () );
			$this->assertInternalType ( 'string', $widget->renderJs () );
		}
	}


	public function testFieldWidget ()
	{
		$model = $this -> modelsFactory()->load($this->loadTestDefinition());
		foreach ( $model->fields() as $field )
		{
			$widget = $field->widget();
			$this->assertInstanceOf ( 'Finnegan\Widgets\Widget', $widget );
			$this -> assertEquals($field, $widget->field());
		}
	}

}


