<?php
namespace Finnegan\Tests;


use Finnegan\Contracts\Modules\Module as ModuleContract;
use Finnegan\Definitions\DefinitionsFactory;
use Finnegan\Foundation\Auth\User;
use Finnegan\Models\ModelsFactory;
use Finnegan\Modules\Module;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class TestCase extends \Illuminate\Foundation\Testing\TestCase
{

	use DatabaseTransactions;

	const TEST_DEFINITION = 'test_definition';
	
	protected $moduleName = 'admin';

	/**
	 * The module instance
	 * @var Module
	 */
	protected $module;

	/**
	 * The definition factory
	 * @var \Finnegan\Definitions\DefinitionsFactory
	 */
	protected $definitionsFactory;

	/**
	 * The model factory
	 * @var \Finnegan\Models\ModelsFactory
	 */
	protected $modelsFactory;


	/**
	 * Creates the application.
	 * @return \Illuminate\Contracts\Container\Container
	 */
	public function createApplication ()
	{
		$app = require __DIR__ . '/../../../../../bootstrap/app.php';
		$app -> make ( \Illuminate\Contracts\Console\Kernel::class ) -> bootstrap ();
		return $app;
	}


	public function setUp ()
	{
		parent:: setUp ();

		$this->module = new Module ( $this->moduleName, $this->app );
		$this->instance ( ModuleContract::class, $this->module );
	}
	
	
	/**
	 * Get the Definition factory instance
	 * @return \Finnegan\Definitions\DefinitionsFactory
	 */
	protected function definitionsFactory ()
	{
		if ( is_null ( $this -> definitionsFactory ) )
		{
			$this -> definitionsFactory = new DefinitionsFactory( $this -> module );
		}
		return $this -> definitionsFactory;
	}
	
	
	/**
	 * Get the Model factory instance
	 * @return \Finnegan\Models\ModelsFactory
	 */
	protected function modelsFactory ()
	{
		if ( is_null ( $this -> modelsFactory ) )
		{
			$this -> modelsFactory = new ModelsFactory( $this -> module );
		}
		return $this -> modelsFactory;
	}


	/**
	 * Load the testing definition
	 * @return \Finnegan\Definitions\ModelDefinition
	 */
	protected function loadTestDefinition ()
	{
		$path = $this->getTestDefinitionPath ();
		return $this->definitionsFactory ()->forceLoad ( $path );
	}


	protected function getTestDefinitionPath ()
	{
		return realpath ( __DIR__ . '/' . self :: TEST_DEFINITION . '.json' );
	}


	protected function createTestUser ()
	{
		$this -> beginDatabaseTransaction();
		$user = new User([
			 'email' => 'foo@bar.test',
			 'password' => Hash :: make ( 'foobar' ),
			 'active' => 1,
			 'role' => 'admin'
		 ]);
		$user -> save();
		return $user;
	}


	/**
	 * Assert the element is in the given array
	 * @param string $element
	 * @param array $array
	 * @param string $message
	 */
	public function assertInArray ( $element, $array, $message = '' )
	{
		$this -> assertTrue ( in_array ( $element, $array ), $message );
	}

}

