#!/usr/bin/env php
<?php


require __DIR__ . '/../../../bootstrap/autoload.php';

$app = require_once __DIR__ . '/../../../bootstrap/app.php';

$input = new Symfony\Component\Console\Input\ArgvInput;

if ( $version = $input->getFirstArgument () )
{
	$path = __DIR__ . '/src/Finnegan/Foundation/Http/Kernel.php';

	$content = file_get_contents ( $path );

	$content = preg_replace ( "/(const VERSION = ')([0-9\.]+)(';)/", '${1}' . $version . '${3}', $content );

	file_put_contents ( $path, $content );

	exit( "Version bumped to {$version}." );
} else
{
	exit( 'Missing new version argument.' );
}





