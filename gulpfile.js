var elixir = require('laravel-elixir');

elixir.config.assetsPath='src/resources/assets/';

elixir(function(mix) {
    mix.sass(['finnegan.scss'], 'src/resources/assets/css/finnegan.css')
        .scriptsIn('src/resources/assets/js/widgets/','src/resources/assets/js/widgets.js')
        .copy('src/resources/assets/css/', '../../../public/assets/finnegan/css/')
        //.copy('src/resources/assets/js/finnegan.js', '../../../public/assets/finnegan/js/finnegan.js')
        .copy('src/resources/assets/js/widgets.js', '../../../public/assets/finnegan/js/widgets.js')
    ;
});
