## FinneganCDS

[![Laravel 5.1](https://img.shields.io/badge/Laravel-5.2-orange.svg?style=flat-square)](http://laravel.com)
[![License](http://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](https://tldrlegal.com/license/mit-license)

FinneganCDS is a web Content Design System built on top of [Laravel framework](http://laravel.com).

## Official Documentation

Documentation for the CDS can be found on the [FinneganCDS website](http://finnegancds.fabio/docs).

## Security Vulnerabilities

If you discover a security vulnerability within FinneganCDS, please send an e-mail at finnegan@hce.it. All security vulnerabilities will be promptly addressed.

### License

FinneganCDS is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
